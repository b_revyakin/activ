<?php

namespace App\Console\Commands;

use App\Entities\Event;
use App\Services\EventService;
use Illuminate\Console\Command;

class CreateDayCapacitiesOnEvents extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'events:create-day-capacities';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create day capacities on all events';

    /**
     * @var EventService
     */
    private $eventService;

    /**
     * Create a new command instance.
     *
     * @param EventService $eventService
     */
    public function __construct(EventService $eventService)
    {
        parent::__construct();
        $this->eventService = $eventService;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $events = Event::all();

        foreach ($events as $event) {
            $result = $this->eventService->createDayCapacities($event);
            
            if (!$result) {
                $this->error('Was not created day capacities for ' . $event->name . ' event!');
            }
        }
    }
}
