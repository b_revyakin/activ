<?php

namespace App\Console\Commands;

use App\Entities\Child;
use App\Entities\Customer;
use Illuminate\Console\Command;

class FirstAndLastNameToCapitalOnCustomersAndChildrens extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'users:up-first-and-last-name';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Make capital first letter of first and last names on customers and childrens';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $children = Child::all();
        $this->setFirstAndLastName($children);
        
        $customers = Customer::all();
        $this->setFirstAndLastName($customers);
    }
    
    protected function setFirstAndLastName($collection)
    {
        foreach ($collection as $subject) {
            $subject->first_name = $subject->first_name;
            $subject->last_name = $subject->last_name;
            $subject->save();
        }
    }
}
