<?php

namespace App\Entities;

use Exception;
use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    const
        WAIT = 0,
        PAID = 1,
        CANCELLED = 2;

    //Statuses
    protected $fillable = ['price', 'discount'];

    public function orders()
    {
        return $this->hasMany(Order::class);
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    /**
     * @return Payment
     */
    public function payment()
    {
        return $this->payments()->first();
    }

    public function payments()
    {
        return $this->hasMany(Payment::class);
    }

    /**
     * @return mixed
     */
    public function variants()
    {
        /** @noinspection PhpUndefinedMethodInspection */
        return $this->belongsToMany(ProductVariant::class)->withTrashed()->withPivot(['count', 'price']);
    }

    public function showStatus()
    {
        switch ($this->status) {
            case self::WAIT:
                $type = 'Processing';
                break;
            case self::PAID:
                $type = 'Completed';
                break;
            case self::CANCELLED:
                $type = 'Cancelled';
                break;
            default:
                throw new Exception('Status not found', 400);
        }

        return $type;
    }

    public function isProcessing()
    {
        return $this->status === self::WAIT;
    }

    public function isPaid()
    {
        return $this->status === self::PAID;
    }

    public function isCancelled()
    {
        return $this->status === self::CANCELLED;
    }

    public function getPrice()
    {
        return '£' . number_format($this->price, 2);
    }

    public function getDiscount()
    {
        return '£' . number_format($this->discount, 2);
    }

    public function getPriceWithoutDiscount()
    {
        return '£' . number_format(($this->price + $this->discount), 2);
    }

    public function scopeActive($query)
    {
        return $query->where('status', '<>', self::CANCELLED);
    }
}
