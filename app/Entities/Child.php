<?php

namespace App\Entities;

use Carbon\Carbon;
use App\Traits\FirstAndLastNameToCapital;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Child extends Model
{
    use SoftDeletes, FirstAndLastNameToCapital;

    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'birthday',
        'sex',
        'relationship',
        'school',
        'doctor_name',
        'doctor_telephone_number',
        'medical_advice_and_treatment',
        'epi_pen',
        'medication',
        'special_requirements',
        'emergency_contact_number',
        'emergency_contact_name',

        'authorised_person_name_1',
        'authorised_person_number_1',
        'authorised_person_name_2',
        'authorised_person_number_2',
        'authorised_person_name_3',
        'authorised_person_number_3',
    ];

    protected $dates = ['birthday'];

    public function canSwim()
    {
        return $this->belongsTo(SwimOption::class);
    }

    public function parent()
    {
        return $this->belongsTo(Customer::class);
    }

    public function getAgeAttribute()
    {
        return Carbon::now()->diffInYears(new Carbon($this->birthday));
    }

    public function getAgeDependenceOfEvent(Event $event)
    {
        return (new Carbon($event->start_date))->diffInYears(new Carbon($this->birthday));
    }

    public function getSexStringAttribute()
    {
        return $this->sex ? 'Male' : 'Female';
    }

    public function getNameAttribute()
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    public function getFullnameAttribute()
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    public function getBirthday()
    {
        return new Carbon($this->birthday);
    }

    public function yearInSchool()
    {
        return $this->belongsTo(YearInSchool::class);
    }

    public function setBirthdayAttribute($value)
    {
        $this->attributes['birthday'] = new Carbon($value);
    }
}
