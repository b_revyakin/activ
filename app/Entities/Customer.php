<?php

namespace App\Entities;

use App\Traits\ActiveTrait;
use App\Traits\FirstAndLastNameToCapital;
use App\Traits\Profile;
use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    use Profile, ActiveTrait, FirstAndLastNameToCapital;

    protected $fillable = [
        'title',
        'first_name',
        'last_name',
        'mobile',
        'home_tel',
        'work_tel',
        'address_line_1',
        'address_line_2',
        'address_line_3',
        'city',
        'county',
        'post_code',
        'hear_from',
        'subscribe_to_newsletter'
    ];

    public function children()
    {
        return $this->hasMany(Child::class, 'parent_id');
    }

    public function bookings()
    {
        return $this->hasMany(Booking::class);
    }

    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function getFullAddressAttribute()
    {
        $fullAddress = $this->address_line_1;

        $delimiter = ', ';

        if($this->address_line_2) {
            $fullAddress .= $delimiter . $this->address_line_2;

            if($this->address_line_3) {
                $fullAddress .= $delimiter . $this->address_line_3;
            }
        }

        $fullAddress .= $delimiter . $this->post_code;

        return $fullAddress;
    }

}
