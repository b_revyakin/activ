<?php

namespace App\Entities;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Entity extends Model
{
    public function setAttribute($key, $value)
    {
        if(in_array($key, $this->dates) && $key != static::CREATED_AT && $key != static::UPDATED_AT) {
            $this->attributes[$key] = new Carbon($value);
        } else {
            parent::setAttribute($key, $value);
        }
    }
}
