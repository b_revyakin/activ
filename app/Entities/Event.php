<?php

namespace App\Entities;

use App\Traits\ActiveTrait;
use Carbon\Carbon;
use DateInterval;
use DatePeriod;
use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    use ActiveTrait;

    protected $fillable = [
        'name',
        'start_date',
        'start_time',
        'end_date',
        'end_time',
        'places',
        'cost_per_day',
        'cost_per_week',
        'allow_individual_day_booking',
        'min_age',
        'max_age',
        'description',
        'excerpt'
    ];

    protected $dates = ['start_date', 'end_date'];

    /**
     * Get venue of event
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function venue()
    {
        return $this->belongsTo(Venue::class);
    }

    /**
     * Get season of event
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function season()
    {
        return $this->belongsTo(Season::class);
    }

    /**
     * Get own(staff) of event who created event
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function staff()
    {
        return $this->belongsTo(Staff::class);
    }

    public function dayCapacities()
    {
        return $this->hasMany(DayCapacity::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function exceptionDates()
    {
        return $this->hasMany(ExceptionDate::class);
    }

    public function getPeriod()
    {
        return (new Carbon($this->start_date))->format('d-m-Y') . ' - ' . (new Carbon($this->end_date))->format('d-m-Y');
    }

    public function getTimes()
    {
        return (new Carbon($this->start_time))->format('H:i') . ' - ' . (new Carbon($this->end_time))->format('H:i');
    }

    public function getArrayWorkDates()
    {
        $arrayDates = [];
        $from = Carbon::parse($this->start_date);
        $to = Carbon::parse($this->end_date)->addDay();

        $interval = new DateInterval('P1D');
        $period = new DatePeriod($from, $interval, $to);

        foreach ($period as $date) {
            if (!$date->isWeekend()) {
                $arrayDates[] = $date;
            }
        }

        return $arrayDates;
    }

    public function getPricePerDay()
    {
        return '£' . $this->cost_per_day;
    }

    public function getPricePerWeek()
    {
        return '£' . $this->cost_per_week;
    }

    public function photos()
    {
        return $this->morphMany(Photo::class, 'imageable');
    }

    public function getCountOrdersInDate(Carbon $date)
    {
        $orders = $this->getActiveOrders();
        $count = 0;

        foreach ($orders as $order) {
            $count += $order->dates()->where('date', $date)->get()->count();
        }

        return $count;
    }

    public function getActiveOrders()
    {
        return $this->orders()->active()->get();
    }

    public function orders()
    {
        return $this->hasMany(Order::class);
    }

    public function getCountOrdersInDateWithChildAge(Carbon $date, $age = null)
    {
        // 1 order equal 1 child
        $orders = $this->orders()
            ->active();

        if ($age) {
            $dateFrom = (new Carbon($this->start_date))->subYears($age + 1);
            $dateTo = (new Carbon($this->start_date))->subYears($age);

            $orders = $orders->whereHas('child', function ($query) use ($dateFrom, $dateTo) {
                $query
                    ->where('birthday', '>', $dateFrom->toDateString())
                    ->where('birthday', '<', $dateTo->toDateString());
            });
        }

        return $orders
            ->whereHas('dates', function ($query) use ($date) {
                $query->where('date', $date);
            })
            //Here not need groupBy by child because by a child can be booked on one day once!
            ->count();
    }

    public function getCountBookingsByAge($age = null)
    {
        if ($age) {
            $dateFrom = (new Carbon($this->start_date))->subYears($age + 1);
            $dateTo = (new Carbon($this->start_date))->subYears($age);
        }

        // 1 order equal 1 child
        $orders = $this->orders()
            ->active();

        if ($age) {
            $orders = $orders->whereHas('child', function ($query) use ($dateFrom, $dateTo) {
                $query
                    ->where('birthday', '>', $dateFrom->toDateString())
                    ->where('birthday', '<=', $dateTo->toDateString());
            });
        }

        return $orders
            ->get()
            ->count();
    }

    public function getBookedDatesCount()
    {
        return $this->getActiveOrders()->sum(function ($order) {
            return $order->dates()->count();
        });
    }

    public function setStartDateAttribute($value)
    {
        $this->attributes['start_date'] = new Carbon($value);
    }

    public function setEndDateAttribute($value)
    {
        $this->attributes['end_date'] = new Carbon($value);
    }
}
