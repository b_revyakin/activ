<?php

namespace App\Entities;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class ExceptionDate extends Model
{
    public $timestamps = false;
    protected $fillable = ['date'];
    protected $dates = ['date'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function event()
    {
        return $this->belongsTo(Event::class);
    }

    public function setDateAttribute($value)
    {
        $this->attributes['date'] = new Carbon($value);
    }
}
