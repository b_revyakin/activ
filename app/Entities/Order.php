<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Collection;

class Order extends Model
{
    use SoftDeletes;

    protected $fillable = ['price'];

    public function child()
    {
        return $this->belongsTo(Child::class)->withTrashed();
    }

    public function booking()
    {
        return $this->belongsTo(Booking::class);
    }

    public function event()
    {
        return $this->belongsTo(Event::class);
    }

    public function price()
    {
        return $this->event->cost_per_day * $this->dates()->count();
    }

    public function dates()
    {
        return $this->hasMany(OrderDate::class);
    }

    public function weeks()
    {
        //Base on dates get weeks with weekdays
        $weeks = new Collection();

        $dates = $this->dates()->orderBy('date')->get();
        foreach ($dates as $date) {


            $week = new Collection();
            $weeks->push($week);
        }
    }

    /**
     * @param $query
     */
    public function scopeActive($query)
    {
        return $query->whereHas('booking', function ($query) {
            $query->where('status', '<>', Booking::CANCELLED);
        });
    }

    public function getPrice()
    {
        return '£' . number_format($this->price, 2);
    }
}
