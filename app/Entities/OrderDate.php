<?php

namespace App\Entities;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class OrderDate extends Model
{
    public $timestamps = false;
    protected $table = 'order_date';
    protected $fillable = ['date'];

    protected $dates = ['date'];

    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    public function getDate()
    {
        return (new Carbon($this->date))->format('l jS F Y');
    }

    public function getDateDay()
    {
        return (new Carbon($this->date))->day;
    }

    public function setDateAttribute($value)
    {
        $this->attributes['date'] = new Carbon($value);
    }
}
