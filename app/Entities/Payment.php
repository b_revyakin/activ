<?php

namespace App\Entities;

use Exception;
use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $fillable = ['amount', 'type', 'method'];

    // Payment Types
    const
        ONLINE = 1,
        CHEQUE = 2,
        VOUCHER = 3;
    
    // Payment Methods
    const 
        SAGEPAY = 'sagepay',
        STRIPE = 'stripe';

    //Statuses
    const
        WAIT = 0,
        PAID = 1,
        CANCELLED = 2;

    public static function getTypeByTitle($title)
    {
        switch ($title) {
            case 'online':
                $type = self::ONLINE;
                break;
            case 'cheque':
                $type = self::CHEQUE;
                break;
            case 'voucher':
                $type = self::VOUCHER;
                break;
            default:
                throw new Exception('Payment type not found', 400);
        }

        return $type;
    }

    public function getType()
    {
        switch ($this->type) {
            case self::ONLINE:
                $type = 'online';
                break;
            case self::CHEQUE:
                $type = 'cheque';
                break;
            case self::VOUCHER:
                $type = 'voucher';
                break;
            default:
                throw new Exception('Payment type not found', 400);
        }

        return $type;
    }

    public function showType()
    {
        switch ($this->type) {
            case self::ONLINE:
                $type = 'Online';
                break;
            case self::CHEQUE:
                $type = 'Cheque';
                break;
            case self::VOUCHER:
                $type = 'Voucher';
                break;
            default:
                throw new Exception('Type not found', 400);
        }

        return $type;
    }

    public function showStatus()
    {
        switch ($this->status) {
            case self::WAIT:
                $type = 'Processing';
                break;
            case self::PAID:
                $type = 'Completed';
                break;
            case self::CANCELLED:
                $type = 'Cancel';
                break;
            default:
                throw new Exception('Status not found', 400);
        }

        return $type;
    }

    public function booking()
    {
        return $this->belongsTo(Booking::class);
    }

    public function isOnline()
    {
        return $this->type === self::ONLINE;
    }

    public function setPaid($transactionId = null)
    {
        $this->status = self::PAID;
        if($transactionId) {
            $this->transaction_id = $transactionId;
        }

        $this->save();
    }

    public function setCancelled()
    {
        $this->status = self::CANCELLED;
        $this->save();
    }
}
