<?php

namespace App\Entities;

use App\Traits\ActiveTrait;
use Illuminate\Database\Eloquent\Model;

class PaymentSystem extends Model
{
    use ActiveTrait;

    public $timestamps = false;
    
    protected $fillable = ['system', 'title', 'active'];

    public static function deactivateAll()
    {
        $systems = self::all();
        
        foreach($systems as $system) {
            $system->deactivate();
        }
    }
}
