<?php

namespace App\Entities;

use App\Traits\ActiveTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

//ToDo: need use in Downloadable Registers export as 'Merchandise ordered' column
class Product extends Model
{
    use SoftDeletes, ActiveTrait;

    protected $fillable = [
        'name',
        'image_path',
        'public_image_path',
        'image_path_th',
        'public_image_path_th',
    ];

    public function variants()
    {
        return $this->hasMany(ProductVariant::class);
    }

    /**
     * @return mixed
     */
    public function image()
    {
        return $this->morphOne(Photo::class, 'imageable')->where('is_thumbnail', false);
    }

    /**
     * @return mixed
     */
    public function imageThumbnail()
    {
        return $this->morphOne(Photo::class, 'imageable')->where('is_thumbnail', true);
    }

    /**
     * Filter by availability variants inside a product
     *
     * @param $query
     * @return mixed
     */
    public function scopeHasAvailableVariants($query)
    {
        /** @noinspection PhpUndefinedMethodInspection */
        return $query->whereHas('variants', function ($query) {
            /** @noinspection PhpUndefinedMethodInspection */
            $query->active()->hasCount();
        });
    }
}
