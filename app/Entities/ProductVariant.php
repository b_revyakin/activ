<?php

namespace App\Entities;

use App\Traits\ActiveTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductVariant extends Model
{
    use SoftDeletes, ActiveTrait;

    public $timestamps = false;

    protected $fillable = ['name', 'price', 'count'];

    public function product()
    {
        return $this->belongsTo(Product::class)->withTrashed();
    }

    /**
     * @return mixed
     */
    public function bookings()
    {
        return $this->belongsToMany(Booking::class)->withPivot(['count', 'price']);
    }

    public function decrementCount($count = 1)
    {
        $this->decrement('count', $count);

        return $this->save();
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeHasCount($query)
    {
        /** @noinspection PhpUndefinedMethodInspection */
        return $query->where('count', '>', 0);
    }
}
