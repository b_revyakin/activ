<?php

namespace App\Entities;

use App\Traits\ActiveTrait;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Season extends Model
{
    use ActiveTrait;

    protected $fillable = ['name', 'description', 'from', 'to'];

    protected $dates = ['from', 'to'];

    public function events()
    {
        return $this->hasMany(Event::class);
    }

    public function setFromAttribute($value)
    {
        $this->attributes['from'] = new Carbon($value);
    }

    public function setToAttribute($value)
    {
        $this->attributes['to'] = new Carbon($value);
    }

}
