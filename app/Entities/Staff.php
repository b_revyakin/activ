<?php

namespace App\Entities;

use App\Traits\ActiveTrait;
use App\Traits\Profile;
use Illuminate\Database\Eloquent\Model;

class Staff extends Model
{
    use Profile, ActiveTrait;

    protected $table = 'staffs';

    protected $fillable = ['first_name', 'last_name', 'mobile', 'qualifications', 'about'];

    public function events()
    {
        return $this->hasMany(Event::class);
    }
}
