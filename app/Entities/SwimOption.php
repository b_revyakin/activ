<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class SwimOption extends Model
{
    protected $fillable = ['option', 'number'];

    public $timestamps = false;
}
