<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class UnconfirmEmailCustomers extends Model
{
    protected $fillable = ['id', 'email_address', 'hash'];
}
