<?php

namespace App\Entities;

use App\Traits\ActiveTrait;
use Illuminate\Database\Eloquent\Model;

class Venue extends Model
{
    use ActiveTrait;

    protected $fillable = [
        'name',
        'address_line_1',
        'address_line_2',
        'country',
        'city',
        'post_code',
        'description',
    ];

    /**
     * Get relationships events
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function events()
    {
        return $this->hasMany(Event::class);
    }

    public function photo()
    {
        return $this->photos()->first();
    }

    public function photos()
    {
        return $this->morphMany(Photo::class, 'imageable');
    }
}
