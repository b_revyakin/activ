<?php

namespace App\Exceptions;

use Auth;
use Bican\Roles\Exceptions\RoleDeniedException;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Session\TokenMismatchException;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\HttpException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        ValidationException::class,
        RoleDeniedException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $e
     * @return void
     */
    public function report(Exception $e)
    {
        parent::report($e);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $e
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {
        // Show 404 page if model was not found
        if ($e instanceof ModelNotFoundException) {
            abort(404);
        }

        if ($e instanceof RoleDeniedException) {
            //request->session()->flash('error', 'You don\'t have permission for this action.');

            if(Auth::user()->is('customer')) {
                return redirect()->to('/');
            } else {
                return redirect()->route('admin::dashboard');
            }
        }

        if($e instanceof TokenMismatchException) {
            $request->session()->flash('error', 'You security session was expired. Try repeat you last action now.');

            return redirect()->back()->withInput($request->all());
        }

        return parent::render($request, $e);
    }
}
