<?php

namespace App\Http\Controllers\Admin\API;

use App\Entities\Booking;
use App\Entities\Customer;
use App\Entities\Order;
use App\Entities\Payment;
use App\Events\Bookings\Cancel;
use App\Events\Bookings\Created;
use App\Events\Bookings\Paid;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\Customer\API\Booking\Book;
use App\Services\BookingService;

class BookingsController extends Controller
{
    /**
     * @var BookingService
     */
    protected $bookingService;

    /**
     * BookingsController constructor.
     * @param BookingService $bookingService
     */
    public function __construct(BookingService $bookingService)
    {
        parent::__construct();

        $this->bookingService = $bookingService;
        $this->middleware('permission:booking.cancel', ['only' => ['cancel', 'cancelOrder']]);
        $this->middleware('permission:booking.success', ['only' => ['success']]);
    }

    public function cancel($id)
    {
        $booking = Booking::findOrFail($id);
        $booking = $this->bookingService->cancel($booking);
        if ($booking) {
            event(new Cancel($booking));
        }

        return response('', $booking ? 200 : 500);
    }

    public function success($id)
    {
        $booking = Booking::findOrFail($id);

        $booking = $this->bookingService->pay($booking);
        if ($booking) {
            event(new Paid($booking));
        }

        return response('', $booking ? 200 : 500);
    }

    public function cancelOrder($orderId)
    {
        $order = Order::findOrFail($orderId);

        $booking = $this->bookingService->cancelOrder($order);

        return response('', $booking ? 200 : 500);
    }

    public function book(Book $request, $customerId)
    {
        $data = $request->all();
        $data['payment_type'] = Payment::getTypeByTitle($data['payment_type']);
        $booking = $this->bookingService->create(Customer::findOrFail($customerId), $data);

        if ($booking) {
            event(new Created($booking));

            $response = [
                'redirect' => route('admin::parents.bookings.index', [$customerId]),
                'method' => 'GET',
                'fields' => ''
            ];
        }

        return response($booking ? $response : 0, $booking ? 200 : 500);
    }
}
