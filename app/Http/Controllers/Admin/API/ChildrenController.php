<?php

namespace App\Http\Controllers\Admin\API;

use App\Entities\Child;
use App\Entities\Customer;
use App\Http\Requests\Customer\API\Children\Remove;
use App\Http\Requests\Customer\API\Children\Store;
use App\Http\Requests\Customer\API\Children\Update;
use App\Services\ParentService;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ChildrenController extends Controller
{
    /**
     * @var ParentService
     */
    protected $parentService;

    /**
     * ChildrenController constructor.
     * @param ParentService $parentService
     */
    public function __construct(ParentService $parentService)
    {
        parent::__construct();

        $this->parentService = $parentService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($customerId)
    {
        return Customer::findOrFail($customerId)
            ->children()
            ->with(['yearInSchool', 'canSwim'])
            ->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Store $request, $customerId)
    {
        return $this->parentService->addChild(Customer::findOrFail($customerId), $request->all()) ?: 0;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($customerId, $id)
    {
        return Child::with(['yearInSchool', 'canSwim'])->findOrFail($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Update $request, $customerId, $id)
    {
        $child = Child::findOrFail($id);

        return $this->parentService->updateChild($child, $request->all()) ?: response(['Server Error.'], 500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Remove $request, $customerId, $id)
    {
        $child = Child::findOrFail($id);

        return $this->parentService->removeChild($child) ? 1 : 0;
    }
}
