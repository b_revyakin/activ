<?php

namespace App\Http\Controllers\Admin;

use App\Entities\Booking;
use App\Entities\Event;
use App\Entities\Product;
use App\Events\Bookings\Cancel;
use App\Events\Bookings\Paid;
use App\Http\Requests;
use App\Services\BookingService;
use Illuminate\Http\Request;

class BookingsController extends Controller
{
    /**
     * @var BookingService
     */
    protected $bookingService;

    /**
     * BookingsController constructor.
     * @param BookingService $bookingService
     */
    public function __construct(BookingService $bookingService)
    {
        parent::__construct();
        
        $this->bookingService = $bookingService;
        $this->middleware('permission:booking.cancel', ['only' => ['cancel']]);
        $this->middleware('permission:booking.success', ['only' => ['success']]);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bookings = Booking::with(['customer.user'])->get();

        return view('admin.bookings.index', compact('bookings'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return redirect()->route('admin::bookings.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $booking = Booking::findOrFail($id);

        return view('admin.bookings.show', compact('booking'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('admin.bookings.edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return redirect()->route('admin::bookings.index');
    }

    public function cancel($id)
    {
        $booking = Booking::findOrFail($id);
        $booking = $this->bookingService->cancel($booking);
        if($booking) {
            event(new Cancel($booking));
        }

        return redirect()->route('admin::bookings.index');
    }

    public function success($id)
    {
        $booking = Booking::findOrFail($id);

        $booking = $this->bookingService->pay($booking);
        if ($booking) {
            event(new Paid($booking));
        }

        return redirect()->route('admin::bookings.index');
    }

    public function orders($id)
    {
        $orders = Booking::findOrFail($id)->orders;

        return view('admin.bookings.orders.index', compact('orders'));
    }

}
