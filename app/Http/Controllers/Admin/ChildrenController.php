<?php

namespace App\Http\Controllers\Admin;

use App\Entities\Child;
use App\Http\Requests;

class ChildrenController extends Controller
{
    public function show($id)
    {
        $child = Child::findOrFail($id);

        return view('admin.children.show', compact('child'));
    }
}
