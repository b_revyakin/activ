<?php

namespace App\Http\Controllers\Admin;

use App\Entities\Staff;
use App\Http\Controllers\Controller as BaseController;
use App\Http\Requests;

class Controller extends BaseController
{
    /**
     * @var Staff
     */
    protected $staff;

    public function __construct()
    {
        parent::__construct();

        if ($this->user) {
            $this->staff = $this->user->staff;;
        }
    }

}
