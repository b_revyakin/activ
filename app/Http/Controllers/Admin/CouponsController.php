<?php

namespace App\Http\Controllers\Admin;

use App\Entities\Coupon;
use App\Http\Requests;
use App\Http\Requests\Admin\Coupons\Activate;
use App\Http\Requests\Admin\Coupons\Deactivate;
use App\Http\Requests\Admin\Coupons\Store;
use App\Http\Requests\Admin\Coupons\Update;

class CouponsController extends Controller
{
    protected $object = 'coupon';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $coupons = Coupon::all();

        return view('admin.coupons.index', compact('coupons'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.coupons.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Store $request
     * @return \Illuminate\Http\Response
     */
    public function store(Store $request)
    {
        $coupon = Coupon::create($request->all());

        if ($coupon) {
            $request->session()->flash('success', trans('manage.created_success', ['object' => $this->object]));
        } else {
            $request->session()->flash('error', trans('manage.created_fail', ['object' => $this->object]));
        }

        return redirect()->route('admin::coupons.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $coupon = Coupon::findOrFail($id);

        return view('admin.coupons.show', compact('coupon'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $coupon = Coupon::findOrFail($id);

        return view('admin.coupons.edit', compact('coupon'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Update $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Update $request, $id)
    {
        $coupon = Coupon::findOrFail($id);

        if ($coupon->update($request->all())) {
            $request->session()->flash('success', trans('manage.updated_success', ['object' => $this->object]));
        } else {
            $request->session()->flash('error', trans('manage.updated_fail', ['object' => $this->object]));
        }

        return redirect()->route('admin::coupons.index');
    }

    /**
     * Activate the specified resource in storage.
     *
     * @param Activate $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function activate(Activate $request, $id)
    {
        $coupon = Coupon::findOrFail($id);

        if ($coupon->activate()) {
            $request->session()->flash('success', trans('manage.activated_success', ['object' => $this->object]));
        } else {
            $request->session()->flash('error', trans('manage.activated_fail', ['object' => $this->object]));
        }

        return redirect()->back();
    }

    /**
     * Deactivate the specified resource in storage.
     *
     * @param Deactivate $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function deactivate(Deactivate $request, $id)
    {
        $coupon = Coupon::findOrFail($id);

        if ($coupon->deactivate()) {
            $request->session()->flash('success', trans('manage.deactivated_success', ['object' => $this->object]));
        } else {
            $request->session()->flash('error', trans('manage.deactivated_fail', ['object' => $this->object]));
        }

        return redirect()->back();
    }

}
