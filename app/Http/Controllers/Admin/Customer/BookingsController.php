<?php

namespace App\Http\Controllers\Admin\Customer;

use App\Entities\Customer;
use App\Entities\PaymentSystem;
use App\Http\Controllers\Controller;
use App\Http\Requests;

class BookingsController extends Controller
{
    public function index($customerId)
    {
        $customer = Customer::findOrFail($customerId);
        $bookings = $customer->bookings()->orderBy('created_at', 'DESC')->get();
        $paymentSystems = PaymentSystem::active()->get();

        return view('admin.parents.bookings.index', compact('customer', 'bookings', 'paymentSystems'));
    }

    public function create($customerId)
    {
        $customer = Customer::findOrFail($customerId);

        return view('admin.parents.bookings.create', compact('customer'));
    }
}
