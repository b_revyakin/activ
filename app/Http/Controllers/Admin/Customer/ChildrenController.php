<?php

namespace App\Http\Controllers\Admin\Customer;

use App\Entities\Customer;
use App\Entities\SwimOption;
use App\Entities\YearInSchool;
use App\Http\Controllers\Customer\Controller;
use App\Http\Requests;
use App\Http\Requests\Admin\Customer\Child\Store;
use App\Http\Requests\Admin\Customer\Child\Update;
use App\Services\ParentService;
use Illuminate\Http\Request;

class ChildrenController extends Controller
{
    /**
     * @var ParentService
     */
    protected $parentService;

    protected $object;

    /**
     * ParentsController constructor.
     * @param ParentService $parentService
     */
    public function __construct(ParentService $parentService)
    {
        parent::__construct();

        $this->parentService = $parentService;
        $this->object = 'Child';
    }

    /**
     * Display a listing of the resource.
     *
     * @param $parentId
     * @return \Illuminate\Http\Response
     */
    public function index($parentId)
    {
        $parent = Customer::findOrFail($parentId);
        $children = $parent->children;

        return view('admin.parents.children.index', compact('parent', 'children'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param $parentId
     * @return \Illuminate\Http\Response
     */
    public function create($parentId)
    {
        $parent = Customer::findOrFail($parentId);
        $yearInSchool = YearInSchool::all();
        $swimOptions = SwimOption::all();

        return view('admin.parents.children.create', compact('parent', 'yearInSchool', 'swimOptions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Store|Request $request
     * @param $parentId
     * @return \Illuminate\Http\Response
     */
    public function store(Store $request, $parentId)
    {
        $parent = Customer::findOrFail($parentId);
        $child = $this->parentService->addChild($parent, $request->all());

        if ($child) {
            $request->session()->flash('success', trans('manage.created_success', ['object' => $this->object]));
        } else {
            $request->session()->flash('success', trans('manage.created_fail', ['object' => $this->object]));
        }

        return redirect()->route('admin::parents.children.index', [$parentId]);
    }

    /**
     * Display the specified resource.
     *
     * @param $parentId
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($parentId, $id)
    {
        $parent = Customer::findOrFail($parentId);
        $child = $parent->children()->findOrFail($id);

        return view('admin.parents.children.show', compact('parent', 'child'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $parentId
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($parentId, $id)
    {
        $parent = Customer::findOrFail($parentId);
        $child = $parent->children()->findOrFail($id);
        $yearInSchool = YearInSchool::all();
        $swimOptions = SwimOption::all();

        return view('admin.parents.children.edit', compact('parent', 'child', 'yearInSchool', 'swimOptions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Update $request
     * @param $parentId
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Update $request, $parentId, $id)
    {
        $parent = Customer::findOrFail($parentId);
        $child = $parent->children()->findOrFail($id);

        $child = $this->parentService->updateChild($child, $request->all());

        if ($child) {
            $request->session()->flash('success', trans('manage.updated_success', ['object' => $this->object]));
        } else {
            $request->session()->flash('success', trans('manage.updated_fail', ['object' => $this->object]));
        }

        return redirect()->route('admin::parents.children.index', [$parentId]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param $parentId
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $parentId, $id)
    {
        $parent = Customer::findOrFail($parentId);
        $child = $parent->children()->findOrFail($id);

        if ($this->parentService->removeChild($child)) {
            $request->session()->flash('success', trans('manage.removed_success', ['object' => $this->object]));
        } else {
            $request->session()->flash('success', trans('manage.removed_fail', ['object' => $this->object]));
        }

        return redirect()->route('admin::parents.children.index', [$parentId]);
    }
}
