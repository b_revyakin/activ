<?php

namespace App\Http\Controllers\Admin;

use App\Entities\Child;
use App\Entities\Event;
use App\Entities\Season;
use App\Entities\Venue;
use App\Http\Requests\Admin\Events\Activate;
use App\Http\Requests\Admin\Events\Deactivate;
use App\Http\Requests\Admin\Events\Report;
use App\Http\Requests\Admin\Events\Store;
use App\Http\Requests\Admin\Events\Update;
use App\Http\Requests\Admin\Events\UpdateDayCapacities;
use App\Services\EventService;
use App\Services\PhotoService;
use Carbon\Carbon;
use Excel;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use PDF;
use Storage;

class EventsController extends Controller
{
    /**
     * @var PhotoService
     */
    protected $photoService;

    /**
     * @var EventService
     */
    protected $eventService;

    /**
     * EventsController constructor.
     * @param EventService $eventService
     */
    public function __construct(EventService $eventService, PhotoService $photoService)
    {
        parent::__construct();

        $this->eventService = $eventService;
        $this->photoService = $photoService;

        $this->middleware('permission:event.create', ['only' => ['create', 'store']]);
        $this->middleware('permission:event.show', ['only' => ['show']]);
        $this->middleware('permission:event.edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:event.capacities', ['only' => ['getDaysCapacity', 'updateDaysCapacity']]);
        $this->middleware('permission:event.activation', ['only' => ['activate', 'deactivate']]);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $events = Event::active();

        if ($request->get('only') === 'person') {
            $events = $events->where('staff_id', $this->staff->id);
        }

        $events = $events->get();

        return view('admin.events.index', compact('events'));
    }

    public function archive()
    {
        $events = Event::notActive()->get();

        return view('admin.events.archive', compact('events'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $venues = Venue::active()->get();
        $seasons = Season::active()->get();

        return view('admin.events.create', compact('venues', 'seasons'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Store $request
     * @return \Illuminate\Http\Response
     */
    public function store(Store $request)
    {
        $event = $this->eventService->create($request->all());

        if (!$event) {
            return redirect()->back()
                ->withErrors('Server Error. Please contact with administrator!');
        }

        if ($request->file('files')) {
            foreach ($request->file('files') as $file) {
                if ($file) {
                    $this->photoService->upload($event, $file);
                }
            }
        }

        return redirect()->route('admin::events.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $event = Event::findOrFail($id);

        return view('admin.events.show', compact('event'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $event = Event::findOrFail($id);
        $venues = Venue::active()->get();
        $seasons = Season::active()->get();

        return view('admin.events.edit', compact('event', 'venues', 'seasons'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Update $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Update $request, $id)
    {
        $event = $this->eventService->update($id, $request->all());

        if (!$event) {
            return redirect()->back()
                ->withErrors('Server Error. Please contact with administrator!');
        }

        if ($request->file('files')) {
            foreach ($request->file('files') as $file) {
                if ($file) {
                    $this->photoService->upload($event, $file);
                }
            }
        }

        return redirect()->route('admin::events.index');
    }

    /**
     * Activate the specified resource in storage.
     *
     * @param Activate $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function activate(Activate $request, $id)
    {
        $venue = Event::findOrFail($id);

        $venue->activate();

        return redirect()->back();
    }

    /**
     * Deactivate the specified resource in storage.
     *
     * @param Deactivate $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function deactivate(Deactivate $request, $id)
    {
        $venue = Event::findOrFail($id);

        $venue->deactivate();

        return redirect()->back();
    }

    public function getDaysCapacity($id)
    {
        $event = Event::findOrFail($id);

        $days = $event->dayCapacities;

        return view('admin.events.days-capacity', compact('event', 'days'));
    }

    /**
     * @param UpdateDayCapacities $request
     * @param $id
     */
    public function updateDaysCapacity(UpdateDayCapacities $request, $id)
    {
        $event = Event::findOrFail($id);

        $updated = $this->eventService->updateDayCapacities($event, $request->all());

        if (!$updated) {
            return redirect()->back()
                ->withErrors('Server Error. Please contact with administrator!');
        }

        return redirect()->route('admin::events.index');
    }

    /**
     * Get photo of project
     *
     * @param $eventId
     * @param $photoId
     * @return mixed
     */
    public function getPhoto($eventId, $photoId)
    {
        $event = Event::findOrFail($eventId);
        $photo = $event->photos()->findOrFail($photoId);

        return Storage::get($photo->path);
    }

    public function deletePhoto($eventId, $photoId)
    {
        $event = Event::findOrFail($eventId);
        $event->photos()->findOrFail($photoId)->delete();

        return back();
    }

    public function getBookings($id)
    {
        $page = 'bookings';

        $event = Event::findOrFail($id);

        return view('admin.events.details.bookings', compact('event', 'page'));
    }

    public function getRegister($id)
    {
        $page = 'register';

        $event = Event::findOrFail($id);

        return view('admin.events.details.event-register', compact('event', 'page'));
    }

    public function getAgeBreaking($id)
    {
        $page = 'age-breaking';

        $event = Event::findOrFail($id);

        return view('admin.events.details.event-age-breaking', compact('event', 'page'));
    }

    public function getReport($id)
    {
        $page = 'report';

        $event = Event::findOrFail($id);

        return view('admin.events.details.event-report', compact('event', 'page'));
    }

    public function getAllReport(Report $request)
    {
        $events = Event::with(['venue', 'season'])
            ->where('season_id', $request->season_id)
            ->get();
        $season = Season::findOrFail($request->season_id);
        $dates = $this->eventService->getAssociationDatesEvents($events, $season);
        $eventsByVenue = $events->groupBy('venue_id');

        return view('admin.events.report', compact('eventsByVenue', 'dates'));
    }

    public function exportChildren($id)
    {
        $event = Event::findOrFail($id);
        $orders = $event->getActiveOrders();
        $children = new Collection();

        $sortChildren = $this->prepareChildren($orders);

        $pdf = PDF::loadView('admin.events.details.children-export', compact('sortChildren', 'event'));

        return $pdf->download($event->name . ' - Child Details.pdf');
    }

    protected function prepareChildren($orders)
    {
        $children = new Collection();

        foreach ($orders as $order) {
            $children->push([$order->child, $order->dates]);
        }

        $children = $children->groupBy(function ($item, $key) {
            return $item[0]->id;
        });

        $children = $children->sortBy(function ($child) {
            return $child[0][0]->last_name;
        });

        $sortChildren = new Collection();

        foreach ($children as $child) {
            $singleChildren = $child[0][0];
            $dates = new Collection();

            foreach ($child as $items) {
                foreach ($items[1] as $item) {
                    $dates->push($item);
                }
            }

            $dates = $dates->sortBy(function ($date) {
                return (new Carbon($date->date))->toDateString();
            });

            $sortChildren->push([$singleChildren, $dates]);
        }

        return $sortChildren;
    }

    public function exportChild($eventId, $childId)
    {
        $event = Event::findOrFail($eventId);
        $theChild = Child::findOrFail($childId);

        $orders = $event->orders()->active()->whereHas('child', function ($query) use ($theChild) {
            $query->where('id', $theChild->id);
        })->get();

        $sortChildren = $this->prepareChildren($orders);

        $pdf = PDF::loadView('admin.events.details.children-export', compact('sortChildren', 'event'));

        return $pdf->download($event->name . ' - ' . $theChild->fullname . ' Details.pdf');
    }

    public function exportRegister($id)
    {
        $event = Event::findOrFail($id);
        $orders = $event->getActiveOrders();
        $children = new Collection();

        foreach ($orders as $order) {
            $children->push([$order->child, $order->dates]);
        }

        $children = $children->groupBy(function ($item, $key) {
            return $item[0]->id;
        });

        $children = $children->sortBy(function ($child) {
            return $child[0][0]->last_name;
        });

        $sortChildren = new Collection();

        foreach ($children as $child) {
            $singleChildren = $child[0][0];
            $dates = new Collection();

            foreach ($child as $items) {
                foreach ($items[1] as $item) {
                    $dates->push($item);
                }
            }

            $sortChildren->push([$singleChildren, $dates]);
        }

        Excel::create($event->name . ' - Registers', function ($excel) use ($event, $sortChildren) {
            $excel->sheet('Registers', function ($sheet) use ($event, $sortChildren) {

                $row = 1;

                $headers = [
                    'Child Surname',
                    'Child Forename',
                    'Age',
                    'School',
                    'Swim ability number',
                ];

                foreach ($event->getArrayWorkDates() as $date) {
                    $headers[] = $date->format('d-m-Y');
                }

                $headers = array_merge($headers, [
                    'Medical Conditions and Additional Needs',
                    'Epipen',
                    'Medication',
                    'Special requirements',
                    'Merchandise ordered'
                ]);

                $sheet->row($row++, $headers);

                foreach ($sortChildren as $child) {
                    $bookedDates = $child[1];
                    $child = $child[0];

                    $values = [
                        $child->last_name,
                        $child->first_name,
                        $child->age,
                        $child->school,
                        $child->canSwim ? $child->canSwim->number : '',
                    ];

                    foreach ($event->getArrayWorkDates() as $date) {
                        $booked = false;

                        foreach ($bookedDates as $bookedDate) {
                            if ($date->toDateString() === $bookedDate->date->toDateString()) {
                                $booked = true;
                                break;
                            }
                        }

                        $values[] = $booked ? '1' : '';
                    }

                    $values = array_merge($values, [
                        $child->medical_advice_and_treatment ? 'Yes' : 'No',
                        $child->epi_pen ? 'Yes' : 'No',
                        $child->medication,
                        $child->special_requirements,
                        ''
                    ]);

                    $sheet->row($row++, $values);
                }

            });
        })->export('xls');
    }

    public function exportMailingLists($id)
    {
        $event = Event::findOrFail($id);
        $orders = $event->getActiveOrders();
        $children = new Collection();

        foreach ($orders as $order) {
            $children->push($order->child);
        }

        $children = $children->groupBy(function ($item, $key) {
            return $item->id;
        });


        $children = $children->sortBy(function ($child) {
            return $child[0]->last_name;
        });

        Excel::create($event->name . ' - Mailing Lists', function ($excel) use ($children) {
            $excel->sheet('Registers', function ($sheet) use ($children) {

                $row = 1;

                $sheet->row($row++, [
                    'Child Surname',
                    'Child Forename',
                    'Parent surname',
                    'Parent First name',
                    'Parent email address',
                    'Mobile telephone number',
                    'Address and post code',
                ]);

                foreach ($children as $child) {
                    $child = $child[0];

                    $sheet->row($row++, array(
                        $child->last_name,
                        $child->first_name,
                        $child->parent->last_name,
                        $child->parent->first_name,
                        $child->parent->user->email,
                        $child->parent->mobile,
                        $child->parent->full_address,
                    ));
                }

            });
        })->export('csv');
    }
}
