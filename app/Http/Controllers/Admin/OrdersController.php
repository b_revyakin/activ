<?php

namespace App\Http\Controllers\Admin;

use App\Entities\Order;
use App\Http\Requests;
use App\Http\Requests\Admin\Orders\ShowRequest;

class OrdersController extends Controller
{
    public function index()
    {
        $orders = Order::all();

        /*if ($this->user->isStaff()) {
            $arrayIds = $this->staff->events()->lists('id');
            $orders = Order::whereIn('event_id', $arrayIds)->get();
        }*/

        return view('admin.orders.index', compact('orders'));
    }

    public function show(ShowRequest $request, $id)
    {
        $order = Order::findOrFail($id);

        return view('admin.orders.show', compact('order'));
    }
}
