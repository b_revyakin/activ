<?php

namespace App\Http\Controllers\Admin;

use App\Entities\PaymentSystem;
use App\Services\PaymentSystemService;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class PaymentSystemsController extends Controller
{
    public function index()
    {
        $paymentSystems = PaymentSystem::all();

        return view('admin.payment-settings.index', compact('paymentSystems'));
    }

    public function save(PaymentSystemService $paymentSystemService, Request $request)
    {
        if($paymentSystemService->saveSettings($request->except('_token'))) {
            $request->session()->flash('success', 'Payment Systems Settings Save!');
        } else {
            $request->session()->flash('error', 'Server Error. Payment Systems Settings Not Save!');
        }

        return back();
    }
}
