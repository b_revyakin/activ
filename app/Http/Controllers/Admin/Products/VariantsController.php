<?php

namespace App\Http\Controllers\Admin\Products;

use App\Entities\Product;
use App\Entities\ProductVariant;
use App\Http\Controllers\Admin\Controller as AdminController;
use App\Http\Requests\Admin\Products\Variants\Active;
use App\Http\Requests\Admin\Products\Variants\Deactivate;
use App\Http\Requests\Admin\Products\Variants\Request;
use App\Http\Requests\Admin\Products\Variants\Save;

class VariantsController extends AdminController
{
    /**
     * Display a listing of the resource.
     *
     * @param Product $product
     * @return \Illuminate\Http\Response
     */
    public function index(Product $product)
    {
        /** @noinspection PhpUndefinedFieldInspection */
        $variants = $product->variants;

        return view('admin.products.variants.index', compact('product', 'variants'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Product $product
     * @return \Illuminate\Http\Response
     */
    public function create(Product $product)
    {
        return view('admin.products.variants.create', compact('product'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Save $request
     * @param Product $product
     * @return \Illuminate\Http\Response
     */
    public function store(Save $request, Product $product)
    {
        if ($product->variants()->save(new ProductVariant($request->all()))) {
            $request->session()->flash('success', 'Type Was Created!');
        } else {
            $request->session()->flash('error', 'Server Error. Type Was Not Created!');
        }

        /** @noinspection PhpUndefinedFieldInspection */
        return redirect()->route('admin::products.variants.index', [$product->id]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Request $request
     * @param Product $product
     * @param ProductVariant $productVariant
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Product $product, ProductVariant $productVariant)
    {
        return view('admin.products.variants.edit', compact('product', 'productVariant'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Save $request
     * @param Product $product
     * @param ProductVariant $productVariant
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function update(Save $request, Product $product, ProductVariant $productVariant)
    {
        if ($productVariant->update($request->all())) {
            $request->session()->flash('success', 'Type Was Updated!');
        } else {
            $request->session()->flash('error', 'Server Error. Type Was Not Updated!');
        }

        /** @noinspection PhpUndefinedFieldInspection */
        return redirect()->route('admin::products.variants.index', [$product->id]);
    }

    /**
     * Activate the specified resource in storage.
     *
     * @param Active $request
     * @param Product $product
     * @param ProductVariant $productVariant
     * @return \Illuminate\Http\RedirectResponse
     * @internal param $id
     */
    public function activate(Active $request, Product $product, ProductVariant $productVariant)
    {
        if ($productVariant->activate()) {
            $request->session()->flash('success', 'The product was activated successfully.');
        } else {
            $request->session()->flash('error', 'Server Error. The product was not activated!');
        }

        return redirect()->back();
    }

    /**
     * Deactivate the specified resource in storage.
     *
     * @param Deactivate $request
     * @param Product $product
     * @param ProductVariant $productVariant
     * @return \Illuminate\Http\RedirectResponse
     * @internal param $id
     */
    public function deactivate(Deactivate $request, Product $product, ProductVariant $productVariant)
    {
        if ($productVariant->deactivate()) {
            $request->session()->flash('success', 'The product was deactivated successfully.');
        } else {
            $request->session()->flash('error', 'Server Error. The product was not deactivated!');
        }

        return redirect()->back();
    }
}
