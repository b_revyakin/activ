<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Requests\Admin\Profile\Avatar;
use App\Http\Requests\Admin\Profile\ChangePassword;
use App\Http\Requests\Admin\Profile\Update;
use App\Services\AdminService;
use App\Services\UserService;
use Auth;
use Image;

class ProfileController extends Controller
{
    protected $adminService;

    protected $userService;

    public function __construct(AdminService $adminService, UserService $userService)
    {
        $this->adminService = $adminService;
        $this->userService = $userService;
    }


    public function getProfile()
    {
        $user = Auth::user();

        return view('admin.profile', compact('user'));
    }

    /**
     * Update admin(staff)'s info
     *
     * @param Update $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateProfile(Update $request)
    {
        $user = $this->adminService->update(Auth::user()->id, $request->all());

        if ($user) {
            $request->session()->flash('success', 'Personal Info successfully saved!');
        } else {
            $request->session()->flash('error', 'Sorry, server\'s error. Personal Info not saved!');
        }

        return redirect()->route('admin::profile.info');
    }

    /**
     * Update admin(staff)'s avatar
     *
     * @param Avatar $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateAvatar(Avatar $request)
    {
        $user = $this->userService->changeAvatar(Auth::user(), Image::make($request->file('avatar')));

        if ($user) {
            $request->session()->flash('success', 'Avatar successfully updated!');
        } else {
            $request->session()->flash('error', 'Sorry, server\'s error. Avatar was not updated!');
        }

        return redirect()->route('admin::profile.info');
    }

    /**
     * Update admin(staff)'s password
     *
     * @param ChangePassword $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updatePassword(ChangePassword $request)
    {
        $user = $this->userService->updatePassword(Auth::user(), $request->get('new_password'));

        if ($user) {
            $request->session()->flash('success', 'Password successfully updated!');
        } else {
            $request->session()->flash('error', 'Sorry, server\'s error. Password was not updated!');
        }

        return redirect()->route('admin::profile.info');
    }
}
