<?php

namespace App\Http\Controllers\Admin\Staffs;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Entities\Staff;
use App\Http\Requests\Admin\Admins\Activate;
use App\Http\Requests\Admin\Admins\Deactivate;
use App\Http\Controllers\Controller as BaseController;

class Controller extends BaseController
{
    /**
     * @var string
     */
    protected $objectName = 'staff';
    
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Activate the specified resource in storage.
     *
     * @param Activate $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function activate(Activate $request, $id)
    {
        $staff = Staff::findOrFail($id);

        if ($staff->activate()) {
            $request->session()->flash('success', trans('manage.activated_success', ['object' => $this->objectName]));
        } else {
            $request->session()->flash('error', trans('manage.activated_fail', ['object' => $this->objectName]));
        }

        return redirect()->back();
    }

    /**
     * Deactivate the specified resource in storage.
     *
     * @param Deactivate $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function deactivate(Deactivate $request, $id)
    {
        $staff = Staff::findOrFail($id);

        if ($staff->deactivate()) {
            $request->session()->flash('success', trans('manage.deactivated_success', ['object' => $this->objectName]));
        } else {
            $request->session()->flash('error', trans('manage.deactivated_fail', ['object' => $this->objectName]));
        }

        return redirect()->back();
    }
}
