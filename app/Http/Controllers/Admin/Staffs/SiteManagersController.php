<?php

namespace App\Http\Controllers\Admin\Staffs;

use App\Entities\Role;
use App\Services\SiteManagerService;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Entities\Staff;
use App\Entities\User;

use App\Http\Requests\Admin\Admins\Store;
use App\Http\Requests\Admin\Admins\Update;

class SiteManagersController extends Controller
{
    /**
     * @var SiteManagerService
     */
    protected $siteManagerService;

    /**
     * AdminsController constructor.
     * @param SiteManagerService $siteManagerService
     */
    public function __construct(SiteManagerService $siteManagerService)
    {
        parent::__construct();
        
        $this->objectName = 'Site Manager';
        $this->siteManagerService = $siteManagerService;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $admins = Role::siteManager()->users()->get();

        return view('admin.site-managers.index', compact('admins'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.site-managers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Store $request
     * @return \Illuminate\Http\Response
     */
    public function store(Store $request)
    {
        $user = $this->siteManagerService->create($request->all());

        if ($user) {
            $request->session()->flash('success', trans('manage.created_success', ['object' => $this->objectName]));
        } else {
            $request->session()->flash('error', trans('manage.created_fail', ['object' => $this->objectName]));
        }

        return redirect()->route('admin::site-managers.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $admin = User::findOrFail($id);

        return view('admin.site-managers.show', compact('admin'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $admin = User::findOrFail($id);

        return view('admin.site-managers.edit', compact('admin'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Update $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Update $request, $id)
    {
        $user = $this->siteManagerService->update($id, $request->all());

        if ($user) {
            $request->session()->flash('success', trans('manage.updated_success', ['object' => $this->objectName]));
        } else {
            $request->session()->flash('error', trans('manage.updated_fail', ['object' => $this->objectName]));
        }

        return redirect()->route('admin::site-managers.index');
    }
}
