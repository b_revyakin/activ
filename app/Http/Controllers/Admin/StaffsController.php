<?php

namespace App\Http\Controllers\Admin;

use App\Entities\Role;
use App\Entities\Staff;
use App\Entities\User;
use App\Http\Requests;
use App\Http\Requests\Admin\Admins\Activate;
use App\Http\Requests\Admin\Admins\Deactivate;
use App\Http\Requests\Admin\Admins\Store;
use App\Http\Requests\Admin\Admins\Update;
use App\Services\AdminService;

class StaffsController extends Controller
{
    /**
     * @var AdminService
     */
    protected $adminService;

    /**
     * @var string
     */
    protected $objectName = 'Staff';

    /**
     * AdminsController constructor.
     * @param AdminService $adminService
     */
    public function __construct(AdminService $adminService)
    {
        $this->adminService = $adminService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $staffs = Role::staff()->users()->get();

        return view('admin.staffs.index', compact('staffs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.staffs.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Store $request
     * @return \Illuminate\Http\Response
     */
    public function store(Store $request)
    {
        $user = $this->adminService->create($request->all(), true);

        if ($user) {
            $request->session()->flash('success', trans('manage.created_success', ['object' => $this->objectName]));
        } else {
            $request->session()->flash('error', trans('manage.created_fail', ['object' => $this->objectName]));
        }

        return redirect()->route('admin::staffs.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $staff = User::findOrFail($id);

        return view('admin.staffs.show', compact('staff'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $staff = User::findOrFail($id);

        return view('admin.staffs.edit', compact('staff'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Update $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Update $request, $id)
    {
        $user = $this->adminService->update($id, $request->all(), true);

        if ($user) {
            $request->session()->flash('success', trans('manage.updated_success', ['object' => $this->objectName]));
        } else {
            $request->session()->flash('error', trans('manage.updated_fail', ['object' => $this->objectName]));
        }

        return redirect()->route('admin::staffs.index');
    }

    /**
     * Activate the specified resource in storage.
     *
     * @param Activate $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function activate(Activate $request, $id)
    {
        $staff = Staff::findOrFail($id);

        if ($staff->activate()) {
            $request->session()->flash('success', 'The staff was activated successfully.');
        } else {
            $request->session()->flash('error', 'Server Error. The staff was not activated!');
        }

        return redirect()->back();
    }

    /**
     * Deactivate the specified resource in storage.
     *
     * @param Deactivate $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function deactivate(Deactivate $request, $id)
    {
        $staff = Staff::findOrFail($id);

        if ($staff->deactivate()) {
            $request->session()->flash('success', 'The staff was deactivated successfully.');
        } else {
            $request->session()->flash('error', 'Server Error. The staff was not deactivated!');
        }

        return redirect()->back();
    }
}
