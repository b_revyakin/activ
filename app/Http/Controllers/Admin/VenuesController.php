<?php

namespace App\Http\Controllers\Admin;

use App\Entities\Venue;
use App\Http\Requests;
use App\Http\Requests\Admin\Venues\Activate;
use App\Http\Requests\Admin\Venues\Deactivate;
use App\Http\Requests\Admin\Venues\Store;
use App\Http\Requests\Admin\Venues\Update;
use App\Services\PhotoService;
use App\Services\VenueService;
use Storage;

class VenuesController extends Controller
{
    /**
     * @var PhotoService
     */
    protected $photoService;

    /**
     * @var VenueService
     */
    protected $venueService;

    /**
     * VenuesController constructor.
     * @param VenueService $venueService
     * @param PhotoService $photoService
     */
    public function __construct(VenueService $venueService, PhotoService $photoService)
    {
        $this->venueService = $venueService;
        $this->photoService = $photoService;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $venues = Venue::all();

        return view('admin.venues.index', compact('venues'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.venues.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Store $request
     * @return \Illuminate\Http\Response
     */
    public function store(Store $request)
    {
        $venue = $this->venueService->create($request->all());

        if ($venue) {
            if ($request->file('files')) {
                foreach ($request->file('files') as $file) {
                    if ($file) {
                        $this->photoService->upload($venue, $file);
                    }
                }
            }

            $request->session()->flash('success', 'The venue was created successfully.');
        } else {
            $request->session()->flash('error', 'Server Error. The venue was not created!');
        }

        return redirect()->route('admin::venues.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $venue = Venue::findOrFail($id);

        return view('admin.venues.show', compact('venue'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $venue = Venue::findOrFail($id);

        return view('admin.venues.edit', compact('venue'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Update $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Update $request, $id)
    {
        $venue = Venue::findOrFail($id);

        $venue = $this->venueService->update($venue, $request->all());

        if ($venue) {
            if ($request->file('files')) {
                foreach ($request->file('files') as $file) {
                    if ($file) {
                        $this->photoService->upload($venue, $file);
                    }
                }
            }

            $request->session()->flash('success', 'The venue was updated successfully.');
        } else {
            $request->session()->flash('error', 'Server Error. The venue was not updated!');
        }

        return redirect()->route('admin::venues.index');
    }

    /**
     * Activate the specified resource in storage.
     *
     * @param Activate $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function activate(Activate $request, $id)
    {
        $venue = Venue::findOrFail($id);

        if ($venue->activate()) {
            $request->session()->flash('success', 'The venue was activated successfully.');
        } else {
            $request->session()->flash('error', 'Server Error. The venue was not activated!');
        }

        return redirect()->back();
    }

    /**
     * Deactivate the specified resource in storage.
     *
     * @param Deactivate $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function deactivate(Deactivate $request, $id)
    {
        $venue = Venue::findOrFail($id);

        if ($venue->deactivate()) {
            $request->session()->flash('success', 'The venue was deactivated successfully.');
        } else {
            $request->session()->flash('error', 'Server Error. The venue was not deactivated!');
        }

        return redirect()->back();
    }

    /**
     * Get photo of project
     *
     * @param $eventId
     * @param $photoId
     * @return mixed
     */
    public function getPhoto($eventId, $photoId)
    {
        $venue = Venue::findOrFail($eventId);
        $photo = $venue->photos()->findOrFail($photoId);

        return Storage::get($photo->path);
    }

    public function deletePhoto($eventId, $photoId)
    {
        $venue = Venue::findOrFail($eventId);
        $venue->photos()->findOrFail($photoId)->delete();

        return back();
    }

    public function events($id)
    {
        $venue = Venue::findOrFail($id);
        $events = $venue->events;

        return view('admin.venues.events.index', compact('venue', 'events'));
    }

}
