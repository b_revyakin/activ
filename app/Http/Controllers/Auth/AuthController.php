<?php

namespace App\Http\Controllers\Auth;

use App\Entities\Country;
use App\Entities\UnconfirmEmailCustomers;
use App\Entities\User;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Customer\Store;
use App\Services\MailService;
use App\Services\ParentService;
use App\Traits\AuthRedirectTrait;
use Auth;
use DB;
use Exception;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Http\Request;
use Log;
use Validator;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins, AuthRedirectTrait {
        AuthRedirectTrait::redirectPath insteadof AuthenticatesAndRegistersUsers;
        AuthenticatesAndRegistersUsers::redirectPath as defaultRedirectPath;
    }

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    protected $redirectPath = 'login';

    /**
     * @var ParentService
     */
    protected $parentService;

    /**
     * Create a new authentication controller instance.
     *
     * @param ParentService $parentService
     */
    public function __construct(ParentService $parentService)
    {
        parent::__construct();

        $this->middleware('guest', ['except' => 'logout']);

        $this->setRedirectPaths();

        $this->parentService = $parentService;
    }

    public function getAdminLogin()
    {
        return view('auth.admin-login');
    }

    /**
     * Handle a login request to the application.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        $this->validate($request, [
            $this->loginUsername() => 'required', 'password' => 'required',
        ]);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        $throttles = $this->isUsingThrottlesLoginsTrait();

        if ($throttles && $this->hasTooManyLoginAttempts($request)) {
            return $this->sendLockoutResponse($request);
        }

        $credentials = $this->getCredentials($request);

        if (Auth::guard($this->getGuard())->attempt($credentials, $request->has('remember'))) {
            $user = Auth::user();

            if (($user->is('admin|staff|site.manager') && !$user->staff->isActive()) ||
                $user->is('customer') && (!$user->customer->isActive())
            ) {
                return $this->deactivate($request, $user->email);
            }

            return $this->handleUserWasAuthenticated($request, $throttles);
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        if ($throttles) {
            $this->incrementLoginAttempts($request);
        }

        return $this->sendFailedLoginResponse($request);
    }

    /**
     * Logout and set flash message
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function deactivate(Request $request, $email)
    {
        Auth::logout();
        $request->session()->flash('error-deactivate', 'Your account is deactivated. Please contact with administration for details.');

        return redirect()->back();
    }

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm()
    {
        $countries = Country::all();

        if (property_exists($this, 'registerView')) {
            return view($this->registerView, compact('countries'));
        }

        return view('auth.register', compact('countries'));
    }

    /**
     * Handle a registration request for the application.
     *
     * @param MailService $mailService
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws Exception
     * @throws \Illuminate\Foundation\Validation\ValidationException
     */
    public function register(MailService $mailService, Request $request)
    {
        $validator = $this->validator($request->all());

        if ($validator->fails()) {
            $this->throwValidationException(
                $request, $validator
            );
        }

        try {
            DB::beginTransaction();

            $user = $this->create($request->all());
            $hash = hash('sha1', $user->email . str_random(10));

            $mailService->sendConfirmEmailAddress($user->customer, $hash);

            UnconfirmEmailCustomers::create([
                'email_address' => $user->email,
                'hash' => $hash
            ]);

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            Log::error(['error' => $e->getMessage(), 'trace' => $e->getTraceAsString()]);

            throw $e;
        }

        $request->session()->flash('info', 'Thanks for creating your account. Please login to the email you registered with and click the verification link to confirm your account.');
//        Auth::guard($this->getGuard())->login($this->create($request->all()));

        return redirect($this->redirectPath());
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $customerRequest = new Store();

        return Validator::make($data, $customerRequest->rules());
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     * @return User
     */
    protected function create(array $data)
    {
        return $this->parentService->create($data)->user;
    }
}
