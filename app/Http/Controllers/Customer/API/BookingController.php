<?php

namespace App\Http\Controllers\Customer\API;

use App\Entities\Coupon;
use App\Entities\Payment;
use App\Entities\Product;
use App\Events\Bookings\Created;
use App\Http\Controllers\Customer\Controller;
use App\Http\Requests\Customer\API\Booking\Book;
use App\Http\Requests\Customer\API\Booking\Calculate;
use App\Services\BookingService;
use App\Services\PaymentService;

class BookingController extends Controller
{
    /*
     * BookingService
     */
    protected $bookingService;

    /**
     * @var PaymentService
     */
    protected $paymentService;

    /**
     * BookingController constructor.
     * @param BookingService $bookingService
     * @param PaymentService $paymentService
     */
    public function __construct(BookingService $bookingService, PaymentService $paymentService)
    {
        parent::__construct();

        $this->bookingService = $bookingService;
        $this->paymentService = $paymentService;
    }


    /**
     * @param Book $request
     * @return array
     */
    public function book(Book $request)
    {
        $data = $request->all();
        $data['payment_type'] = Payment::getTypeByTitle($data['payment_type']);
        $booking = $this->bookingService->create($this->customer, $data);

        if ($booking) {
            event(new Created($booking));

            $booking->encryptedCode = $this->paymentService->set($booking, $this->customer);

            if ($booking->payment()->isOnline()) {
                if (key_exists('payment_system', $data) && $data['payment_system'] == 'sagepay') {
                    //SagePay
                    $response = [
                        'redirect' => config('sagepay.endPointPayment'),
                        'method' => 'POST',
                        'fields' =>
                            "<input type=\"hidden\" name=\"VPSProtocol\" value=\"3.00\">
                                <input type=\"hidden\" name=\"TxType\" value=\"PAYMENT\">
                                <input type=\"hidden\" name=\"Vendor\" value=\"" . config('sagepay.vendor') . "\">
                                <input type=\"hidden\" name=\"Crypt\" value=\"" . $booking->encryptedCode . "\">"
                    ];
                } else {
                    //Stripe
                    $response = [
                        'redirect' => route('customer::bookings.payment.stripe', $booking->id),
                        'method' => 'GET',
                        'fields' => ''
                    ];
                }
            } else {
                $response = [
                    'redirect' => route('customer::bookings.index'),
                    'method' => 'GET',
                    'fields' => ''
                ];
            }
        }

        /** @noinspection PhpUndefinedVariableInspection */
        return response($booking ? $response : 0, $booking ? 200 : 500);
    }

    public function paymentTypes()
    {

    }

    //Check exists and active requested coupon
    public function checkCoupon($coupon)
    {
        /** @noinspection PhpUndefinedMethodInspection */
        $coupon = Coupon::active()->coupon($coupon)->first();

        return response($coupon ? $coupon->percent : '', $coupon ? 200 : 404);
    }

    /**
     * Calculate full cost of orders
     *
     * @param Calculate $request
     * @return array|float|int
     */
    public function calculate(Calculate $request)
    {
        /** @noinspection PhpUndefinedFieldInspection */
        return $this->bookingService->calculatePrice($request->orders);
    }

    /**
     * @return mixed
     */
    public function products()
    {
        /** @noinspection PhpUndefinedMethodInspection */
        return Product::active()->hasAvailableVariants()->with(['variants' => function ($query) {
            /** @noinspection PhpUndefinedMethodInspection */
            $query->active()->hasCount();
        }, 'imageThumbnail'])->get();
    }
}
