<?php

namespace App\Http\Controllers\Customer\API;

use App\Entities\Child;
use App\Entities\SwimOption;
use App\Entities\YearInSchool;
use App\Http\Controllers\Customer\Controller;
use App\Http\Requests;
use App\Http\Requests\Customer\API\Children\Remove;
use App\Http\Requests\Customer\API\Children\Store;
use App\Http\Requests\Customer\API\Children\Update;
use App\Services\ParentService;
use Illuminate\Http\Request;

class ChildrenController extends Controller
{
    /**
     * @var ParentService
     */
    protected $parentService;

    /**
     * ChildrenController constructor.
     * @param ParentService $parentService
     */
    public function __construct(ParentService $parentService)
    {
        parent::__construct();

        $this->parentService = $parentService;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->customer
            ->children()
            ->with(['yearInSchool', 'canSwim'])
            ->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Store $request
     * @return \Illuminate\Http\Response
     */
    public function store(Store $request)
    {
        return $this->parentService->addChild($this->customer, $request->all()) ?: 0;
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\\Response
     */
    public function show($id)
    {
        return Child::with(['yearInSchool', 'canSwim'])->findOrFail($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Update $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Update $request, $id)
    {
        $child = Child::findOrFail($id);

        return $this->parentService->updateChild($child, $request->all()) ?: response(['Server Error.'], 500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Remove $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Remove $request, $id)
    {
        $child = Child::findOrFail($id);

        return $this->parentService->removeChild($child) ? 1 : 0;
    }

    public function swimOptions()
    {
        return SwimOption::all();
    }

    public function yearsInSchool()
    {
        return YearInSchool::all();
    }
}
