<?php

namespace App\Http\Controllers\Customer\API;

use App\Entities\Country;
use App\Http\Controllers\Controller;
use App\Http\Requests;

class CountriesController extends Controller
{
    public function index()
    {
        $countries = Country::all();

        return response()->json($countries);
    }
}
