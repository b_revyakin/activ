<?php

namespace App\Http\Controllers\Customer\API;

use App\Entities\Event;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Services\EventService;

class EventsController extends Controller
{
    public $eventService;

    public function __construct(EventService $eventService)
    {
        parent::__construct();

        $this->eventService = $eventService;

    }

    public function index()
    {
        $events = Event::active()->with(['venue', 'exceptionDates'])->get();

        foreach ($events as $event) {
            $busyDates = [];

            foreach ($event->getArrayWorkDates() as $date) {
                if (!$this->eventService->isAvailable($event, $date)) {
                    $busyDates[] = $date;
                }
            }

            $event->busy_dates = $busyDates;
        }


        return $events;
    }
}
