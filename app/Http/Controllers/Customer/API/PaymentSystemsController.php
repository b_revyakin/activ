<?php

namespace App\Http\Controllers\Customer\API;

use App\Entities\PaymentSystem;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class PaymentSystemsController extends Controller
{
    public function index()
    {
        return PaymentSystem::active()->get();
    }
}
