<?php

namespace App\Http\Controllers\Customer\API;

use App\Http\Controllers\Customer\Controller;
use App\Http\Requests;
use App\Http\Requests\Customer\API\Profile;
use App\Services\ParentService;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    /**
     * @var ParentService
     */
    protected $parentService;

    /**
     * ProfileController constructor.
     * @param ParentService $parentService
     */
    public function __construct(ParentService $parentService)
    {
        parent::__construct();

        $this->parentService = $parentService;
    }

    /**
     * Show customer's account
     *
     * @return $this
     */
    public function show()
    {
        return $this->customer->load(['user', 'country']);
    }

    /**
     * Update customer's profile
     *
     * @param Profile $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function update(Profile $request)
    {
        return $this->parentService->update($this->customer->load(['user', 'country']), $request->all()) ?: response('', 500);
    }
}
