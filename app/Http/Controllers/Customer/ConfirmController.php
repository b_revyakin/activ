<?php

namespace App\Http\Controllers\Customer;

use App\Entities\UnconfirmEmailCustomers;
use App\Entities\User;
use App\Http\Requests;
use App\Services\MailService;
use Session;

class ConfirmController extends Controller
{
    public function confirmEmail($hash)
    {
        $confirm = UnconfirmEmailCustomers::where('hash', $hash)->first();
        if (!$confirm) {
            return abort(404);
        }

        $email = $confirm->email_address;

        if (!hash_equals($confirm->hash, $hash)) {
            return abort(404);
        }

        $user = User::where('email', $email)->first();
        $user->customer->is_confirmed = true;
        $user->customer->save();

        $confirm->delete();

        Session::flash('info', 'Your email has now been verified. Please login with username and password you signed up with');

        return view('auth.login', compact('email'));

    }

    public function repeatConfirmMail(MailService $mailService, $email)
    {
        $user = $user = User::where('email', $email)->first();
        $response = ['info' => 'You email is confirmed. Please contact with administration for details.'];

        if (!$user->is_confirmed) {
            $unconfirmEmail = UnconfirmEmailCustomers::where('email_address', $email)->first();
            if ($unconfirmEmail) {
                $hash = $unconfirmEmail->hash;
            } else {
                $hash = hash('sha1', $user->email . str_random(10));

                UnconfirmEmailCustomers::create([
                    'email_address' => $user->email,
                    'hash' => $hash
                ]);
            }
            $mailService->sendConfirmEmailAddress($user->customer, $hash);
            $response = ['info' => 'Confirmation email was sent'];
        }

        return redirect()->back()->with($response);
    }
}
