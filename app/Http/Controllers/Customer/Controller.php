<?php

namespace App\Http\Controllers\Customer;

use App\Entities\Customer;
use App\Http\Controllers\Controller as BaseController;
use App\Http\Requests;

class Controller extends BaseController
{
    /**
     * @var Customer
     */
    protected $customer;

    public function __construct()
    {
        parent::__construct();

        if ($this->user) {
            $this->customer = $this->user->customer;
        }
    }
}
