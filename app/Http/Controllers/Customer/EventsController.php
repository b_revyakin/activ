<?php

namespace App\Http\Controllers\Customer;

use App\Entities\Event;
use App\Http\Requests;
use Storage;

class EventsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $events = Event::active()->orderBy('name')->get();

        return view('customer.events.index', compact('events'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $event = Event::active()->findOrFail($id);

        return view('customer.events.show', compact('event'));
    }

    /**
     * Get photo of project
     *
     * @param $eventId
     * @param $photoId
     * @return mixed
     */
    public function getPhoto($eventId, $photoId)
    {
        $event = Event::findOrFail($eventId);
        $photo = $event->photos()->findOrFail($photoId);

        return Storage::get($photo->path);
    }
}
