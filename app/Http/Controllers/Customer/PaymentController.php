<?php

namespace App\Http\Controllers\Customer;

use App\Entities\Booking;
use App\Events\Bookings\Paid;
use App\Http\Requests;
use App\Services\BookingService;
use App\Services\PaymentService;
use Illuminate\Http\Request;
use SagePay;

class PaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function payment(PaymentService $paymentService, Request $request, $bookingsId)
    {
        $countryCode = null;//geoip_continent_code_by_name($request->ip());
        $booking = Booking::findOrFail($bookingsId);
        $code = $paymentService->set($booking, $this->customer, $countryCode);
        $response = $paymentService->execute($code);

        return $response->getBody();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function response(PaymentService $paymentService, BookingService $bookingService, Request $request, $bookingId)
    {
        if ($request->has('crypt')) {
            $responseArray = SagePay::decode($request->crypt);
            if ($responseArray["Status"] === "OK") {
                $booking = Booking::findOrFail($bookingId);
                $booking = $bookingService->pay($booking, $responseArray["VPSTxId"]);

                if ($booking) {
                    event(new Paid($booking));
                }

                return redirect()->route('customer::bookings.index')->with(['success' => 'Payment successful']);
            }
        }

        return redirect()->route('customer::bookings.index')->with(['error' => 'Payment fail']);
    }
}
