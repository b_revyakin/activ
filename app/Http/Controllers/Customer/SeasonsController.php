<?php

namespace App\Http\Controllers\Customer;

use App\Entities\Season;
use App\Http\Requests;

class SeasonsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $seasons = Season::active()->orderBy('name')->get();

        return view('customer.seasons.index', compact('seasons'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $season = Season::active()->findOrFail($id);
        $events = $season->events()->active()->get();

        return view('customer.seasons.show', compact('season', 'events'));
    }
}
