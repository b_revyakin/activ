<?php

namespace App\Http\Requests\Admin\Admins;

use Auth;

class Delete extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::user()->id != $this->getUserId();
    }
}
