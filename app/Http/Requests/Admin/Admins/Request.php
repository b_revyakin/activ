<?php

namespace App\Http\Requests\Admin\Admins;

use App\Entities\Staff;
use App\Http\Requests\Request as BaseRequest;

abstract class Request extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [];
    }

    protected function getRules()
    {
        return [
            'first_name' => 'required|string|between:2,256',
            'last_name' => 'required|string|between:2,256',
            'mobile' => 'string|max:20',
            'qualification' => 'string|max:256',
            'about' => 'string|max:1024',
        ];
    }

    /**
     * Get requested staff model
     *
     * @return Staff
     */
    protected function getStaff()
    {
        return Staff::findOrFail($this->getUserId());
    }

    protected function getUserId()
    {
        return $this->admins | $this->staffs | $this->site_managers;
    }
}
