<?php

namespace App\Http\Requests\Admin\Coupons;

use App\Entities\Coupon;
use App\Http\Requests\Request as BaseRequest;

abstract class Request extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    protected function getRules()
    {
        return [
            'percent' => 'required|numeric|between:0,100'
        ];
    }

    /**
     * @return Coupon
     */
    protected function getCoupon()
    {
        return Coupon::findOrFail($this->coupons);
    }
}
