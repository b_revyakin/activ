<?php

namespace App\Http\Requests\Admin\Coupons;

class Update extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return array_merge($this->getRules(), [
            'phrase' => 'required|unique:coupons,phrase,' . $this->getCoupon()->id
        ]);
    }
}
