<?php

namespace App\Http\Requests\Admin\Customer\Child;

use App\Entities\Child;
use App\Http\Requests\Admin\Customer\Request as CustomerRequest;

abstract class Request extends CustomerRequest
{
    /**
     * Get requested child
     *
     * @return Child
     */
    protected function getChild()
    {
        return $this->getParent()->children()->findOrFail($this->children);
    }

    protected function getRules()
    {
        return config('validation-rules.child');
    }
}
