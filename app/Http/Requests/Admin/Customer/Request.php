<?php

namespace App\Http\Requests\Admin\Customer;

use App\Entities\Customer;
use App\Http\Requests\Request as BaseRequest;

abstract class Request extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [];
    }

    /**
     * Get requested parent
     *
     * @return Customer
     */
    protected function getParent()
    {
        return Customer::findOrFail($this->parents);
    }

    protected function getRules()
    {
        return config('validation-rules.customer');
    }
}
