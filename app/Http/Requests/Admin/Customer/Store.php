<?php

namespace App\Http\Requests\Admin\Customer;

class Store extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return array_merge($this->getRules(), [
            'email' => 'required|email|confirmed|unique:users',
            'password' => 'required|between:6,40|confirmed',
        ]);
    }
}
