<?php

namespace App\Http\Requests\Admin\Events;

class Deactivate extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->getEvent()->isActive();
    }
}
