<?php

namespace App\Http\Requests\Admin\Events;

use App\Http\Requests\Request;

class Report extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'season_id' => 'required|exists:seasons,id'
        ];
    }


}
