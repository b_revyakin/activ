<?php

namespace App\Http\Requests\Admin\Events;

use App\Entities\Event;
use App\Http\Requests\Request as BaseRequest;

abstract class Request extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [];
    }

    /**
     * Get requested event
     *
     * @return Event
     */
    protected function getEvent()
    {
        return Event::findOrFail($this->events);
    }

    protected function getRules()
    {
        return [
            'name' => 'required|string',
            'venue_id' => 'required|exists:venues,id',
            'season_id' => 'required|exists:seasons,id',
            'start_date' => 'required|date|before:end_date',
            'start_time' => 'required',//ToDo: add time validator!
            'end_date' => 'required|date',
            'end_time' => 'required',
            'places' => 'required|integer|min:0',
            'cost_per_day' => 'required_if:allow_individual_day_booking,true|numeric|min:0',
            'cost_per_week' => 'required_if:allow_individual_day_booking,false|numeric|min:0',
            'allow_individual_day_booking' => 'required|boolean',
            'min_age' => 'required|integer|between:3,18',//ToDo: add greater validator!
            'max_age' => 'required|integer|between:3,18',
            'description' => 'required|string',
            'files[]' => 'mimes:jpeg,jpg,png,gif|max:10240',
            'exception_dates[]' => '*|date'
        ];
    }
}
