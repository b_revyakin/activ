<?php

namespace App\Http\Requests\Admin\Events;


class Store extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return $this->getRules();
    }
}
