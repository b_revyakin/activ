<?php

namespace App\Http\Requests\Admin\Events;

use Illuminate\Contracts\Validation\Factory as ValidationFactory;
use App\Http\Requests\Request;

class UpdateDayCapacities extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            '*' => 'required|integer|min:0'
        ];
    }

    /**
     * Get the validator instance for the request.
     *
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function getValidatorInstance()
    {
        $factory = $this->container->make(ValidationFactory::class);

        if (method_exists($this, 'validator')) {
            return $this->container->call([$this, 'validator'], compact('factory'));
        }

        $validator = $factory->make(
            $this->except(['_token', '_method']), $this->container->call([$this, 'rules']), $this->messages(), $this->attributes()
        );

        //ToDo: add check for current busy on day and new value!

        return $validator;
    }
}
