<?php

namespace App\Http\Requests\Admin\Orders;

use App\Entities\Order;
use App\Http\Requests\Request;
use Auth;

class ShowRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->checkOrder($this->orders);
    }

    protected function checkOrder($orderId)
    {
        $user = Auth::user();

        return true;

        /*if ($user->isStaff()) {
            $eventId = Order::findOrFail($orderId)->event->id;

            if ($user->staff->events->find($eventId)) {
                return true;
            }

            return false;

        } else {
            return true;
        }*/
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    public function forbiddenResponse()
    {
        abort(404);
    }
}
