<?php

namespace App\Http\Requests\Admin\Products;

class Deactivate extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        /** @noinspection PhpUndefinedFieldInspection */
        return $this->product->isActive();
    }
}
