<?php

namespace App\Http\Requests\Admin\Products\Variants;

use App\Http\Requests\Request as BaseRequest;

class Request extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->checkExists();
    }

    public function checkExists()
    {
        /** @noinspection PhpUndefinedFieldInspection */
        return $this->product->variants()->find($this->productVariant->id);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    public function getRules()
    {
        /** @noinspection PhpUndefinedFieldInspection */
        return [
            'name' => 'required|unique:product_variants,name,' . ($this->productVariant ? $this->productVariant->id : ''),
            'price' => 'required|numeric|min:1',
            'count' => 'required|numeric|min:1'
        ];
    }

    public function checkActive()
    {
        /** @noinspection PhpUndefinedFieldInspection */
        return $this->product->isActive() && $this->productVariant->isActive();
    }

    public function checkNotActive()
    {
        /** @noinspection PhpUndefinedFieldInspection */
        return $this->product->isActive() && !$this->productVariant->isActive();
    }
}
