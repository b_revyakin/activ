<?php

namespace App\Http\Requests\Admin\Profile;

use App\Http\Requests\Request;
use Auth;
use Hash;

class ChangePassword extends Request
{
    /**
     * Get the response for a forbidden operation.
     *
     * @return \Illuminate\Http\Response
     */
    public function forbiddenResponse()
    {
        $this->session()->flash('error', 'Old Password is invalid!');

        return redirect()->back();
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $oldPassword = $this->get('old_password');

        //If not exists old password then it will validate in 'rules' method
        if (!$oldPassword) {
            return true;
        }

        if (!Hash::check($oldPassword, Auth::user()->password)) {
            return false;
        }

        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'old_password' => 'required',
            'new_password' => 'required|between:6,32|confirmed'
        ];
    }
}
