<?php

namespace App\Http\Requests\Admin\Seasons;


class Activate extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return !$this->getSeason()->isActive();
    }
}
