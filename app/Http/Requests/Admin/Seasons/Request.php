<?php

namespace App\Http\Requests\Admin\Seasons;

use App\Entities\Season;
use App\Http\Requests\Request as BaseRequest;

abstract class Request extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [];
    }

    /**
     * Get requested season
     *
     * @return Season
     */
    protected function getSeason()
    {
        return Season::findOrFail($this->seasons);
    }

    protected function getRules()
    {
        return [
            'name' => 'required',
            'description' => 'string|max:1024',
            'from' => 'required|date|before:to',
            'to' => 'required|date'
        ];
    }
}
