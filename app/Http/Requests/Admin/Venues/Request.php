<?php

namespace App\Http\Requests\Admin\Venues;

use App\Entities\Venue;
use App\Http\Requests\Request as BaseRequest;

abstract class Request extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [];
    }

    /**
     * Get requested venue
     *
     * @return Venue
     */
    protected function getVenue()
    {
        return Venue::findOrFail($this->venues);
    }

    /**
     * Get common rules for venues
     *
     * @return array
     */
    protected function getRules()
    {
        return [
            'name' => 'required|string',
            'address_line_1' => 'required|string',
            'address_line_2' => 'string',
            'country' => 'required|string',
            'city' => 'required|string',
            'post_code' => 'required|string',
            'description' => 'required|string',
            'files[]' => 'mimes:jpeg,jpg,png,gif|max:10240',
        ];
    }
}
