<?php

namespace App\Http\Requests\Admin\Venues;

class Update extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return $this->getRules();
    }
}
