<?php

namespace App\Http\Requests\Customer\API\Booking;

use App\Entities\Child;
use App\Entities\Event;
use App\Entities\Order;
use App\Entities\ProductVariant;
use App\Http\Requests\Request;
use App\Services\EventService;
use Auth;
use Carbon\Carbon;
use Exception;
use Log;

class Book extends Request
{
    /**
     * @var EventService;
     */
    protected $eventService;

    /**
     * Book constructor.
     * @param EventService $eventService
     */
    public function __construct(EventService $eventService)
    {
        parent::__construct();

        $this->eventService = $eventService;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return array_merge(config('validation-rules.orders'), [
            'payment_type' => 'required|in:online,voucher', // also have cheque
            'coupon' => 'exists:coupons,phrase,active,1',
            'variants.*.id' => 'exists:product_variants,id,active,1',
            'variants.*.count' => 'required|integer|min:1'
        ]);
    }

    public function getValidatorInstance()
    {
        $validator = parent::getValidatorInstance();

        //Not need check available capacity if admin booking!
        if(Auth::user()->is('admin|staff')) {
            return $validator;
        }

        try {
            $data = $this->all();
            $validator->after(function () use ($validator, $data) {

                foreach ($this->orders as $orderData) {
                    $event = Event::findOrFail($orderData['event_id']);
                    $child = Child::findOrFail($orderData['child_id']);


                    foreach ($orderData['selectedDates'] as $selectedDate) {
                        $carbonOrderDate = new Carbon($selectedDate);

                        if($carbonOrderDate->isWeekend()) {
                            $validator->errors()->add('You can\'t order weekend day - ' . $carbonOrderDate->format('d-m-Y'));
                        }

                        $isExceptionDate = $event->exceptionDates()->where('date', $carbonOrderDate)->count();
                        if($isExceptionDate) {
                            $validator->errors()->add('You can\'t order exception day - ' . $carbonOrderDate->format('d-m-Y'));
                        }

                        if (!$this->eventService->isAvailable($event, $carbonOrderDate)) {
                            $validator->errors()->add('Not Available', 'On the ' . $event->name . ' camp the date' . $carbonOrderDate->format('d-m-Y') . ' was busy full.');
                        }

                        $hasSameOrder = Order::where('child_id', $child->id)
                            ->whereHas('booking', function ($query) {
                                $query->active();
                            })
                            ->whereHas('dates', function ($query) use ($carbonOrderDate) {
                                $query->where('date', $carbonOrderDate->toDateString());
                            })
                            ->exists();
                        if ($hasSameOrder) {
                            $validator->errors()->add('Already a child booked', 'Already you booked your child ' . $child->fullname . ' on another camp on ' . $carbonOrderDate->format('d-m-Y') . ' date');
                        }

                        // ToDo: check that between children and dates don't across
                    }
                }

                foreach ($this->variants as $variantData) {
                    $variant = ProductVariant::findOrFail($variantData['id']);

                    if ($variant->count < $variantData['count']) {
                        $validator->errors()->add('Limit quantity', 'The quantity may not be greater than ' . $variant->count . ' for ' . $variant->product->name . ' size ' . $variant->name);
                    }
                }
            });
        } catch (Exception $e) {
            Log::error($e->getMessage());
            Log::error($e->getTraceAsString());

            $validator->errors()->add('Server Error by validation your request. Please contact with administration.');

            return $validator;
        }

        return $validator;
    }
}
