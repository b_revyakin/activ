<?php

namespace App\Http\Requests\Customer\API\Children;

use App\Entities\Child;
use App\Entities\Customer;
use App\Http\Requests\Request as BaseRequest;
use Auth;

abstract class Request extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [];
    }


    /**
     * @return Child
     */
    protected function getRequestedChild()
    {
        return Child::findOrFail($this->children);
    }

    /**
     * @return bool
     */
    protected function isParentOfRequestedChild()
    {
        return $this->getCustomer()->id === $this->getRequestedChild()->parent->id;
    }

    /**
     * @return Customer
     */
    protected function getCustomer()
    {
        return $this->customers ? Customer::findOrFail($this->customers) : Auth::user()->customer;
    }
}
