<?php

namespace App\Http\Requests\Customer\API\Children;

class Store extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return config('validation-rules.child');
    }
}
