<?php

namespace App\Http\Requests\Customer\API\Children;

use App\Entities\Child;
use Auth;

class Update extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->isParentOfRequestedChild();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return config('validation-rules.child');
    }

}
