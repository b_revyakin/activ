<?php

namespace App\Http\Requests\Customer\API;

use App\Http\Requests\Request;
use Auth;

class Profile extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return array_merge(config('validation-rules.customer'),
            [
                'email' => 'required|email|unique:users,email,' . Auth::user()->id
            ]);
    }
}
