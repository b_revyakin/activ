<?php

namespace App\Http\Requests\Customer\Booking;

use App\Entities\Booking;
use App\Http\Requests\Request;

class Cancel extends Request
{
    /**
     * Get the response for a forbidden operation.
     *
     * @return \Illuminate\Http\Response
     */
    public function forbiddenResponse()
    {
        $this->session()->flash('error', 'Access denied!');

        return redirect()->back();
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $booking = Booking::findOrFail($this->bookings);

        if($booking->customer->id !== \Auth::user()->customer->id) {
            return false;
        }

        if($booking->status !== Booking::WAIT) {
            return false;
        }

        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [];
    }
}
