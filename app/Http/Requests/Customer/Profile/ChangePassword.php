<?php

namespace App\Http\Requests\Customer\Profile;

use App\Http\Requests\Request;
use Auth;
use Hash;

class ChangePassword extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Hash::check($this->old_password, Auth::user()->password);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'old_password' => 'required',
            'password' => 'required|min:6|confirmed'
        ];
    }

    public function forbiddenResponse()
    {
        return back()->withErrors(['Old Password Not Valid']);
    }
}
