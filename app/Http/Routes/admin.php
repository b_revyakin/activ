<?php

Route::group([
    'middleware' => ['web'],
], function () {

    Route::get('/admin', ['as' => 'auth.admin_login', 'uses' => 'Auth\AuthController@getAdminLogin']);

    Route::group([
        'middleware' => 'auth',
        'namespace' => 'Admin',
        'as' => 'admin::',
        'prefix' => 'admin',
    ], function () {

        Route::group([
            'middleware' => ['role:admin'],
        ], function () {
            Route::resource('admins', 'AdminsController', [
                'except' => ['destroy'],
                'names' => [
                    'index' => 'admins.index',
                    'create' => 'admins.create',
                    'store' => 'admins.store',
                    'show' => 'admins.show',
                    'edit' => 'admins.edit',
                    'update' => 'admins.update'
                ]
            ]);
            Route::put('admins/{admins}/activate', ['as' => 'admins.activate',
                'uses' => 'AdminsController@activate']);
            Route::put('admins/{admins}/deactivate', ['as' => 'admins.deactivate',
                'uses' => 'AdminsController@deactivate']);


            Route::resource('staffs', 'StaffsController', [
                'except' => ['destroy'],
                'names' => [
                    'index' => 'staffs.index',
                    'create' => 'staffs.create',
                    'store' => 'staffs.store',
                    'show' => 'staffs.show',
                    'edit' => 'staffs.edit',
                    'update' => 'staffs.update'
                ]
            ]);
            
            Route::get('payment-systems', ['as' => 'payment-systems.index', 'uses' => 'PaymentSystemsController@index']);
            Route::post('payment-systems/save', ['as' => 'payment-systems.save', 'uses' => 'PaymentSystemsController@save']);

            Route::resource('site-managers', 'Staffs\SiteManagersController', [
                'except' => ['destroy'],
                'names' => [
                    'index' => 'site-managers.index',
                    'create' => 'site-managers.create',
                    'store' => 'site-managers.store',
                    'show' => 'site-managers.show',
                    'edit' => 'site-managers.edit',
                    'update' => 'site-managers.update'
                ]
            ]);
            Route::put('staffs/{staffs}/activate', ['as' => 'staffs.activate',
                'uses' => 'Staffs\Controller@activate']);
            Route::put('staffs/{staffs}/deactivate', ['as' => 'staffs.deactivate',
                'uses' => 'Staffs\Controller@deactivate']);
            
            //Manage Products

            Route::group([
                'namespace' => 'Products',
            ], function () {
                Route::put('products/{product}/activate', ['as' => 'products.activate', 'uses' => 'Controller@activate']);
                Route::put('products/{product}/deactivate', ['as' => 'products.deactivate', 'uses' => 'Controller@deactivate']);
                Route::resource('products', 'Controller', [
                    'parameters' => ['products' => 'product'],
                    'except' => ['destroy'],
                    'names' => [
                        'index' => 'products.index',
                        'create' => 'products.create',
                        'store' => 'products.store',
                        'show' => 'products.show',
                        'edit' => 'products.edit',
                        'update' => 'products.update'
                    ]
                ]);

                Route::resource('products.variants', 'VariantsController', [
                    'parameters' => [
                        'products' => 'product',
                        'variants' => 'productVariant'
                    ],
                    'except' => ['show', 'destroy'],
                    'names' => [
                        'index' => 'products.variants.index',
                        'create' => 'products.variants.create',
                        'store' => 'products.variants.store',
                        'show' => 'products.variants.show',
                        'edit' => 'products.variants.edit',
                        'update' => 'products.variants.update'
                    ]
                ]);
                Route::put('products/{product}/variants/{productVariant}/activate', ['as' => 'products.variants.activate',
                    'uses' => 'VariantsController@activate']);
                Route::put('products/{product}/variants/{productVariant}/deactivate', ['as' => 'products.variants.deactivate',
                    'uses' => 'VariantsController@deactivate']);
            });

            Route::resource('coupons', 'CouponsController', [
                'except' => ['destroy'],
                'names' => [
                    'index' => 'coupons.index',
                    'create' => 'coupons.create',
                    'store' => 'coupons.store',
                    'show' => 'coupons.show',
                    'edit' => 'coupons.edit',
                    'update' => 'coupons.update'
                ]
            ]);
            Route::put('coupons/{coupons}/activate', ['as' => 'coupons.activate',
                'uses' => 'CouponsController@activate']);
            Route::put('coupons/{coupons}/deactivate', ['as' => 'coupons.deactivate',
                'uses' => 'CouponsController@deactivate']);
        });

        Route::group([
            'middleware' => ['role:admin|staff|site.manager'],
        ], function () {

            Route::get('dashboard', ['as' => 'dashboard', 'uses' => 'HomeController@getDashboard']);

            Route::get('venues/{venues}/photos/{photos}', ['as' => 'venue.photos', 'uses' => 'VenuesController@getPhoto']);
            Route::delete('venues/{venues}/photos/{photos}', ['as' => 'venues.photos', 'uses' => 'VenuesController@deletePhoto']);
            Route::resource('venues', 'VenuesController', [
                'except' => ['destroy'],
                'names' => [
                    'index' => 'venues.index',
                    'create' => 'venues.create',
                    'store' => 'venues.store',
                    'show' => 'venues.show',
                    'edit' => 'venues.edit',
                    'update' => 'venues.update'
                ]
            ]);
            Route::put('venues/{venues}/activate', ['as' => 'venues.activate',
                'uses' => 'VenuesController@activate']);
            Route::put('venues/{venues}/deactivate', ['as' => 'venues.deactivate',
                'uses' => 'VenuesController@deactivate']);
            Route::get('venues/{venues}/events', ['as' => 'venues.events', 'uses' => 'VenuesController@events']);


            Route::resource('seasons', 'SeasonsController', [
                'except' => ['destroy'],
                'names' => [
                    'index' => 'seasons.index',
                    'create' => 'seasons.create',
                    'store' => 'seasons.store',
                    'show' => 'seasons.show',
                    'edit' => 'seasons.edit',
                    'update' => 'seasons.update'
                ]
            ]);
            Route::put('seasons/{seasons}/activate', ['as' => 'seasons.activate',
                'uses' => 'SeasonsController@activate']);
            Route::put('seasons/{seasons}/deactivate', ['as' => 'seasons.deactivate',
                'uses' => 'SeasonsController@deactivate']);

            Route::get('events/report', ['as' => 'events.report', 'uses' => 'EventsController@getAllReport']);
            Route::get('events/{events}/details/bookings', ['as' => 'events.details.bookings', 'uses' => 'EventsController@getBookings']);
            Route::get('events/{events}/details/register', ['as' => 'events.details.register', 'uses' => 'EventsController@getRegister']);
            Route::get('events/{events}/details/age-breakdown', ['as' => 'events.details.age-breaking', 'uses' => 'EventsController@getAgeBreaking']);
            Route::get('events/{events}/details/report', ['as' => 'events.details.report', 'uses' => 'EventsController@getReport']);
            Route::get('events/{events}/details/register/export', ['as' => 'events.details.register.export', 'uses' => 'EventsController@exportRegister']);
            Route::get('events/{events}/details/children/export', ['as' => 'events.details.children.export', 'uses' => 'EventsController@exportChildren']);
            Route::get('events/{events}/details/mailing-lists/export', ['as' => 'events.details.mailing-lists.export', 'uses' => 'EventsController@exportMailingLists']);
            Route::get('events/{events}/children/{childId}/export', ['as' => 'events.details.child.export', 'uses' => 'EventsController@exportChild']);

            Route::get('events/{events}/photos/{photos}', ['as' => 'event.photos', 'uses' => 'EventsController@getPhoto']);
            Route::delete('events/{events}/photos/{photos}', ['as' => 'event.photos', 'uses' => 'EventsController@deletePhoto']);
            Route::resource('events', 'EventsController', [
                'except' => ['destroy'],
                'names' => [
                    'index' => 'events.index',
                    'create' => 'events.create',
                    'store' => 'events.store',
                    'show' => 'events.show',
                    'edit' => 'events.edit',
                    'update' => 'events.update'
                ]
            ]);

            Route::get('events/archive', ['as' => 'events.archive',
                'uses' => 'EventsController@archive']);
            Route::put('events/{events}/activate', ['as' => 'events.activate',
                'uses' => 'EventsController@activate']);
            Route::put('events/{events}/deactivate', ['as' => 'events.deactivate',
                'uses' => 'EventsController@deactivate']);
            Route::get('events/{events}/day-capacities', ['as' => 'events.day-capacities',
                'uses' => 'EventsController@getDaysCapacity']);
            Route::put('events/{events}/day-capacities', ['as' => 'events.day-capacities',
                'uses' => 'EventsController@updateDaysCapacity']);

            //Parents and children
            Route::group([
                'namespace' => 'Customer',
            ], function () {

                Route::resource('parents', 'ParentsController', [
                    'except' => ['destroy'],
                    'names' => [
                        'index' => 'parents.index',
                        'create' => 'parents.create',
                        'store' => 'parents.store',
                        'show' => 'parents.show',
                        'edit' => 'parents.edit',
                        'update' => 'parents.update'
                    ]
                ]);
                Route::resource('parents.bookings', 'BookingsController', [
                    'only' => ['index', 'create'],
                    'names' => [
                        'index' => 'parents.bookings.index',
                        'create' => 'parents.bookings.create'
                    ]
                ]);
                Route::put('parents/{parents}/activate', ['as' => 'parents.activate',
                    'uses' => 'ParentsController@activate']);
                Route::put('parents/{parents}/deactivate', ['as' => 'parents.deactivate',
                    'uses' => 'ParentsController@deactivate']);


                Route::resource('parents.children', 'ChildrenController', [
                    'names' => [
                        'index' => 'parents.children.index',
                        'create' => 'parents.children.create',
                        'store' => 'parents.children.store',
                        'show' => 'parents.children.show',
                        'edit' => 'parents.children.edit',
                        'update' => 'parents.children.update',
                        'destroy' => 'parents.children.destroy'
                    ]
                ]);

            });


            // Profile
            Route::group([
                'prefix' => 'profile',
                'as' => 'profile.',
            ], function () {

                Route::get('', ['as' => 'info', 'uses' => 'ProfileController@getProfile']);

                //update info
                Route::put('info', ['as' => 'update', 'uses' => 'ProfileController@updateProfile']);

                //update avatar
                Route::put('avatar', ['as' => 'avatar', 'uses' => 'ProfileController@updateAvatar']);

                //update password
                Route::put('profile/avatar', ['as' => 'password', 'uses' => 'ProfileController@updatePassword']);

            });

            Route::get('bookings/{bookings}/orders', ['as' => 'bookings.orders', 'uses' => 'BookingsController@orders']);
            Route::delete('bookings/{bookingId}/cancel', ['as' => 'bookings.cancel', 'uses' => 'BookingsController@cancel']);
            Route::put('bookings/{bookingId}/success', ['as' => 'bookings.success', 'uses' => 'BookingsController@success']);
            Route::resource('bookings', 'BookingsController', [
                'except' => ['destroy'],
                'names' => [
                    'index' => 'bookings.index',
                    'create' => 'bookings.create',
                    'store' => 'bookings.store',
                    'show' => 'bookings.show',
                    'edit' => 'bookings.edit',
                    'update' => 'bookings.update',
                ]
            ]);

            Route::resource('orders', 'OrdersController', [
                'only' => ['index', 'show'],
                'names' => [
                    'index' => 'orders.index',
                    'show' => 'orders.show'
                ]
            ]);

            Route::resource('children', 'ChildrenController', [
                'only' => ['show'],
                'names' => [
                    'show' => 'children.show'
                ]
            ]);

            //API
            Route::group([
                'namespace' => 'API',
                'prefix' => 'api',
                'as' => 'api::',
            ], function () {

                Route::post('{customerId}/booking', 'BookingsController@book');

                Route::resource('customers.children', 'ChildrenController', [
                    'except' => ['create', 'edit']
                ]);

                Route::get('venues', 'VenuesController@index');
                Route::get('customers', 'CustomersController@searchCustomers');
                Route::get('customers/{customerId}', 'CustomersController@show');
                Route::put('customers/{customerId}', 'CustomersController@update');
                Route::get('customers/{customerId}/children', 'CustomersController@getChildren');

                Route::post('bookings/{bookingId}/cancel', ['as' => 'bookings.cancel', 'uses' => 'BookingsController@cancel']);
                Route::post('bookings/{bookingId}/success', ['as' => 'bookings.success', 'uses' => 'BookingsController@success']);

                Route::post('orders/{orderId}/cancel', ['as' => 'orders.cancel', 'uses' => 'BookingsController@cancelOrder']);

                Route::get('seasons', 'SeasonsController@index');

            });

        });

    });

});
