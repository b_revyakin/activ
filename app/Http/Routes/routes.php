<?php

Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

Route::group(['middleware' => ['web']], function () {

    Route::get('/', function () {
        return response()->redirectToRoute('customer::bookings.create');
    });

    // Authentication Routes...
    $this->get('login', 'Auth\AuthController@showLoginForm');
    $this->post('login', ['as' => 'auth.login', 'uses' => 'Auth\AuthController@login']);
    $this->get('logout', ['as' => 'auth.logout', 'uses' => 'Auth\AuthController@logout']);

    // Registration Routes...
    $this->get('register', ['as' => 'auth.register_form', 'uses' => 'Auth\AuthController@showRegistrationForm']);
    $this->post('register', 'Auth\AuthController@register');

    // Password Reset Routes...
    $this->get('password/reset/{token?}', 'Auth\PasswordController@showResetForm');
    $this->post('password/email', 'Auth\PasswordController@sendResetLinkEmail');
    $this->post('password/reset', 'Auth\PasswordController@reset');

});
