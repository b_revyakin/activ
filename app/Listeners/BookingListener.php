<?php

namespace App\Listeners;

use App\Entities\Payment;
use App\Events\Bookings\Created;
use App\Events\Bookings\Event as BookingEvent;
use App\Events\Bookings\Paid;
use Illuminate\Contracts\Queue\ShouldQueue;
use Mail;

class BookingListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  BookingEvent $event
     * @return void
     */
    public function handle(BookingEvent $event)
    {
        $send = true;

        if ($event instanceof Created) {
            $subject = 'Activ Camps: Booking Details';

            switch($event->booking->payment()->type) {
                case Payment::ONLINE:
                    $view = 'emails.bookings.paing-sagepay';
                    break;
                case Payment::VOUCHER:
                    $view = 'emails.bookings.paing-voucher';
                    break;
                default:
                    $send = false;
                    break;
            }
        } elseif ($event instanceof Paid) {
            $view = 'emails.bookings.paid';
            $subject = 'Activ Camps: Booking Confirmation';
        } else {
            $send = false;
        }

        if (!$send) {
            return;
        }

        $customer = $event->booking->customer;
        $emails = [
            $customer->user->email,
            config('mail.duplicate-email')
        ];

        Mail::send($view, ['booking' => $event->booking, 'customer' => $customer], function ($m) use ($customer, $subject, $emails) {
            $m->to($emails, $customer->user->name)->subject($subject);
        });
    }
}
