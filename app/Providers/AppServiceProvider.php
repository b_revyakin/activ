<?php

namespace App\Providers;

use App\Entities\Season;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        switch ($this->app->environment()) {
            case 'local':
                $this->app->register(
                    \Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider::class
                );
        }

        view()->composer('admin.*', function ($view) {
            $seasons = Season::active()->orderBy('created_at', 'desc')->get();
            $view->with(compact('seasons'));
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
