<?php

namespace App\Providers;

use Carbon\Carbon;
use Illuminate\Support\ServiceProvider;
use Validator;

class ValidatorServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        Validator::extend('min_age', function ($attribute, $value, $parameters, $validator) {
            return $parameters[0] <= Carbon::now()->diff(new Carbon($value))->format('%y');
        });
        Validator::replacer('min_age', function ($message, $attribute, $rule, $parameters) {
            return str_replace([':min_age', ':attribute'], [$parameters[0], $attribute], $message);
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
