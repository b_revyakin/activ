<?php

namespace App\Services;


use App\Entities\Booking;
use App\Entities\Child;
use App\Entities\Coupon;
use App\Entities\Customer;
use App\Entities\Event;
use App\Entities\Order;
use App\Entities\OrderDate;
use App\Entities\Payment;
use App\Entities\ProductVariant;
use Carbon\Carbon;
use DB;
use Exception;
use Illuminate\Support\Collection;
use Log;

class BookingService
{
    /**
     * @param Customer $customer
     * @param array $data
     * @return Booking|bool
     */
    public function create(Customer $customer, array $data)
    {
        DB::beginTransaction();

        try {
            $booking = new Booking();
            $booking->status = Booking::WAIT;
            $booking->customer()->associate($customer);
            $booking->save();

            $orders = new Collection();

            $ordersWithPrice = $this->calculatePrice($data['orders']);
            foreach ($ordersWithPrice as $orderData) {
                $order = new Order();
                $order->child()->associate(Child::findOrFail($orderData['child_id']));
                $order->event()->associate(Event::findOrFail($orderData['event_id']));
                $order->booking()->associate($booking);
                $order->price = $orderData['amount'] - $orderData['discount']; //price calculate with discount!
                $order->save();

                $dates = new Collection();
                foreach ($orderData['selectedDates'] as $selectedDate) {
                    $date = new OrderDate(['date' => $selectedDate]);
                    $dates->push($date);
                }
                $order->dates()->saveMany($dates);

                $orders->push($order);
            }

            $coupon = isset($data['coupon']) && $data['coupon'] ? Coupon::coupon($data['coupon'])->first()->checkActive() : false;
            $priceList = $this->calculatePrice($data['orders'], true);

            $priceProducts = 0;
            if (array_key_exists('variants', $data)) {
                foreach ($data['variants'] as $variant) {
                    /** @noinspection PhpUndefinedMethodInspection */
                    $productVariant = ProductVariant::findOrFail($variant['id']);
                    /** @noinspection PhpUndefinedMethodInspection */
                    $productVariant->decrementCount($variant['count']);
                    $priceProducts += $price = $variant['count'] * $productVariant->price;
                    $booking->variants()->attach($productVariant, ['count' => $variant['count'], 'price' => $price]);
                }
            }

            if ($coupon) {
                $price = $priceList['totalAmount'] * (1 - $coupon);
            } else {
                $price = $priceList['totalAmount'] - $priceList['totalDiscount'];
            }
            $booking->price = $price + $priceProducts;
            $booking->discount = $priceList['totalDiscount'];
            $booking->save();

            $payment = new Payment(['amount' => $price, 'type' => $data['payment_type']]);
            $payment->status = Payment::WAIT;
            $booking->payments()->save($payment);

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();

            Log::error($e->getTraceAsString());

            return false;
        }

        return $booking;
    }

    /**
     * @param array $orders
     * @param bool|false $onlyPrice
     * @return array|float|int
     */
    public function calculatePrice(array $orders, $onlyPrice = false)
    {
        $price = 0;
        $totalAmount = 0;
        $totalDiscount = 0;
        $discount = 0;

        foreach ($orders as $i => $order) {
            $orders[$i]['discounts'] = [];

            $event = Event::findOrFail($order['event_id']);

            $countDates = count($order['selectedDates']);

            if ($event->allow_individual_day_booking) {
                $price = $countDates * $event->cost_per_day;
            } else {
                $price = ceil($countDates / 5) * $event->cost_per_week;
            }

            $discounts = ['1' => 0, '2' => 0, '3' => 0];

            //discount 1 type
            $weeks = $this->hasWeeks($order['selectedDates']);

            if ($event->allow_individual_day_booking && $weeks && $event->cost_per_week) {
                $discount = ($event->cost_per_day * 5 - $event->cost_per_week) * $weeks;
                $discounts['1'] = $discount;
            }

            //discount 2 type
            if ($event->allow_individual_day_booking && $countDates >= 10) {
                $discount = $countDates * $event->cost_per_day * 0.1;
                $discounts['2'] = $discount;
            }

            //discount 3 type
            $childId = $order['child_id'];
            $moreTwoChild = false;

            foreach ($orders as $otherOrder) {
                if ($childId !== $otherOrder['child_id']) {
                    $moreTwoChild = true;
                    break;
                }
            }

            if ($moreTwoChild) {
                $discount = $countDates * $event->cost_per_day * 0.1;
                $discounts['3'] = $discount;
            }

            //total for each order
            $orders[$i]['amount'] = $price;
            $orders[$i]['discount'] = $discounts;

            $totalAmount += $price;
        }

        $discounts = new Collection();
        $discounts->push(array_sum(array_pluck($orders, 'discount.1')));
        $discounts->push(array_sum(array_pluck($orders, 'discount.2')));
        $discounts->push(array_sum(array_pluck($orders, 'discount.3')));
        $totalDiscount += $discounts->max();

        for ($i = 0; $i < $discounts->count(); $i++) {
            if ($totalDiscount === $discounts[$i]) {
                break;
            }
        }
        foreach ($orders as $j => $order) {
            $orders[$j]['discount'] = $orders[$j]['discount'][$i+1];
        }


        if ($onlyPrice) {
            return compact('totalAmount', 'totalDiscount');
        }

        return $orders;
    }

    /**
     * @param array $dates
     * @return int
     */
    protected function hasWeeks(array $dates)
    {
        $count = 0;
        $weeks = 0;
        $carbonDates = new Collection();

        foreach ($dates as $date) {
            $carbonDates->push(Carbon::parse($date));
        }

        $carbonDates = $carbonDates->sortBy(function (Carbon $date) {
            return $date->toDateString();
        });

        $startDate = $carbonDates->first();

        foreach ($carbonDates as $carbonDate) {
            if ($startDate == $carbonDate) {
                $count++;
                $startDate->addDay();

                if ($count == 5) {
                    $count = 0;
                    $weeks++;
                }
            } else {
                $count = 1;
                $startDate = (new Carbon($carbonDate))->addDay();
            }
        }

        return $weeks;
    }

    /**
     * @param Booking $booking
     * @param null $transactionId
     * @return Booking|bool
     */
    public function pay(Booking $booking, $transactionId = null)
    {
        try {
            DB::beginTransaction();

            $booking->status = Booking::PAID;
            $booking->save();

            $booking->payment()->setPaid($transactionId);

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();

            Log::error('Error in BookingService. Method pay.', ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]);

            return false;
        }

        return $booking;
    }

    /**
     * @param Order $order
     * @return Booking|bool
     */
    public function cancelOrder(Order $order)
    {
        $booking = $order->booking;

        if ($booking->orders()->count() === 1) {
            return $this->cancel($booking);
        }

        try {
            DB::beginTransaction();

            // 1. Delete order
            $order->delete();

            // 2. Recalculate booking
            $orders = $booking->orders->load(['dates'])->toArray();
            foreach ($orders as $i => $order) {
                $orders[$i]['selectedDates'] = array_map(function ($array) {
                    return $array['date'];
                }, $order['dates']);
            }

            //ToDo: refactor this - remove twice calling same method
            $orders = $this->calculatePrice($orders);
            $totalPrice = $this->calculatePrice($orders, true);

            foreach ($orders as $orderData) {
                $order = Order::findOrFail($orderData['id']);
                $order->price = $orderData['amount'] - $orderData['discount'];
                $order->save();
            }

            $booking->price = $totalPrice['totalAmount'];
            $booking->discount = $totalPrice['totalDiscount'];
            $booking->save();

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();

            Log::error('Error in BookingService. Method cancelOrder.', ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]);

            return false;
        }


        return $booking;
    }

    /**
     * @param Booking $booking
     * @return Booking|bool
     */
    public function cancel(Booking $booking)
    {
        try {
            DB::beginTransaction();

            $booking->status = Booking::CANCELLED;
            $booking->save();

            $booking->payment()->setCancelled();

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();

            Log::error('Error in BookingService. Method cancel.', ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]);

            return false;
        }

        return $booking;
    }
}