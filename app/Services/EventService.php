<?php

namespace App\Services;

use App\Entities\DayCapacity;
use App\Entities\Event;
use App\Entities\ExceptionDate;
use App\Entities\OrderDate;
use App\Entities\Photo;
use App\Entities\Season;
use App\Entities\Venue;
use Auth;
use Carbon\Carbon;
use DateInterval;
use DatePeriod;
use DB;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Log;
use Storage;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class EventService
{
    /**
     * Create new Event
     *
     * @param array $data
     * @return Event
     */
    public function create(array $data)
    {
        DB::beginTransaction();

        try {
            $event = new Event($data);

            $event->venue()->associate(Venue::findOrFail($data['venue_id']));
            $event->season()->associate(Season::findOrFail($data['season_id']));
            $event->staff()->associate(Auth::user()->staff);

            $event->save();

            //Exception dates
            if (isset($data['exception_dates'])) {
                foreach ($data['exception_dates'] as $exceptionDate) {
                    if ($exceptionDate) {
                        $exceptionDate = new ExceptionDate(['date' => $exceptionDate]);
                        $exceptionDate->event()->associate($event);
                        $exceptionDate->save();
                    }
                }
            }

            //Create daily capacity
            $this->createDayCapacities($event);

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();

            Log::error($e->getMessage());
            Log::error($e->getTraceAsString());

            return false;
        }


        return $event;
    }

    /**
     * Update event's info
     *
     * @param $id
     * @param array $data
     * @return Event
     */
    public function update($id, array $data)
    {
        DB::beginTransaction();

        try {
            $event = Event::findOrFail($id);

            if ($event->venue_id != $data['venue_id']) {
                $venue = Venue::findOrFail($data['venue_id']);
                $event->venue()->associate($venue);
            }

            if ($event->season_id != $data['season_id']) {
                $season = Season::findOrFail($data['season_id']);
                $event->season()->associate($season);
            }

            $event->update($data);

            //Remove previous exception dates
            $event->exceptionDates()->delete();

            //Create new exception dates
            if (isset($data['exception_dates'])) {
                foreach ($data['exception_dates'] as $exceptionDate) {
                    if ($exceptionDate) {
                        $exceptionDate = new ExceptionDate(['date' => $exceptionDate]);
                        $exceptionDate->event()->associate($event);
                        $exceptionDate->save();
                    }
                }
            }

            $this->createDayCapacities($event);

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();

            Log::error($e->getMessage());
            Log::error($e->getTraceAsString());

            return false;
        }

        return $event;
    }

    /**
     * @param Event $event
     * @return bool
     */
    public function createDayCapacities(Event $event)
    {
        DB::beginTransaction();

        try {
            $from = new Carbon($event->start_date);
            $to = new Carbon($event->end_date);

            for ($current = $from->copy(); $current->toDateString() <= $to->toDateString(); $current->addDay()) {
                //Check that current not weekend, not already exists and that it is not exception date!
                if (!$current->isWeekend() &&
                    !$event->dayCapacities()->where('date', $current->toDateString())->exists() &&
                    !$event->exceptionDates()->where('date', $current->toDateString())->exists()
                ) {
                    $dayCapacity = new DayCapacity([
                        'date' => $current,
                        'capacity' => $event->places
                    ]);
                    $dayCapacity->event()->associate($event);
                    $dayCapacity->save();
                }
            }

            //Delete dates that not include to the event
            $event->dayCapacities()
                ->where(function ($query) use ($from, $to) {
                    $query
                        ->where('date', '<', $from->toDateString())
                        ->orWhere('date', '>', $to->toDateString());
                })
                ->delete();

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();

            Log::error($e->getMessage());
            Log::error($e->getTraceAsString());

            return false;
        }

        return true;
    }

    public function updateDayCapacities(Event $event, array $data)
    {
        DB::beginTransaction();

        try {
            foreach ($data as $date => $capacity) {
                $day = $event->dayCapacities()->where('date', $date)->first();
                if ($day) {
                    $day->capacity = $capacity;
                    $day->save();
                }
            }

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();

            Log::error($e->getMessage());
            Log::error($e->getTraceAsString());

            return false;
        }

        return true;
    }

    /**
     * @param Model $model
     * @param UploadedFile $file
     * @return Photo|bool
     */
    public function uploadPhoto(Model $model, UploadedFile $file)
    {
        $name = Carbon::now()->toDateTimeString() . ' - ' . str_random() . '.' . $file->getClientOriginalExtension();
        $path = 'photos/' . $name;

        try {
            //1. Upload photo
            $result = Storage::put(
                $path,
                file_get_contents($file->getRealPath())
            );

            if (!$result) {
                throw new Exception('Photo was not saved!');
            }

            //2. Save record about photo to db
            $photo = new Photo(['path' => $path]);
            $model->photos()->save($photo);
        } catch (Exception $e) {
            Log::error('Error by uploading photo to project', ['trace' => $e->getTraceAsString()]);

            if (Storage::exists($path)) {
                Storage::delete($path);
            }

            return false;
        }

        return $photo;
    }

    /**
     * @param Event $event
     * @param Carbon $date
     * @return bool
     */
    public function isAvailable(Event $event, Carbon $date)
    {
        //Max count
        //Booking that not cancelled with current date -> count
        // max count - min count -> true / false

        //all places busy
        $busyPlaces = OrderDate::where('date', $date->toDateString())->whereHas('order', function ($query) use ($event) {
            $query
                ->where('event_id', $event->id)
                ->active();

        })->count();

        $dayCapacity = $event->dayCapacities()->where('date', $date->toDateString())->first();
        $totalPlaces = $dayCapacity ? $dayCapacity->capacity : $event->places;

        return $totalPlaces > $busyPlaces;
    }

    /**
     * @param Collection $events
     * @param Season $season
     * @return array
     */
    public function getAssociationDatesEvents(Collection $events, Season $season = null)
    {
        if (!$events->count()) {
            return [];
        }

        if ($season && $season->from && $season->to) {
            $from = $season->from;
            $to = $season->to;
        } else {
            $from = Carbon::parse($events->first()->start_date);
            $to = Carbon::parse($events->first()->end_date);

            foreach ($events as $event) {
                $currentStartDate = Carbon::parse($event->start_date);
                $currentEndDate = Carbon::parse($event->end_date);

                if ($currentStartDate < $from) {
                    $from = $currentStartDate;
                }

                if ($currentEndDate > $to) {
                    $to = $currentEndDate;
                }
            }
        }

        $dates = [];
        $interval = new DateInterval('P1D'); // P1D - set interval = 1 day.
        $to->add($interval);
        $period = new DatePeriod($from, $interval, $to);

        foreach ($period as $date) {
            if (!$date->isWeekend()) {
                $dates[] = $date;
            }
        }

        return $dates;
    }
}
