<?php

namespace App\Services;

use App\Entities\Customer;
use Mail;

class MailService
{
    public function sendConfirmEmailAddress(Customer $customer, $hash)
    {
        Mail::send('emails.confirm-email-address', ['customer' => $customer],
            function ($m) use ($customer) {
                $m->to($customer->user->email, $customer->first_name)->subject('Activ Camps: Account Set Up');
            });
    }
}
