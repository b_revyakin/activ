<?php

namespace App\Services;

use App\Entities\Child;
use App\Entities\Country;
use App\Entities\Customer;
use App\Entities\Role;
use App\Entities\SwimOption;
use App\Entities\YearInSchool;
use DB;
use Exception;
use Log;

class ParentService
{
    /**
     * @var UserService
     */
    protected $userService;

    /**
     * ParentService constructor.
     * @param UserService $userService
     */
    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    /**
     * Create new parent
     *
     * @param array $data
     * @return Customer|bool
     */
    public function create(array $data)
    {
        DB::beginTransaction();

        try {
            $user = $this->userService->create($data);

            $parent = new Customer($data);
            $parent->country()->associate(Country::findOrFail($data['country_id']));
            $user->customer()->save($parent);
            $user->attachRole(Role::customer());

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();

            Log::error('Error by creating parent', ['trace' => $e->getTraceAsString()]);

            return false;
        }

        return $parent;
    }

    /**
     * Update a parent
     *
     * @param Customer $parent
     * @param $data
     * @return Customer|bool
     */
    public function update(Customer $parent, $data)
    {
        DB::beginTransaction();

        try {
            $parent->update($data);

            $parent->user->update($data);
            $parent->country()->associate(Country::findOrFail($data['country_id']));
            $parent->save();


            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();

            Log::error('Error by updating parent', ['trace' => $e->getTraceAsString()]);

            return false;
        }

        return $parent;
    }

    /**
     * Add child to parent
     *
     * @param Customer $parent
     * @param array $childData
     * @return Child|bool
     */
    public function addChild(Customer $parent, array $childData)
    {
        try {
            $child = new Child($childData);

            $swimOption = SwimOption::findOrFail($childData['can_swim_id']);
            $yearInSchool = YearInSchool::findOrFail($childData['year_in_school_id']);
            $child->canSwim()->associate($swimOption);
            $child->yearInSchool()->associate($yearInSchool);

            $parent->children()->save($child);
        } catch (Exception $e) {
            Log::error('Error by creating a child', ['trace' => $e->getTraceAsString()]);

            return false;
        }

        return $child;
    }

    /**
     * Update child
     *
     * @param Child $child
     * @param array $data
     * @return Child|bool
     */
    public function updateChild(Child $child, array $data)
    {
        try {
            $child->update($data);

            $swimOption = SwimOption::findOrFail($data['can_swim_id']);
            $yearInSchool = YearInSchool::findOrFail($data['year_in_school_id']);
            $child->canSwim()->associate($swimOption);
            $child->yearInSchool()->associate($yearInSchool);

            $child->save();

        } catch (Exception $e) {
            Log::error($e->getTraceAsString());

            return false;
        }

        return $child;
    }

    public function removeChild(Child $child)
    {
        return $child->delete();
    }
}
