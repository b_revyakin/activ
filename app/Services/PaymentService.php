<?php

namespace App\Services;

use App\Entities\Booking;
use App\Entities\Customer;
use App\Entities\Payment;
use DB;
use Exception;
use GuzzleHttp\Client;
use Log;
use SagePay;

class PaymentService
{
    /**
     * Set data payer for SagePay
     *
     * @param Booking $booking
     * @param Customer $customer
     * @param string $countryCode
     * @return bool|string
     */
    public function set(Booking $booking, Customer $customer, $countryCode = 'GB')
    {
        try {
            DB::beginTransaction();

            SagePay::setAmount($booking->price);
            SagePay::setDescription('Payment for Activ');
            SagePay::setBillingSurname($customer->last_name);
            SagePay::setBillingFirstnames($customer->first_name);
            SagePay::setBillingCity($customer->city);
            SagePay::setBillingPostCode($customer->post_code);
            SagePay::setBillingAddress1($customer->address_line_1);
            SagePay::setBillingCountry($countryCode ?: 'GB');
            SagePay::setDeliverySameAsBilling();
            SagePay::setSuccessURL(route('customer::bookings.payment.response', $booking->id));
            SagePay::setFailureURL(route('customer::bookings.payment.response', $booking->id));

            $encryptedCode = SagePay::getCrypt();

            DB::commit();
        } catch (Exception $exception) {
            Log::error('Error in PaymentService. Method setPayment.');
            Log::error(['error' => $exception->getMessage(), 'trace' => $exception->getTraceAsString()]);

            DB::rollback();

            return false;
        }

        return $encryptedCode;
    }

    /**
     * Execute request for payment SagePay
     *
     * @param $code
     * @return mixed|\Psr\Http\Message\ResponseInterface
     */
    public function execute($code)
    {
        $request = [];
        $client = new Client();
        $request['method'] = 'POST';
        $request['end_point'] = config('sagepay.endPointPayment');
        $request['option']['form_params'] = [
            'VPSProtocol' => '3.00',
            'TxType' => 'PAYMENT',
            'Vendor' => config('sagepay.vendor'),
            'Crypt' => $code
        ];

        return $client->request($request['method'], $request['end_point'], $request['option']);
    }
}