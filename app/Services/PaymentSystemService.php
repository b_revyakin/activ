<?php

namespace App\Services;

use App\Entities\PaymentSystem;
use DB;
use Exception;
use Log;

class PaymentSystemService
{
    /**
     * @param array $data
     * @return bool
     */
    public function saveSettings(array $data)
    {
        try {
            DB::beginTransaction();

            PaymentSystem::deactivateAll();

            foreach($data as $system => $value) {
                $system = PaymentSystem::where('system', $system)->first();
                $system->activate();
            }
            
            DB::commit();
        } catch(Exception $e) {
            Log::error($e->getMessage());
            
            DB::rollBack();
            
            return false;
        }
        
        return true;
    }
}