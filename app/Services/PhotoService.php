<?php

namespace App\Services;

use App\Entities\Photo;
use Carbon\Carbon;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Log;
use Storage;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class PhotoService
{
    /**
     * @param Model $model
     * @param UploadedFile $file
     * @return Photo|bool
     */
    public function upload(Model $model, UploadedFile $file)
    {
        $name = Carbon::now()->toDateTimeString() . ' - ' . str_random() . '.' . $file->getClientOriginalExtension();
        $path = 'photos/' . $name;

        try {
            //1. Upload photo
            $result = Storage::put(
                $path,
                file_get_contents($file->getRealPath())
            );

            if (!$result) {
                throw new Exception('Photo was not saved!');
            }

            //2. Save record about photo to db
            $photo = new Photo(['path' => $path]);
            $model->photos()->save($photo);
        } catch (Exception $e) {
            Log::error('Error by uploading photo to project', ['trace' => $e->getTraceAsString()]);

            if (Storage::exists($path)) {
                Storage::delete($path);
            }

            return false;
        }

        return $photo;
    }
}