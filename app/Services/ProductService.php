<?php

namespace App\Services;


use App\Entities\Photo;
use App\Entities\Product;
use Carbon\Carbon;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Image;
use Log;
use Storage;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class ProductService
{
    /**
     * Create new product
     *
     * @param array $data
     * @param UploadedFile $file
     * @return Product|bool
     */
    public function create(array $data, UploadedFile $file = null)
    {
        try {
            $product = Product::create($data);

            if ($file) {
                $this->uploadPhoto($product, $file);
            }
        } catch (Exception $e) {
            Log::error('Error by creating new product', ['trace' => $e->getTraceAsString()]);

            return false;
        }

        return $product;
    }

    public function update(Product $product, array $data, UploadedFile $file = null)
    {
        try {
            $product->update($data);

            if ($file) {
                $this->uploadPhoto($product, $file);
            }
        } catch (Exception $e) {
            Log::error('Error by updating product', ['trace' => $e->getTraceAsString()]);

            return false;
        }

        return $product;
    }

    /**
     * @param Model $model
     * @param UploadedFile $file
     * @return Photo|bool
     */
    public function uploadPhoto(Model $model, UploadedFile $file)
    {
        /** @noinspection PhpUndefinedMethodInspection */
        $model->image()->delete();
        /** @noinspection PhpUndefinedMethodInspection */
        $model->imageThumbnail()->delete();

        $name = Carbon::now()->toDateTimeString() . ' - ' . str_random() . '.' . $file->getClientOriginalExtension();
        $path = 'photos/' . $name;
        $pathThumbnail = 'photos/th-' . $name;

        try {
            if (!Storage::put($path, file_get_contents($file->getRealPath()))) {
                throw new Exception('Photo was not saved!');
            }

            $photo = new Photo(['path' => $path, 'is_thumbnail' => false]);
            /** @noinspection PhpUndefinedMethodInspection */
            $model->image()->save($photo);


            $imageThumbnail = Image::make($file)
                ->resize(100, 100, function ($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                });

            if (!Storage::put($pathThumbnail, $imageThumbnail->stream()->__toString())) {
                throw new Exception('Photo was not saved!');
            }

            $photoThumbnail = new Photo(['path' => $pathThumbnail, 'is_thumbnail' => true]);
            /** @noinspection PhpUndefinedMethodInspection */
            $model->imageThumbnail()->save($photoThumbnail);
        } catch (Exception $e) {
            Log::error('Error by uploading photo to project', ['trace' => $e->getTraceAsString()]);

            if (Storage::exists($path)) {
                Storage::delete($path);
            }

            if (Storage::exists($pathThumbnail)) {
                Storage::delete($pathThumbnail);
            }

            return false;
        }

        return $photo;
    }
}
