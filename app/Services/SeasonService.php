<?php

namespace App\Services;


use App\Entities\Season;
use Exception;
use Log;

class SeasonService
{
    /**
     * Create new season
     *
     * @param array $data
     * @return bool|Season
     */
    public function create(array $data)
    {
        try {
            $season = Season::create($data);
        } catch (Exception $e) {
            Log::error('Error by creating new season', ['trace' => $e->getTraceAsString()]);

            return false;
        }

        return $season;
    }

    /**
     * Update a season
     *
     * @param Season $season
     * @param array $data
     * @return Season|bool
     */
    public function update(Season $season, array $data)
    {
        try {
            $season->update($data);
        } catch (Exception $e) {
            Log::error('Error by updating new season', ['trace' => $e->getTraceAsString()]);

            return false;
        }

        return $season;
    }
}
