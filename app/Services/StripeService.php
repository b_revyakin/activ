<?php

namespace App\Services;

use App\Entities\Booking;
use Exception;
use Log;
use Stripe\Stripe;
use Stripe\Charge;

class StripeService
{
    /**
     * @param Booking $booking
     * @param array $data
     * @return bool|Charge
     */
    public function executeWithStripe(Booking $booking, array $data)
    {
        Stripe::setApiKey(config('stripe.secret_key'));

        try {
            $charge = Charge::create([
                'amount' => $booking->price * 100,
                'currency' => 'gbp',
                'description' => $booking->customer->user->email,
                'card' => $data['stripe_token']
            ]);

        } catch(Exception $e) {
            Log::error($e->getMessage());
            
            return false;
        }

        return $charge;
    }
}
