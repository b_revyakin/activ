<?php

namespace App\Services;

use App\Entities\Venue;
use Exception;
use Log;

class VenueService
{
    /**
     * Create new venue
     *
     * @param array $data
     * @return bool|Venue
     */
    public function create(array $data)
    {
        try {
            $venue = Venue::create($data);
        } catch (Exception $e) {
            Log::error('Error by creating new venue', ['trace' => $e->getTraceAsString()]);

            return false;
        }

        return $venue;
    }

    /**
     * Update a venue
     *
     * @param Venue $venue
     * @param array $data
     * @return Venue|bool
     */
    public function update(Venue $venue, array $data)
    {
        try {
            $venue->update($data);
        } catch (Exception $e) {
            Log::error('Error by updating venue', ['trace' => $e->getTraceAsString()]);

            return false;
        }

        return $venue;
    }
}
