<?php
/**
 * Created by PhpStorm.
 * User: nikolay
 * Date: 2/8/16
 * Time: 4:13 PM
 */

namespace App\Traits;


use Auth;

trait AuthRedirectTrait
{
    /**
     * Need call in construct
     */
    public function setRedirectPaths()
    {
        $this->authRedirect = [
            'customer' => route('customer::bookings.create'),
            'staff' => route('admin::dashboard'),
            'admin' => route('admin::dashboard'),
        ];
    }

    /**
     * Get the post register / login redirect path.
     *
     * @return string
     */
    public function redirectPath()
    {
        $user = Auth::user();

        if ($user) {
            foreach ($this->authRedirect as $role => $path) {
                if ($user->hasRole($role)) {
                    return $path;
                }
            }
        }

        return $this->defaultRedirectPath();
    }
}