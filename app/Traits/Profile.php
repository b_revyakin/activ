<?php

namespace App\Traits;

use App\Entities\User;

trait Profile
{
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function getNameAttribute()
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    public function name()
    {
        return $this->first_name . ' ' . $this->last_name;
    }
}
