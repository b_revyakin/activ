<?php

use App\Entities\Event;
use App\Entities\Season;
use App\Entities\Venue;
use Carbon\Carbon;

if (!function_exists('is_venue_and_season_date')) {
    /**
     * Check if the requested date include to events by the venue and the season
     *
     * @param Carbon $date
     * @param Venue $venue
     * @param Season $season
     * @return mixed
     */
    function is_venue_and_season_date(Carbon $date, Venue $venue, Season $season)
    {
        return Event::query()
            ->whereHas('venue', function ($query) use ($venue) {
                $query->where('id', $venue->id);
            })
            ->whereHas('season', function ($query) use ($season) {
                $query->where('id', $season->id);
            })
            ->where(function ($query) use ($date) {
                $query
                    ->whereDate('start_date', '<=', $date)
                    ->whereDate('end_date', '>=', $date);
            })
            ->exists();
    }

}
