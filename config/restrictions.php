<?php

return [
    'children' => [
        'min_age' => env('MIN_AGE_CHILDREN', 3),
    ],
];
