<?php

return [
    'customer' => [
        'title' => 'required|in:Mr,Miss,Mrs,Ms,Dr',
        'first_name' => 'required|string',
        'last_name' => 'required|string',
        'subscribe_to_newsletter' => 'required|boolean',
        'mobile' => 'required',
        'home_tel' => 'required',
        'work_tel' => '',
        'address_line_1' => 'required',
        'address_line_2' => '',
        'city' => 'required',
//        'county' => 'required',
        'country_id' => 'required|exists:countries,id',
        'post_code' => 'required',
        'hear_from' => 'string',
    ],

    'child' => [
        'first_name' => 'required|string',
        'last_name' => 'required|string',
        'email' => 'email',
        'birthday' => 'required|date|min_age:' . config('restrictions.children.min_age'),
        'sex' => 'required|boolean',
        'relationship' => 'required',
        'school' => 'required',
        'year_in_school_id' => 'required|exists:years_in_school,id',
        'doctor_name' => 'required',
        'doctor_telephone_number' => 'required',
        'medical_advice_and_treatment' => 'required|boolean',
        'epi_pen' => 'required|boolean',
        'medication' => 'required',
        'special_requirements' => 'required',
        'can_swim_id' => 'required|exists:swim_options,id',
        'emergency_contact_number' => 'required',
        'emergency_contact_name' => 'required',

        'authorised_person_name_1' => 'string',
        'authorised_person_number_1' => 'string',
        'authorised_person_name_2' => 'string',
        'authorised_person_number_2' => 'string',
        'authorised_person_name_3' => 'string',
        'authorised_person_number_3' => 'string',
    ],

    'orders' => [
        'orders.*.child_id' =>  'required|exists:children,id',
        'orders.*.event_id' => 'required|exists:events,id',
        'orders.*.selectedDates.*' => 'required|date',
    ]
];