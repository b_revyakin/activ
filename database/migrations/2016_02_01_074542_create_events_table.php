<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->increments('id');

            //Own of event
            $table->integer('staff_id')->unsigned();
            $table->foreign('staff_id')->references('id')->on('staffs');

            $table->string('name');

            $table->integer('venue_id')->unsigned();
            $table->foreign('venue_id')->references('id')->on('venues');

            $table->integer('season_id')->unsigned();
            $table->foreign('season_id')->references('id')->on('seasons');

            $table->date('start_date');
            $table->time('start_time');
            $table->date('end_date');
            $table->time('end_time');

            $table->integer('places')->unsigned();//Max places on one event

            $table->float('cost_per_day')->unsigned();
            $table->float('cost_per_week')->unsigned();

            $table->boolean('allow_individual_day_booking');

            $table->integer('min_age')->unsigned();
            $table->integer('max_age')->unsigned();

            $table->text('description');

            $table->boolean('active')->default(true);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('events');
    }
}
