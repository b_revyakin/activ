<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateChildrenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('children', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('parent_id')->unsigned();
            $table->foreign('parent_id')->references('id')->on('customers');

            $table->string('first_name');
            $table->string('last_name');
            $table->string('email');
            $table->date('birthday');
            $table->boolean('sex');
            $table->string('relationship');
            $table->string('school');
            $table->integer('year_in_school')->unsigned();
            $table->string('doctor_name');
            $table->string('doctor_telephone_number');

            $table->boolean('medical_advice_and_treatment');
            $table->boolean('epi_pen');

            $table->text('medication')->nullable();
            $table->text('special_requirements')->nullable();

            $table->softDeletes();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('children');
    }
}
