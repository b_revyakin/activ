<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddFieldsToPhotosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('photos', function (Blueprint $table) {
            if (Schema::hasColumn('photos', 'event_id')) {
                $table->dropForeign('photos_event_id_foreign');
                $table->dropColumn('event_id');
            }

            $table->integer('imageable_id');
            $table->string('imageable_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('photos', function (Blueprint $table) {
            if (!Schema::hasColumn('photos', 'event_id')) {
                $table->integer('event_id')->unsigned();
                $table->foreign('event_id')->references('id')->on('events');
            }

            $table->dropColumn('imageable_id');
            $table->dropColumn('imageable_type');
        });
    }
}
