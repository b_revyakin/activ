<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRelationshipChildrenWithYearInSchool extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('children', function (Blueprint $table) {
            $table->integer('year_in_school_id')->nullable()->unsigned();
            $table->foreign('year_in_school_id')->references('id')->on('years_in_school');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('children', function (Blueprint $table) {
            $table->dropForeign('children_year_in_school_id_foreign');
            $table->dropColumn('year_in_school_id');
        });
    }
}
