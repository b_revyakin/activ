<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddThreeAuthorisedPeopleToChildrenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('children', function (Blueprint $table) {
            $table->string('authorised_person_name_1')->nullable();
            $table->string('authorised_person_number_1')->nullable();

            $table->string('authorised_person_name_2')->nullable();
            $table->string('authorised_person_number_2')->nullable();

            $table->string('authorised_person_name_3')->nullable();
            $table->string('authorised_person_number_3')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('children', function (Blueprint $table) {
            $table->dropColumn([
                'authorised_person_name_1',
                'authorised_person_number_1',

                'authorised_person_name_2',
                'authorised_person_number_2',

                'authorised_person_name_3',
                'authorised_person_number_3',
            ]);
        });
    }
}
