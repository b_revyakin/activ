<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProductVariantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_variants', function (Blueprint $table) {
            $table->increments('id');

            /** @noinspection PhpUndefinedMethodInspection */
            $table->integer('product_id')->unsigned();
            /** @noinspection PhpUndefinedMethodInspection */
            $table->foreign('product_id')->references('id')->on('products');

            /** @noinspection PhpUndefinedMethodInspection */
            $table->string('name')->nullable();
            /** @noinspection PhpUndefinedMethodInspection */
            $table->float('price')->unsigned()->nullable();
            /** @noinspection PhpUndefinedMethodInspection */
            $table->integer('count')->unsigned()->nullable();
            /** @noinspection PhpUndefinedMethodInspection */
            $table->boolean('active')->default(true);

            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('product_variants');
    }
}
