<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class DropOrderProductAndOrderProductVatioantTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('order_product');
        Schema::drop('order_product_variant');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //Create only order_product_variant table. order_product not used.
        Schema::create('order_product_variant', function (Blueprint $table) {
            $table->primary(['order_id', 'product_variant_id']);

            $table->integer('order_id')->unsigned();
            $table->foreign('order_id')->references('id')->on('orders');

            $table->integer('product_variant_id')->unsigned();
            $table->foreign('product_variant_id')->references('id')->on('product_variants');

            $table->integer('count');
            $table->float('price');
        });
    }
}
