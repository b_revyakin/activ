<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBookingProductVariantTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('booking_product_variant', function (Blueprint $table) {
            $table->primary(['booking_id', 'product_variant_id']);

            /** @noinspection PhpUndefinedMethodInspection */
            $table->integer('booking_id')->unsigned();
            /** @noinspection PhpUndefinedMethodInspection */
            $table->foreign('booking_id')->references('id')->on('bookings');

            /** @noinspection PhpUndefinedMethodInspection */
            $table->integer('product_variant_id')->unsigned();
            /** @noinspection PhpUndefinedMethodInspection */
            $table->foreign('product_variant_id')->references('id')->on('product_variants');

            $table->integer('count');
            $table->float('price');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('booking_product_variant');
    }
}
