<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Eloquent::reguard();

        $this->call(RolesTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(SwimOptionsTableSeeder::class);
        $this->call(YearsInSchoolTableSeeder::class);
        $this->call(CountriesTableSeeder::class);
        $this->call(PaymentSystemsTableSeeder::class);
    }
}
