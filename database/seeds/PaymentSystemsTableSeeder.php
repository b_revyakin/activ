<?php

use App\Entities\PaymentSystem;
use Illuminate\Database\Seeder;

class PaymentSystemsTableSeeder extends Seeder
{
    protected $paymentSystems = [
        ['SagePay', 'sagepay' , false],
        ['Stripe', 'stripe' , false]
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->paymentSystems as $paymentSystem) {
            PaymentSystem::create([
                'title' => $paymentSystem[0],
                'system' => $paymentSystem[1],
                'active' => $paymentSystem[2],
            ]);
        }
    }
}
