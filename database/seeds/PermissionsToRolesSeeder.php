<?php

use App\Entities\Role;
use Bican\Roles\Models\Permission;
use Illuminate\Database\Seeder;

class PermissionsToRolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $createEvent = Permission::create([
            'name' => 'Show an event',
            'slug' => 'event.create',
        ]);

        $showEvent = Permission::create([
            'name' => 'Show an event',
            'slug' => 'event.show',
        ]);

        $editEvent = Permission::create([
            'name' => 'Edit an event',
            'slug' => 'event.edit',
        ]);
        
        $capacitiesEvent = Permission::create([
            'name' => 'Capacities an event',
            'slug' => 'event.capacities',
        ]);
        
        $activationEvent = Permission::create([
            'name' => 'Activation an event',
            'slug' => 'event.activation',
            'description' => 'Activation and deactivation events'
        ]);
        
        $cancelBooking = Permission::create([
            'name' => 'Cancel Booking',
            'slug' => 'booking.cancel'
        ]);
        
        $successBooking = Permission::create([
            'name' => 'Success Booking',
            'slug' => 'booking.success',
            'description' => 'Mark a booking as paid'
        ]);
        
        $permissions = [
            $createEvent,
            $showEvent,
            $editEvent,
            $capacitiesEvent,
            $activationEvent,
            $cancelBooking,
            $successBooking,
        ];

        $roles = [
            Role::admin(),
            Role::staff()
        ];

        foreach ($roles as $role) {
            foreach ($permissions as $permission) {
                $role->attachPermission($permission);
            }
        }

    }
}