<?php

use App\Entities\SwimOption;
use Illuminate\Database\Seeder;

class SwimOptionsTableSeeder extends Seeder
{
    protected $options = [
        ['Non swimmer', 1],
        ['Not to do swimming activities', 2],
        ['Not confident in water', 3],
        ['Uses bouyancy aid', 4],
        ['Can only swim width of pool', 5],
        ['Can swim length of pool non-stop without aid', 6],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->options as $option) {
            SwimOption::create([
                'option' => $option[0],
                'number' => $option[1],
            ]);
        }
    }
}