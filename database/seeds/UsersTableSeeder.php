<?php

use App\Services\AdminService;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    protected $users = [
        [
            'email' => 'admin@activ.com',
            'password' => 'test123',
            'first_name' => 'Admin',
            'last_name' => 'Activ',
            'mobile' => '',
            'qualifications' => '',
            'about' => '',
            'isStaff' => false
        ],
        [
            'email' => 'staff@activ.com',
            'password' => 'test123',
            'first_name' => 'Staff',
            'last_name' => 'Activ',
            'mobile' => '',
            'qualifications' => '',
            'about' => '',
            'isStaff' => true
        ]
    ];

    /**
     * @var AdminService
     */
    protected $adminService;

    /**
     * UsersTableSeeder constructor.
     * @param AdminService $adminService
     */
    public function __construct(AdminService $adminService)
    {
        $this->adminService = $adminService;
    }


    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->users as $user) {
            $this->adminService->create($user, $user['isStaff']);
        }
    }
}
