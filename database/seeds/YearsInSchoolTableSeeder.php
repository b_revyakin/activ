<?php

use App\Entities\YearInSchool;
use Illuminate\Database\Seeder;

class YearsInSchoolTableSeeder extends Seeder
{
    protected $uniqueYears = [
        'Nursery',
        'Reception'
    ];

    protected $minYear = 1;

    protected $maxYear = 13;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach($this->uniqueYears as $year) {
            YearInSchool::create([
                'title' => $year
            ]);
        }

        for($i = $this->minYear; $i <= $this->maxYear; $i++) {
            YearInSchool::create([
                'title' => 'Year ' . $i
            ]);
        }
    }
}
