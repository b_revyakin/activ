var elixir = require('laravel-elixir');

require('laravel-elixir-angular');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    mix.angular('public/booking/', 'public/js/', 'app.js');

    mix.angular('public/admin-booking/', 'public/js/', 'admin-app.js');

    mix.version([
        'js/app.js',
        'js/admin-app.js'
    ]);
});
