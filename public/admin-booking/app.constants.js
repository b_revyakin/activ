(function () {

  'use strict';

  angular
    .module('booking')
    .constant('accosts', ['Mr', 'Miss', 'Mrs', 'Ms', 'Dr'])
    .constant('booleanValues', [
      {
        title: 'Yes',
        value: 1
      },
      {
        title: 'No',
        value: 0
      }
    ])
    .constant('paymentTypes', [
      {
        title: 'Pay Online by Credit / Debit Card',
        value: 'online'
      },
      {
        title: 'Using childcare vouchers',
        value: 'voucher'
      },
      //{
      //  title: 'By cheque',
      //  value: 'cheque'
      //}
    ]);

})();
