(function () {
  'use strict';

  angular
    .module('booking')
    .run(run);

  run.$inject = ['$state', '$rootScope', 'orderService'];

  function run($state, $rootScope, orderService) {

    $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
      var preventEvent = false,
        stateName;

      if(toState.name === 'booking.complete' && !orderService.count()) {
        preventEvent = true;
        stateName = 'booking.orders.create';
      }

      if(preventEvent) {
        event.preventDefault();
        if(stateName) {
          $state.go(stateName);
        }
      } else {
        $rootScope.tab = toState.data.tab;
      }

    });
  }
})();
