(function () {
  'use strict';

  angular
    .module('booking')
    .controller('CreateChildController', CreateChildController);

  CreateChildController.$inject = ['childService', '$state', 'toaster', 'swimOptions', 'yearsInSchool'];

  /* @ngInject */
  function CreateChildController(childService, $state, toaster, swimOptions, yearsInSchool) {
    /* jshint validthis: true */
    var vm = this;

    vm.swimOptions = swimOptions;
    vm.yearsInSchool = yearsInSchool;

    vm.form = {
      first_name: undefined,
      last_name: undefined,
      email: undefined,
      birthday: undefined,
      sex: undefined,
      relationship: undefined,
      school: undefined,
      year_in_school: undefined,
      doctor_name: undefined,
      doctor_telephone_number: undefined,
      medical_advice_and_treatment: undefined,
      epi_pen: undefined,
      medication: undefined,
      special_requirements: undefined,
      can_swim: undefined,
      emergency_contact_number: undefined,
    };

    vm.create = create;

    vm.datePickerOpen = false;

    //////////////////////////////

    function create() {
      childService.store(vm.form).then(function () {
        toaster.success('The child was created!');

        $state.go('^.index');
      });
    }

  }
})();
