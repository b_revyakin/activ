(function () {
  'use strict';

  angular
    .module('booking')
    .controller('CompleteController', CompleteController);

  /* @ngInject */
  function CompleteController(ordersWithPrices, store, bookingService, paymentTypes, alertService, orderService, helperService, $filter, $state, $uibModal, products) {
    /* jshint validthis: true */
    var vm = this;

    vm.orders = ordersWithPrices;
    vm.orders.sum = $filter('sumByKey')(vm.orders, 'sum');
    vm.orders.discount = $filter('sumByKey')(vm.orders, 'discount');
    vm.sum_variants = $filter('sumByKey')(products, 'price');
    vm.orders.coupon = 0;
    vm.products = products;

    vm.paymentTypes = paymentTypes;
    vm.couponApplied = false;

    vm.form = {
      payment_type: undefined,
      orders: [],
      coupon: undefined,
    };
    vm.agreed = false;
    vm.request = false;

    prepareProducts();
    calculateTotalPrice();
    setOrdersToBookingForm();
    addToOrderProductsIfStored();

    vm.book = book;
    vm.remove = remove;
    vm.applyCoupon = applyCoupon;
    vm.cancelCoupon = cancelCoupon;
    vm.calculateProductPrice = calculateProductPrice;
    vm.saveProducts = saveProducts;

    ////////////////////////////////

    function applyCoupon() {
      if (!vm.form.coupon) {
        return;
      }

      vm.request = true;

      bookingService.checkCoupon(vm.form.coupon).then(function (persent) {
        vm.orders.couponPersent = persent;
        calculateTotalPrice();
        alertService.success('Your coupon is activated.');
        vm.couponApplied = true;
      }, function () {
        alertService.error('Coupon not exists or expired.');
      }).finally(function () {
        vm.request = false;
      });
    }

    function cancelCoupon() {
      //Clear data about previous coupon
      vm.form.coupon = '';
      vm.orders.couponPersent = undefined;
      vm.orders.coupon = 0;
      vm.couponApplied = false;

      calculateTotalPrice();

      alertService.success('You coupon was deactivated.');
    }

    function remove(id) {
      var modalInstance = $uibModal.open({
        templateUrl: '/booking/complete/confirm-remove-modal.html',
        controller: 'ConfirmRemoveOrderController',
        controllerAs: 'ConfirmRemoveCtrl',
        size: 'md'
      });

      modalInstance.result.then(function () {
        orderService.remove(id).then(function () {
          //Refresh data
          orderService.all().then(function (orders) {
            alertService.success('The order was removed');

            if(! orders.length) {
              $state.go('booking.orders.create');
            }

            var _orders = helperService.prepareOrders(orders);
            bookingService.calculate(_orders).then(function (prices) {
              var ordersWithPrice = [];

              for (var i = 0, order; (order = orders[i]); i++) {
                order.sum = 0;

                var price = $filter('filter')(prices, {_id: order._id}, true);
                if (price.length && price[0].amount) {
                  order.sum = price[0].amount;
                  order.discount = price[0].discount;
                }
                ordersWithPrice.push(order);
              }

              vm.orders = ordersWithPrice;
              vm.orders.sum = $filter('sumByKey')(vm.orders, 'sum');
              vm.orders.discount = $filter('sumByKey')(vm.orders, 'discount');
              vm.orders.coupon = 0;
              calculateTotalPrice();
            });
          });
        });
      });
    }

    function book() {
      if(! vm.agreed) {
        alertService.error('You should with booking\'s terms and conditions');
        return ;
      }

      vm.request = true;

      return bookingService.book(vm.form).then(function (data) {
        alertService.success('You booked!');

        //Clear current orders
        orderService.clear();

        helperService.redirect(data.redirect, data.method, data.fields);
      }, function () {
        vm.request = false;
      });
    }

    function setOrdersToBookingForm() {
      vm.form.orders = helperService.prepareOrders(vm.orders);
    }

    function calculateTotalPrice() {
      if(vm.form.coupon && vm.orders.couponPersent) {
        vm.orders.coupon = vm.orders.sum * vm.orders.couponPersent / 100;
        vm.orders.total = vm.orders.sum - vm.orders.coupon;
      } else {
        vm.orders.total = vm.orders.sum - vm.orders.discount;
      }
      vm.orders.total = vm.orders.total + vm.sum_variants;
    }

    function prepareProducts() {
      angular.forEach(vm.products, function (product) {
        if (!product.selectedQuantity) {
          product.selectedQuantity = 1;
        }

        if (!product.selectedVariant) {
          product.selectedVariant = product.variants.length ? product.variants[0].id : 0;
        }

        Object.defineProperty(product, 'price', {
          get: function () {
            var price = 0;

            var selectedVariant = this.selectedVariant;
            var variant = this.variants.filter(function (variant) {
              return selectedVariant && selectedVariant === variant.id;
            })[0];

            if (variant && this.selectedQuantity) {
              price = variant.price * this.selectedQuantity;
            }

            return price;
          }
        });
        Object.defineProperty(product, 'maxCount', {
          get: function () {
            var countMax = 0;

            var selectedVariant = this.selectedVariant;
            var variant = this.variants.filter(function (variant) {
              return selectedVariant && selectedVariant === variant.id;
            })[0];

            if (variant && this.selectedQuantity) {
              countMax = variant.count;
            }

            return countMax;
          }
        });

        calculateProductPrice(product, true);
      });
    }

    function calculateProductPrice(product, notShowChangeProducts) {
      vm.changedProducts = !notShowChangeProducts;
      //product.price = 0;
      //product.maxCount = undefined;

      var variant = product.variants.filter(function (variant) {
        return product.selectedVariant && product.selectedVariant === variant.id;
      })[0];

      if (variant && product.selectedQuantity) {
        //product.price = variant.price * product.selectedQuantity;
        //product.maxCount = variant.count;
      }
    }

    function saveProducts() {
      var products = vm.products.filter(function (product) {
        return product.price;
      });

      orderService.saveProducts(products).then(function () {
        alertService.success('Products saved');

        vm.form.products = products;

        vm.sum_variants = $filter('sumByKey')(products, 'price');

        calculateTotalPrice();

        vm.changedProducts = false;
      });
    }

    function addToOrderProductsIfStored() {
      var products = store.get('products');

      if (!products || !products.length) {
        return false;
      }

      vm.form.products = products;
    }

  }
})();
