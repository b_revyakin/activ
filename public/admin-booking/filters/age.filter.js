(function(){
  'use strict';

  angular
    .module('booking')
    .filter('age', age);

  age.$inject = [];

  function age() {
    return main;

    function main(person, date) {
      date = date ? new Date(date) : new Date();

      var birthday = new Date(person.birthday);
      var ageDifMs = date.getTime() - birthday.getTime();
      var ageDate = new Date(ageDifMs); // miliseconds from epoch
      return Math.abs(ageDate.getFullYear() - 1970);
    }
  }
})();