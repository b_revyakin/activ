(function(){
  'use strict';

  angular
    .module('booking')
    .filter('event', event);

  event.$inject = [];

  function event() {
    return main;

    function main(event) {
      return event.venue.name + ' - ' + event.name;
    }
  }
})();