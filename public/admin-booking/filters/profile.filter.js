(function(){
  'use strict';

  angular
    .module('booking')
    .filter('profile', profile);

  profile.$inject = [];

  function profile() {
    return main;

    function main(user) {
      return user.first_name + ' ' + user.last_name;
    }
  }
})();