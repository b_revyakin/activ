(function(){
  'use strict';

  angular
    .module('booking')
    .filter('sex', sex);

  sex.$inject = [];

  function sex() {
    return main;

    function main(value) {
      return value ? 'Male' : 'Female';
    }
  }
})();