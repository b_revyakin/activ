(function () {
  'use strict';

  angular
    .module('booking')
    .controller('CreateOrderController', CreateOrderController);

  CreateOrderController.$inject = ['children', 'events', 'orderService', '$state', 'alertService', 'helperService', '$filter', 'products', 'countOrders'];

  /* @ngInject */
  function CreateOrderController(children, events, orderService, $state, alertService, helperService, $filter, products, countOrders) {
    /* jshint validthis: true */
    var vm = this;

    if (!children.length) {
      alertService.error('Please add your children!');
      $state.go('booking.children.create');
    }

    vm.countOrders = countOrders;
    vm.children = children;
    vm.products = products;
    vm.events = [];//events;

    vm.request = false;

    vm.form = {
      child_id: undefined,
      event_id: undefined,
      dates: [],
      products: []
    };
    vm.selectedProduct = undefined;

    vm.create = create;
    vm.selectedEvent = selectedEvent;
    vm.selectedWeek = selectedWeek;
    vm.selectedDay = selectedDay;
    vm.selectedChild = selectedChild;
    vm.addProduct = addProduct;

    ///////////////////////

    function addProduct() {

    }

    function selectedChild() {
      var child = $filter('filter')(children, {id: vm.form.child_id})[0];

      vm.events = $filter('filter')(events, function(event) {
        var age = $filter('age')(child, event.start_date);
        return event.min_age <= age && age <= event.max_age;
      });
    }

    function create() {
      if (helperService.ShowValidationErrors(orderService.validate(vm.form, children, events)).length) {
        return;
      }

      vm.request = true;

      orderService.add(vm.form).then(function () {
        alertService.success('Order was saved');

        $state.go('^.^.complete');
      }).finally(function () {
        //vm.request = false;
      });
    }

    function selectedEvent() {
      if(vm.form.event_id) {
        helperService.pushEventDates(vm.events, vm.form);
      }
    }

    function selectedWeek(week) {
      if (week.selected) {
        setSelectedDatesOfWeek(week, true);
        week.selectedAnyDates = true;
      } else {
        week.selectedAnyDates = false;
        setSelectedDatesOfWeek(week, false);
      }
    }

    function selectedDay(week, day) {
      if (!day.selected && week.selected) {
        week.selected = false;
      }

      if (day.selected) {
        week.selectedAnyDates = true;
        var allDaysOfWeekSelected = true;

        for (var i = 0, weekday; (weekday = week.dates[i]); i++) {
          if (weekday.allow && !weekday.selected) {
            allDaysOfWeekSelected = false;
            break;
          }
        }

        if (allDaysOfWeekSelected) {
          week.selected = true;
        }
      } else {
        //Check that any day is selected
        var selectedAnyDate = false;
        for (var j = 0, date; (date = week.dates[j]); j++) {
          if (date.selected) {
            selectedAnyDate = true;
            break;
          }
        }

        week.selectedAnyDates = selectedAnyDate;
      }
    }

    function setSelectedDatesOfWeek(week, value) {
      for (var i = 0, day; (day = week.dates[i]); i++) {
        if (day.allow) {
          day.selected = value;
        }
      }
    }

  }
})();
