(function () {
  'use strict';

  angular
    .module('booking')
    .service('bookingService', bookingService);

  bookingService.$inject = ['$http', 'helperService', '$rootScope'];

  /* @ngInject */
  function bookingService($http, helperService, $rootScope) {
    /* jshint validthis: true */
    var vm = this;

    vm.book = book;
    vm.calculate = calculate;
    vm.checkCoupon = checkCoupon;

    /////////////////////////

    function book(data) {
      return $http.post('/admin/api/' + $rootScope.customerId + '/booking/', helperService.prepareBookingForRequest(data)).then(helperService.fetchResponse);
    }

    function calculate(orders) {
      return $http.post('/api/booking/calculate', {orders: orders}).then(helperService.fetchResponse).then(function (data) {
        return data;
      });
    }

    function checkCoupon(coupon) {
      return $http.get('/api/booking/coupons/' + coupon).then(helperService.fetchResponse);
    }
  }
})();
