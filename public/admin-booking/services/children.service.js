(function () {
  'use strict';

  angular
    .module('booking')
    .service('childService', childService);

  childService.$inject = ['$resource', '$http', 'helperService', '$rootScope', '$timeout', '$q'];

  /* @ngInject */
  function childService($resource, $http, helperService, $rootScope, $timeout, $q) {
    /* jshint validthis: true */
    var vm = this;

    var Child;

    var run = $timeout(function () {
      Child = $resource('/admin/api/customers/' + $rootScope.customerId + '/children/:childId', {childId: '@id'}, {
        update: {method: 'PUT'}
      });
    });

    vm.all = all;
    vm.get = get;
    vm.store = store;
    vm.update = update;
    vm.remove = remove;
    vm.swimOptions = swimOptions;
    vm.yearsInSchool = yearsInSchool;

    ///////////////////////

    function yearsInSchool() {
      return $http.get('/api/years-in-school').then(helperService.fetchResponse);
    }

    function swimOptions() {
      return $http.get('/api/swim-options').then(helperService.fetchResponse);
    }

    function all() {
      return $q.all([run]).then(function () {
        return Child.query()
            .$promise
            .then(function (children) {
              for(var i = 0, child; (child = children[i]); i++) {
                child.birthday = moment(child.birthday).toDate();
              }
                
              return children;
            });
      });
    }

    function get(id) {
      return $q.all([run]).then(function () {
        return Child.get({childId: id})
          .$promise
          .then(function (child) {
            child.birthday = moment(child.birthday).toDate();

            return child;
          });
      });
    }

    function store(data) {
      return Child.save(data).$promise;
    }

    function update(data) {
      return Child.update(data).$promise;
    }

    function remove(id) {
      return Child.delete({childId: id}).$promise;
    }
  }
})();
