(function() {
  'use strict';

  angular
    .module('booking')
    .service('eventService', eventService);

  eventService.$inject = ['$http', 'helperService'];

  /* @ngInject */
  function eventService($http, helperService) {
    /* jshint validthis: true */
    var vm = this;

    vm.all = all;

    ///////////////////////////

    function all() {
      return $http.get('/api/events')
        .then(helperService.fetchResponse, helperService.rejectResponse)
        .then(function (events) {
          for(var i = 0, event; (event = events[i]); i++) {
            event.start_date = moment(event.start_date);
            event.end_date = moment(event.end_date);
          }

          return events;
        });
    }
  }
})();
