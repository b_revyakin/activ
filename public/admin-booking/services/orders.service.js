(function () {
  'use strict';

  angular
    .module('booking')
    .service('orderService', orderService);

  orderService.$inject = ['$filter', '$q', 'store', 'childService', 'eventService', '$http', 'helperService', '$timeout', '$rootScope'];

  /* @ngInject */
  function orderService($filter, $q, store, childService, eventService, $http, helperService, $timeout, $rootScope) {
    /* jshint validthis: true */
    var vm = this;

    vm.customerId = store.get('customer-id');

    var run = $timeout(function () {
      if(vm.customerId != $rootScope.customerId) {
        vm.customerId = $rootScope.customerId;
        store.set('customer-id', vm.customerId);

        store.remove('orders');
        store.remove('index');
      }

      vm.orders = store.get('orders');
      vm.index = store.get('index');

      if (!vm.orders) {
        vm.orders = [];
        vm.index = 1;
      }

      vm.promises = $q.all([childService.all(), eventService.all()]).then(function (data) {
        vm.children = data[0];
        vm.events = data[1];
      });
    });

    vm.validate = validate;

    vm.count = count;
    vm.all = all;
    vm.add = add;
    vm.get = get;
    vm.update = update;
    vm.remove = remove;
    vm.calculate = calculate;
    vm.clear = clear;
    vm.products = products;
    vm.saveProducts = saveProducts;

    ///////////////////////

    function products() {
      return $http.get('/api/products').then(helperService.fetchResponse).then(helperService.prepareProducts);
    }

    function saveProducts(products) {
      var localPromise = $q.defer();

      vm.promises.then(function () {
        store.set('products', products);

        localPromise.resolve(true);
      });

      return localPromise.promise;
    }

    function count() {
      return vm.orders.length;
    }

    function clear() {
      var orders = angular.copy(vm.orders);

      var localPromise = $q.defer();

      vm.promises.then(function () {
        store.remove('orders');
        store.remove('index');
        store.remove('products');

        localPromise.resolve(true);
      });

      return localPromise.promise;
    }

    function calculate(order) {
      var sum = 0;

      for (var i = 0, week; (week = order.weeks[i]); i++) {
        if (week.selected) {
          sum += order.event.cost_per_week;
        } else {
          for (var j = 0, weekday; (weekday = week.dates[j]); j++) {
            if(weekday.selected) {
              sum += order.event.cost_per_day;
            }
          }
        }
      }

      angular.forEach(order.variants, function (variant) {
        sum += variant.price * variant.count;
      });

      return sum;
    }

    function validate(order, children, events) {
      var errors = [];

      var child = $filter('filter')(children, {id: order.child_id});
      if (!(order.child_id && child.length)) {
        errors.push({
          title: 'child',
          message: 'Necessary select child'
        });
      }

      var event = $filter('filter')(events, {id: order.event_id});
      if (!(order.event_id && event.length)) {
        errors.push({
          title: 'event',
          message: 'Necessary select event'
        });
      }

      //Check that selected as minimum one day
      var selected = false;
      for (var i = 0, week; order.weeks && (week = order.weeks[i]); i++) {
        for (var j = 0, day; (day = week.dates[j]); j++) {
          if (day.selected) {
            selected = true;
            break;
          }
        }

        if (selected) {
          break;
        }
      }

      if (!selected) {
        errors.push({
          title: 'dates',
          message: 'Necessary select at least one day'
        });
      }

      return errors.reverse();
    }

    function all() {
      return $q.all([run]).then(function () {
        var orders = angular.copy(vm.orders);

        var localPromise = $q.defer();

        vm.promises.then(function () {
          for (var i = 0, order; (order = orders[i]); i++) {
            setChild(order);
            setEvent(order);
          }

          localPromise.resolve(orders);
        });

        return localPromise.promise;
      });
    }

    function add(order) {
      var localPromise = $q.defer();

      vm.promises.then(function () {
        order._id = vm.index++;
        vm.orders.push(order);
        saveOrders();

        localPromise.resolve(
          angular.copy(order)
        );
      });


      return localPromise.promise;
    }

    function get(id) {
      var localPromise = $q.defer();

      vm.promises.then(function () {
        var order = angular.copy($filter('filter')(vm.orders, {_id: id})[0]);

        setChild(order);
        setEvent(order);

        localPromise.resolve(order);
      });


      return localPromise.promise;
    }

    function update(id, order) {
      var localPromise = $q.defer();

      var _order = $filter('filter')(vm.orders, {_id: id})[0];

      _order.child_id = order.child_id;
      _order.child = order.child;
      _order.event_id = order.event_id;
      _order.event = order.event;
      _order.dates = order.dates;

      saveOrders();

      localPromise.resolve(_order);

      return localPromise.promise;
    }

    function remove(id) {
      var localPromise = $q.defer();

      var order = $filter('filter')(vm.orders, {_id: id})[0];

      vm.orders.splice(vm.orders.indexOf(order), 1);

      saveOrders();

      localPromise.resolve(true);

      return localPromise.promise;
    }

    function saveOrders() {
      store.set('orders', vm.orders);
      store.set('index', vm.index);
    }

    function setChild(order) {
      order.child = $filter('filter')(vm.children, {id: order.child_id})[0];
    }

    function setEvent(order) {
      order.event = $filter('filter')(vm.events, {id: order.event_id})[0];
    }

  }
})();
