(function () {

  'use strict';

  angular
    .module('booking')
    .constant('accosts', ['Mr', 'Miss', 'Mrs', 'Ms', 'Dr'])
    .constant('booleanValues', [
      {
        title: 'Yes',
        value: 1
      },
      {
        title: 'No',
        value: 0
      }
    ])
    .constant('paymentTypes', [
      {
        title: 'Pay Online by Credit / Debit Card',
        value: 'online'
      },
      {
        title: 'Using childcare vouchers',
        value: 'voucher'
      },
      //{
      //  title: 'By cheque',
      //  value: 'cheque'
      //}
    ])
    .constant('months', [
      {
        value: 0,
        name: 'January'
      },
      {
        value: 1,
        name: 'February'
      },
      {
        value: 2,
        name: 'March'
      },
      {
        value: 3,
        name: 'April'
      },
      {
        value: 4,
        name: 'May'
      },
      {
        value: 5,
        name: 'June'
      },
      {
        value: 6,
        name: 'July'
      },
      {
        value: 7,
        name: 'August'
      },
      {
        value: 8,
        name: 'September'
      },
      {
        value: 9,
        name: 'October'
      },
      {
        value: 10,
        name: 'November'
      },
      {
        value: 11,
        name: 'December'
      }
    ]);

})();
