(function () {
  'use strict';

  angular
    .module('booking')
    .config(config);

  config.$inject = ['$stateProvider', '$urlRouterProvider'];

  function config($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise('/profile/show');

    var uiView = '<ui-view></ui-view>';

    var dependecies = {
      orders: ['orderService', function (orderService) {
        return orderService.all();
      }],
      order: ['orderService', '$stateParams', function (orderService, $stateParams) {
        return orderService.get($stateParams.orderId);
      }],
      children: ['childService', function (childService) {
        return childService.all();
      }],
      child: ['childService', '$stateParams', function (childService, $stateParams) {
        return childService.get($stateParams.childId);
      }],
      events: ['eventService', function (eventService) {
        return eventService.all();
      }],
      swimOptions: ['childService', function(childService) {
        return childService.swimOptions();
      }],
      yearsInSchool: ['childService', function(childService) {
        return childService.yearsInSchool();
      }],
      products: ['orderService', function(orderService) {
        return orderService.products();
      }],
      countries: ['profileService', function (profileService) {
        return profileService.countries();
      }],
      profile: ['profileService', function (profileService) {
        return profileService.get();
      }]
    };

    $stateProvider
      .state('booking', {
        abstract: true,
        templateUrl: '/booking/template.html'
      })


      .state('booking.profile', {
        abstract: true,
        url: '/profile',
        data: {
          tab: 'profile'
        },
        template: uiView,
        controller: 'ProfileController',
        controllerAs: 'ProfileCtrl',
        resolve: {
          profile: dependecies.profile,
          countries: dependecies.countries
        }
      })
      .state('booking.profile.show', {
        url: '/show',
        templateUrl: '/booking/profile/show.html',
      })
      .state('booking.profile.edit', {
        url: '/edit',
        templateUrl: '/booking/profile/edit.html',
      })


      .state('booking.children', {
        abstract: true,
        url: '/children',
        template: uiView,
        data: {
          tab: 'children'
        },
      })
      .state('booking.children.index', {
        url: '/index',
        templateUrl: '/booking/children/index.html',
        controller: 'ChildrenController',
        controllerAs: 'ChildrenCtrl',
        resolve: {
          children: dependecies.children
        }
      })
      .state('booking.children.create', {
        url: '/create',
        templateUrl: '/booking/children/create.html',
        controller: 'CreateChildController',
        controllerAs: 'CreateChildCtrl',
        resolve: {
          swimOptions: dependecies.swimOptions,
          yearsInSchool: dependecies.yearsInSchool
        }
      })
      .state('booking.children.show', {
        url: '/:childId/show',
        templateUrl: '/booking/children/show.html',
        controller: 'ShowChildController',
        controllerAs: 'ShowChildCtrl',
        resolve: {
          child: dependecies.child
        }
      })
      .state('booking.children.edit', {
        url: '/:childId/edit',
        templateUrl: '/booking/children/edit.html',
        controller: 'EditChildController',
        controllerAs: 'EditChildCtrl',
        resolve: {
          child: dependecies.child,
          swimOptions: dependecies.swimOptions,
          yearsInSchool: dependecies.yearsInSchool
        }
      })

      .state('booking.orders', {
        abstract: true,
        url: '/orders',
        template: uiView,
        data: {
          tab: 'orders'
        },
      })
      /*
       .state('booking.orders.index', {
       url: '/index',
       templateUrl: '/booking/orders/index.html',
       controller: 'OrdersController',
       controllerAs: 'OrdersCtrl',
       resolve: {
       orders: dependecies.orders
       }
       })
       */
      .state('booking.orders.create', {
        url: '/create',
        templateUrl: '/booking/orders/create.html',
        controller: 'CreateOrderController',
        controllerAs: 'CreateOrderCtrl',
        resolve: {
          children: dependecies.children,
          events: dependecies.events,
          countOrders: ['orderService', function (orderService) {
            return orderService.count();
          }]
        }
      })
      /*
       .state('booking.orders.show', {
       url: '/:orderId/show',
       templateUrl: '/booking/orders/show.html',
       controller: 'ShowOrderController',
       controllerAs: 'ShowOrderCtrl',
       resolve: {
       order: dependecies.order,
       }
       })
       */
      /*
       .state('booking.orders.edit', {
       url: '/:orderId/edit',
       templateUrl: '/booking/orders/edit.html',
       controller: 'EditOrderController',
       controllerAs: 'EditOrderCtrl',
       resolve: {
       order: dependecies.order,
       children: dependecies.children,
       events: dependecies.events
       }
       })
       */
      // Showing total info, need select payment type and submit action
      // At the end create Booking and go to All Bookings page(not SPA)
      .state('booking.complete', {
        url: '/complete',
        templateUrl: '/booking/complete/complete.html',
        data: {
          tab: 'complete'
        },
        controller: 'CompleteController',
        controllerAs: 'CompleteCtrl',
        resolve: {
          products: dependecies.products,
          orders: ['orderService', function (orderService) {
            return orderService.all();
          }],
          prices: ['orders', 'bookingService', 'helperService', function(orders, bookingService, helperService) {
            var _orders = helperService.prepareOrders(orders);

            return bookingService.calculate(_orders);
          }],
          ordersWithPrices: ['orders', 'prices', '$filter', function (orders, prices, $filter) {
            var ordersWithPrice = [];

            for (var i = 0, order; (order = orders[i]); i++) {
              order.sum = 0;

              var price = $filter('filter')(prices, {_id: order._id}, true);
              if(price.length && price[0].amount) {
                order.sum = price[0].amount;
                order.discount = price[0].discount;
                order.price_variants = price[0].price_variants;
              }

              order.variants = price[0].variants;

              ordersWithPrice.push(order);
            }

            return ordersWithPrice;
          }],
          paymentSystems: ['bookingService', function (bookingService) {
            return bookingService.paymentSystems();
          }]
        }
      });
  }
})();
