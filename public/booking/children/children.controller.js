(function () {
  'use strict';

  angular
    .module('booking')
    .controller('ChildrenController', ChildrenController);

  ChildrenController.$inject = ['children', 'childService', 'alertService', '$state'];

  /* @ngInject */
  function ChildrenController(children, childService, alertService, $state) {
    /* jshint validthis: true */
    var vm = this;

    vm.children = children;
    vm.confirmedUpdated = false;
    vm.confirmedLegal = false;

    vm.remove = remove;
    vm.next = next;

    ///////////////////

    function next() {
      if (!vm.confirmedUpdated) {
        alertService.error('Please confirm that you have updated all of your children\'s contact and medical information before booking.');

        return;
      }
      if (!vm.confirmedLegal) {
        alertService.error('Please confirm that you are the legal guardian of the children above.');

        return;
      }

      $state.go('booking.orders.create');
    }

    function remove(child) {
      childService.remove(child.id).then(function () {
        alertService.success('The child was removed!');

        vm.children.splice(vm.children.indexOf(child), 1);
      });
    }
  }
})();
