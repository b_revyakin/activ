(function () {
  'use strict';

  angular
    .module('booking')
    .controller('EditChildController', EditChildController);

  EditChildController.$inject = ['child', 'childService', '$state', 'toaster', 'swimOptions', 'yearsInSchool'];

  /* @ngInject */
  function EditChildController(child, childService, $state, toaster, swimOptions, yearsInSchool) {
    /* jshint validthis: true */
    var vm = this;

    vm.swimOptions = swimOptions;
    vm.yearsInSchool = yearsInSchool;

    vm.form = child;

    vm.update = update;

    vm.datePickerOpen = false;

    ////////////

    function update() {
      childService.update(vm.form).then(function () {
        toaster.success('The child was updated!');

        $state.go('^.index');
      });
    }

  }
})();
