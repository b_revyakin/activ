(function () {
  'use strict';

  angular
    .module('booking')
    .controller('ShowChildController', ShowChildController);

  ShowChildController.$inject = ['child'];

  /* @ngInject */
  function ShowChildController(child) {
    /* jshint validthis: true */
    var vm = this;

    vm.child = child;
  }
})();
