(function () {
  'use strict';

  angular
    .module('booking')
    .controller('ConfirmRemoveOrderController', ConfirmRemoveOrderController);

  ConfirmRemoveOrderController.$inject = ['$uibModalInstance'];

  /* @ngInject */
  function ConfirmRemoveOrderController($uibModalInstance) {
    /* jshint validthis: true */
    var vm = this;

    vm.ok = ok;
    vm.cancel = cancel;

    function ok() {
      $uibModalInstance.close();
    }

    function cancel() {
      $uibModalInstance.dismiss('cancel');
    }
  }
})();
