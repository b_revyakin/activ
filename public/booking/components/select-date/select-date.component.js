(function () {
  'use strict';

  angular
    .module('booking')
    .component('selectDate', {
      bindings: {
        date: '=ngModel'
      },
      templateUrl: '/booking/components/select-date/select-date.html',
      controllerAs: 'SelectDateCtrl',
      controller: 'SelectDateController'
    });
})();