(function () {
  'use strict';

  angular
    .module('booking')
    .controller('SelectDateController', SelectDateController);

  /* @ngInject */
  function SelectDateController($filter, months) {
    /* jshint validthis: true */
    var vm = this;

    vm.days = $filter('range')([], 1, 31);
    vm.months = months;
    vm.years = $filter('range')([], moment().get('year') - 100, moment().get('year'), 'desc');

    initDate();

    vm.change = change;

    //////////////////////////////

    function change() {
      if (!vm.day || vm.month === undefined || !vm.year) {
        return;
      }

      while (!moment([vm.year, vm.month, vm.day]).isValid()) {
        --vm.day;
      }

      vm.date = moment([vm.year, vm.month, vm.day]);
    }

    function initDate() {
      var date = moment(vm.date);
      if (date.isValid()) {
        vm.day = parseInt(date.format('D'));
        vm.month = date.month();
        vm.year = date.year();
      }
    }

  }
})();
