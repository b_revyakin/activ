(function(){
  'use strict';

  angular
    .module('booking')
    .filter('currencyLb', currencyLb);

  currencyLb.$inject = ['$filter'];

  function currencyLb($filter) {
    return main;

    function main(value) {
      return $filter('currency')(value, '£');
    }
  }
})();