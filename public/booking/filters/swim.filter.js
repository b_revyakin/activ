(function(){
  'use strict';

  angular
    .module('booking')
    .filter('swim', swim);

  swim.$inject = [];

  function swim() {
    return main;

    function main(child) {
      return child.can_swim ? (child.can_swim.number + ' - '  + child.can_swim.option) : '';
    }
  }
})();