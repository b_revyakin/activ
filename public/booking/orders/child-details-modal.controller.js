(function () {
  'use strict';

  angular
    .module('booking')
    .controller('ChildDetailsModalController', ChildDetailsModalController);

  ChildDetailsModalController.$inject = ['$uibModalInstance', 'child'];

  /* @ngInject */
  function ChildDetailsModalController($uibModalInstance, child) {
    /* jshint validthis: true */
    var vm = this;

    vm.child = child;

    vm.close = close;

    /////////////////////

    function close() {
      $uibModalInstance.close();
    }

  }
})();
