(function () {
  'use strict';

  angular
    .module('booking')
    .controller('EditOrderController', EditOrderController);

  EditOrderController.$inject = ['order', 'children', 'events', 'orderService', '$state', 'alertService', 'helperService'];

  /* @ngInject */
  function EditOrderController(order, children, events, orderService, $state, alertService, helperService) {
    /* jshint validthis: true */
    var vm = this;

    vm.form = angular.copy(order);
    vm.children = children;
    vm.events = events;

    vm.update = update;
    vm.selectedEvent = selectedEvent;

    ///////////////////////

    function update() {
      if(helperService.ShowValidationErrors(orderService.validate(vm.form)).length) {
        return;
      }

      orderService.update(vm.form._id, vm.form).then(function () {
        alertService.success('The order was updated!');

        $state.go('^.index');
      });
    }

    function selectedEvent() {
      helperService.pushEventDates(vm.events, vm.form);
    }


  }
})();
