(function () {
  'use strict';

  angular
    .module('booking')
    .controller('EventInfoController', EventInfoController);

  EventInfoController.$inject = ['$uibModalInstance', 'event'];

  /* @ngInject */
  function EventInfoController($uibModalInstance, event) {
    /* jshint validthis: true */
    var vm = this;

    vm.event = event;

    vm.confirm = confirm;
    vm.close = close;

    //////////

    function confirm() {
      $uibModalInstance.close();
    }

    function close() {
      $uibModalInstance.dismiss();
    }
  }
})();
