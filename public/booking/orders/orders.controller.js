(function () {
  'use strict';

  angular
    .module('booking')
    .controller('OrdersController', OrdersController);

  OrdersController.$inject = ['orders', 'orderService', 'toaster'];

  /* @ngInject */
  function OrdersController(orders, orderService, toaster) {
    /* jshint validthis: true */
    var vm = this;

    vm.orders = orders;

    vm.remove = remove;

    //////////////////////////

    function remove(id) {
      orderService.remove(id).then(function () {
        //Refresh data
        orderService.all().then(function (orders) {
          vm.orders = orders;

          toaster.success('The order was removed');
        });
      });
    }

  }
})();
