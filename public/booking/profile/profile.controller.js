(function () {
  'use strict';

  angular
    .module('booking')
    .controller('ProfileController', ProfileController);

  ProfileController.$inject = ['profile', 'profileService', '$state', 'toaster', 'accosts', 'booleanValues', 'countries', '$filter'];

  /* @ngInject */
  function ProfileController(profile, profileService, $state, toaster, accosts, booleanValues, countries, $filter) {
    /* jshint validthis: true */
    var vm = this;

    vm.profile = angular.copy(profile);
    vm.form = angular.copy(vm.profile);
    vm.accosts = accosts;
    vm.booleanValues = booleanValues;
    vm.countries = countries;
    vm.request = false;

    vm.update = update;
    vm.reset = reset;

    ////////////////////////////////

    function update() {
      var form = angular.copy(vm.form);
      form.country_id = form.country.id;

      vm.request = true;

      profileService.update(form).then(function () {
        vm.profile = vm.form;

        toaster.success('Profile updated!');

        $state.go('^.show');
      }).finally(function () {
        vm.request = false;
      });
    }

    function reset() {
      vm.form = angular.copy(vm.profile);
    }
  }
})();
