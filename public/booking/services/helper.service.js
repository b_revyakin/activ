(function () {
  'use strict';

  angular
    .module('booking')
    .service('helperService', helperService);

  /* @ngInject */
  function helperService($filter, alertService, store) {
    /* jshint validthis: true */
    var vm = this;

    vm.fetchResponse = fetchResponse;
    vm.pushEventDates = pushEventDates;
    vm.ShowValidationErrors = ShowValidationErrors;
    vm.redirect = redirect;
    vm.prepareOrders = prepareOrders;
    vm.prepareProducts = prepareProducts;
    vm.prepareBookingForRequest = prepareBookingForRequest;
    vm.prepareDatesForRequest = prepareDatesForRequest;

    /////////////////////////

    function prepareOrders(orders)
    {
      var _orders = [];

      for (var i = 0, order; (order = orders[i]); i++) {
        var selectedDates = [];
        for (var j = 0, week; (week = order.weeks[j]); j++) {
          for (var k = 0, day; (day = week.dates[k]); k++) {
            if (day.selected) {
              var date = new Date(day.date);
              if(! (date.getMinutes() || date.getHours())) {
                date.setMinutes(date.getMinutes() - date.getTimezoneOffset());
              }

              selectedDates.push(date);
            }
          }
        }

        _orders.push({
          _id: order._id,
          child_id: order.child_id,
          event_id: order.event_id,
          selectedDates: selectedDates,
          variants: order.variants
        });
      }

      return _orders;
    }

    function redirect(url, method, fields) {
      $('<form>', {
        method: method,
        action: url
      }).append(fields).appendTo('body').submit();
    }

    function fetchResponse(data) {
      return data.data;
    }

    function ShowValidationErrors(errors) {
      if (errors.length) {
        for (var i = 0, error; (error = errors[i]); i++) {
          alertService.error(error.message);
        }
      }

      return errors;
    }

    function existsDay(dates, date) {
      date.setHours(0);
      date.setMinutes(0);

      return $filter('filter')(dates, function (day) {
        return moment(date).diff(moment(day.date)) === 0;
      }).length;
    }

    function findEvent(events, eventId) {
      for(var i = 0, event; (event = events[i]); i++) {
        if(event.id === eventId) {
          return angular.copy(event);
        }
      }
    }

    function pushEventDates(events, form) {
      //Preparation for edit order
      /*if (form.weeks) {
       var selectedDates = [];

       for (var k = 0, previousWeek; (previousWeek = form.weeks[k]); k++) {
       for (var p = 0, day; (day = previousWeek.dates[p]); p++) {
       if (day.selected) {
       selectedDates.push(day);
       }
       }
       }
       }*/

      //Find the event
      var event = findEvent(events, form.event_id);

      form.event = event;
      form.weeks = [];

      var startDate = angular.copy(event.start_date);

      // Remove saturday and sunday from period if from this start. Sunday is 0
      if (startDate.weekday() === 0) {
        startDate.add(1, 'd');
      } else if (startDate.weekday() === 6) {
        startDate.add(2, 'd');
      }


      for (var startWeek = startDate, mondayOfWeek = new Date(startDate); (startWeek <= event.end_date); startWeek = new Date((new Date(mondayOfWeek)).setDate((new Date(mondayOfWeek)).getDate() + 7)), startDate = new Date(startWeek)) {
        // Get difference between real start event and monday (1 - because Monday is 1)
        var difference = moment(startWeek).weekday() - 1;

        //Get monday of week
        mondayOfWeek = new Date(startWeek);
        mondayOfWeek.setDate(mondayOfWeek.getDate() - difference);

        //Get end of week(now friday)
        var fridayOfWeek = new Date(mondayOfWeek);
        fridayOfWeek.setDate(fridayOfWeek.getDate() + 4);
        var endOfWeek = new Date(fridayOfWeek);

        var week = {
          monday: new Date(mondayOfWeek),
          friday: new Date(endOfWeek),
          selected: false,
          allow: true,//Allow select full week or not. Must be 'false' if at least one weekday is fully
          fully: true,
          dates: [],
          selectedAnyDates: false
        };

        //If event finish before friday
        if (endOfWeek > event.end_date) {
          endOfWeek = event.end_date;
        }

        if (mondayOfWeek < startDate) {
          for (var tDate = new Date(mondayOfWeek); tDate < startDate; tDate.setDate(tDate.getDate() + 1)) {
            week.dates.push({
              date: angular.copy(tDate),
              allow: false,
              selected: false,
              fully: false
            });
          }
        }

        for (var date = new Date(startDate); date <= endOfWeek; date.setDate(date.getDate() + 1)) {
          /*jshint -W083 */
          var except = $filter('filter')(event.exception_dates, (function (exceptionDate) {
              exceptionDate = moment(exceptionDate.date).toDate();
              return exceptionDate.toDateString() === date.toDateString();
            })),
            allow = (except.length > 0) ? false : true;

          week.dates.push({
            date: angular.copy(date),
            allow: allow,
            selected: false,
            fully: allow ? existsDay(form.event.busy_dates, date) : false
          });
        }

        if (fridayOfWeek > endOfWeek) {
          for (date = new Date(endOfWeek), date.setDate(date.getDate() + 1); date <= fridayOfWeek; date.setDate(date.getDate() + 1)) {
            week.dates.push({
              date: angular.copy(date),
              allow: false,
              selected: false,
              fully: false
            });
          }
        }

        for(var k = 0, dayOfWeek; (dayOfWeek = week.dates[k]); k++) {
          //Ha-ha what is it?!
          if(dayOfWeek.allow && !dayOfWeek.fully) {
            week.fully = false;
          }

          if(dayOfWeek.allow && dayOfWeek.fully) {
            week.allow = false;
          }
        }

        form.weeks.push(week);
      }
    }

    function prepareProducts(products) {
      var localProducts = store.get('products');

      return angular.forEach(products, function (product) {
        product.imagePath = product.image_thumbnail ? '/' + product.image_thumbnail.path : '';

        if (localProducts) {
          var localProduct = $filter('filter')(localProducts, {id: product.id})[0];

          if (localProduct) {
            product.selectedVariant = localProduct.selectedVariant;
            product.selectedQuantity = localProduct.selectedQuantity;
          }
        }

        product.price = 0;

        var variant = product.variants.filter(function (variant) {
          return product.selectedVariant && product.selectedVariant === variant.id;
        })[0];

        if (variant && product.selectedQuantity) {
          product.price = variant.price * product.selectedQuantity;
        }
      });
    }

    function prepareBookingForRequest(_data) {
      var data = angular.copy(_data);

      data.variants = [];

      angular.forEach(data.products, function (product) {
        if (product.selectedVariant && product.selectedQuantity) {
          data.variants.push({
            id: product.selectedVariant,
            count: product.selectedQuantity
          });
        }
      });

      delete data.products;

      return data;
    }

    function prepareDatesForRequest(object, properties, needClone) {
      var localObject = needClone ? angular.copy(object) : object;

      properties.forEach(function (property) {
        localObject[property] = moment(localObject[property]).format('YYYY-MM-DD');
      });

      return localObject;
    }
  }
})();
