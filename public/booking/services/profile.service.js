(function () {
  'use strict';

  angular
    .module('booking')
    .service('profileService', profile);

  profile.$inject = ['$http', 'helperService'];

  /* @ngInject */
  function profile($http, helperService) {
    /* jshint validthis: true */
    var vm = this;

    vm.get = get;
    vm.update = update;
    vm.countries = countries;

    ////////////////////////////

    function get() {
      return $http.get('/api/profile')
        .then(helperService.fetchResponse)
        .then(function (data) {
          data.email = data.user.email;
          return data;
        });
    }

    function update(data) {
      return $http.put('/api/profile', data)
        .then(helperService.fetchResponse)
        .then(function (data) {
          data.email = data.user.email;
          return data;
        });
    }

    function countries() {
      return $http.get('/api/countries').then(helperService.fetchResponse);
    }
  }
})();
