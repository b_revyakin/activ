(function () {
  'use strict';

  angular.module('booking', [
    'ngResource',
    'ui.router',
    'toaster',
    'angular-storage',
    'ui.bootstrap',
    'ui.select',
    'ngSanitize'
  ]);

})();

(function () {
  'use strict';

  angular
    .module('booking')
    .config(config);

  config.$inject = ['$httpProvider'];

  function config($httpProvider) {

    $httpProvider.interceptors.push('slInterceptor');
  }

})();

(function () {

  'use strict';

  angular
    .module('booking')
    .constant('accosts', ['Mr', 'Miss', 'Mrs', 'Ms', 'Dr'])
    .constant('booleanValues', [
      {
        title: 'Yes',
        value: 1
      },
      {
        title: 'No',
        value: 0
      }
    ])
    .constant('paymentTypes', [
      {
        title: 'Pay Online by Credit / Debit Card',
        value: 'online'
      },
      {
        title: 'Using childcare vouchers',
        value: 'voucher'
      },
      //{
      //  title: 'By cheque',
      //  value: 'cheque'
      //}
    ]);

})();

(function () {
  'use strict';

  angular
    .module('booking')
    .factory('slInterceptor', slInterceptor);

  slInterceptor.$inject = ['alertService', '$q'];

  function slInterceptor(alertService, $q) {
    return {
      responseError: function (res) {
        var status = res.status,
          fetchedResponse = res.data;

        if (fetchedResponse === 'Unauthorized.') {
          alertService.error('Your session was expired. Please login again.');

          window.location.href = '/login';
        } else if (fetchedResponse.length === undefined || fetchedResponse.length < 256) {
          alertService.showResponseError(fetchedResponse, titleByCodeStatus(status));
        } else {
          alertService.showResponseError(['Server Error'], titleByCodeStatus(status));
        }

        return $q.reject(fetchedResponse);
      }
    };

    function titleByCodeStatus(status) {
      var title;

      switch (status) {
        case 422:
          title = 'Validation Fail!';
          break;
        case 403:
          title = 'Access Denied!';
          break;
        case 500:
          title = 'Sorry. Server Error.';
          break;
      }

      return title;
    }
  }
})();

(function () {
  'use strict';

  angular
    .module('booking')
    .config(config);

  config.$inject = ['$stateProvider', '$urlRouterProvider'];

  function config($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise('/profile/show');

    var uiView = '<ui-view></ui-view>';

    var dependecies = {
      orders: ['orderService', function (orderService) {
        return orderService.all();
      }],
      order: ['orderService', '$stateParams', function (orderService, $stateParams) {
        return orderService.get($stateParams.orderId);
      }],
      children: ['childService', function (childService) {
        return childService.all();
      }],
      child: ['childService', '$stateParams', function (childService, $stateParams) {
        return childService.get($stateParams.childId);
      }],
      events: ['eventService', function (eventService) {
        return eventService.all();
      }],
      swimOptions: ['childService', function (childService) {
        return childService.swimOptions();
      }],
      yearsInSchool: ['childService', function (childService) {
        return childService.yearsInSchool();
      }],
      products: ['orderService', function (orderService) {
        return orderService.products();
      }],
      countries: ['profileService', function (profileService) {
        return profileService.countries();
      }],
      profile: ['profileService', function (profileService) {
        return profileService.get();
      }]
    };

    $stateProvider
      .state('booking', {
        abstract: true,
        templateUrl: '/admin-booking/template.html'
      })


      .state('booking.profile', {
        abstract: true,
        url: '/profile',
        data: {
          tab: 'profile'
        },
        template: uiView,
        controller: 'ProfileController',
        controllerAs: 'ProfileCtrl',
        resolve: {
          profile: dependecies.profile,
          countries: dependecies.countries
        }
      })
      .state('booking.profile.show', {
        url: '/show',
        templateUrl: '/admin-booking/profile/show.html',
      })
      .state('booking.profile.edit', {
        url: '/edit',
        templateUrl: '/admin-booking/profile/edit.html',
      })


      .state('booking.children', {
        abstract: true,
        url: '/children',
        template: uiView,
        data: {
          tab: 'children'
        },
      })
      .state('booking.children.index', {
        url: '/index',
        templateUrl: '/admin-booking/children/index.html',
        controller: 'ChildrenController',
        controllerAs: 'ChildrenCtrl',
        resolve: {
          children: dependecies.children
        }
      })
      .state('booking.children.create', {
        url: '/create',
        templateUrl: '/admin-booking/children/create.html',
        controller: 'CreateChildController',
        controllerAs: 'CreateChildCtrl',
        resolve: {
          swimOptions: dependecies.swimOptions,
          yearsInSchool: dependecies.yearsInSchool
        }
      })
      .state('booking.children.show', {
        url: '/:childId/show',
        templateUrl: '/admin-booking/children/show.html',
        controller: 'ShowChildController',
        controllerAs: 'ShowChildCtrl',
        resolve: {
          child: dependecies.child
        }
      })
      .state('booking.children.edit', {
        url: '/:childId/edit',
        templateUrl: '/admin-booking/children/edit.html',
        controller: 'EditChildController',
        controllerAs: 'EditChildCtrl',
        resolve: {
          child: dependecies.child,
          swimOptions: dependecies.swimOptions,
          yearsInSchool: dependecies.yearsInSchool
        }
      })

      .state('booking.orders', {
        abstract: true,
        url: '/orders',
        template: uiView,
        data: {
          tab: 'orders'
        },
      })
      /*
       .state('booking.orders.index', {
       url: '/index',
       templateUrl: '/admin-booking/orders/index.html',
       controller: 'OrdersController',
       controllerAs: 'OrdersCtrl',
       resolve: {
       orders: dependecies.orders
       }
       })
       */
      .state('booking.orders.create', {
        url: '/create',
        templateUrl: '/admin-booking/orders/create.html',
        controller: 'CreateOrderController',
        controllerAs: 'CreateOrderCtrl',
        resolve: {
          children: dependecies.children,
          events: dependecies.events,
          products: dependecies.products,
          countOrders: ['orderService', function (orderService) {
            return orderService.count();
          }]
        }
      })
      /*
       .state('booking.orders.show', {
       url: '/:orderId/show',
       templateUrl: '/admin-booking/orders/show.html',
       controller: 'ShowOrderController',
       controllerAs: 'ShowOrderCtrl',
       resolve: {
       order: dependecies.order,
       }
       })
       */
      /*
       .state('booking.orders.edit', {
       url: '/:orderId/edit',
       templateUrl: '/admin-booking/orders/edit.html',
       controller: 'EditOrderController',
       controllerAs: 'EditOrderCtrl',
       resolve: {
       order: dependecies.order,
       children: dependecies.children,
       events: dependecies.events
       }
       })
       */
      // Showing total info, need select payment type and submit action
      // At the end create Booking and go to All Bookings page(not SPA)
      .state('booking.complete', {
        url: '/complete',
        templateUrl: '/admin-booking/complete/complete.html',
        data: {
          tab: 'complete'
        },
        controller: 'CompleteController',
        controllerAs: 'CompleteCtrl',
        resolve: {
          products: dependecies.products,
          orders: ['orderService', function (orderService) {
            return orderService.all();
          }],
          prices: ['orders', 'bookingService', 'helperService', function (orders, bookingService, helperService) {
            var _orders = helperService.prepareOrders(orders);

            return bookingService.calculate(_orders);
          }],
          ordersWithPrices: ['orders', 'prices', '$filter', function (orders, prices, $filter) {
            var ordersWithPrice = [];

            for (var i = 0, order; (order = orders[i]); i++) {
              order.sum = 0;

              var price = $filter('filter')(prices, {_id: order._id}, true);
              if (price.length && price[0].amount) {
                order.sum = price[0].amount;
                order.discount = price[0].discount;
                order.price_variants = price[0].price_variants;
              }

              order.variants = price[0].variants;

              ordersWithPrice.push(order);
            }

            return ordersWithPrice;
          }]
        }
      });
  }
})();

(function () {
  'use strict';

  angular
    .module('booking')
    .run(run);

  run.$inject = ['$state', '$rootScope', 'orderService'];

  function run($state, $rootScope, orderService) {

    $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
      var preventEvent = false,
        stateName;

      if (toState.name === 'booking.complete' && !orderService.count()) {
        preventEvent = true;
        stateName = 'booking.orders.create';
      }

      if (preventEvent) {
        event.preventDefault();
        if (stateName) {
          $state.go(stateName);
        }
      } else {
        $rootScope.tab = toState.data.tab;
      }

    });
  }
})();

(function () {
  'use strict';

  angular
    .module('booking')
    .controller('BookingController', BookingController);

  BookingController.$inject = ['$state'];

  /* @ngInject */
  function BookingController($state) {
    /* jshint validthis: true */
    var vm = this;

    var tab = $state.current.data.tab;
  }
})();

(function () {
  'use strict';

  angular
    .module('booking')
    .controller('ChildrenController', ChildrenController);

  ChildrenController.$inject = ['children', 'childService', 'alertService', '$state'];

  /* @ngInject */
  function ChildrenController(children, childService, alertService, $state) {
    /* jshint validthis: true */
    var vm = this;

    vm.children = children;
    vm.confirmedUpdated = false;
    vm.confirmedLegal = false;

    vm.remove = remove;
    vm.next = next;

    ///////////////////

    function next() {
      if (!vm.confirmedUpdated) {
        alertService.error('Please confirm that you have updated all of your children\'s contact and medical information before booking.');

        return;
      }
      if (!vm.confirmedLegal) {
        alertService.error('Please confirm that you are the legal guardian of the children above.');

        return;
      }

      $state.go('booking.orders.create');
    }

    function remove(child) {
      childService.remove(child.id).then(function () {
        alertService.success('The child was removed!');

        vm.children.splice(vm.children.indexOf(child), 1);
      });
    }
  }
})();

(function () {
  'use strict';

  angular
    .module('booking')
    .controller('CreateChildController', CreateChildController);

  CreateChildController.$inject = ['childService', '$state', 'toaster', 'swimOptions', 'yearsInSchool'];

  /* @ngInject */
  function CreateChildController(childService, $state, toaster, swimOptions, yearsInSchool) {
    /* jshint validthis: true */
    var vm = this;

    vm.swimOptions = swimOptions;
    vm.yearsInSchool = yearsInSchool;

    vm.form = {
      first_name: undefined,
      last_name: undefined,
      email: undefined,
      birthday: undefined,
      sex: undefined,
      relationship: undefined,
      school: undefined,
      year_in_school: undefined,
      doctor_name: undefined,
      doctor_telephone_number: undefined,
      medical_advice_and_treatment: undefined,
      epi_pen: undefined,
      medication: undefined,
      special_requirements: undefined,
      can_swim: undefined,
      emergency_contact_number: undefined,
    };

    vm.create = create;

    vm.datePickerOpen = false;

    //////////////////////////////

    function create() {
      childService.store(vm.form).then(function () {
        toaster.success('The child was created!');

        $state.go('^.index');
      });
    }

  }
})();

(function () {
  'use strict';

  angular
    .module('booking')
    .controller('EditChildController', EditChildController);

  EditChildController.$inject = ['child', 'childService', '$state', 'toaster', 'swimOptions', 'yearsInSchool'];

  /* @ngInject */
  function EditChildController(child, childService, $state, toaster, swimOptions, yearsInSchool) {
    /* jshint validthis: true */
    var vm = this;

    vm.swimOptions = swimOptions;
    vm.yearsInSchool = yearsInSchool;

    vm.form = child;

    vm.update = update;

    vm.datePickerOpen = false;

    ////////////

    function update() {
      childService.update(vm.form).then(function () {
        toaster.success('The child was updated!');

        $state.go('^.index');
      });
    }

  }
})();

(function () {
  'use strict';

  angular
    .module('booking')
    .controller('ShowChildController', ShowChildController);

  ShowChildController.$inject = ['child'];

  /* @ngInject */
  function ShowChildController(child) {
    /* jshint validthis: true */
    var vm = this;

    vm.child = child;
  }
})();

(function () {
  'use strict';

  CompleteController.$inject = ["ordersWithPrices", "store", "bookingService", "paymentTypes", "alertService", "orderService", "helperService", "$filter", "$state", "$uibModal", "products"];
  angular
    .module('booking')
    .controller('CompleteController', CompleteController);

  /* @ngInject */
  function CompleteController(ordersWithPrices, store, bookingService, paymentTypes, alertService, orderService, helperService, $filter, $state, $uibModal, products) {
    /* jshint validthis: true */
    var vm = this;

    vm.orders = ordersWithPrices;
    vm.orders.sum = $filter('sumByKey')(vm.orders, 'sum');
    vm.orders.discount = $filter('sumByKey')(vm.orders, 'discount');
    vm.sum_variants = $filter('sumByKey')(products, 'price');
    vm.orders.coupon = 0;
    vm.products = products;

    vm.paymentTypes = paymentTypes;
    vm.couponApplied = false;

    vm.form = {
      payment_type: undefined,
      orders: [],
      coupon: undefined,
    };
    vm.agreed = false;
    vm.request = false;

    prepareProducts();
    calculateTotalPrice();
    setOrdersToBookingForm();
    addToOrderProductsIfStored();

    vm.book = book;
    vm.remove = remove;
    vm.applyCoupon = applyCoupon;
    vm.cancelCoupon = cancelCoupon;
    vm.calculateProductPrice = calculateProductPrice;
    vm.saveProducts = saveProducts;

    ////////////////////////////////

    function applyCoupon() {
      if (!vm.form.coupon) {
        return;
      }

      vm.request = true;

      bookingService.checkCoupon(vm.form.coupon).then(function (persent) {
        vm.orders.couponPersent = persent;
        calculateTotalPrice();
        alertService.success('Your coupon is activated.');
        vm.couponApplied = true;
      }, function () {
        alertService.error('Coupon not exists or expired.');
      }).finally(function () {
        vm.request = false;
      });
    }

    function cancelCoupon() {
      //Clear data about previous coupon
      vm.form.coupon = '';
      vm.orders.couponPersent = undefined;
      vm.orders.coupon = 0;
      vm.couponApplied = false;

      calculateTotalPrice();

      alertService.success('You coupon was deactivated.');
    }

    function remove(id) {
      var modalInstance = $uibModal.open({
        templateUrl: '/booking/complete/confirm-remove-modal.html',
        controller: 'ConfirmRemoveOrderController',
        controllerAs: 'ConfirmRemoveCtrl',
        size: 'md'
      });

      modalInstance.result.then(function () {
        orderService.remove(id).then(function () {
          //Refresh data
          orderService.all().then(function (orders) {
            alertService.success('The order was removed');

            if (!orders.length) {
              $state.go('booking.orders.create');
            }

            var _orders = helperService.prepareOrders(orders);
            bookingService.calculate(_orders).then(function (prices) {
              var ordersWithPrice = [];

              for (var i = 0, order; (order = orders[i]); i++) {
                order.sum = 0;

                var price = $filter('filter')(prices, {_id: order._id}, true);
                if (price.length && price[0].amount) {
                  order.sum = price[0].amount;
                  order.discount = price[0].discount;
                }
                ordersWithPrice.push(order);
              }

              vm.orders = ordersWithPrice;
              vm.orders.sum = $filter('sumByKey')(vm.orders, 'sum');
              vm.orders.discount = $filter('sumByKey')(vm.orders, 'discount');
              vm.orders.coupon = 0;
              calculateTotalPrice();
            });
          });
        });
      });
    }

    function book() {
      if (!vm.agreed) {
        alertService.error('You should with booking\'s terms and conditions');
        return;
      }

      vm.request = true;

      return bookingService.book(vm.form).then(function (data) {
        alertService.success('You booked!');

        //Clear current orders
        orderService.clear();

        helperService.redirect(data.redirect, data.method, data.fields);
      }, function () {
        vm.request = false;
      });
    }

    function setOrdersToBookingForm() {
      vm.form.orders = helperService.prepareOrders(vm.orders);
    }

    function calculateTotalPrice() {
      if (vm.form.coupon && vm.orders.couponPersent) {
        vm.orders.coupon = vm.orders.sum * vm.orders.couponPersent / 100;
        vm.orders.total = vm.orders.sum - vm.orders.coupon;
      } else {
        vm.orders.total = vm.orders.sum - vm.orders.discount;
      }
      vm.orders.total = vm.orders.total + vm.sum_variants;
    }

    function prepareProducts() {
      angular.forEach(vm.products, function (product) {
        if (!product.selectedQuantity) {
          product.selectedQuantity = 1;
        }

        if (!product.selectedVariant) {
          product.selectedVariant = product.variants.length ? product.variants[0].id : 0;
        }

        Object.defineProperty(product, 'price', {
          get: function () {
            var price = 0;

            var selectedVariant = this.selectedVariant;
            var variant = this.variants.filter(function (variant) {
              return selectedVariant && selectedVariant === variant.id;
            })[0];

            if (variant && this.selectedQuantity) {
              price = variant.price * this.selectedQuantity;
            }

            return price;
          }
        });
        Object.defineProperty(product, 'maxCount', {
          get: function () {
            var countMax = 0;

            var selectedVariant = this.selectedVariant;
            var variant = this.variants.filter(function (variant) {
              return selectedVariant && selectedVariant === variant.id;
            })[0];

            if (variant && this.selectedQuantity) {
              countMax = variant.count;
            }

            return countMax;
          }
        });

        calculateProductPrice(product, true);
      });
    }

    function calculateProductPrice(product, notShowChangeProducts) {
      vm.changedProducts = !notShowChangeProducts;
      //product.price = 0;
      //product.maxCount = undefined;

      var variant = product.variants.filter(function (variant) {
        return product.selectedVariant && product.selectedVariant === variant.id;
      })[0];

      if (variant && product.selectedQuantity) {
        //product.price = variant.price * product.selectedQuantity;
        //product.maxCount = variant.count;
      }
    }

    function saveProducts() {
      var products = vm.products.filter(function (product) {
        return product.price;
      });

      orderService.saveProducts(products).then(function () {
        alertService.success('Products saved');

        vm.form.products = products;

        vm.sum_variants = $filter('sumByKey')(products, 'price');

        calculateTotalPrice();

        vm.changedProducts = false;
      });
    }

    function addToOrderProductsIfStored() {
      var products = store.get('products');

      if (!products || !products.length) {
        return false;
      }

      vm.form.products = products;
    }

  }
})();

(function () {
  'use strict';

  angular
    .module('booking')
    .controller('ConfirmRemoveOrderController', ConfirmRemoveOrderController);

  ConfirmRemoveOrderController.$inject = ['$uibModalInstance'];

  /* @ngInject */
  function ConfirmRemoveOrderController($uibModalInstance) {
    /* jshint validthis: true */
    var vm = this;

    vm.ok = ok;
    vm.cancel = cancel;

    function ok() {
      $uibModalInstance.close();
    }

    function cancel() {
      $uibModalInstance.dismiss('cancel');
    }
  }
})();

(function () {
  'use strict';

  angular
    .module('booking')
    .filter('age', age);

  age.$inject = [];

  function age() {
    return main;

    function main(person, date) {
      date = date ? new Date(date) : new Date();

      var birthday = new Date(person.birthday);
      var ageDifMs = date.getTime() - birthday.getTime();
      var ageDate = new Date(ageDifMs); // miliseconds from epoch
      return Math.abs(ageDate.getFullYear() - 1970);
    }
  }
})();
(function () {
  'use strict';

  angular
    .module('booking')
    .filter('currencyLb', currencyLb);

  currencyLb.$inject = ['$filter'];

  function currencyLb($filter) {
    return main;

    function main(value) {
      return $filter('currency')(value, '£');
    }
  }
})();
(function () {
  'use strict';

  angular
    .module('booking')
    .filter('event', event);

  event.$inject = [];

  function event() {
    return main;

    function main(event) {
      return event.venue.name + ' - ' + event.name;
    }
  }
})();
(function () {
  'use strict';

  angular
    .module('booking')
    .filter('logical', logical);

  logical.$inject = [];

  function logical() {
    return main;

    function main(value) {
      return value ? 'Yes' : 'No';
    }
  }
})();
(function () {
  'use strict';

  angular
    .module('booking')
    .filter('profile', profile);

  profile.$inject = [];

  function profile() {
    return main;

    function main(user) {
      return user.first_name + ' ' + user.last_name;
    }
  }
})();
(function () {
  'use strict';

  angular
    .module('booking')
    .filter('sex', sex);

  sex.$inject = [];

  function sex() {
    return main;

    function main(value) {
      return value ? 'Male' : 'Female';
    }
  }
})();
(function () {
  'use strict';

  angular
    .module('booking')
    .filter('sumByKey', function () {
      return function (data, key) {
        if (typeof(data) === 'undefined' || typeof(key) === 'undefined') {
          return 0;
        }

        var sum = 0;
        for (var i = data.length - 1; i >= 0; i--) {
          sum += parseFloat(data[i][key]);
        }

        return sum;
      };
    });

})();

(function () {
  'use strict';

  angular
    .module('booking')
    .filter('swim', swim);

  swim.$inject = [];

  function swim() {
    return main;

    function main(child) {
      return child.can_swim ? (child.can_swim.number + ' - ' + child.can_swim.option) : '';
    }
  }
})();
(function () {
  'use strict';

  angular
    .module('booking')
    .filter('weekday', weekday);

  weekday.$inject = [];

  function weekday() {
    return main;

    function main(date) {
      if (!(date instanceof Date)) {
        date = new Date(date);
      }

      var weekday;

      switch (date.getDay()) {
        case 0:
          weekday = 'Sunday';
          break;
        case 1:
          weekday = 'Monday';
          break;
        case 2:
          weekday = 'Tuesday';
          break;
        case 3:
          weekday = 'Wednesday';
          break;
        case 4:
          weekday = 'Thursday';
          break;
        case 5:
          weekday = 'Friday';
          break;
        case 6:
          weekday = 'Saturday';
          break;

      }

      return weekday;
    }
  }
})();
(function () {
  'use strict';

  angular
    .module('booking')
    .controller('CreateOrderController', CreateOrderController);

  CreateOrderController.$inject = ['children', 'events', 'orderService', '$state', 'alertService', 'helperService', '$filter', 'products', 'countOrders'];

  /* @ngInject */
  function CreateOrderController(children, events, orderService, $state, alertService, helperService, $filter, products, countOrders) {
    /* jshint validthis: true */
    var vm = this;

    if (!children.length) {
      alertService.error('Please add your children!');
      $state.go('booking.children.create');
    }

    vm.countOrders = countOrders;
    vm.children = children;
    vm.products = products;
    vm.events = [];//events;

    vm.request = false;

    vm.form = {
      child_id: undefined,
      event_id: undefined,
      dates: [],
      products: []
    };
    vm.selectedProduct = undefined;

    vm.create = create;
    vm.selectedEvent = selectedEvent;
    vm.selectedWeek = selectedWeek;
    vm.selectedDay = selectedDay;
    vm.selectedChild = selectedChild;
    vm.addProduct = addProduct;

    ///////////////////////

    function addProduct() {

    }

    function selectedChild() {
      var child = $filter('filter')(children, {id: vm.form.child_id})[0];

      vm.events = $filter('filter')(events, function (event) {
        var age = $filter('age')(child, event.start_date);
        return event.min_age <= age && age <= event.max_age;
      });
    }

    function create() {
      if (helperService.ShowValidationErrors(orderService.validate(vm.form, children, events)).length) {
        return;
      }

      vm.request = true;

      orderService.add(vm.form).then(function () {
        alertService.success('Order was saved');

        $state.go('^.^.complete');
      }).finally(function () {
        //vm.request = false;
      });
    }

    function selectedEvent() {
      if (vm.form.event_id) {
        helperService.pushEventDates(vm.events, vm.form);
      }
    }

    function selectedWeek(week) {
      if (week.selected) {
        setSelectedDatesOfWeek(week, true);
        week.selectedAnyDates = true;
      } else {
        week.selectedAnyDates = false;
        setSelectedDatesOfWeek(week, false);
      }
    }

    function selectedDay(week, day) {
      if (!day.selected && week.selected) {
        week.selected = false;
      }

      if (day.selected) {
        week.selectedAnyDates = true;
        var allDaysOfWeekSelected = true;

        for (var i = 0, weekday; (weekday = week.dates[i]); i++) {
          if (weekday.allow && !weekday.selected) {
            allDaysOfWeekSelected = false;
            break;
          }
        }

        if (allDaysOfWeekSelected) {
          week.selected = true;
        }
      } else {
        //Check that any day is selected
        var selectedAnyDate = false;
        for (var j = 0, date; (date = week.dates[j]); j++) {
          if (date.selected) {
            selectedAnyDate = true;
            break;
          }
        }

        week.selectedAnyDates = selectedAnyDate;
      }
    }

    function setSelectedDatesOfWeek(week, value) {
      for (var i = 0, day; (day = week.dates[i]); i++) {
        if (day.allow) {
          day.selected = value;
        }
      }
    }

  }
})();

(function () {
  'use strict';

  angular
    .module('booking')
    .controller('EditOrderController', EditOrderController);

  EditOrderController.$inject = ['order', 'children', 'events', 'orderService', '$state', 'alertService', 'helperService'];

  /* @ngInject */
  function EditOrderController(order, children, events, orderService, $state, alertService, helperService) {
    /* jshint validthis: true */
    var vm = this;

    vm.form = angular.copy(order);
    vm.children = children;
    vm.events = events;

    vm.update = update;
    vm.selectedEvent = selectedEvent;

    ///////////////////////

    function update() {
      if (helperService.ShowValidationErrors(orderService.validate(vm.form)).length) {
        return;
      }

      orderService.update(vm.form._id, vm.form).then(function () {
        alertService.success('The order was updated!');

        $state.go('^.index');
      });
    }

    function selectedEvent() {
      helperService.pushEventDates(vm.events, vm.form);
    }


  }
})();

(function () {
  'use strict';

  angular
    .module('booking')
    .controller('OrdersController', OrdersController);

  OrdersController.$inject = ['orders', 'orderService', 'toaster'];

  /* @ngInject */
  function OrdersController(orders, orderService, toaster) {
    /* jshint validthis: true */
    var vm = this;

    vm.orders = orders;

    vm.remove = remove;

    //////////////////////////

    function remove(id) {
      orderService.remove(id).then(function () {
        //Refresh data
        orderService.all().then(function (orders) {
          vm.orders = orders;

          toaster.success('The order was removed');
        });
      });
    }

  }
})();

(function () {
  'use strict';

  angular
    .module('booking')
    .controller('ShowOrderController', ShowOrderController);

  ShowOrderController.$inject = ['order'];

  /* @ngInject */
  function ShowOrderController(order) {
    /* jshint validthis: true */
    var vm = this;

    vm.order = order;
  }
})();

(function () {
  'use strict';

  angular
    .module('booking')
    .controller('ProfileController', ProfileController);

  ProfileController.$inject = ['profile', 'profileService', '$state', 'toaster', 'accosts', 'booleanValues', 'countries', '$filter'];

  /* @ngInject */
  function ProfileController(profile, profileService, $state, toaster, accosts, booleanValues, countries, $filter) {
    /* jshint validthis: true */
    var vm = this;

    vm.profile = angular.copy(profile);
    vm.form = angular.copy(vm.profile);
    vm.accosts = accosts;
    vm.booleanValues = booleanValues;
    vm.countries = countries;
    vm.request = false;

    vm.update = update;
    vm.reset = reset;

    ////////////////////////////////

    function update() {
      var form = angular.copy(vm.form);
      form.country_id = form.country.id;

      vm.request = true;

      profileService.update(form).then(function () {
        vm.profile = vm.form;

        toaster.success('Profile updated!');

        $state.go('^.show');
      }).finally(function () {
        vm.request = false;
      });
    }

    function reset() {
      vm.form = angular.copy(vm.profile);
    }
  }
})();

(function () {
  'use strict';

  angular
    .module('booking')
    .service('alertService', alertService);

  alertService.$inject = ['toaster'];

  /* @ngInject */
  function alertService(toaster) {
    /* jshint validthis: true */
    var vm = this;

    vm.pop = pop;
    vm.success = success;
    vm.error = error;
    vm.info = info;
    vm.showResponseError = showResponseError;

    function pop(data) {
      toaster.pop(data);
    }

    function success(data) {
      data = prepareData(data, 'Success');
      toaster.success(data);
    }

    function info(data) {
      prepareData(data, 'Info');
      toaster.info(data);
    }

    function error(data) {
      prepareData(data, 'Error');
      toaster.error(data);
    }

    function showResponseError(response, title) {
      for (var key in response) {
        if (response.hasOwnProperty(key) && (response[key] instanceof Array)) {
          for (var i in response[key]) {
            if (response[key].hasOwnProperty(i)) {
              error({
                title: title ? title : 'Error',
                body: response[key][i]
              });
            }
          }
        } else {
          error({
            title: title,
            body: response[key]
          });
        }
      }
    }

    function prepareData(data, title) {
      if (!(data instanceof Object)) {
        data = {
          body: data
        };
      }

      data.title = data.title ? data.title : title;

      return data;
    }
  }


})();

(function () {
  'use strict';

  angular
    .module('booking')
    .service('bookingService', bookingService);

  bookingService.$inject = ['$http', 'helperService', '$rootScope'];

  /* @ngInject */
  function bookingService($http, helperService, $rootScope) {
    /* jshint validthis: true */
    var vm = this;

    vm.book = book;
    vm.calculate = calculate;
    vm.checkCoupon = checkCoupon;

    /////////////////////////

    function book(data) {
      return $http.post('/admin/api/' + $rootScope.customerId + '/booking/', helperService.prepareBookingForRequest(data)).then(helperService.fetchResponse);
    }

    function calculate(orders) {
      return $http.post('/api/booking/calculate', {orders: orders}).then(helperService.fetchResponse).then(function (data) {
        return data;
      });
    }

    function checkCoupon(coupon) {
      return $http.get('/api/booking/coupons/' + coupon).then(helperService.fetchResponse);
    }
  }
})();

(function () {
  'use strict';

  angular
    .module('booking')
    .service('childService', childService);

  childService.$inject = ['$resource', '$http', 'helperService', '$rootScope', '$timeout', '$q'];

  /* @ngInject */
  function childService($resource, $http, helperService, $rootScope, $timeout, $q) {
    /* jshint validthis: true */
    var vm = this;

    var Child;

    var run = $timeout(function () {
      Child = $resource('/admin/api/customers/' + $rootScope.customerId + '/children/:childId', {childId: '@id'}, {
        update: {method: 'PUT'}
      });
    });

    vm.all = all;
    vm.get = get;
    vm.store = store;
    vm.update = update;
    vm.remove = remove;
    vm.swimOptions = swimOptions;
    vm.yearsInSchool = yearsInSchool;

    ///////////////////////

    function yearsInSchool() {
      return $http.get('/api/years-in-school').then(helperService.fetchResponse);
    }

    function swimOptions() {
      return $http.get('/api/swim-options').then(helperService.fetchResponse);
    }

    function all() {
      return $q.all([run]).then(function () {
        return Child.query()
          .$promise
          .then(function (children) {
            for (var i = 0, child; (child = children[i]); i++) {
              child.birthday = moment(child.birthday).toDate();
            }

            return children;
          });
      });
    }

    function get(id) {
      return $q.all([run]).then(function () {
        return Child.get({childId: id})
          .$promise
          .then(function (child) {
            child.birthday = moment(child.birthday).toDate();

            return child;
          });
      });
    }

    function store(data) {
      return Child.save(data).$promise;
    }

    function update(data) {
      return Child.update(data).$promise;
    }

    function remove(id) {
      return Child.delete({childId: id}).$promise;
    }
  }
})();

(function () {
  'use strict';

  angular
    .module('booking')
    .service('eventService', eventService);

  eventService.$inject = ['$http', 'helperService'];

  /* @ngInject */
  function eventService($http, helperService) {
    /* jshint validthis: true */
    var vm = this;

    vm.all = all;

    ///////////////////////////

    function all() {
      return $http.get('/api/events')
        .then(helperService.fetchResponse, helperService.rejectResponse)
        .then(function (events) {
          for (var i = 0, event; (event = events[i]); i++) {
            event.start_date = moment(event.start_date);
            event.end_date = moment(event.end_date);
          }

          return events;
        });
    }
  }
})();

(function () {
  'use strict';

  helperService.$inject = ["$filter", "alertService", "$q", "store"];
  angular
    .module('booking')
    .service('helperService', helperService);

  /* @ngInject */
  function helperService($filter, alertService, $q, store) {
    /* jshint validthis: true */
    var vm = this;

    vm.fetchResponse = fetchResponse;
    vm.rejectResponse = rejectResponse;
    vm.pushEventDates = pushEventDates;
    vm.ShowValidationErrors = ShowValidationErrors;
    vm.redirect = redirect;
    vm.prepareOrders = prepareOrders;
    vm.prepareProducts = prepareProducts;
    vm.prepareBookingForRequest = prepareBookingForRequest;

    /////////////////////////

    function prepareOrders(orders) {
      var _orders = [];

      for (var i = 0, order; (order = orders[i]); i++) {
        var selectedDates = [];
        for (var j = 0, week; (week = order.weeks[j]); j++) {
          for (var k = 0, day; (day = week.dates[k]); k++) {
            if (day.selected) {
              var date = new Date(day.date);
              if (!(date.getMinutes() || date.getHours())) {
                date.setMinutes(date.getMinutes() - date.getTimezoneOffset());
              }

              selectedDates.push(date);
            }
          }
        }

        _orders.push({
          _id: order._id,
          child_id: order.child_id,
          event_id: order.event_id,
          selectedDates: selectedDates,
          variants: order.variants
        });
      }

      return _orders;
    }

    function prepareProducts(products) {
      var localProducts = store.get('products');

      return angular.forEach(products, function (product) {
        product.imagePath = product.image_thumbnail ? '/' + product.image_thumbnail.path : '';

        if (localProducts) {
          var localProduct = $filter('filter')(localProducts, {id: product.id})[0];

          if (localProduct) {
            product.selectedVariant = localProduct.selectedVariant;
            product.selectedQuantity = localProduct.selectedQuantity;
          }
        }

        product.price = 0;

        var variant = product.variants.filter(function (variant) {
          return product.selectedVariant && product.selectedVariant === variant.id;
        })[0];

        if (variant && product.selectedQuantity) {
          product.price = variant.price * product.selectedQuantity;
        }
      });
    }

    function redirect(url, method, fields) {
      $('<form>', {
        method: method,
        action: url
      }).append(fields).appendTo('body').submit();
    }

    function prepareBookingForRequest(_data) {
      var data = angular.copy(_data);

      data.variants = [];

      angular.forEach(data.products, function (product) {
        if (product.selectedVariant && product.selectedQuantity) {
          data.variants.push({
            id: product.selectedVariant,
            count: product.selectedQuantity
          });
        }
      });

      delete data.products;

      return data;
    }

    function fetchResponse(data) {
      return data.data;
    }

    function rejectResponse(data) {
      return $q.reject(data.data);
    }

    function ShowValidationErrors(errors) {
      if (errors.length) {
        for (var i = 0, error; (error = errors[i]); i++) {
          alertService.error(error.message);
        }
      }

      return errors;
    }

    function existsDay(dates, date) {
      date.setHours(0);
      date.setMinutes(0);

      return $filter('filter')(dates, function (day) {
        return moment(date).diff(moment(day.date)) === 0;
      }).length;
    }

    function findEvent(events, eventId) {
      for (var i = 0, event; (event = events[i]); i++) {
        if (event.id === eventId) {
          return angular.copy(event);
        }
      }
    }

    function pushEventDates(events, form) {
      //Preparation for edit order
      /*if (form.weeks) {
       var selectedDates = [];

       for (var k = 0, previousWeek; (previousWeek = form.weeks[k]); k++) {
       for (var p = 0, day; (day = previousWeek.dates[p]); p++) {
       if (day.selected) {
       selectedDates.push(day);
       }
       }
       }
       }*/

      //Find the event
      var event = findEvent(events, form.event_id);

      form.event = event;
      form.weeks = [];

      var startDate = angular.copy(event.start_date);

      // Remove saturday and sunday from period if from this start
      if (startDate.weekday() === 0) {
        startDate.add(1, 'd');
      } else if (startDate.weekday() === 6) {
        startDate.add(2, 'd');
      }


      for (var startWeek = startDate, mondayOfWeek = new Date(startDate); (startWeek <= event.end_date); startWeek = new Date((new Date(mondayOfWeek)).setDate((new Date(mondayOfWeek)).getDate() + 7)), startDate = new Date(startWeek)) {
        // Get difference between real start event and monday (1 - because Monday is 1)
        var difference = moment(startWeek).weekday() - 1;

        //Get monday of week
        mondayOfWeek = new Date(startWeek);
        mondayOfWeek.setDate(mondayOfWeek.getDate() - difference);

        //Get end of week(now friday)
        var fridayOfWeek = new Date(mondayOfWeek);
        fridayOfWeek.setDate(fridayOfWeek.getDate() + 4);
        var endOfWeek = new Date(fridayOfWeek);

        var week = {
          monday: new Date(mondayOfWeek),
          friday: new Date(endOfWeek),
          selected: false,
          allow: true,//Allow select full week or not. Must be 'false' if at least one weekday is fully
          fully: true,
          dates: [],
          selectedAnyDates: false
        };

        //If event finish before friday
        if (endOfWeek > event.end_date) {
          endOfWeek = event.end_date;
        }

        if (mondayOfWeek < startDate) {
          for (var tDate = new Date(mondayOfWeek); tDate < startDate; tDate.setDate(tDate.getDate() + 1)) {
            week.dates.push({
              date: angular.copy(tDate),
              allow: false,
              selected: false,
              fully: false
            });
          }
        }

        for (var date = new Date(startDate); date <= endOfWeek; date.setDate(date.getDate() + 1)) {
          /*jshint -W083 */
          var except = $filter('filter')(event.exception_dates, (function (exceptionDate) {
              exceptionDate = moment(exceptionDate.date).toDate();
              return exceptionDate.toDateString() === date.toDateString();
            })),
            allow = (except.length > 0) ? false : true;

          week.dates.push({
            date: angular.copy(date),
            allow: allow,
            selected: false,
            fully: allow ? existsDay(form.event.busy_dates, date) : false
          });
        }

        if (fridayOfWeek > endOfWeek) {
          for (date = new Date(endOfWeek), date.setDate(date.getDate() + 1); date <= fridayOfWeek; date.setDate(date.getDate() + 1)) {
            week.dates.push({
              date: angular.copy(date),
              allow: false,
              selected: false,
              fully: false
            });
          }
        }

        for (var k = 0, dayOfWeek; (dayOfWeek = week.dates[k]); k++) {
          if (dayOfWeek.allow && !dayOfWeek.fully) {
            week.fully = false;
            break;
          }
        }

        form.weeks.push(week);
      }
    }
  }
})();

(function () {
  'use strict';

  angular
    .module('booking')
    .service('orderService', orderService);

  orderService.$inject = ['$filter', '$q', 'store', 'childService', 'eventService', '$http', 'helperService', '$timeout', '$rootScope'];

  /* @ngInject */
  function orderService($filter, $q, store, childService, eventService, $http, helperService, $timeout, $rootScope) {
    /* jshint validthis: true */
    var vm = this;

    vm.customerId = store.get('customer-id');

    var run = $timeout(function () {
      if (vm.customerId != $rootScope.customerId) {
        vm.customerId = $rootScope.customerId;
        store.set('customer-id', vm.customerId);

        store.remove('orders');
        store.remove('index');
      }

      vm.orders = store.get('orders');
      vm.index = store.get('index');

      if (!vm.orders) {
        vm.orders = [];
        vm.index = 1;
      }

      vm.promises = $q.all([childService.all(), eventService.all()]).then(function (data) {
        vm.children = data[0];
        vm.events = data[1];
      });
    });

    vm.validate = validate;

    vm.count = count;
    vm.all = all;
    vm.add = add;
    vm.get = get;
    vm.update = update;
    vm.remove = remove;
    vm.calculate = calculate;
    vm.clear = clear;
    vm.products = products;
    vm.saveProducts = saveProducts;

    ///////////////////////

    function products() {
      return $http.get('/api/products').then(helperService.fetchResponse).then(helperService.prepareProducts);
    }

    function saveProducts(products) {
      var localPromise = $q.defer();

      vm.promises.then(function () {
        store.set('products', products);

        localPromise.resolve(true);
      });

      return localPromise.promise;
    }

    function count() {
      return vm.orders.length;
    }

    function clear() {
      var orders = angular.copy(vm.orders);

      var localPromise = $q.defer();

      vm.promises.then(function () {
        store.remove('orders');
        store.remove('index');
        store.remove('products');

        localPromise.resolve(true);
      });

      return localPromise.promise;
    }

    function calculate(order) {
      var sum = 0;

      for (var i = 0, week; (week = order.weeks[i]); i++) {
        if (week.selected) {
          sum += order.event.cost_per_week;
        } else {
          for (var j = 0, weekday; (weekday = week.dates[j]); j++) {
            if (weekday.selected) {
              sum += order.event.cost_per_day;
            }
          }
        }
      }

      angular.forEach(order.variants, function (variant) {
        sum += variant.price * variant.count;
      });

      return sum;
    }

    function validate(order, children, events) {
      var errors = [];

      var child = $filter('filter')(children, {id: order.child_id});
      if (!(order.child_id && child.length)) {
        errors.push({
          title: 'child',
          message: 'Necessary select child'
        });
      }

      var event = $filter('filter')(events, {id: order.event_id});
      if (!(order.event_id && event.length)) {
        errors.push({
          title: 'event',
          message: 'Necessary select event'
        });
      }

      //Check that selected as minimum one day
      var selected = false;
      for (var i = 0, week; order.weeks && (week = order.weeks[i]); i++) {
        for (var j = 0, day; (day = week.dates[j]); j++) {
          if (day.selected) {
            selected = true;
            break;
          }
        }

        if (selected) {
          break;
        }
      }

      if (!selected) {
        errors.push({
          title: 'dates',
          message: 'Necessary select at least one day'
        });
      }

      return errors.reverse();
    }

    function all() {
      return $q.all([run]).then(function () {
        var orders = angular.copy(vm.orders);

        var localPromise = $q.defer();

        vm.promises.then(function () {
          for (var i = 0, order; (order = orders[i]); i++) {
            setChild(order);
            setEvent(order);
          }

          localPromise.resolve(orders);
        });

        return localPromise.promise;
      });
    }

    function add(order) {
      var localPromise = $q.defer();

      vm.promises.then(function () {
        order._id = vm.index++;
        vm.orders.push(order);
        saveOrders();

        localPromise.resolve(
          angular.copy(order)
        );
      });


      return localPromise.promise;
    }

    function get(id) {
      var localPromise = $q.defer();

      vm.promises.then(function () {
        var order = angular.copy($filter('filter')(vm.orders, {_id: id})[0]);

        setChild(order);
        setEvent(order);

        localPromise.resolve(order);
      });


      return localPromise.promise;
    }

    function update(id, order) {
      var localPromise = $q.defer();

      var _order = $filter('filter')(vm.orders, {_id: id})[0];

      _order.child_id = order.child_id;
      _order.child = order.child;
      _order.event_id = order.event_id;
      _order.event = order.event;
      _order.dates = order.dates;

      saveOrders();

      localPromise.resolve(_order);

      return localPromise.promise;
    }

    function remove(id) {
      var localPromise = $q.defer();

      var order = $filter('filter')(vm.orders, {_id: id})[0];

      vm.orders.splice(vm.orders.indexOf(order), 1);

      saveOrders();

      localPromise.resolve(true);

      return localPromise.promise;
    }

    function saveOrders() {
      store.set('orders', vm.orders);
      store.set('index', vm.index);
    }

    function setChild(order) {
      order.child = $filter('filter')(vm.children, {id: order.child_id})[0];
    }

    function setEvent(order) {
      order.event = $filter('filter')(vm.events, {id: order.event_id})[0];
    }

  }
})();

(function () {
  'use strict';

  angular
    .module('booking')
    .service('profileService', profile);

  profile.$inject = ['$http', 'helperService', '$rootScope'];

  /* @ngInject */
  function profile($http, helperService, $rootScope) {
    /* jshint validthis: true */
    var vm = this;

    vm.get = get;
    vm.update = update;
    vm.countries = countries;

    ////////////////////////////

    function get() {
      return $http.get('/admin/api/customers/' + $rootScope.customerId)
        .then(helperService.fetchResponse)
        .then(function (data) {
          data.email = data.user.email;
          return data;
        });
    }

    function update(data) {
      return $http.put('/admin/api/customers/' + $rootScope.customerId, data)
        .then(helperService.fetchResponse)
        .then(function (data) {
          data.email = data.user.email;
          return data;
        });
    }

    function countries() {
      return $http.get('/api/countries').then(helperService.fetchResponse);
    }
  }
})();

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC5tb2R1bGUuanMiLCJhcHAuY29uZmlnLmpzIiwiYXBwLmNvbnN0YW50cy5qcyIsImFwcC5pbnRlcmNlcHRvci5qcyIsImFwcC5yb3V0ZXMuanMiLCJhcHAucnVuLmpzIiwiYm9va2luZy5jb250cm9sbGVyLmpzIiwiY2hpbGRyZW4vY2hpbGRyZW4uY29udHJvbGxlci5qcyIsImNoaWxkcmVuL2NyZWF0ZS5jb250cm9sbGVyLmpzIiwiY2hpbGRyZW4vZWRpdC5jb250cm9sbGVyLmpzIiwiY2hpbGRyZW4vc2hvdy5jb250cm9sbGVyLmpzIiwiY29tcGxldGUvY29tcGxldGUuY29udHJvbGxlci5qcyIsImNvbXBsZXRlL2NvbmZpcm0tcmVtb3ZlLW9yZGVyLmNvbnRyb2xsZXIuanMiLCJmaWx0ZXJzL2FnZS5maWx0ZXIuanMiLCJmaWx0ZXJzL2N1cnJlbmN5LWxiLmZpbHRlci5qcyIsImZpbHRlcnMvZXZlbnQuanMiLCJmaWx0ZXJzL2xvZ2ljYWwuZmlsdGVyLmpzIiwiZmlsdGVycy9wcm9maWxlLmZpbHRlci5qcyIsImZpbHRlcnMvc2V4LmZpbHRlci5qcyIsImZpbHRlcnMvc3VtQnlLZXkuanMiLCJmaWx0ZXJzL3N3aW0uZmlsdGVyLmpzIiwiZmlsdGVycy93ZWVrZGF5LmpzIiwib3JkZXJzL2NyZWF0ZS5jb250cm9sbGVyLmpzIiwib3JkZXJzL2VkaXQuY29udHJvbGxlci5qcyIsIm9yZGVycy9vcmRlcnMuY29udHJvbGxlci5qcyIsIm9yZGVycy9zaG93LmNvbnRyb2xsZXIuanMiLCJwcm9maWxlL3Byb2ZpbGUuY29udHJvbGxlci5qcyIsInNlcnZpY2VzL2FsZXJ0LnNlcnZpY2UuanMiLCJzZXJ2aWNlcy9ib29raW5nLnNlcnZpY2UuanMiLCJzZXJ2aWNlcy9jaGlsZHJlbi5zZXJ2aWNlLmpzIiwic2VydmljZXMvZXZlbnRzLnNlcnZpY2UuanMiLCJzZXJ2aWNlcy9oZWxwZXIuc2VydmljZS5qcyIsInNlcnZpY2VzL29yZGVycy5zZXJ2aWNlLmpzIiwic2VydmljZXMvcHJvZmlsZS5zZXJ2aWNlLmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLENBQUEsWUFBQTtFQUNBOztFQUVBLFFBQUEsT0FBQSxXQUFBO0lBQ0E7SUFDQTtJQUNBO0lBQ0E7SUFDQTtJQUNBO0lBQ0E7Ozs7O0FDVkEsQ0FBQSxXQUFBO0VBQ0E7O0VBRUE7S0FDQSxPQUFBO0tBQ0EsT0FBQTs7RUFFQSxPQUFBLFVBQUEsQ0FBQTs7RUFFQSxTQUFBLE9BQUEsZUFBQTs7SUFFQSxjQUFBLGFBQUEsS0FBQTs7Ozs7QUNYQSxDQUFBLFlBQUE7O0VBRUE7O0VBRUE7S0FDQSxPQUFBO0tBQ0EsU0FBQSxXQUFBLENBQUEsTUFBQSxRQUFBLE9BQUEsTUFBQTtLQUNBLFNBQUEsaUJBQUE7TUFDQTtRQUNBLE9BQUE7UUFDQSxPQUFBOztNQUVBO1FBQ0EsT0FBQTtRQUNBLE9BQUE7OztLQUdBLFNBQUEsZ0JBQUE7TUFDQTtRQUNBLE9BQUE7UUFDQSxPQUFBOztNQUVBO1FBQ0EsT0FBQTtRQUNBLE9BQUE7Ozs7Ozs7Ozs7QUN4QkEsQ0FBQSxZQUFBO0VBQ0E7O0VBRUE7S0FDQSxPQUFBO0tBQ0EsUUFBQSxpQkFBQTs7RUFFQSxjQUFBLFVBQUEsQ0FBQSxnQkFBQTs7RUFFQSxTQUFBLGNBQUEsY0FBQSxJQUFBO0lBQ0EsT0FBQTtNQUNBLGVBQUEsVUFBQSxLQUFBO1FBQ0EsSUFBQSxTQUFBLElBQUE7VUFDQSxrQkFBQSxJQUFBOztRQUVBLEdBQUEsb0JBQUEsaUJBQUE7VUFDQSxhQUFBLE1BQUE7O1VBRUEsT0FBQSxTQUFBLE9BQUE7ZUFDQSxHQUFBLGdCQUFBLFdBQUEsYUFBQSxnQkFBQSxTQUFBLEtBQUE7VUFDQSxhQUFBLGtCQUFBLGlCQUFBLGtCQUFBO2dCQUNBO1VBQ0EsYUFBQSxrQkFBQSxDQUFBLGlCQUFBLGtCQUFBOzs7UUFHQSxPQUFBLEdBQUEsT0FBQTs7OztJQUlBLFNBQUEsa0JBQUEsUUFBQTtNQUNBLElBQUE7O01BRUEsUUFBQTtRQUNBLEtBQUE7VUFDQSxRQUFBO1VBQ0E7UUFDQSxLQUFBO1VBQ0EsUUFBQTtVQUNBO1FBQ0EsS0FBQTtVQUNBLFFBQUE7VUFDQTs7O01BR0EsT0FBQTs7Ozs7QUM1Q0EsQ0FBQSxZQUFBO0VBQ0E7O0VBRUE7S0FDQSxPQUFBO0tBQ0EsT0FBQTs7RUFFQSxPQUFBLFVBQUEsQ0FBQSxrQkFBQTs7RUFFQSxTQUFBLE9BQUEsZ0JBQUEsb0JBQUE7O0lBRUEsbUJBQUEsVUFBQTs7SUFFQSxJQUFBLFNBQUE7O0lBRUEsSUFBQSxjQUFBO01BQ0EsUUFBQSxDQUFBLGdCQUFBLFVBQUEsY0FBQTtRQUNBLE9BQUEsYUFBQTs7TUFFQSxPQUFBLENBQUEsZ0JBQUEsZ0JBQUEsVUFBQSxjQUFBLGNBQUE7UUFDQSxPQUFBLGFBQUEsSUFBQSxhQUFBOztNQUVBLFVBQUEsQ0FBQSxnQkFBQSxVQUFBLGNBQUE7UUFDQSxPQUFBLGFBQUE7O01BRUEsT0FBQSxDQUFBLGdCQUFBLGdCQUFBLFVBQUEsY0FBQSxjQUFBO1FBQ0EsT0FBQSxhQUFBLElBQUEsYUFBQTs7TUFFQSxRQUFBLENBQUEsZ0JBQUEsVUFBQSxjQUFBO1FBQ0EsT0FBQSxhQUFBOztNQUVBLGFBQUEsQ0FBQSxnQkFBQSxTQUFBLGNBQUE7UUFDQSxPQUFBLGFBQUE7O01BRUEsZUFBQSxDQUFBLGdCQUFBLFNBQUEsY0FBQTtRQUNBLE9BQUEsYUFBQTs7TUFFQSxVQUFBLENBQUEsZ0JBQUEsU0FBQSxjQUFBO1FBQ0EsT0FBQSxhQUFBOztNQUVBLFdBQUEsQ0FBQSxrQkFBQSxVQUFBLGdCQUFBO1FBQ0EsT0FBQSxlQUFBOztNQUVBLFNBQUEsQ0FBQSxrQkFBQSxVQUFBLGdCQUFBO1FBQ0EsT0FBQSxlQUFBOzs7O0lBSUE7T0FDQSxNQUFBLFdBQUE7UUFDQSxVQUFBO1FBQ0EsYUFBQTs7OztPQUlBLE1BQUEsbUJBQUE7UUFDQSxVQUFBO1FBQ0EsS0FBQTtRQUNBLE1BQUE7VUFDQSxLQUFBOztRQUVBLFVBQUE7UUFDQSxZQUFBO1FBQ0EsY0FBQTtRQUNBLFNBQUE7VUFDQSxTQUFBLFlBQUE7VUFDQSxXQUFBLFlBQUE7OztPQUdBLE1BQUEsd0JBQUE7UUFDQSxLQUFBO1FBQ0EsYUFBQTs7T0FFQSxNQUFBLHdCQUFBO1FBQ0EsS0FBQTtRQUNBLGFBQUE7Ozs7T0FJQSxNQUFBLG9CQUFBO1FBQ0EsVUFBQTtRQUNBLEtBQUE7UUFDQSxVQUFBO1FBQ0EsTUFBQTtVQUNBLEtBQUE7OztPQUdBLE1BQUEsMEJBQUE7UUFDQSxLQUFBO1FBQ0EsYUFBQTtRQUNBLFlBQUE7UUFDQSxjQUFBO1FBQ0EsU0FBQTtVQUNBLFVBQUEsWUFBQTs7O09BR0EsTUFBQSwyQkFBQTtRQUNBLEtBQUE7UUFDQSxhQUFBO1FBQ0EsWUFBQTtRQUNBLGNBQUE7UUFDQSxTQUFBO1VBQ0EsYUFBQSxZQUFBO1VBQ0EsZUFBQSxZQUFBOzs7T0FHQSxNQUFBLHlCQUFBO1FBQ0EsS0FBQTtRQUNBLGFBQUE7UUFDQSxZQUFBO1FBQ0EsY0FBQTtRQUNBLFNBQUE7VUFDQSxPQUFBLFlBQUE7OztPQUdBLE1BQUEseUJBQUE7UUFDQSxLQUFBO1FBQ0EsYUFBQTtRQUNBLFlBQUE7UUFDQSxjQUFBO1FBQ0EsU0FBQTtVQUNBLE9BQUEsWUFBQTtVQUNBLGFBQUEsWUFBQTtVQUNBLGVBQUEsWUFBQTs7OztPQUlBLE1BQUEsa0JBQUE7UUFDQSxVQUFBO1FBQ0EsS0FBQTtRQUNBLFVBQUE7UUFDQSxNQUFBO1VBQ0EsS0FBQTs7Ozs7Ozs7Ozs7Ozs7T0FjQSxNQUFBLHlCQUFBO1FBQ0EsS0FBQTtRQUNBLGFBQUE7UUFDQSxZQUFBO1FBQ0EsY0FBQTtRQUNBLFNBQUE7VUFDQSxVQUFBLFlBQUE7VUFDQSxRQUFBLFlBQUE7VUFDQSxVQUFBLFlBQUE7VUFDQSxhQUFBLENBQUEsZ0JBQUEsVUFBQSxjQUFBO1lBQ0EsT0FBQSxhQUFBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7T0E4QkEsTUFBQSxvQkFBQTtRQUNBLEtBQUE7UUFDQSxhQUFBO1FBQ0EsTUFBQTtVQUNBLEtBQUE7O1FBRUEsWUFBQTtRQUNBLGNBQUE7UUFDQSxTQUFBO1VBQ0EsVUFBQSxZQUFBO1VBQ0EsUUFBQSxDQUFBLGdCQUFBLFVBQUEsY0FBQTtZQUNBLE9BQUEsYUFBQTs7VUFFQSxRQUFBLENBQUEsVUFBQSxrQkFBQSxpQkFBQSxTQUFBLFFBQUEsZ0JBQUEsZUFBQTtZQUNBLElBQUEsVUFBQSxjQUFBLGNBQUE7O1lBRUEsT0FBQSxlQUFBLFVBQUE7O1VBRUEsa0JBQUEsQ0FBQSxVQUFBLFVBQUEsV0FBQSxVQUFBLFFBQUEsUUFBQSxTQUFBO1lBQ0EsSUFBQSxrQkFBQTs7WUFFQSxLQUFBLElBQUEsSUFBQSxHQUFBLFFBQUEsUUFBQSxPQUFBLEtBQUEsS0FBQTtjQUNBLE1BQUEsTUFBQTs7Y0FFQSxJQUFBLFFBQUEsUUFBQSxVQUFBLFFBQUEsQ0FBQSxLQUFBLE1BQUEsTUFBQTtjQUNBLEdBQUEsTUFBQSxVQUFBLE1BQUEsR0FBQSxRQUFBO2dCQUNBLE1BQUEsTUFBQSxNQUFBLEdBQUE7Z0JBQ0EsTUFBQSxXQUFBLE1BQUEsR0FBQTtnQkFDQSxNQUFBLGlCQUFBLE1BQUEsR0FBQTs7O2NBR0EsTUFBQSxXQUFBLE1BQUEsR0FBQTs7Y0FFQSxnQkFBQSxLQUFBOzs7WUFHQSxPQUFBOzs7Ozs7O0FDOU5BLENBQUEsWUFBQTtFQUNBOztFQUVBO0tBQ0EsT0FBQTtLQUNBLElBQUE7O0VBRUEsSUFBQSxVQUFBLENBQUEsVUFBQSxjQUFBOztFQUVBLFNBQUEsSUFBQSxRQUFBLFlBQUEsY0FBQTs7SUFFQSxXQUFBLElBQUEscUJBQUEsVUFBQSxPQUFBLFNBQUEsVUFBQSxXQUFBLFlBQUE7TUFDQSxJQUFBLGVBQUE7UUFDQTs7TUFFQSxHQUFBLFFBQUEsU0FBQSxzQkFBQSxDQUFBLGFBQUEsU0FBQTtRQUNBLGVBQUE7UUFDQSxZQUFBOzs7TUFHQSxHQUFBLGNBQUE7UUFDQSxNQUFBO1FBQ0EsR0FBQSxXQUFBO1VBQ0EsT0FBQSxHQUFBOzthQUVBO1FBQ0EsV0FBQSxNQUFBLFFBQUEsS0FBQTs7Ozs7OztBQzFCQSxDQUFBLFlBQUE7RUFDQTs7RUFFQTtLQUNBLE9BQUE7S0FDQSxXQUFBLHFCQUFBOztFQUVBLGtCQUFBLFVBQUEsQ0FBQTs7O0VBR0EsU0FBQSxrQkFBQSxRQUFBOztJQUVBLElBQUEsS0FBQTs7SUFFQSxJQUFBLE1BQUEsT0FBQSxRQUFBLEtBQUE7Ozs7QUNkQSxDQUFBLFlBQUE7RUFDQTs7RUFFQTtLQUNBLE9BQUE7S0FDQSxXQUFBLHNCQUFBOztFQUVBLG1CQUFBLFVBQUEsQ0FBQSxZQUFBLGdCQUFBLGdCQUFBOzs7RUFHQSxTQUFBLG1CQUFBLFVBQUEsY0FBQSxjQUFBLFFBQUE7O0lBRUEsSUFBQSxLQUFBOztJQUVBLEdBQUEsV0FBQTtJQUNBLEdBQUEsbUJBQUE7SUFDQSxHQUFBLGlCQUFBOztJQUVBLEdBQUEsU0FBQTtJQUNBLEdBQUEsT0FBQTs7OztJQUlBLFNBQUEsT0FBQTtNQUNBLElBQUEsQ0FBQSxHQUFBLGtCQUFBO1FBQ0EsYUFBQSxNQUFBOztRQUVBOztNQUVBLElBQUEsQ0FBQSxHQUFBLGdCQUFBO1FBQ0EsYUFBQSxNQUFBOztRQUVBOzs7TUFHQSxPQUFBLEdBQUE7OztJQUdBLFNBQUEsT0FBQSxPQUFBO01BQ0EsYUFBQSxPQUFBLE1BQUEsSUFBQSxLQUFBLFlBQUE7UUFDQSxhQUFBLFFBQUE7O1FBRUEsR0FBQSxTQUFBLE9BQUEsR0FBQSxTQUFBLFFBQUEsUUFBQTs7Ozs7O0FDMUNBLENBQUEsWUFBQTtFQUNBOztFQUVBO0tBQ0EsT0FBQTtLQUNBLFdBQUEseUJBQUE7O0VBRUEsc0JBQUEsVUFBQSxDQUFBLGdCQUFBLFVBQUEsV0FBQSxlQUFBOzs7RUFHQSxTQUFBLHNCQUFBLGNBQUEsUUFBQSxTQUFBLGFBQUEsZUFBQTs7SUFFQSxJQUFBLEtBQUE7O0lBRUEsR0FBQSxjQUFBO0lBQ0EsR0FBQSxnQkFBQTs7SUFFQSxHQUFBLE9BQUE7TUFDQSxZQUFBO01BQ0EsV0FBQTtNQUNBLE9BQUE7TUFDQSxVQUFBO01BQ0EsS0FBQTtNQUNBLGNBQUE7TUFDQSxRQUFBO01BQ0EsZ0JBQUE7TUFDQSxhQUFBO01BQ0EseUJBQUE7TUFDQSw4QkFBQTtNQUNBLFNBQUE7TUFDQSxZQUFBO01BQ0Esc0JBQUE7TUFDQSxVQUFBO01BQ0EsMEJBQUE7OztJQUdBLEdBQUEsU0FBQTs7SUFFQSxHQUFBLGlCQUFBOzs7O0lBSUEsU0FBQSxTQUFBO01BQ0EsYUFBQSxNQUFBLEdBQUEsTUFBQSxLQUFBLFlBQUE7UUFDQSxRQUFBLFFBQUE7O1FBRUEsT0FBQSxHQUFBOzs7Ozs7O0FDOUNBLENBQUEsWUFBQTtFQUNBOztFQUVBO0tBQ0EsT0FBQTtLQUNBLFdBQUEsdUJBQUE7O0VBRUEsb0JBQUEsVUFBQSxDQUFBLFNBQUEsZ0JBQUEsVUFBQSxXQUFBLGVBQUE7OztFQUdBLFNBQUEsb0JBQUEsT0FBQSxjQUFBLFFBQUEsU0FBQSxhQUFBLGVBQUE7O0lBRUEsSUFBQSxLQUFBOztJQUVBLEdBQUEsY0FBQTtJQUNBLEdBQUEsZ0JBQUE7O0lBRUEsR0FBQSxPQUFBOztJQUVBLEdBQUEsU0FBQTs7SUFFQSxHQUFBLGlCQUFBOzs7O0lBSUEsU0FBQSxTQUFBO01BQ0EsYUFBQSxPQUFBLEdBQUEsTUFBQSxLQUFBLFlBQUE7UUFDQSxRQUFBLFFBQUE7O1FBRUEsT0FBQSxHQUFBOzs7Ozs7O0FDN0JBLENBQUEsWUFBQTtFQUNBOztFQUVBO0tBQ0EsT0FBQTtLQUNBLFdBQUEsdUJBQUE7O0VBRUEsb0JBQUEsVUFBQSxDQUFBOzs7RUFHQSxTQUFBLG9CQUFBLE9BQUE7O0lBRUEsSUFBQSxLQUFBOztJQUVBLEdBQUEsUUFBQTs7OztBQ2RBLENBQUEsWUFBQTtFQUNBOzs7RUFFQTtLQUNBLE9BQUE7S0FDQSxXQUFBLHNCQUFBOzs7RUFHQSxTQUFBLG1CQUFBLGtCQUFBLE9BQUEsZ0JBQUEsY0FBQSxjQUFBLGNBQUEsZUFBQSxTQUFBLFFBQUEsV0FBQSxVQUFBOztJQUVBLElBQUEsS0FBQTs7SUFFQSxHQUFBLFNBQUE7SUFDQSxHQUFBLE9BQUEsTUFBQSxRQUFBLFlBQUEsR0FBQSxRQUFBO0lBQ0EsR0FBQSxPQUFBLFdBQUEsUUFBQSxZQUFBLEdBQUEsUUFBQTtJQUNBLEdBQUEsZUFBQSxRQUFBLFlBQUEsVUFBQTtJQUNBLEdBQUEsT0FBQSxTQUFBO0lBQ0EsR0FBQSxXQUFBOztJQUVBLEdBQUEsZUFBQTtJQUNBLEdBQUEsZ0JBQUE7O0lBRUEsR0FBQSxPQUFBO01BQ0EsY0FBQTtNQUNBLFFBQUE7TUFDQSxRQUFBOztJQUVBLEdBQUEsU0FBQTtJQUNBLEdBQUEsVUFBQTs7SUFFQTtJQUNBO0lBQ0E7SUFDQTs7SUFFQSxHQUFBLE9BQUE7SUFDQSxHQUFBLFNBQUE7SUFDQSxHQUFBLGNBQUE7SUFDQSxHQUFBLGVBQUE7SUFDQSxHQUFBLHdCQUFBO0lBQ0EsR0FBQSxlQUFBOzs7O0lBSUEsU0FBQSxjQUFBO01BQ0EsSUFBQSxDQUFBLEdBQUEsS0FBQSxRQUFBO1FBQ0E7OztNQUdBLEdBQUEsVUFBQTs7TUFFQSxlQUFBLFlBQUEsR0FBQSxLQUFBLFFBQUEsS0FBQSxVQUFBLFNBQUE7UUFDQSxHQUFBLE9BQUEsZ0JBQUE7UUFDQTtRQUNBLGFBQUEsUUFBQTtRQUNBLEdBQUEsZ0JBQUE7U0FDQSxZQUFBO1FBQ0EsYUFBQSxNQUFBO1NBQ0EsUUFBQSxZQUFBO1FBQ0EsR0FBQSxVQUFBOzs7O0lBSUEsU0FBQSxlQUFBOztNQUVBLEdBQUEsS0FBQSxTQUFBO01BQ0EsR0FBQSxPQUFBLGdCQUFBO01BQ0EsR0FBQSxPQUFBLFNBQUE7TUFDQSxHQUFBLGdCQUFBOztNQUVBOztNQUVBLGFBQUEsUUFBQTs7O0lBR0EsU0FBQSxPQUFBLElBQUE7TUFDQSxJQUFBLGdCQUFBLFVBQUEsS0FBQTtRQUNBLGFBQUE7UUFDQSxZQUFBO1FBQ0EsY0FBQTtRQUNBLE1BQUE7OztNQUdBLGNBQUEsT0FBQSxLQUFBLFlBQUE7UUFDQSxhQUFBLE9BQUEsSUFBQSxLQUFBLFlBQUE7O1VBRUEsYUFBQSxNQUFBLEtBQUEsVUFBQSxRQUFBO1lBQ0EsYUFBQSxRQUFBOztZQUVBLEdBQUEsRUFBQSxPQUFBLFFBQUE7Y0FDQSxPQUFBLEdBQUE7OztZQUdBLElBQUEsVUFBQSxjQUFBLGNBQUE7WUFDQSxlQUFBLFVBQUEsU0FBQSxLQUFBLFVBQUEsUUFBQTtjQUNBLElBQUEsa0JBQUE7O2NBRUEsS0FBQSxJQUFBLElBQUEsR0FBQSxRQUFBLFFBQUEsT0FBQSxLQUFBLEtBQUE7Z0JBQ0EsTUFBQSxNQUFBOztnQkFFQSxJQUFBLFFBQUEsUUFBQSxVQUFBLFFBQUEsQ0FBQSxLQUFBLE1BQUEsTUFBQTtnQkFDQSxJQUFBLE1BQUEsVUFBQSxNQUFBLEdBQUEsUUFBQTtrQkFDQSxNQUFBLE1BQUEsTUFBQSxHQUFBO2tCQUNBLE1BQUEsV0FBQSxNQUFBLEdBQUE7O2dCQUVBLGdCQUFBLEtBQUE7OztjQUdBLEdBQUEsU0FBQTtjQUNBLEdBQUEsT0FBQSxNQUFBLFFBQUEsWUFBQSxHQUFBLFFBQUE7Y0FDQSxHQUFBLE9BQUEsV0FBQSxRQUFBLFlBQUEsR0FBQSxRQUFBO2NBQ0EsR0FBQSxPQUFBLFNBQUE7Y0FDQTs7Ozs7OztJQU9BLFNBQUEsT0FBQTtNQUNBLEdBQUEsRUFBQSxHQUFBLFFBQUE7UUFDQSxhQUFBLE1BQUE7UUFDQTs7O01BR0EsR0FBQSxVQUFBOztNQUVBLE9BQUEsZUFBQSxLQUFBLEdBQUEsTUFBQSxLQUFBLFVBQUEsTUFBQTtRQUNBLGFBQUEsUUFBQTs7O1FBR0EsYUFBQTs7UUFFQSxjQUFBLFNBQUEsS0FBQSxVQUFBLEtBQUEsUUFBQSxLQUFBO1NBQ0EsWUFBQTtRQUNBLEdBQUEsVUFBQTs7OztJQUlBLFNBQUEseUJBQUE7TUFDQSxHQUFBLEtBQUEsU0FBQSxjQUFBLGNBQUEsR0FBQTs7O0lBR0EsU0FBQSxzQkFBQTtNQUNBLEdBQUEsR0FBQSxLQUFBLFVBQUEsR0FBQSxPQUFBLGVBQUE7UUFDQSxHQUFBLE9BQUEsU0FBQSxHQUFBLE9BQUEsTUFBQSxHQUFBLE9BQUEsZ0JBQUE7UUFDQSxHQUFBLE9BQUEsUUFBQSxHQUFBLE9BQUEsTUFBQSxHQUFBLE9BQUE7YUFDQTtRQUNBLEdBQUEsT0FBQSxRQUFBLEdBQUEsT0FBQSxNQUFBLEdBQUEsT0FBQTs7TUFFQSxHQUFBLE9BQUEsUUFBQSxHQUFBLE9BQUEsUUFBQSxHQUFBOzs7SUFHQSxTQUFBLGtCQUFBO01BQ0EsUUFBQSxRQUFBLEdBQUEsVUFBQSxVQUFBLFNBQUE7UUFDQSxJQUFBLENBQUEsUUFBQSxrQkFBQTtVQUNBLFFBQUEsbUJBQUE7OztRQUdBLElBQUEsQ0FBQSxRQUFBLGlCQUFBO1VBQ0EsUUFBQSxrQkFBQSxRQUFBLFNBQUEsU0FBQSxRQUFBLFNBQUEsR0FBQSxLQUFBOzs7UUFHQSxPQUFBLGVBQUEsU0FBQSxTQUFBO1VBQ0EsS0FBQSxZQUFBO1lBQ0EsSUFBQSxRQUFBOztZQUVBLElBQUEsa0JBQUEsS0FBQTtZQUNBLElBQUEsVUFBQSxLQUFBLFNBQUEsT0FBQSxVQUFBLFNBQUE7Y0FDQSxPQUFBLG1CQUFBLG9CQUFBLFFBQUE7ZUFDQTs7WUFFQSxJQUFBLFdBQUEsS0FBQSxrQkFBQTtjQUNBLFFBQUEsUUFBQSxRQUFBLEtBQUE7OztZQUdBLE9BQUE7OztRQUdBLE9BQUEsZUFBQSxTQUFBLFlBQUE7VUFDQSxLQUFBLFlBQUE7WUFDQSxJQUFBLFdBQUE7O1lBRUEsSUFBQSxrQkFBQSxLQUFBO1lBQ0EsSUFBQSxVQUFBLEtBQUEsU0FBQSxPQUFBLFVBQUEsU0FBQTtjQUNBLE9BQUEsbUJBQUEsb0JBQUEsUUFBQTtlQUNBOztZQUVBLElBQUEsV0FBQSxLQUFBLGtCQUFBO2NBQ0EsV0FBQSxRQUFBOzs7WUFHQSxPQUFBOzs7O1FBSUEsc0JBQUEsU0FBQTs7OztJQUlBLFNBQUEsc0JBQUEsU0FBQSx1QkFBQTtNQUNBLEdBQUEsa0JBQUEsQ0FBQTs7OztNQUlBLElBQUEsVUFBQSxRQUFBLFNBQUEsT0FBQSxVQUFBLFNBQUE7UUFDQSxPQUFBLFFBQUEsbUJBQUEsUUFBQSxvQkFBQSxRQUFBO1NBQ0E7O01BRUEsSUFBQSxXQUFBLFFBQUEsa0JBQUE7Ozs7OztJQU1BLFNBQUEsZUFBQTtNQUNBLElBQUEsV0FBQSxHQUFBLFNBQUEsT0FBQSxVQUFBLFNBQUE7UUFDQSxPQUFBLFFBQUE7OztNQUdBLGFBQUEsYUFBQSxVQUFBLEtBQUEsWUFBQTtRQUNBLGFBQUEsUUFBQTs7UUFFQSxHQUFBLEtBQUEsV0FBQTs7UUFFQSxHQUFBLGVBQUEsUUFBQSxZQUFBLFVBQUE7O1FBRUE7O1FBRUEsR0FBQSxrQkFBQTs7OztJQUlBLFNBQUEsNkJBQUE7TUFDQSxJQUFBLFdBQUEsTUFBQSxJQUFBOztNQUVBLElBQUEsQ0FBQSxZQUFBLENBQUEsU0FBQSxRQUFBO1FBQ0EsT0FBQTs7O01BR0EsR0FBQSxLQUFBLFdBQUE7Ozs7OztBQ2hQQSxDQUFBLFlBQUE7RUFDQTs7RUFFQTtLQUNBLE9BQUE7S0FDQSxXQUFBLGdDQUFBOztFQUVBLDZCQUFBLFVBQUEsQ0FBQTs7O0VBR0EsU0FBQSw2QkFBQSxtQkFBQTs7SUFFQSxJQUFBLEtBQUE7O0lBRUEsR0FBQSxLQUFBO0lBQ0EsR0FBQSxTQUFBOztJQUVBLFNBQUEsS0FBQTtNQUNBLGtCQUFBOzs7SUFHQSxTQUFBLFNBQUE7TUFDQSxrQkFBQSxRQUFBOzs7OztBQ3RCQSxDQUFBLFVBQUE7RUFDQTs7RUFFQTtLQUNBLE9BQUE7S0FDQSxPQUFBLE9BQUE7O0VBRUEsSUFBQSxVQUFBOztFQUVBLFNBQUEsTUFBQTtJQUNBLE9BQUE7O0lBRUEsU0FBQSxLQUFBLFFBQUEsTUFBQTtNQUNBLE9BQUEsT0FBQSxJQUFBLEtBQUEsUUFBQSxJQUFBOztNQUVBLElBQUEsV0FBQSxJQUFBLEtBQUEsT0FBQTtNQUNBLElBQUEsV0FBQSxLQUFBLFlBQUEsU0FBQTtNQUNBLElBQUEsVUFBQSxJQUFBLEtBQUE7TUFDQSxPQUFBLEtBQUEsSUFBQSxRQUFBLGdCQUFBOzs7O0FDbEJBLENBQUEsVUFBQTtFQUNBOztFQUVBO0tBQ0EsT0FBQTtLQUNBLE9BQUEsY0FBQTs7RUFFQSxXQUFBLFVBQUEsQ0FBQTs7RUFFQSxTQUFBLFdBQUEsU0FBQTtJQUNBLE9BQUE7O0lBRUEsU0FBQSxLQUFBLE9BQUE7TUFDQSxPQUFBLFFBQUEsWUFBQSxPQUFBOzs7O0FDYkEsQ0FBQSxVQUFBO0VBQ0E7O0VBRUE7S0FDQSxPQUFBO0tBQ0EsT0FBQSxTQUFBOztFQUVBLE1BQUEsVUFBQTs7RUFFQSxTQUFBLFFBQUE7SUFDQSxPQUFBOztJQUVBLFNBQUEsS0FBQSxPQUFBO01BQ0EsT0FBQSxNQUFBLE1BQUEsT0FBQSxRQUFBLE1BQUE7Ozs7QUNiQSxDQUFBLFVBQUE7RUFDQTs7RUFFQTtLQUNBLE9BQUE7S0FDQSxPQUFBLFdBQUE7O0VBRUEsUUFBQSxVQUFBOztFQUVBLFNBQUEsVUFBQTtJQUNBLE9BQUE7O0lBRUEsU0FBQSxLQUFBLE9BQUE7TUFDQSxPQUFBLFFBQUEsUUFBQTs7OztBQ2JBLENBQUEsVUFBQTtFQUNBOztFQUVBO0tBQ0EsT0FBQTtLQUNBLE9BQUEsV0FBQTs7RUFFQSxRQUFBLFVBQUE7O0VBRUEsU0FBQSxVQUFBO0lBQ0EsT0FBQTs7SUFFQSxTQUFBLEtBQUEsTUFBQTtNQUNBLE9BQUEsS0FBQSxhQUFBLE1BQUEsS0FBQTs7OztBQ2JBLENBQUEsVUFBQTtFQUNBOztFQUVBO0tBQ0EsT0FBQTtLQUNBLE9BQUEsT0FBQTs7RUFFQSxJQUFBLFVBQUE7O0VBRUEsU0FBQSxNQUFBO0lBQ0EsT0FBQTs7SUFFQSxTQUFBLEtBQUEsT0FBQTtNQUNBLE9BQUEsUUFBQSxTQUFBOzs7O0FDYkEsQ0FBQSxZQUFBO0VBQ0E7O0VBRUE7S0FDQSxPQUFBO0tBQ0EsT0FBQSxZQUFBLFlBQUE7TUFDQSxPQUFBLFVBQUEsTUFBQSxLQUFBO1FBQ0EsSUFBQSxPQUFBLFVBQUEsZUFBQSxPQUFBLFNBQUEsYUFBQTtVQUNBLE9BQUE7OztRQUdBLElBQUEsTUFBQTtRQUNBLEtBQUEsSUFBQSxJQUFBLEtBQUEsU0FBQSxHQUFBLEtBQUEsR0FBQSxLQUFBO1VBQ0EsT0FBQSxXQUFBLEtBQUEsR0FBQTs7O1FBR0EsT0FBQTs7Ozs7O0FDaEJBLENBQUEsWUFBQTtFQUNBOztFQUVBO0tBQ0EsT0FBQTtLQUNBLE9BQUEsUUFBQTs7RUFFQSxLQUFBLFVBQUE7O0VBRUEsU0FBQSxPQUFBO0lBQ0EsT0FBQTs7SUFFQSxTQUFBLEtBQUEsT0FBQTtNQUNBLE9BQUEsTUFBQSxZQUFBLE1BQUEsU0FBQSxTQUFBLFFBQUEsTUFBQSxTQUFBLFVBQUE7Ozs7QUNiQSxDQUFBLFlBQUE7RUFDQTs7RUFFQTtLQUNBLE9BQUE7S0FDQSxPQUFBLFdBQUE7O0VBRUEsUUFBQSxVQUFBOztFQUVBLFNBQUEsVUFBQTtJQUNBLE9BQUE7O0lBRUEsU0FBQSxLQUFBLE1BQUE7TUFDQSxJQUFBLEVBQUEsZ0JBQUEsT0FBQTtRQUNBLE9BQUEsSUFBQSxLQUFBOzs7TUFHQSxJQUFBOztNQUVBLFFBQUEsS0FBQTtRQUNBLEtBQUE7VUFDQSxVQUFBO1VBQ0E7UUFDQSxLQUFBO1VBQ0EsVUFBQTtVQUNBO1FBQ0EsS0FBQTtVQUNBLFVBQUE7VUFDQTtRQUNBLEtBQUE7VUFDQSxVQUFBO1VBQ0E7UUFDQSxLQUFBO1VBQ0EsVUFBQTtVQUNBO1FBQ0EsS0FBQTtVQUNBLFVBQUE7VUFDQTtRQUNBLEtBQUE7VUFDQSxVQUFBO1VBQ0E7Ozs7TUFJQSxPQUFBOzs7O0FDNUNBLENBQUEsWUFBQTtFQUNBOztFQUVBO0tBQ0EsT0FBQTtLQUNBLFdBQUEseUJBQUE7O0VBRUEsc0JBQUEsVUFBQSxDQUFBLFlBQUEsVUFBQSxnQkFBQSxVQUFBLGdCQUFBLGlCQUFBLFdBQUEsWUFBQTs7O0VBR0EsU0FBQSxzQkFBQSxVQUFBLFFBQUEsY0FBQSxRQUFBLGNBQUEsZUFBQSxTQUFBLFVBQUEsYUFBQTs7SUFFQSxJQUFBLEtBQUE7O0lBRUEsSUFBQSxDQUFBLFNBQUEsUUFBQTtNQUNBLGFBQUEsTUFBQTtNQUNBLE9BQUEsR0FBQTs7O0lBR0EsR0FBQSxjQUFBO0lBQ0EsR0FBQSxXQUFBO0lBQ0EsR0FBQSxXQUFBO0lBQ0EsR0FBQSxTQUFBOztJQUVBLEdBQUEsVUFBQTs7SUFFQSxHQUFBLE9BQUE7TUFDQSxVQUFBO01BQ0EsVUFBQTtNQUNBLE9BQUE7TUFDQSxVQUFBOztJQUVBLEdBQUEsa0JBQUE7O0lBRUEsR0FBQSxTQUFBO0lBQ0EsR0FBQSxnQkFBQTtJQUNBLEdBQUEsZUFBQTtJQUNBLEdBQUEsY0FBQTtJQUNBLEdBQUEsZ0JBQUE7SUFDQSxHQUFBLGFBQUE7Ozs7SUFJQSxTQUFBLGFBQUE7Ozs7SUFJQSxTQUFBLGdCQUFBO01BQ0EsSUFBQSxRQUFBLFFBQUEsVUFBQSxVQUFBLENBQUEsSUFBQSxHQUFBLEtBQUEsV0FBQTs7TUFFQSxHQUFBLFNBQUEsUUFBQSxVQUFBLFFBQUEsU0FBQSxPQUFBO1FBQ0EsSUFBQSxNQUFBLFFBQUEsT0FBQSxPQUFBLE1BQUE7UUFDQSxPQUFBLE1BQUEsV0FBQSxPQUFBLE9BQUEsTUFBQTs7OztJQUlBLFNBQUEsU0FBQTtNQUNBLElBQUEsY0FBQSxxQkFBQSxhQUFBLFNBQUEsR0FBQSxNQUFBLFVBQUEsU0FBQSxRQUFBO1FBQ0E7OztNQUdBLEdBQUEsVUFBQTs7TUFFQSxhQUFBLElBQUEsR0FBQSxNQUFBLEtBQUEsWUFBQTtRQUNBLGFBQUEsUUFBQTs7UUFFQSxPQUFBLEdBQUE7U0FDQSxRQUFBLFlBQUE7Ozs7O0lBS0EsU0FBQSxnQkFBQTtNQUNBLEdBQUEsR0FBQSxLQUFBLFVBQUE7UUFDQSxjQUFBLGVBQUEsR0FBQSxRQUFBLEdBQUE7Ozs7SUFJQSxTQUFBLGFBQUEsTUFBQTtNQUNBLElBQUEsS0FBQSxVQUFBO1FBQ0EsdUJBQUEsTUFBQTtRQUNBLEtBQUEsbUJBQUE7YUFDQTtRQUNBLEtBQUEsbUJBQUE7UUFDQSx1QkFBQSxNQUFBOzs7O0lBSUEsU0FBQSxZQUFBLE1BQUEsS0FBQTtNQUNBLElBQUEsQ0FBQSxJQUFBLFlBQUEsS0FBQSxVQUFBO1FBQ0EsS0FBQSxXQUFBOzs7TUFHQSxJQUFBLElBQUEsVUFBQTtRQUNBLEtBQUEsbUJBQUE7UUFDQSxJQUFBLHdCQUFBOztRQUVBLEtBQUEsSUFBQSxJQUFBLEdBQUEsVUFBQSxVQUFBLEtBQUEsTUFBQSxLQUFBLEtBQUE7VUFDQSxJQUFBLFFBQUEsU0FBQSxDQUFBLFFBQUEsVUFBQTtZQUNBLHdCQUFBO1lBQ0E7Ozs7UUFJQSxJQUFBLHVCQUFBO1VBQ0EsS0FBQSxXQUFBOzthQUVBOztRQUVBLElBQUEsa0JBQUE7UUFDQSxLQUFBLElBQUEsSUFBQSxHQUFBLE9BQUEsT0FBQSxLQUFBLE1BQUEsS0FBQSxLQUFBO1VBQ0EsSUFBQSxLQUFBLFVBQUE7WUFDQSxrQkFBQTtZQUNBOzs7O1FBSUEsS0FBQSxtQkFBQTs7OztJQUlBLFNBQUEsdUJBQUEsTUFBQSxPQUFBO01BQ0EsS0FBQSxJQUFBLElBQUEsR0FBQSxNQUFBLE1BQUEsS0FBQSxNQUFBLEtBQUEsS0FBQTtRQUNBLElBQUEsSUFBQSxPQUFBO1VBQ0EsSUFBQSxXQUFBOzs7Ozs7OztBQzVIQSxDQUFBLFlBQUE7RUFDQTs7RUFFQTtLQUNBLE9BQUE7S0FDQSxXQUFBLHVCQUFBOztFQUVBLG9CQUFBLFVBQUEsQ0FBQSxTQUFBLFlBQUEsVUFBQSxnQkFBQSxVQUFBLGdCQUFBOzs7RUFHQSxTQUFBLG9CQUFBLE9BQUEsVUFBQSxRQUFBLGNBQUEsUUFBQSxjQUFBLGVBQUE7O0lBRUEsSUFBQSxLQUFBOztJQUVBLEdBQUEsT0FBQSxRQUFBLEtBQUE7SUFDQSxHQUFBLFdBQUE7SUFDQSxHQUFBLFNBQUE7O0lBRUEsR0FBQSxTQUFBO0lBQ0EsR0FBQSxnQkFBQTs7OztJQUlBLFNBQUEsU0FBQTtNQUNBLEdBQUEsY0FBQSxxQkFBQSxhQUFBLFNBQUEsR0FBQSxPQUFBLFFBQUE7UUFDQTs7O01BR0EsYUFBQSxPQUFBLEdBQUEsS0FBQSxLQUFBLEdBQUEsTUFBQSxLQUFBLFlBQUE7UUFDQSxhQUFBLFFBQUE7O1FBRUEsT0FBQSxHQUFBOzs7O0lBSUEsU0FBQSxnQkFBQTtNQUNBLGNBQUEsZUFBQSxHQUFBLFFBQUEsR0FBQTs7Ozs7OztBQ3BDQSxDQUFBLFlBQUE7RUFDQTs7RUFFQTtLQUNBLE9BQUE7S0FDQSxXQUFBLG9CQUFBOztFQUVBLGlCQUFBLFVBQUEsQ0FBQSxVQUFBLGdCQUFBOzs7RUFHQSxTQUFBLGlCQUFBLFFBQUEsY0FBQSxTQUFBOztJQUVBLElBQUEsS0FBQTs7SUFFQSxHQUFBLFNBQUE7O0lBRUEsR0FBQSxTQUFBOzs7O0lBSUEsU0FBQSxPQUFBLElBQUE7TUFDQSxhQUFBLE9BQUEsSUFBQSxLQUFBLFlBQUE7O1FBRUEsYUFBQSxNQUFBLEtBQUEsVUFBQSxRQUFBO1VBQ0EsR0FBQSxTQUFBOztVQUVBLFFBQUEsUUFBQTs7Ozs7Ozs7QUMxQkEsQ0FBQSxZQUFBO0VBQ0E7O0VBRUE7S0FDQSxPQUFBO0tBQ0EsV0FBQSx1QkFBQTs7RUFFQSxvQkFBQSxVQUFBLENBQUE7OztFQUdBLFNBQUEsb0JBQUEsT0FBQTs7SUFFQSxJQUFBLEtBQUE7O0lBRUEsR0FBQSxRQUFBOzs7O0FDZEEsQ0FBQSxZQUFBO0VBQ0E7O0VBRUE7S0FDQSxPQUFBO0tBQ0EsV0FBQSxxQkFBQTs7RUFFQSxrQkFBQSxVQUFBLENBQUEsV0FBQSxrQkFBQSxVQUFBLFdBQUEsV0FBQSxpQkFBQSxhQUFBOzs7RUFHQSxTQUFBLGtCQUFBLFNBQUEsZ0JBQUEsUUFBQSxTQUFBLFNBQUEsZUFBQSxXQUFBLFNBQUE7O0lBRUEsSUFBQSxLQUFBOztJQUVBLEdBQUEsVUFBQSxRQUFBLEtBQUE7SUFDQSxHQUFBLE9BQUEsUUFBQSxLQUFBLEdBQUE7SUFDQSxHQUFBLFVBQUE7SUFDQSxHQUFBLGdCQUFBO0lBQ0EsR0FBQSxZQUFBO0lBQ0EsR0FBQSxVQUFBOztJQUVBLEdBQUEsU0FBQTtJQUNBLEdBQUEsUUFBQTs7OztJQUlBLFNBQUEsU0FBQTtNQUNBLElBQUEsT0FBQSxRQUFBLEtBQUEsR0FBQTtNQUNBLEtBQUEsYUFBQSxLQUFBLFFBQUE7O01BRUEsR0FBQSxVQUFBOztNQUVBLGVBQUEsT0FBQSxNQUFBLEtBQUEsWUFBQTtRQUNBLEdBQUEsVUFBQSxHQUFBOztRQUVBLFFBQUEsUUFBQTs7UUFFQSxPQUFBLEdBQUE7U0FDQSxRQUFBLFlBQUE7UUFDQSxHQUFBLFVBQUE7Ozs7SUFJQSxTQUFBLFFBQUE7TUFDQSxHQUFBLE9BQUEsUUFBQSxLQUFBLEdBQUE7Ozs7O0FDNUNBLENBQUEsV0FBQTtFQUNBOztFQUVBO0tBQ0EsT0FBQTtLQUNBLFFBQUEsZ0JBQUE7O0VBRUEsYUFBQSxVQUFBLENBQUE7OztFQUdBLFNBQUEsY0FBQSxTQUFBOztJQUVBLElBQUEsS0FBQTs7SUFFQSxHQUFBLE1BQUE7SUFDQSxHQUFBLFVBQUE7SUFDQSxHQUFBLFFBQUE7SUFDQSxHQUFBLE9BQUE7SUFDQSxHQUFBLG9CQUFBOztJQUVBLFNBQUEsSUFBQSxNQUFBO01BQ0EsUUFBQSxJQUFBOzs7SUFHQSxTQUFBLFNBQUEsTUFBQTtNQUNBLE9BQUEsWUFBQSxNQUFBO01BQ0EsUUFBQSxRQUFBOzs7SUFHQSxTQUFBLEtBQUEsTUFBQTtNQUNBLFlBQUEsTUFBQTtNQUNBLFFBQUEsS0FBQTs7O0lBR0EsU0FBQSxNQUFBLE1BQUE7TUFDQSxZQUFBLE1BQUE7TUFDQSxRQUFBLE1BQUE7OztJQUdBLFNBQUEsa0JBQUEsVUFBQSxPQUFBO01BQ0EsSUFBQSxJQUFBLE9BQUEsVUFBQTtRQUNBLElBQUEsU0FBQSxlQUFBLFNBQUEsU0FBQSxnQkFBQSxRQUFBO1VBQ0EsSUFBQSxJQUFBLEtBQUEsU0FBQSxNQUFBO1lBQ0EsSUFBQSxTQUFBLEtBQUEsZUFBQSxJQUFBO2NBQ0EsTUFBQTtnQkFDQSxPQUFBLFFBQUEsUUFBQTtnQkFDQSxNQUFBLFNBQUEsS0FBQTs7OztlQUlBO1VBQ0EsTUFBQTtZQUNBLE9BQUE7WUFDQSxNQUFBLFNBQUE7Ozs7OztJQU1BLFNBQUEsWUFBQSxNQUFBLE9BQUE7TUFDQSxJQUFBLEVBQUEsZ0JBQUEsU0FBQTtRQUNBLE9BQUE7VUFDQSxNQUFBOzs7O01BSUEsS0FBQSxRQUFBLEtBQUEsUUFBQSxLQUFBLFFBQUE7O01BRUEsT0FBQTs7Ozs7OztBQ3BFQSxDQUFBLFlBQUE7RUFDQTs7RUFFQTtLQUNBLE9BQUE7S0FDQSxRQUFBLGtCQUFBOztFQUVBLGVBQUEsVUFBQSxDQUFBLFNBQUEsaUJBQUE7OztFQUdBLFNBQUEsZUFBQSxPQUFBLGVBQUEsWUFBQTs7SUFFQSxJQUFBLEtBQUE7O0lBRUEsR0FBQSxPQUFBO0lBQ0EsR0FBQSxZQUFBO0lBQ0EsR0FBQSxjQUFBOzs7O0lBSUEsU0FBQSxLQUFBLE1BQUE7TUFDQSxPQUFBLE1BQUEsS0FBQSxnQkFBQSxXQUFBLGFBQUEsYUFBQSxjQUFBLHlCQUFBLE9BQUEsS0FBQSxjQUFBOzs7SUFHQSxTQUFBLFVBQUEsUUFBQTtNQUNBLE9BQUEsTUFBQSxLQUFBLDBCQUFBLENBQUEsUUFBQSxTQUFBLEtBQUEsY0FBQSxlQUFBLEtBQUEsVUFBQSxNQUFBO1FBQ0EsT0FBQTs7OztJQUlBLFNBQUEsWUFBQSxRQUFBO01BQ0EsT0FBQSxNQUFBLElBQUEsMEJBQUEsUUFBQSxLQUFBLGNBQUE7Ozs7O0FDL0JBLENBQUEsWUFBQTtFQUNBOztFQUVBO0tBQ0EsT0FBQTtLQUNBLFFBQUEsZ0JBQUE7O0VBRUEsYUFBQSxVQUFBLENBQUEsYUFBQSxTQUFBLGlCQUFBLGNBQUEsWUFBQTs7O0VBR0EsU0FBQSxhQUFBLFdBQUEsT0FBQSxlQUFBLFlBQUEsVUFBQSxJQUFBOztJQUVBLElBQUEsS0FBQTs7SUFFQSxJQUFBOztJQUVBLElBQUEsTUFBQSxTQUFBLFlBQUE7TUFDQSxRQUFBLFVBQUEsMEJBQUEsV0FBQSxhQUFBLHNCQUFBLENBQUEsU0FBQSxRQUFBO1FBQ0EsUUFBQSxDQUFBLFFBQUE7Ozs7SUFJQSxHQUFBLE1BQUE7SUFDQSxHQUFBLE1BQUE7SUFDQSxHQUFBLFFBQUE7SUFDQSxHQUFBLFNBQUE7SUFDQSxHQUFBLFNBQUE7SUFDQSxHQUFBLGNBQUE7SUFDQSxHQUFBLGdCQUFBOzs7O0lBSUEsU0FBQSxnQkFBQTtNQUNBLE9BQUEsTUFBQSxJQUFBLHdCQUFBLEtBQUEsY0FBQTs7O0lBR0EsU0FBQSxjQUFBO01BQ0EsT0FBQSxNQUFBLElBQUEscUJBQUEsS0FBQSxjQUFBOzs7SUFHQSxTQUFBLE1BQUE7TUFDQSxPQUFBLEdBQUEsSUFBQSxDQUFBLE1BQUEsS0FBQSxZQUFBO1FBQ0EsT0FBQSxNQUFBO2FBQ0E7YUFDQSxLQUFBLFVBQUEsVUFBQTtjQUNBLElBQUEsSUFBQSxJQUFBLEdBQUEsUUFBQSxRQUFBLFNBQUEsS0FBQSxLQUFBO2dCQUNBLE1BQUEsV0FBQSxPQUFBLE1BQUEsVUFBQTs7O2NBR0EsT0FBQTs7Ozs7SUFLQSxTQUFBLElBQUEsSUFBQTtNQUNBLE9BQUEsR0FBQSxJQUFBLENBQUEsTUFBQSxLQUFBLFlBQUE7UUFDQSxPQUFBLE1BQUEsSUFBQSxDQUFBLFNBQUE7V0FDQTtXQUNBLEtBQUEsVUFBQSxPQUFBO1lBQ0EsTUFBQSxXQUFBLE9BQUEsTUFBQSxVQUFBOztZQUVBLE9BQUE7Ozs7O0lBS0EsU0FBQSxNQUFBLE1BQUE7TUFDQSxPQUFBLE1BQUEsS0FBQSxNQUFBOzs7SUFHQSxTQUFBLE9BQUEsTUFBQTtNQUNBLE9BQUEsTUFBQSxPQUFBLE1BQUE7OztJQUdBLFNBQUEsT0FBQSxJQUFBO01BQ0EsT0FBQSxNQUFBLE9BQUEsQ0FBQSxTQUFBLEtBQUE7Ozs7O0FDM0VBLENBQUEsV0FBQTtFQUNBOztFQUVBO0tBQ0EsT0FBQTtLQUNBLFFBQUEsZ0JBQUE7O0VBRUEsYUFBQSxVQUFBLENBQUEsU0FBQTs7O0VBR0EsU0FBQSxhQUFBLE9BQUEsZUFBQTs7SUFFQSxJQUFBLEtBQUE7O0lBRUEsR0FBQSxNQUFBOzs7O0lBSUEsU0FBQSxNQUFBO01BQ0EsT0FBQSxNQUFBLElBQUE7U0FDQSxLQUFBLGNBQUEsZUFBQSxjQUFBO1NBQ0EsS0FBQSxVQUFBLFFBQUE7VUFDQSxJQUFBLElBQUEsSUFBQSxHQUFBLFFBQUEsUUFBQSxPQUFBLEtBQUEsS0FBQTtZQUNBLE1BQUEsYUFBQSxPQUFBLE1BQUE7WUFDQSxNQUFBLFdBQUEsT0FBQSxNQUFBOzs7VUFHQSxPQUFBOzs7Ozs7QUMzQkEsQ0FBQSxZQUFBO0VBQ0E7OztFQUVBO0tBQ0EsT0FBQTtLQUNBLFFBQUEsaUJBQUE7OztFQUdBLFNBQUEsY0FBQSxTQUFBLGNBQUEsSUFBQSxPQUFBOztJQUVBLElBQUEsS0FBQTs7SUFFQSxHQUFBLGdCQUFBO0lBQ0EsR0FBQSxpQkFBQTtJQUNBLEdBQUEsaUJBQUE7SUFDQSxHQUFBLHVCQUFBO0lBQ0EsR0FBQSxXQUFBO0lBQ0EsR0FBQSxnQkFBQTtJQUNBLEdBQUEsa0JBQUE7SUFDQSxHQUFBLDJCQUFBOzs7O0lBSUEsU0FBQSxjQUFBO0lBQ0E7TUFDQSxJQUFBLFVBQUE7O01BRUEsS0FBQSxJQUFBLElBQUEsR0FBQSxRQUFBLFFBQUEsT0FBQSxLQUFBLEtBQUE7UUFDQSxJQUFBLGdCQUFBO1FBQ0EsS0FBQSxJQUFBLElBQUEsR0FBQSxPQUFBLE9BQUEsTUFBQSxNQUFBLEtBQUEsS0FBQTtVQUNBLEtBQUEsSUFBQSxJQUFBLEdBQUEsTUFBQSxNQUFBLEtBQUEsTUFBQSxLQUFBLEtBQUE7WUFDQSxJQUFBLElBQUEsVUFBQTtjQUNBLElBQUEsT0FBQSxJQUFBLEtBQUEsSUFBQTtjQUNBLEdBQUEsR0FBQSxLQUFBLGdCQUFBLEtBQUEsYUFBQTtnQkFDQSxLQUFBLFdBQUEsS0FBQSxlQUFBLEtBQUE7OztjQUdBLGNBQUEsS0FBQTs7Ozs7UUFLQSxRQUFBLEtBQUE7VUFDQSxLQUFBLE1BQUE7VUFDQSxVQUFBLE1BQUE7VUFDQSxVQUFBLE1BQUE7VUFDQSxlQUFBO1VBQ0EsVUFBQSxNQUFBOzs7O01BSUEsT0FBQTs7O0lBR0EsU0FBQSxnQkFBQSxVQUFBO01BQ0EsSUFBQSxnQkFBQSxNQUFBLElBQUE7O01BRUEsT0FBQSxRQUFBLFFBQUEsVUFBQSxVQUFBLFNBQUE7UUFDQSxRQUFBLFlBQUEsUUFBQSxrQkFBQSxNQUFBLFFBQUEsZ0JBQUEsT0FBQTs7UUFFQSxJQUFBLGVBQUE7VUFDQSxJQUFBLGVBQUEsUUFBQSxVQUFBLGVBQUEsQ0FBQSxJQUFBLFFBQUEsS0FBQTs7VUFFQSxJQUFBLGNBQUE7WUFDQSxRQUFBLGtCQUFBLGFBQUE7WUFDQSxRQUFBLG1CQUFBLGFBQUE7Ozs7UUFJQSxRQUFBLFFBQUE7O1FBRUEsSUFBQSxVQUFBLFFBQUEsU0FBQSxPQUFBLFVBQUEsU0FBQTtVQUNBLE9BQUEsUUFBQSxtQkFBQSxRQUFBLG9CQUFBLFFBQUE7V0FDQTs7UUFFQSxJQUFBLFdBQUEsUUFBQSxrQkFBQTtVQUNBLFFBQUEsUUFBQSxRQUFBLFFBQUEsUUFBQTs7Ozs7SUFLQSxTQUFBLFNBQUEsS0FBQSxRQUFBLFFBQUE7TUFDQSxFQUFBLFVBQUE7UUFDQSxRQUFBO1FBQ0EsUUFBQTtTQUNBLE9BQUEsUUFBQSxTQUFBLFFBQUE7OztJQUdBLFNBQUEseUJBQUEsT0FBQTtNQUNBLElBQUEsT0FBQSxRQUFBLEtBQUE7O01BRUEsS0FBQSxXQUFBOztNQUVBLFFBQUEsUUFBQSxLQUFBLFVBQUEsVUFBQSxTQUFBO1FBQ0EsSUFBQSxRQUFBLG1CQUFBLFFBQUEsa0JBQUE7VUFDQSxLQUFBLFNBQUEsS0FBQTtZQUNBLElBQUEsUUFBQTtZQUNBLE9BQUEsUUFBQTs7Ozs7TUFLQSxPQUFBLEtBQUE7O01BRUEsT0FBQTs7O0lBR0EsU0FBQSxjQUFBLE1BQUE7TUFDQSxPQUFBLEtBQUE7OztJQUdBLFNBQUEsZUFBQSxNQUFBO01BQ0EsT0FBQSxHQUFBLE9BQUEsS0FBQTs7O0lBR0EsU0FBQSxxQkFBQSxRQUFBO01BQ0EsSUFBQSxPQUFBLFFBQUE7UUFDQSxLQUFBLElBQUEsSUFBQSxHQUFBLFFBQUEsUUFBQSxPQUFBLEtBQUEsS0FBQTtVQUNBLGFBQUEsTUFBQSxNQUFBOzs7O01BSUEsT0FBQTs7O0lBR0EsU0FBQSxVQUFBLE9BQUEsTUFBQTtNQUNBLEtBQUEsU0FBQTtNQUNBLEtBQUEsV0FBQTs7TUFFQSxPQUFBLFFBQUEsVUFBQSxPQUFBLFVBQUEsS0FBQTtRQUNBLE9BQUEsT0FBQSxNQUFBLEtBQUEsT0FBQSxJQUFBLFdBQUE7U0FDQTs7O0lBR0EsU0FBQSxVQUFBLFFBQUEsU0FBQTtNQUNBLElBQUEsSUFBQSxJQUFBLEdBQUEsUUFBQSxRQUFBLE9BQUEsS0FBQSxLQUFBO1FBQ0EsR0FBQSxNQUFBLE9BQUEsU0FBQTtVQUNBLE9BQUEsUUFBQSxLQUFBOzs7OztJQUtBLFNBQUEsZUFBQSxRQUFBLE1BQUE7Ozs7Ozs7Ozs7Ozs7OztNQWVBLElBQUEsUUFBQSxVQUFBLFFBQUEsS0FBQTs7TUFFQSxLQUFBLFFBQUE7TUFDQSxLQUFBLFFBQUE7O01BRUEsSUFBQSxZQUFBLFFBQUEsS0FBQSxNQUFBOzs7TUFHQSxJQUFBLFVBQUEsY0FBQSxHQUFBO1FBQ0EsVUFBQSxJQUFBLEdBQUE7YUFDQSxJQUFBLFVBQUEsY0FBQSxHQUFBO1FBQ0EsVUFBQSxJQUFBLEdBQUE7Ozs7TUFJQSxLQUFBLElBQUEsWUFBQSxXQUFBLGVBQUEsSUFBQSxLQUFBLGFBQUEsYUFBQSxNQUFBLFdBQUEsWUFBQSxJQUFBLEtBQUEsQ0FBQSxJQUFBLEtBQUEsZUFBQSxRQUFBLENBQUEsSUFBQSxLQUFBLGVBQUEsWUFBQSxLQUFBLFlBQUEsSUFBQSxLQUFBLFlBQUE7O1FBRUEsSUFBQSxhQUFBLE9BQUEsV0FBQSxZQUFBOzs7UUFHQSxlQUFBLElBQUEsS0FBQTtRQUNBLGFBQUEsUUFBQSxhQUFBLFlBQUE7OztRQUdBLElBQUEsZUFBQSxJQUFBLEtBQUE7UUFDQSxhQUFBLFFBQUEsYUFBQSxZQUFBO1FBQ0EsSUFBQSxZQUFBLElBQUEsS0FBQTs7UUFFQSxJQUFBLE9BQUE7VUFDQSxRQUFBLElBQUEsS0FBQTtVQUNBLFFBQUEsSUFBQSxLQUFBO1VBQ0EsVUFBQTtVQUNBLE9BQUE7VUFDQSxPQUFBO1VBQ0EsT0FBQTtVQUNBLGtCQUFBOzs7O1FBSUEsSUFBQSxZQUFBLE1BQUEsVUFBQTtVQUNBLFlBQUEsTUFBQTs7O1FBR0EsSUFBQSxlQUFBLFdBQUE7VUFDQSxLQUFBLElBQUEsUUFBQSxJQUFBLEtBQUEsZUFBQSxRQUFBLFdBQUEsTUFBQSxRQUFBLE1BQUEsWUFBQSxJQUFBO1lBQ0EsS0FBQSxNQUFBLEtBQUE7Y0FDQSxNQUFBLFFBQUEsS0FBQTtjQUNBLE9BQUE7Y0FDQSxVQUFBO2NBQ0EsT0FBQTs7Ozs7UUFLQSxLQUFBLElBQUEsT0FBQSxJQUFBLEtBQUEsWUFBQSxRQUFBLFdBQUEsS0FBQSxRQUFBLEtBQUEsWUFBQSxJQUFBOztVQUVBLElBQUEsU0FBQSxRQUFBLFVBQUEsTUFBQSxrQkFBQSxVQUFBLGVBQUE7Y0FDQSxnQkFBQSxPQUFBLGNBQUEsTUFBQTtjQUNBLE9BQUEsY0FBQSxtQkFBQSxLQUFBOztZQUVBLFFBQUEsQ0FBQSxPQUFBLFNBQUEsS0FBQSxRQUFBOztVQUVBLEtBQUEsTUFBQSxLQUFBO1lBQ0EsTUFBQSxRQUFBLEtBQUE7WUFDQSxPQUFBO1lBQ0EsVUFBQTtZQUNBLE9BQUEsUUFBQSxVQUFBLEtBQUEsTUFBQSxZQUFBLFFBQUE7Ozs7UUFJQSxJQUFBLGVBQUEsV0FBQTtVQUNBLEtBQUEsT0FBQSxJQUFBLEtBQUEsWUFBQSxLQUFBLFFBQUEsS0FBQSxZQUFBLElBQUEsUUFBQSxjQUFBLEtBQUEsUUFBQSxLQUFBLFlBQUEsSUFBQTtZQUNBLEtBQUEsTUFBQSxLQUFBO2NBQ0EsTUFBQSxRQUFBLEtBQUE7Y0FDQSxPQUFBO2NBQ0EsVUFBQTtjQUNBLE9BQUE7Ozs7O1FBS0EsSUFBQSxJQUFBLElBQUEsR0FBQSxZQUFBLFlBQUEsS0FBQSxNQUFBLEtBQUEsS0FBQTtVQUNBLEdBQUEsVUFBQSxTQUFBLENBQUEsVUFBQSxPQUFBO1lBQ0EsS0FBQSxRQUFBO1lBQ0E7Ozs7UUFJQSxLQUFBLE1BQUEsS0FBQTs7Ozs7O0FDclBBLENBQUEsWUFBQTtFQUNBOztFQUVBO0tBQ0EsT0FBQTtLQUNBLFFBQUEsZ0JBQUE7O0VBRUEsYUFBQSxVQUFBLENBQUEsV0FBQSxNQUFBLFNBQUEsZ0JBQUEsZ0JBQUEsU0FBQSxpQkFBQSxZQUFBOzs7RUFHQSxTQUFBLGFBQUEsU0FBQSxJQUFBLE9BQUEsY0FBQSxjQUFBLE9BQUEsZUFBQSxVQUFBLFlBQUE7O0lBRUEsSUFBQSxLQUFBOztJQUVBLEdBQUEsYUFBQSxNQUFBLElBQUE7O0lBRUEsSUFBQSxNQUFBLFNBQUEsWUFBQTtNQUNBLEdBQUEsR0FBQSxjQUFBLFdBQUEsWUFBQTtRQUNBLEdBQUEsYUFBQSxXQUFBO1FBQ0EsTUFBQSxJQUFBLGVBQUEsR0FBQTs7UUFFQSxNQUFBLE9BQUE7UUFDQSxNQUFBLE9BQUE7OztNQUdBLEdBQUEsU0FBQSxNQUFBLElBQUE7TUFDQSxHQUFBLFFBQUEsTUFBQSxJQUFBOztNQUVBLElBQUEsQ0FBQSxHQUFBLFFBQUE7UUFDQSxHQUFBLFNBQUE7UUFDQSxHQUFBLFFBQUE7OztNQUdBLEdBQUEsV0FBQSxHQUFBLElBQUEsQ0FBQSxhQUFBLE9BQUEsYUFBQSxRQUFBLEtBQUEsVUFBQSxNQUFBO1FBQ0EsR0FBQSxXQUFBLEtBQUE7UUFDQSxHQUFBLFNBQUEsS0FBQTs7OztJQUlBLEdBQUEsV0FBQTs7SUFFQSxHQUFBLFFBQUE7SUFDQSxHQUFBLE1BQUE7SUFDQSxHQUFBLE1BQUE7SUFDQSxHQUFBLE1BQUE7SUFDQSxHQUFBLFNBQUE7SUFDQSxHQUFBLFNBQUE7SUFDQSxHQUFBLFlBQUE7SUFDQSxHQUFBLFFBQUE7SUFDQSxHQUFBLFdBQUE7SUFDQSxHQUFBLGVBQUE7Ozs7SUFJQSxTQUFBLFdBQUE7TUFDQSxPQUFBLE1BQUEsSUFBQSxpQkFBQSxLQUFBLGNBQUEsZUFBQSxLQUFBLGNBQUE7OztJQUdBLFNBQUEsYUFBQSxVQUFBO01BQ0EsSUFBQSxlQUFBLEdBQUE7O01BRUEsR0FBQSxTQUFBLEtBQUEsWUFBQTtRQUNBLE1BQUEsSUFBQSxZQUFBOztRQUVBLGFBQUEsUUFBQTs7O01BR0EsT0FBQSxhQUFBOzs7SUFHQSxTQUFBLFFBQUE7TUFDQSxPQUFBLEdBQUEsT0FBQTs7O0lBR0EsU0FBQSxRQUFBO01BQ0EsSUFBQSxTQUFBLFFBQUEsS0FBQSxHQUFBOztNQUVBLElBQUEsZUFBQSxHQUFBOztNQUVBLEdBQUEsU0FBQSxLQUFBLFlBQUE7UUFDQSxNQUFBLE9BQUE7UUFDQSxNQUFBLE9BQUE7UUFDQSxNQUFBLE9BQUE7O1FBRUEsYUFBQSxRQUFBOzs7TUFHQSxPQUFBLGFBQUE7OztJQUdBLFNBQUEsVUFBQSxPQUFBO01BQ0EsSUFBQSxNQUFBOztNQUVBLEtBQUEsSUFBQSxJQUFBLEdBQUEsT0FBQSxPQUFBLE1BQUEsTUFBQSxLQUFBLEtBQUE7UUFDQSxJQUFBLEtBQUEsVUFBQTtVQUNBLE9BQUEsTUFBQSxNQUFBO2VBQ0E7VUFDQSxLQUFBLElBQUEsSUFBQSxHQUFBLFVBQUEsVUFBQSxLQUFBLE1BQUEsS0FBQSxLQUFBO1lBQ0EsR0FBQSxRQUFBLFVBQUE7Y0FDQSxPQUFBLE1BQUEsTUFBQTs7Ozs7O01BTUEsUUFBQSxRQUFBLE1BQUEsVUFBQSxVQUFBLFNBQUE7UUFDQSxPQUFBLFFBQUEsUUFBQSxRQUFBOzs7TUFHQSxPQUFBOzs7SUFHQSxTQUFBLFNBQUEsT0FBQSxVQUFBLFFBQUE7TUFDQSxJQUFBLFNBQUE7O01BRUEsSUFBQSxRQUFBLFFBQUEsVUFBQSxVQUFBLENBQUEsSUFBQSxNQUFBO01BQ0EsSUFBQSxFQUFBLE1BQUEsWUFBQSxNQUFBLFNBQUE7UUFDQSxPQUFBLEtBQUE7VUFDQSxPQUFBO1VBQ0EsU0FBQTs7OztNQUlBLElBQUEsUUFBQSxRQUFBLFVBQUEsUUFBQSxDQUFBLElBQUEsTUFBQTtNQUNBLElBQUEsRUFBQSxNQUFBLFlBQUEsTUFBQSxTQUFBO1FBQ0EsT0FBQSxLQUFBO1VBQ0EsT0FBQTtVQUNBLFNBQUE7Ozs7O01BS0EsSUFBQSxXQUFBO01BQ0EsS0FBQSxJQUFBLElBQUEsR0FBQSxNQUFBLE1BQUEsVUFBQSxPQUFBLE1BQUEsTUFBQSxLQUFBLEtBQUE7UUFDQSxLQUFBLElBQUEsSUFBQSxHQUFBLE1BQUEsTUFBQSxLQUFBLE1BQUEsS0FBQSxLQUFBO1VBQ0EsSUFBQSxJQUFBLFVBQUE7WUFDQSxXQUFBO1lBQ0E7Ozs7UUFJQSxJQUFBLFVBQUE7VUFDQTs7OztNQUlBLElBQUEsQ0FBQSxVQUFBO1FBQ0EsT0FBQSxLQUFBO1VBQ0EsT0FBQTtVQUNBLFNBQUE7Ozs7TUFJQSxPQUFBLE9BQUE7OztJQUdBLFNBQUEsTUFBQTtNQUNBLE9BQUEsR0FBQSxJQUFBLENBQUEsTUFBQSxLQUFBLFlBQUE7UUFDQSxJQUFBLFNBQUEsUUFBQSxLQUFBLEdBQUE7O1FBRUEsSUFBQSxlQUFBLEdBQUE7O1FBRUEsR0FBQSxTQUFBLEtBQUEsWUFBQTtVQUNBLEtBQUEsSUFBQSxJQUFBLEdBQUEsUUFBQSxRQUFBLE9BQUEsS0FBQSxLQUFBO1lBQ0EsU0FBQTtZQUNBLFNBQUE7OztVQUdBLGFBQUEsUUFBQTs7O1FBR0EsT0FBQSxhQUFBOzs7O0lBSUEsU0FBQSxJQUFBLE9BQUE7TUFDQSxJQUFBLGVBQUEsR0FBQTs7TUFFQSxHQUFBLFNBQUEsS0FBQSxZQUFBO1FBQ0EsTUFBQSxNQUFBLEdBQUE7UUFDQSxHQUFBLE9BQUEsS0FBQTtRQUNBOztRQUVBLGFBQUE7VUFDQSxRQUFBLEtBQUE7Ozs7O01BS0EsT0FBQSxhQUFBOzs7SUFHQSxTQUFBLElBQUEsSUFBQTtNQUNBLElBQUEsZUFBQSxHQUFBOztNQUVBLEdBQUEsU0FBQSxLQUFBLFlBQUE7UUFDQSxJQUFBLFFBQUEsUUFBQSxLQUFBLFFBQUEsVUFBQSxHQUFBLFFBQUEsQ0FBQSxLQUFBLEtBQUE7O1FBRUEsU0FBQTtRQUNBLFNBQUE7O1FBRUEsYUFBQSxRQUFBOzs7O01BSUEsT0FBQSxhQUFBOzs7SUFHQSxTQUFBLE9BQUEsSUFBQSxPQUFBO01BQ0EsSUFBQSxlQUFBLEdBQUE7O01BRUEsSUFBQSxTQUFBLFFBQUEsVUFBQSxHQUFBLFFBQUEsQ0FBQSxLQUFBLEtBQUE7O01BRUEsT0FBQSxXQUFBLE1BQUE7TUFDQSxPQUFBLFFBQUEsTUFBQTtNQUNBLE9BQUEsV0FBQSxNQUFBO01BQ0EsT0FBQSxRQUFBLE1BQUE7TUFDQSxPQUFBLFFBQUEsTUFBQTs7TUFFQTs7TUFFQSxhQUFBLFFBQUE7O01BRUEsT0FBQSxhQUFBOzs7SUFHQSxTQUFBLE9BQUEsSUFBQTtNQUNBLElBQUEsZUFBQSxHQUFBOztNQUVBLElBQUEsUUFBQSxRQUFBLFVBQUEsR0FBQSxRQUFBLENBQUEsS0FBQSxLQUFBOztNQUVBLEdBQUEsT0FBQSxPQUFBLEdBQUEsT0FBQSxRQUFBLFFBQUE7O01BRUE7O01BRUEsYUFBQSxRQUFBOztNQUVBLE9BQUEsYUFBQTs7O0lBR0EsU0FBQSxhQUFBO01BQ0EsTUFBQSxJQUFBLFVBQUEsR0FBQTtNQUNBLE1BQUEsSUFBQSxTQUFBLEdBQUE7OztJQUdBLFNBQUEsU0FBQSxPQUFBO01BQ0EsTUFBQSxRQUFBLFFBQUEsVUFBQSxHQUFBLFVBQUEsQ0FBQSxJQUFBLE1BQUEsV0FBQTs7O0lBR0EsU0FBQSxTQUFBLE9BQUE7TUFDQSxNQUFBLFFBQUEsUUFBQSxVQUFBLEdBQUEsUUFBQSxDQUFBLElBQUEsTUFBQSxXQUFBOzs7Ozs7QUMxUEEsQ0FBQSxZQUFBO0VBQ0E7O0VBRUE7S0FDQSxPQUFBO0tBQ0EsUUFBQSxrQkFBQTs7RUFFQSxRQUFBLFVBQUEsQ0FBQSxTQUFBLGlCQUFBOzs7RUFHQSxTQUFBLFFBQUEsT0FBQSxlQUFBLFlBQUE7O0lBRUEsSUFBQSxLQUFBOztJQUVBLEdBQUEsTUFBQTtJQUNBLEdBQUEsU0FBQTtJQUNBLEdBQUEsWUFBQTs7OztJQUlBLFNBQUEsTUFBQTtNQUNBLE9BQUEsTUFBQSxJQUFBLDBCQUFBLFdBQUE7U0FDQSxLQUFBLGNBQUE7U0FDQSxLQUFBLFVBQUEsTUFBQTtVQUNBLEtBQUEsUUFBQSxLQUFBLEtBQUE7VUFDQSxPQUFBOzs7O0lBSUEsU0FBQSxPQUFBLE1BQUE7TUFDQSxPQUFBLE1BQUEsSUFBQSwwQkFBQSxXQUFBLFlBQUE7U0FDQSxLQUFBLGNBQUE7U0FDQSxLQUFBLFVBQUEsTUFBQTtVQUNBLEtBQUEsUUFBQSxLQUFBLEtBQUE7VUFDQSxPQUFBOzs7O0lBSUEsU0FBQSxZQUFBO01BQ0EsT0FBQSxNQUFBLElBQUEsa0JBQUEsS0FBQSxjQUFBOzs7O0FBSUEiLCJmaWxlIjoiYWRtaW4tYXBwLmpzIiwic291cmNlc0NvbnRlbnQiOlsiKGZ1bmN0aW9uICgpIHtcbiAgJ3VzZSBzdHJpY3QnO1xuXG4gIGFuZ3VsYXIubW9kdWxlKCdib29raW5nJywgW1xuICAgICduZ1Jlc291cmNlJyxcbiAgICAndWkucm91dGVyJyxcbiAgICAndG9hc3RlcicsXG4gICAgJ2FuZ3VsYXItc3RvcmFnZScsXG4gICAgJ3VpLmJvb3RzdHJhcCcsXG4gICAgJ3VpLnNlbGVjdCcsXG4gICAgJ25nU2FuaXRpemUnXG4gIF0pO1xuXG59KSgpO1xuIiwiKGZ1bmN0aW9uKCkge1xuICAndXNlIHN0cmljdCc7XG5cbiAgYW5ndWxhclxuICAgIC5tb2R1bGUoJ2Jvb2tpbmcnKVxuICAgIC5jb25maWcoY29uZmlnKTtcblxuICBjb25maWcuJGluamVjdCA9IFsnJGh0dHBQcm92aWRlciddO1xuXG4gIGZ1bmN0aW9uIGNvbmZpZygkaHR0cFByb3ZpZGVyKSB7XG5cbiAgICAkaHR0cFByb3ZpZGVyLmludGVyY2VwdG9ycy5wdXNoKCdzbEludGVyY2VwdG9yJyk7XG4gIH1cblxufSkoKTtcbiIsIihmdW5jdGlvbiAoKSB7XG5cbiAgJ3VzZSBzdHJpY3QnO1xuXG4gIGFuZ3VsYXJcbiAgICAubW9kdWxlKCdib29raW5nJylcbiAgICAuY29uc3RhbnQoJ2FjY29zdHMnLCBbJ01yJywgJ01pc3MnLCAnTXJzJywgJ01zJywgJ0RyJ10pXG4gICAgLmNvbnN0YW50KCdib29sZWFuVmFsdWVzJywgW1xuICAgICAge1xuICAgICAgICB0aXRsZTogJ1llcycsXG4gICAgICAgIHZhbHVlOiAxXG4gICAgICB9LFxuICAgICAge1xuICAgICAgICB0aXRsZTogJ05vJyxcbiAgICAgICAgdmFsdWU6IDBcbiAgICAgIH1cbiAgICBdKVxuICAgIC5jb25zdGFudCgncGF5bWVudFR5cGVzJywgW1xuICAgICAge1xuICAgICAgICB0aXRsZTogJ1BheSBPbmxpbmUgYnkgQ3JlZGl0IC8gRGViaXQgQ2FyZCcsXG4gICAgICAgIHZhbHVlOiAnb25saW5lJ1xuICAgICAgfSxcbiAgICAgIHtcbiAgICAgICAgdGl0bGU6ICdVc2luZyBjaGlsZGNhcmUgdm91Y2hlcnMnLFxuICAgICAgICB2YWx1ZTogJ3ZvdWNoZXInXG4gICAgICB9LFxuICAgICAgLy97XG4gICAgICAvLyAgdGl0bGU6ICdCeSBjaGVxdWUnLFxuICAgICAgLy8gIHZhbHVlOiAnY2hlcXVlJ1xuICAgICAgLy99XG4gICAgXSk7XG5cbn0pKCk7XG4iLCIoZnVuY3Rpb24gKCkge1xuICAndXNlIHN0cmljdCc7XG5cbiAgYW5ndWxhclxuICAgIC5tb2R1bGUoJ2Jvb2tpbmcnKVxuICAgIC5mYWN0b3J5KCdzbEludGVyY2VwdG9yJywgc2xJbnRlcmNlcHRvcik7XG5cbiAgc2xJbnRlcmNlcHRvci4kaW5qZWN0ID0gWydhbGVydFNlcnZpY2UnLCAnJHEnXTtcblxuICBmdW5jdGlvbiBzbEludGVyY2VwdG9yKGFsZXJ0U2VydmljZSwgJHEpIHtcbiAgICByZXR1cm4ge1xuICAgICAgcmVzcG9uc2VFcnJvcjogZnVuY3Rpb24gKHJlcykge1xuICAgICAgICB2YXIgc3RhdHVzID0gcmVzLnN0YXR1cyxcbiAgICAgICAgICBmZXRjaGVkUmVzcG9uc2UgPSByZXMuZGF0YTtcblxuICAgICAgICBpZihmZXRjaGVkUmVzcG9uc2UgPT09ICdVbmF1dGhvcml6ZWQuJykge1xuICAgICAgICAgIGFsZXJ0U2VydmljZS5lcnJvcignWW91ciBzZXNzaW9uIHdhcyBleHBpcmVkLiBQbGVhc2UgbG9naW4gYWdhaW4uJyk7XG5cbiAgICAgICAgICB3aW5kb3cubG9jYXRpb24uaHJlZiA9ICcvbG9naW4nO1xuICAgICAgICB9IGVsc2UgaWYoZmV0Y2hlZFJlc3BvbnNlLmxlbmd0aCA9PT0gdW5kZWZpbmVkIHx8IGZldGNoZWRSZXNwb25zZS5sZW5ndGggPCAyNTYpIHtcbiAgICAgICAgICBhbGVydFNlcnZpY2Uuc2hvd1Jlc3BvbnNlRXJyb3IoZmV0Y2hlZFJlc3BvbnNlLCB0aXRsZUJ5Q29kZVN0YXR1cyhzdGF0dXMpKTtcbiAgICAgICAgfSBlbHNlICB7XG4gICAgICAgICAgYWxlcnRTZXJ2aWNlLnNob3dSZXNwb25zZUVycm9yKFsnU2VydmVyIEVycm9yJ10sIHRpdGxlQnlDb2RlU3RhdHVzKHN0YXR1cykpO1xuICAgICAgICB9XG5cbiAgICAgICAgcmV0dXJuICRxLnJlamVjdChmZXRjaGVkUmVzcG9uc2UpO1xuICAgICAgfVxuICAgIH07XG5cbiAgICBmdW5jdGlvbiB0aXRsZUJ5Q29kZVN0YXR1cyhzdGF0dXMpIHtcbiAgICAgIHZhciB0aXRsZTtcblxuICAgICAgc3dpdGNoIChzdGF0dXMpIHtcbiAgICAgICAgY2FzZSA0MjI6XG4gICAgICAgICAgdGl0bGUgPSAnVmFsaWRhdGlvbiBGYWlsISc7XG4gICAgICAgICAgYnJlYWs7XG4gICAgICAgIGNhc2UgNDAzOlxuICAgICAgICAgIHRpdGxlID0gJ0FjY2VzcyBEZW5pZWQhJztcbiAgICAgICAgICBicmVhaztcbiAgICAgICAgY2FzZSA1MDA6XG4gICAgICAgICAgdGl0bGUgPSAnU29ycnkuIFNlcnZlciBFcnJvci4nO1xuICAgICAgICAgIGJyZWFrO1xuICAgICAgfVxuXG4gICAgICByZXR1cm4gdGl0bGU7XG4gICAgfVxuICB9XG59KSgpO1xuIiwiKGZ1bmN0aW9uICgpIHtcbiAgJ3VzZSBzdHJpY3QnO1xuXG4gIGFuZ3VsYXJcbiAgICAubW9kdWxlKCdib29raW5nJylcbiAgICAuY29uZmlnKGNvbmZpZyk7XG5cbiAgY29uZmlnLiRpbmplY3QgPSBbJyRzdGF0ZVByb3ZpZGVyJywgJyR1cmxSb3V0ZXJQcm92aWRlciddO1xuXG4gIGZ1bmN0aW9uIGNvbmZpZygkc3RhdGVQcm92aWRlciwgJHVybFJvdXRlclByb3ZpZGVyKSB7XG5cbiAgICAkdXJsUm91dGVyUHJvdmlkZXIub3RoZXJ3aXNlKCcvcHJvZmlsZS9zaG93Jyk7XG5cbiAgICB2YXIgdWlWaWV3ID0gJzx1aS12aWV3PjwvdWktdmlldz4nO1xuXG4gICAgdmFyIGRlcGVuZGVjaWVzID0ge1xuICAgICAgb3JkZXJzOiBbJ29yZGVyU2VydmljZScsIGZ1bmN0aW9uIChvcmRlclNlcnZpY2UpIHtcbiAgICAgICAgcmV0dXJuIG9yZGVyU2VydmljZS5hbGwoKTtcbiAgICAgIH1dLFxuICAgICAgb3JkZXI6IFsnb3JkZXJTZXJ2aWNlJywgJyRzdGF0ZVBhcmFtcycsIGZ1bmN0aW9uIChvcmRlclNlcnZpY2UsICRzdGF0ZVBhcmFtcykge1xuICAgICAgICByZXR1cm4gb3JkZXJTZXJ2aWNlLmdldCgkc3RhdGVQYXJhbXMub3JkZXJJZCk7XG4gICAgICB9XSxcbiAgICAgIGNoaWxkcmVuOiBbJ2NoaWxkU2VydmljZScsIGZ1bmN0aW9uIChjaGlsZFNlcnZpY2UpIHtcbiAgICAgICAgcmV0dXJuIGNoaWxkU2VydmljZS5hbGwoKTtcbiAgICAgIH1dLFxuICAgICAgY2hpbGQ6IFsnY2hpbGRTZXJ2aWNlJywgJyRzdGF0ZVBhcmFtcycsIGZ1bmN0aW9uIChjaGlsZFNlcnZpY2UsICRzdGF0ZVBhcmFtcykge1xuICAgICAgICByZXR1cm4gY2hpbGRTZXJ2aWNlLmdldCgkc3RhdGVQYXJhbXMuY2hpbGRJZCk7XG4gICAgICB9XSxcbiAgICAgIGV2ZW50czogWydldmVudFNlcnZpY2UnLCBmdW5jdGlvbiAoZXZlbnRTZXJ2aWNlKSB7XG4gICAgICAgIHJldHVybiBldmVudFNlcnZpY2UuYWxsKCk7XG4gICAgICB9XSxcbiAgICAgIHN3aW1PcHRpb25zOiBbJ2NoaWxkU2VydmljZScsIGZ1bmN0aW9uKGNoaWxkU2VydmljZSkge1xuICAgICAgICByZXR1cm4gY2hpbGRTZXJ2aWNlLnN3aW1PcHRpb25zKCk7XG4gICAgICB9XSxcbiAgICAgIHllYXJzSW5TY2hvb2w6IFsnY2hpbGRTZXJ2aWNlJywgZnVuY3Rpb24oY2hpbGRTZXJ2aWNlKSB7XG4gICAgICAgIHJldHVybiBjaGlsZFNlcnZpY2UueWVhcnNJblNjaG9vbCgpO1xuICAgICAgfV0sXG4gICAgICBwcm9kdWN0czogWydvcmRlclNlcnZpY2UnLCBmdW5jdGlvbihvcmRlclNlcnZpY2UpIHtcbiAgICAgICAgcmV0dXJuIG9yZGVyU2VydmljZS5wcm9kdWN0cygpO1xuICAgICAgfV0sXG4gICAgICBjb3VudHJpZXM6IFsncHJvZmlsZVNlcnZpY2UnLCBmdW5jdGlvbiAocHJvZmlsZVNlcnZpY2UpIHtcbiAgICAgICAgcmV0dXJuIHByb2ZpbGVTZXJ2aWNlLmNvdW50cmllcygpO1xuICAgICAgfV0sXG4gICAgICBwcm9maWxlOiBbJ3Byb2ZpbGVTZXJ2aWNlJywgZnVuY3Rpb24gKHByb2ZpbGVTZXJ2aWNlKSB7XG4gICAgICAgIHJldHVybiBwcm9maWxlU2VydmljZS5nZXQoKTtcbiAgICAgIH1dXG4gICAgfTtcblxuICAgICRzdGF0ZVByb3ZpZGVyXG4gICAgICAuc3RhdGUoJ2Jvb2tpbmcnLCB7XG4gICAgICAgIGFic3RyYWN0OiB0cnVlLFxuICAgICAgICB0ZW1wbGF0ZVVybDogJy9hZG1pbi1ib29raW5nL3RlbXBsYXRlLmh0bWwnXG4gICAgICB9KVxuXG5cbiAgICAgIC5zdGF0ZSgnYm9va2luZy5wcm9maWxlJywge1xuICAgICAgICBhYnN0cmFjdDogdHJ1ZSxcbiAgICAgICAgdXJsOiAnL3Byb2ZpbGUnLFxuICAgICAgICBkYXRhOiB7XG4gICAgICAgICAgdGFiOiAncHJvZmlsZSdcbiAgICAgICAgfSxcbiAgICAgICAgdGVtcGxhdGU6IHVpVmlldyxcbiAgICAgICAgY29udHJvbGxlcjogJ1Byb2ZpbGVDb250cm9sbGVyJyxcbiAgICAgICAgY29udHJvbGxlckFzOiAnUHJvZmlsZUN0cmwnLFxuICAgICAgICByZXNvbHZlOiB7XG4gICAgICAgICAgcHJvZmlsZTogZGVwZW5kZWNpZXMucHJvZmlsZSxcbiAgICAgICAgICBjb3VudHJpZXM6IGRlcGVuZGVjaWVzLmNvdW50cmllc1xuICAgICAgICB9XG4gICAgICB9KVxuICAgICAgLnN0YXRlKCdib29raW5nLnByb2ZpbGUuc2hvdycsIHtcbiAgICAgICAgdXJsOiAnL3Nob3cnLFxuICAgICAgICB0ZW1wbGF0ZVVybDogJy9hZG1pbi1ib29raW5nL3Byb2ZpbGUvc2hvdy5odG1sJyxcbiAgICAgIH0pXG4gICAgICAuc3RhdGUoJ2Jvb2tpbmcucHJvZmlsZS5lZGl0Jywge1xuICAgICAgICB1cmw6ICcvZWRpdCcsXG4gICAgICAgIHRlbXBsYXRlVXJsOiAnL2FkbWluLWJvb2tpbmcvcHJvZmlsZS9lZGl0Lmh0bWwnLFxuICAgICAgfSlcblxuXG4gICAgICAuc3RhdGUoJ2Jvb2tpbmcuY2hpbGRyZW4nLCB7XG4gICAgICAgIGFic3RyYWN0OiB0cnVlLFxuICAgICAgICB1cmw6ICcvY2hpbGRyZW4nLFxuICAgICAgICB0ZW1wbGF0ZTogdWlWaWV3LFxuICAgICAgICBkYXRhOiB7XG4gICAgICAgICAgdGFiOiAnY2hpbGRyZW4nXG4gICAgICAgIH0sXG4gICAgICB9KVxuICAgICAgLnN0YXRlKCdib29raW5nLmNoaWxkcmVuLmluZGV4Jywge1xuICAgICAgICB1cmw6ICcvaW5kZXgnLFxuICAgICAgICB0ZW1wbGF0ZVVybDogJy9hZG1pbi1ib29raW5nL2NoaWxkcmVuL2luZGV4Lmh0bWwnLFxuICAgICAgICBjb250cm9sbGVyOiAnQ2hpbGRyZW5Db250cm9sbGVyJyxcbiAgICAgICAgY29udHJvbGxlckFzOiAnQ2hpbGRyZW5DdHJsJyxcbiAgICAgICAgcmVzb2x2ZToge1xuICAgICAgICAgIGNoaWxkcmVuOiBkZXBlbmRlY2llcy5jaGlsZHJlblxuICAgICAgICB9XG4gICAgICB9KVxuICAgICAgLnN0YXRlKCdib29raW5nLmNoaWxkcmVuLmNyZWF0ZScsIHtcbiAgICAgICAgdXJsOiAnL2NyZWF0ZScsXG4gICAgICAgIHRlbXBsYXRlVXJsOiAnL2FkbWluLWJvb2tpbmcvY2hpbGRyZW4vY3JlYXRlLmh0bWwnLFxuICAgICAgICBjb250cm9sbGVyOiAnQ3JlYXRlQ2hpbGRDb250cm9sbGVyJyxcbiAgICAgICAgY29udHJvbGxlckFzOiAnQ3JlYXRlQ2hpbGRDdHJsJyxcbiAgICAgICAgcmVzb2x2ZToge1xuICAgICAgICAgIHN3aW1PcHRpb25zOiBkZXBlbmRlY2llcy5zd2ltT3B0aW9ucyxcbiAgICAgICAgICB5ZWFyc0luU2Nob29sOiBkZXBlbmRlY2llcy55ZWFyc0luU2Nob29sXG4gICAgICAgIH1cbiAgICAgIH0pXG4gICAgICAuc3RhdGUoJ2Jvb2tpbmcuY2hpbGRyZW4uc2hvdycsIHtcbiAgICAgICAgdXJsOiAnLzpjaGlsZElkL3Nob3cnLFxuICAgICAgICB0ZW1wbGF0ZVVybDogJy9hZG1pbi1ib29raW5nL2NoaWxkcmVuL3Nob3cuaHRtbCcsXG4gICAgICAgIGNvbnRyb2xsZXI6ICdTaG93Q2hpbGRDb250cm9sbGVyJyxcbiAgICAgICAgY29udHJvbGxlckFzOiAnU2hvd0NoaWxkQ3RybCcsXG4gICAgICAgIHJlc29sdmU6IHtcbiAgICAgICAgICBjaGlsZDogZGVwZW5kZWNpZXMuY2hpbGRcbiAgICAgICAgfVxuICAgICAgfSlcbiAgICAgIC5zdGF0ZSgnYm9va2luZy5jaGlsZHJlbi5lZGl0Jywge1xuICAgICAgICB1cmw6ICcvOmNoaWxkSWQvZWRpdCcsXG4gICAgICAgIHRlbXBsYXRlVXJsOiAnL2FkbWluLWJvb2tpbmcvY2hpbGRyZW4vZWRpdC5odG1sJyxcbiAgICAgICAgY29udHJvbGxlcjogJ0VkaXRDaGlsZENvbnRyb2xsZXInLFxuICAgICAgICBjb250cm9sbGVyQXM6ICdFZGl0Q2hpbGRDdHJsJyxcbiAgICAgICAgcmVzb2x2ZToge1xuICAgICAgICAgIGNoaWxkOiBkZXBlbmRlY2llcy5jaGlsZCxcbiAgICAgICAgICBzd2ltT3B0aW9uczogZGVwZW5kZWNpZXMuc3dpbU9wdGlvbnMsXG4gICAgICAgICAgeWVhcnNJblNjaG9vbDogZGVwZW5kZWNpZXMueWVhcnNJblNjaG9vbFxuICAgICAgICB9XG4gICAgICB9KVxuXG4gICAgICAuc3RhdGUoJ2Jvb2tpbmcub3JkZXJzJywge1xuICAgICAgICBhYnN0cmFjdDogdHJ1ZSxcbiAgICAgICAgdXJsOiAnL29yZGVycycsXG4gICAgICAgIHRlbXBsYXRlOiB1aVZpZXcsXG4gICAgICAgIGRhdGE6IHtcbiAgICAgICAgICB0YWI6ICdvcmRlcnMnXG4gICAgICAgIH0sXG4gICAgICB9KVxuICAgICAgLypcbiAgICAgICAuc3RhdGUoJ2Jvb2tpbmcub3JkZXJzLmluZGV4Jywge1xuICAgICAgIHVybDogJy9pbmRleCcsXG4gICAgICAgdGVtcGxhdGVVcmw6ICcvYWRtaW4tYm9va2luZy9vcmRlcnMvaW5kZXguaHRtbCcsXG4gICAgICAgY29udHJvbGxlcjogJ09yZGVyc0NvbnRyb2xsZXInLFxuICAgICAgIGNvbnRyb2xsZXJBczogJ09yZGVyc0N0cmwnLFxuICAgICAgIHJlc29sdmU6IHtcbiAgICAgICBvcmRlcnM6IGRlcGVuZGVjaWVzLm9yZGVyc1xuICAgICAgIH1cbiAgICAgICB9KVxuICAgICAgICovXG4gICAgICAuc3RhdGUoJ2Jvb2tpbmcub3JkZXJzLmNyZWF0ZScsIHtcbiAgICAgICAgdXJsOiAnL2NyZWF0ZScsXG4gICAgICAgIHRlbXBsYXRlVXJsOiAnL2FkbWluLWJvb2tpbmcvb3JkZXJzL2NyZWF0ZS5odG1sJyxcbiAgICAgICAgY29udHJvbGxlcjogJ0NyZWF0ZU9yZGVyQ29udHJvbGxlcicsXG4gICAgICAgIGNvbnRyb2xsZXJBczogJ0NyZWF0ZU9yZGVyQ3RybCcsXG4gICAgICAgIHJlc29sdmU6IHtcbiAgICAgICAgICBjaGlsZHJlbjogZGVwZW5kZWNpZXMuY2hpbGRyZW4sXG4gICAgICAgICAgZXZlbnRzOiBkZXBlbmRlY2llcy5ldmVudHMsXG4gICAgICAgICAgcHJvZHVjdHM6IGRlcGVuZGVjaWVzLnByb2R1Y3RzLFxuICAgICAgICAgIGNvdW50T3JkZXJzOiBbJ29yZGVyU2VydmljZScsIGZ1bmN0aW9uIChvcmRlclNlcnZpY2UpIHtcbiAgICAgICAgICAgIHJldHVybiBvcmRlclNlcnZpY2UuY291bnQoKTtcbiAgICAgICAgICB9XVxuICAgICAgICB9XG4gICAgICB9KVxuICAgICAgLypcbiAgICAgICAuc3RhdGUoJ2Jvb2tpbmcub3JkZXJzLnNob3cnLCB7XG4gICAgICAgdXJsOiAnLzpvcmRlcklkL3Nob3cnLFxuICAgICAgIHRlbXBsYXRlVXJsOiAnL2FkbWluLWJvb2tpbmcvb3JkZXJzL3Nob3cuaHRtbCcsXG4gICAgICAgY29udHJvbGxlcjogJ1Nob3dPcmRlckNvbnRyb2xsZXInLFxuICAgICAgIGNvbnRyb2xsZXJBczogJ1Nob3dPcmRlckN0cmwnLFxuICAgICAgIHJlc29sdmU6IHtcbiAgICAgICBvcmRlcjogZGVwZW5kZWNpZXMub3JkZXIsXG4gICAgICAgfVxuICAgICAgIH0pXG4gICAgICAgKi9cbiAgICAgIC8qXG4gICAgICAgLnN0YXRlKCdib29raW5nLm9yZGVycy5lZGl0Jywge1xuICAgICAgIHVybDogJy86b3JkZXJJZC9lZGl0JyxcbiAgICAgICB0ZW1wbGF0ZVVybDogJy9hZG1pbi1ib29raW5nL29yZGVycy9lZGl0Lmh0bWwnLFxuICAgICAgIGNvbnRyb2xsZXI6ICdFZGl0T3JkZXJDb250cm9sbGVyJyxcbiAgICAgICBjb250cm9sbGVyQXM6ICdFZGl0T3JkZXJDdHJsJyxcbiAgICAgICByZXNvbHZlOiB7XG4gICAgICAgb3JkZXI6IGRlcGVuZGVjaWVzLm9yZGVyLFxuICAgICAgIGNoaWxkcmVuOiBkZXBlbmRlY2llcy5jaGlsZHJlbixcbiAgICAgICBldmVudHM6IGRlcGVuZGVjaWVzLmV2ZW50c1xuICAgICAgIH1cbiAgICAgICB9KVxuICAgICAgICovXG4gICAgICAvLyBTaG93aW5nIHRvdGFsIGluZm8sIG5lZWQgc2VsZWN0IHBheW1lbnQgdHlwZSBhbmQgc3VibWl0IGFjdGlvblxuICAgICAgLy8gQXQgdGhlIGVuZCBjcmVhdGUgQm9va2luZyBhbmQgZ28gdG8gQWxsIEJvb2tpbmdzIHBhZ2Uobm90IFNQQSlcbiAgICAgIC5zdGF0ZSgnYm9va2luZy5jb21wbGV0ZScsIHtcbiAgICAgICAgdXJsOiAnL2NvbXBsZXRlJyxcbiAgICAgICAgdGVtcGxhdGVVcmw6ICcvYWRtaW4tYm9va2luZy9jb21wbGV0ZS9jb21wbGV0ZS5odG1sJyxcbiAgICAgICAgZGF0YToge1xuICAgICAgICAgIHRhYjogJ2NvbXBsZXRlJ1xuICAgICAgICB9LFxuICAgICAgICBjb250cm9sbGVyOiAnQ29tcGxldGVDb250cm9sbGVyJyxcbiAgICAgICAgY29udHJvbGxlckFzOiAnQ29tcGxldGVDdHJsJyxcbiAgICAgICAgcmVzb2x2ZToge1xuICAgICAgICAgIHByb2R1Y3RzOiBkZXBlbmRlY2llcy5wcm9kdWN0cyxcbiAgICAgICAgICBvcmRlcnM6IFsnb3JkZXJTZXJ2aWNlJywgZnVuY3Rpb24gKG9yZGVyU2VydmljZSkge1xuICAgICAgICAgICAgcmV0dXJuIG9yZGVyU2VydmljZS5hbGwoKTtcbiAgICAgICAgICB9XSxcbiAgICAgICAgICBwcmljZXM6IFsnb3JkZXJzJywgJ2Jvb2tpbmdTZXJ2aWNlJywgJ2hlbHBlclNlcnZpY2UnLCBmdW5jdGlvbihvcmRlcnMsIGJvb2tpbmdTZXJ2aWNlLCBoZWxwZXJTZXJ2aWNlKSB7XG4gICAgICAgICAgICB2YXIgX29yZGVycyA9IGhlbHBlclNlcnZpY2UucHJlcGFyZU9yZGVycyhvcmRlcnMpO1xuXG4gICAgICAgICAgICByZXR1cm4gYm9va2luZ1NlcnZpY2UuY2FsY3VsYXRlKF9vcmRlcnMpO1xuICAgICAgICAgIH1dLFxuICAgICAgICAgIG9yZGVyc1dpdGhQcmljZXM6IFsnb3JkZXJzJywgJ3ByaWNlcycsICckZmlsdGVyJywgZnVuY3Rpb24gKG9yZGVycywgcHJpY2VzLCAkZmlsdGVyKSB7XG4gICAgICAgICAgICB2YXIgb3JkZXJzV2l0aFByaWNlID0gW107XG5cbiAgICAgICAgICAgIGZvciAodmFyIGkgPSAwLCBvcmRlcjsgKG9yZGVyID0gb3JkZXJzW2ldKTsgaSsrKSB7XG4gICAgICAgICAgICAgIG9yZGVyLnN1bSA9IDA7XG5cbiAgICAgICAgICAgICAgdmFyIHByaWNlID0gJGZpbHRlcignZmlsdGVyJykocHJpY2VzLCB7X2lkOiBvcmRlci5faWR9LCB0cnVlKTtcbiAgICAgICAgICAgICAgaWYocHJpY2UubGVuZ3RoICYmIHByaWNlWzBdLmFtb3VudCkge1xuICAgICAgICAgICAgICAgIG9yZGVyLnN1bSA9IHByaWNlWzBdLmFtb3VudDtcbiAgICAgICAgICAgICAgICBvcmRlci5kaXNjb3VudCA9IHByaWNlWzBdLmRpc2NvdW50O1xuICAgICAgICAgICAgICAgIG9yZGVyLnByaWNlX3ZhcmlhbnRzID0gcHJpY2VbMF0ucHJpY2VfdmFyaWFudHM7XG4gICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICBvcmRlci52YXJpYW50cyA9IHByaWNlWzBdLnZhcmlhbnRzO1xuXG4gICAgICAgICAgICAgIG9yZGVyc1dpdGhQcmljZS5wdXNoKG9yZGVyKTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgcmV0dXJuIG9yZGVyc1dpdGhQcmljZTtcbiAgICAgICAgICB9XVxuICAgICAgICB9XG4gICAgICB9KTtcbiAgfVxufSkoKTtcbiIsIihmdW5jdGlvbiAoKSB7XG4gICd1c2Ugc3RyaWN0JztcblxuICBhbmd1bGFyXG4gICAgLm1vZHVsZSgnYm9va2luZycpXG4gICAgLnJ1bihydW4pO1xuXG4gIHJ1bi4kaW5qZWN0ID0gWyckc3RhdGUnLCAnJHJvb3RTY29wZScsICdvcmRlclNlcnZpY2UnXTtcblxuICBmdW5jdGlvbiBydW4oJHN0YXRlLCAkcm9vdFNjb3BlLCBvcmRlclNlcnZpY2UpIHtcblxuICAgICRyb290U2NvcGUuJG9uKCckc3RhdGVDaGFuZ2VTdGFydCcsIGZ1bmN0aW9uIChldmVudCwgdG9TdGF0ZSwgdG9QYXJhbXMsIGZyb21TdGF0ZSwgZnJvbVBhcmFtcykge1xuICAgICAgdmFyIHByZXZlbnRFdmVudCA9IGZhbHNlLFxuICAgICAgICBzdGF0ZU5hbWU7XG5cbiAgICAgIGlmKHRvU3RhdGUubmFtZSA9PT0gJ2Jvb2tpbmcuY29tcGxldGUnICYmICFvcmRlclNlcnZpY2UuY291bnQoKSkge1xuICAgICAgICBwcmV2ZW50RXZlbnQgPSB0cnVlO1xuICAgICAgICBzdGF0ZU5hbWUgPSAnYm9va2luZy5vcmRlcnMuY3JlYXRlJztcbiAgICAgIH1cblxuICAgICAgaWYocHJldmVudEV2ZW50KSB7XG4gICAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG4gICAgICAgIGlmKHN0YXRlTmFtZSkge1xuICAgICAgICAgICRzdGF0ZS5nbyhzdGF0ZU5hbWUpO1xuICAgICAgICB9XG4gICAgICB9IGVsc2Uge1xuICAgICAgICAkcm9vdFNjb3BlLnRhYiA9IHRvU3RhdGUuZGF0YS50YWI7XG4gICAgICB9XG5cbiAgICB9KTtcbiAgfVxufSkoKTtcbiIsIihmdW5jdGlvbiAoKSB7XG4gICd1c2Ugc3RyaWN0JztcblxuICBhbmd1bGFyXG4gICAgLm1vZHVsZSgnYm9va2luZycpXG4gICAgLmNvbnRyb2xsZXIoJ0Jvb2tpbmdDb250cm9sbGVyJywgQm9va2luZ0NvbnRyb2xsZXIpO1xuXG4gIEJvb2tpbmdDb250cm9sbGVyLiRpbmplY3QgPSBbJyRzdGF0ZSddO1xuXG4gIC8qIEBuZ0luamVjdCAqL1xuICBmdW5jdGlvbiBCb29raW5nQ29udHJvbGxlcigkc3RhdGUpIHtcbiAgICAvKiBqc2hpbnQgdmFsaWR0aGlzOiB0cnVlICovXG4gICAgdmFyIHZtID0gdGhpcztcblxuICAgIHZhciB0YWIgPSAkc3RhdGUuY3VycmVudC5kYXRhLnRhYjtcbiAgfVxufSkoKTtcbiIsIihmdW5jdGlvbiAoKSB7XG4gICd1c2Ugc3RyaWN0JztcblxuICBhbmd1bGFyXG4gICAgLm1vZHVsZSgnYm9va2luZycpXG4gICAgLmNvbnRyb2xsZXIoJ0NoaWxkcmVuQ29udHJvbGxlcicsIENoaWxkcmVuQ29udHJvbGxlcik7XG5cbiAgQ2hpbGRyZW5Db250cm9sbGVyLiRpbmplY3QgPSBbJ2NoaWxkcmVuJywgJ2NoaWxkU2VydmljZScsICdhbGVydFNlcnZpY2UnLCAnJHN0YXRlJ107XG5cbiAgLyogQG5nSW5qZWN0ICovXG4gIGZ1bmN0aW9uIENoaWxkcmVuQ29udHJvbGxlcihjaGlsZHJlbiwgY2hpbGRTZXJ2aWNlLCBhbGVydFNlcnZpY2UsICRzdGF0ZSkge1xuICAgIC8qIGpzaGludCB2YWxpZHRoaXM6IHRydWUgKi9cbiAgICB2YXIgdm0gPSB0aGlzO1xuXG4gICAgdm0uY2hpbGRyZW4gPSBjaGlsZHJlbjtcbiAgICB2bS5jb25maXJtZWRVcGRhdGVkID0gZmFsc2U7XG4gICAgdm0uY29uZmlybWVkTGVnYWwgPSBmYWxzZTtcblxuICAgIHZtLnJlbW92ZSA9IHJlbW92ZTtcbiAgICB2bS5uZXh0ID0gbmV4dDtcblxuICAgIC8vLy8vLy8vLy8vLy8vLy8vLy9cblxuICAgIGZ1bmN0aW9uIG5leHQoKSB7XG4gICAgICBpZiAoIXZtLmNvbmZpcm1lZFVwZGF0ZWQpIHtcbiAgICAgICAgYWxlcnRTZXJ2aWNlLmVycm9yKCdQbGVhc2UgY29uZmlybSB0aGF0IHlvdSBoYXZlIHVwZGF0ZWQgYWxsIG9mIHlvdXIgY2hpbGRyZW5cXCdzIGNvbnRhY3QgYW5kIG1lZGljYWwgaW5mb3JtYXRpb24gYmVmb3JlIGJvb2tpbmcuJyk7XG5cbiAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuICAgICAgaWYgKCF2bS5jb25maXJtZWRMZWdhbCkge1xuICAgICAgICBhbGVydFNlcnZpY2UuZXJyb3IoJ1BsZWFzZSBjb25maXJtIHRoYXQgeW91IGFyZSB0aGUgbGVnYWwgZ3VhcmRpYW4gb2YgdGhlIGNoaWxkcmVuIGFib3ZlLicpO1xuXG4gICAgICAgIHJldHVybjtcbiAgICAgIH1cblxuICAgICAgJHN0YXRlLmdvKCdib29raW5nLm9yZGVycy5jcmVhdGUnKTtcbiAgICB9XG5cbiAgICBmdW5jdGlvbiByZW1vdmUoY2hpbGQpIHtcbiAgICAgIGNoaWxkU2VydmljZS5yZW1vdmUoY2hpbGQuaWQpLnRoZW4oZnVuY3Rpb24gKCkge1xuICAgICAgICBhbGVydFNlcnZpY2Uuc3VjY2VzcygnVGhlIGNoaWxkIHdhcyByZW1vdmVkIScpO1xuXG4gICAgICAgIHZtLmNoaWxkcmVuLnNwbGljZSh2bS5jaGlsZHJlbi5pbmRleE9mKGNoaWxkKSwgMSk7XG4gICAgICB9KTtcbiAgICB9XG4gIH1cbn0pKCk7XG4iLCIoZnVuY3Rpb24gKCkge1xuICAndXNlIHN0cmljdCc7XG5cbiAgYW5ndWxhclxuICAgIC5tb2R1bGUoJ2Jvb2tpbmcnKVxuICAgIC5jb250cm9sbGVyKCdDcmVhdGVDaGlsZENvbnRyb2xsZXInLCBDcmVhdGVDaGlsZENvbnRyb2xsZXIpO1xuXG4gIENyZWF0ZUNoaWxkQ29udHJvbGxlci4kaW5qZWN0ID0gWydjaGlsZFNlcnZpY2UnLCAnJHN0YXRlJywgJ3RvYXN0ZXInLCAnc3dpbU9wdGlvbnMnLCAneWVhcnNJblNjaG9vbCddO1xuXG4gIC8qIEBuZ0luamVjdCAqL1xuICBmdW5jdGlvbiBDcmVhdGVDaGlsZENvbnRyb2xsZXIoY2hpbGRTZXJ2aWNlLCAkc3RhdGUsIHRvYXN0ZXIsIHN3aW1PcHRpb25zLCB5ZWFyc0luU2Nob29sKSB7XG4gICAgLyoganNoaW50IHZhbGlkdGhpczogdHJ1ZSAqL1xuICAgIHZhciB2bSA9IHRoaXM7XG5cbiAgICB2bS5zd2ltT3B0aW9ucyA9IHN3aW1PcHRpb25zO1xuICAgIHZtLnllYXJzSW5TY2hvb2wgPSB5ZWFyc0luU2Nob29sO1xuXG4gICAgdm0uZm9ybSA9IHtcbiAgICAgIGZpcnN0X25hbWU6IHVuZGVmaW5lZCxcbiAgICAgIGxhc3RfbmFtZTogdW5kZWZpbmVkLFxuICAgICAgZW1haWw6IHVuZGVmaW5lZCxcbiAgICAgIGJpcnRoZGF5OiB1bmRlZmluZWQsXG4gICAgICBzZXg6IHVuZGVmaW5lZCxcbiAgICAgIHJlbGF0aW9uc2hpcDogdW5kZWZpbmVkLFxuICAgICAgc2Nob29sOiB1bmRlZmluZWQsXG4gICAgICB5ZWFyX2luX3NjaG9vbDogdW5kZWZpbmVkLFxuICAgICAgZG9jdG9yX25hbWU6IHVuZGVmaW5lZCxcbiAgICAgIGRvY3Rvcl90ZWxlcGhvbmVfbnVtYmVyOiB1bmRlZmluZWQsXG4gICAgICBtZWRpY2FsX2FkdmljZV9hbmRfdHJlYXRtZW50OiB1bmRlZmluZWQsXG4gICAgICBlcGlfcGVuOiB1bmRlZmluZWQsXG4gICAgICBtZWRpY2F0aW9uOiB1bmRlZmluZWQsXG4gICAgICBzcGVjaWFsX3JlcXVpcmVtZW50czogdW5kZWZpbmVkLFxuICAgICAgY2FuX3N3aW06IHVuZGVmaW5lZCxcbiAgICAgIGVtZXJnZW5jeV9jb250YWN0X251bWJlcjogdW5kZWZpbmVkLFxuICAgIH07XG5cbiAgICB2bS5jcmVhdGUgPSBjcmVhdGU7XG5cbiAgICB2bS5kYXRlUGlja2VyT3BlbiA9IGZhbHNlO1xuXG4gICAgLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vXG5cbiAgICBmdW5jdGlvbiBjcmVhdGUoKSB7XG4gICAgICBjaGlsZFNlcnZpY2Uuc3RvcmUodm0uZm9ybSkudGhlbihmdW5jdGlvbiAoKSB7XG4gICAgICAgIHRvYXN0ZXIuc3VjY2VzcygnVGhlIGNoaWxkIHdhcyBjcmVhdGVkIScpO1xuXG4gICAgICAgICRzdGF0ZS5nbygnXi5pbmRleCcpO1xuICAgICAgfSk7XG4gICAgfVxuXG4gIH1cbn0pKCk7XG4iLCIoZnVuY3Rpb24gKCkge1xuICAndXNlIHN0cmljdCc7XG5cbiAgYW5ndWxhclxuICAgIC5tb2R1bGUoJ2Jvb2tpbmcnKVxuICAgIC5jb250cm9sbGVyKCdFZGl0Q2hpbGRDb250cm9sbGVyJywgRWRpdENoaWxkQ29udHJvbGxlcik7XG5cbiAgRWRpdENoaWxkQ29udHJvbGxlci4kaW5qZWN0ID0gWydjaGlsZCcsICdjaGlsZFNlcnZpY2UnLCAnJHN0YXRlJywgJ3RvYXN0ZXInLCAnc3dpbU9wdGlvbnMnLCAneWVhcnNJblNjaG9vbCddO1xuXG4gIC8qIEBuZ0luamVjdCAqL1xuICBmdW5jdGlvbiBFZGl0Q2hpbGRDb250cm9sbGVyKGNoaWxkLCBjaGlsZFNlcnZpY2UsICRzdGF0ZSwgdG9hc3Rlciwgc3dpbU9wdGlvbnMsIHllYXJzSW5TY2hvb2wpIHtcbiAgICAvKiBqc2hpbnQgdmFsaWR0aGlzOiB0cnVlICovXG4gICAgdmFyIHZtID0gdGhpcztcblxuICAgIHZtLnN3aW1PcHRpb25zID0gc3dpbU9wdGlvbnM7XG4gICAgdm0ueWVhcnNJblNjaG9vbCA9IHllYXJzSW5TY2hvb2w7XG5cbiAgICB2bS5mb3JtID0gY2hpbGQ7XG5cbiAgICB2bS51cGRhdGUgPSB1cGRhdGU7XG5cbiAgICB2bS5kYXRlUGlja2VyT3BlbiA9IGZhbHNlO1xuXG4gICAgLy8vLy8vLy8vLy8vXG5cbiAgICBmdW5jdGlvbiB1cGRhdGUoKSB7XG4gICAgICBjaGlsZFNlcnZpY2UudXBkYXRlKHZtLmZvcm0pLnRoZW4oZnVuY3Rpb24gKCkge1xuICAgICAgICB0b2FzdGVyLnN1Y2Nlc3MoJ1RoZSBjaGlsZCB3YXMgdXBkYXRlZCEnKTtcblxuICAgICAgICAkc3RhdGUuZ28oJ14uaW5kZXgnKTtcbiAgICAgIH0pO1xuICAgIH1cblxuICB9XG59KSgpO1xuIiwiKGZ1bmN0aW9uICgpIHtcbiAgJ3VzZSBzdHJpY3QnO1xuXG4gIGFuZ3VsYXJcbiAgICAubW9kdWxlKCdib29raW5nJylcbiAgICAuY29udHJvbGxlcignU2hvd0NoaWxkQ29udHJvbGxlcicsIFNob3dDaGlsZENvbnRyb2xsZXIpO1xuXG4gIFNob3dDaGlsZENvbnRyb2xsZXIuJGluamVjdCA9IFsnY2hpbGQnXTtcblxuICAvKiBAbmdJbmplY3QgKi9cbiAgZnVuY3Rpb24gU2hvd0NoaWxkQ29udHJvbGxlcihjaGlsZCkge1xuICAgIC8qIGpzaGludCB2YWxpZHRoaXM6IHRydWUgKi9cbiAgICB2YXIgdm0gPSB0aGlzO1xuXG4gICAgdm0uY2hpbGQgPSBjaGlsZDtcbiAgfVxufSkoKTtcbiIsIihmdW5jdGlvbiAoKSB7XG4gICd1c2Ugc3RyaWN0JztcblxuICBhbmd1bGFyXG4gICAgLm1vZHVsZSgnYm9va2luZycpXG4gICAgLmNvbnRyb2xsZXIoJ0NvbXBsZXRlQ29udHJvbGxlcicsIENvbXBsZXRlQ29udHJvbGxlcik7XG5cbiAgLyogQG5nSW5qZWN0ICovXG4gIGZ1bmN0aW9uIENvbXBsZXRlQ29udHJvbGxlcihvcmRlcnNXaXRoUHJpY2VzLCBzdG9yZSwgYm9va2luZ1NlcnZpY2UsIHBheW1lbnRUeXBlcywgYWxlcnRTZXJ2aWNlLCBvcmRlclNlcnZpY2UsIGhlbHBlclNlcnZpY2UsICRmaWx0ZXIsICRzdGF0ZSwgJHVpYk1vZGFsLCBwcm9kdWN0cykge1xuICAgIC8qIGpzaGludCB2YWxpZHRoaXM6IHRydWUgKi9cbiAgICB2YXIgdm0gPSB0aGlzO1xuXG4gICAgdm0ub3JkZXJzID0gb3JkZXJzV2l0aFByaWNlcztcbiAgICB2bS5vcmRlcnMuc3VtID0gJGZpbHRlcignc3VtQnlLZXknKSh2bS5vcmRlcnMsICdzdW0nKTtcbiAgICB2bS5vcmRlcnMuZGlzY291bnQgPSAkZmlsdGVyKCdzdW1CeUtleScpKHZtLm9yZGVycywgJ2Rpc2NvdW50Jyk7XG4gICAgdm0uc3VtX3ZhcmlhbnRzID0gJGZpbHRlcignc3VtQnlLZXknKShwcm9kdWN0cywgJ3ByaWNlJyk7XG4gICAgdm0ub3JkZXJzLmNvdXBvbiA9IDA7XG4gICAgdm0ucHJvZHVjdHMgPSBwcm9kdWN0cztcblxuICAgIHZtLnBheW1lbnRUeXBlcyA9IHBheW1lbnRUeXBlcztcbiAgICB2bS5jb3Vwb25BcHBsaWVkID0gZmFsc2U7XG5cbiAgICB2bS5mb3JtID0ge1xuICAgICAgcGF5bWVudF90eXBlOiB1bmRlZmluZWQsXG4gICAgICBvcmRlcnM6IFtdLFxuICAgICAgY291cG9uOiB1bmRlZmluZWQsXG4gICAgfTtcbiAgICB2bS5hZ3JlZWQgPSBmYWxzZTtcbiAgICB2bS5yZXF1ZXN0ID0gZmFsc2U7XG5cbiAgICBwcmVwYXJlUHJvZHVjdHMoKTtcbiAgICBjYWxjdWxhdGVUb3RhbFByaWNlKCk7XG4gICAgc2V0T3JkZXJzVG9Cb29raW5nRm9ybSgpO1xuICAgIGFkZFRvT3JkZXJQcm9kdWN0c0lmU3RvcmVkKCk7XG5cbiAgICB2bS5ib29rID0gYm9vaztcbiAgICB2bS5yZW1vdmUgPSByZW1vdmU7XG4gICAgdm0uYXBwbHlDb3Vwb24gPSBhcHBseUNvdXBvbjtcbiAgICB2bS5jYW5jZWxDb3Vwb24gPSBjYW5jZWxDb3Vwb247XG4gICAgdm0uY2FsY3VsYXRlUHJvZHVjdFByaWNlID0gY2FsY3VsYXRlUHJvZHVjdFByaWNlO1xuICAgIHZtLnNhdmVQcm9kdWN0cyA9IHNhdmVQcm9kdWN0cztcblxuICAgIC8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vXG5cbiAgICBmdW5jdGlvbiBhcHBseUNvdXBvbigpIHtcbiAgICAgIGlmICghdm0uZm9ybS5jb3Vwb24pIHtcbiAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuXG4gICAgICB2bS5yZXF1ZXN0ID0gdHJ1ZTtcblxuICAgICAgYm9va2luZ1NlcnZpY2UuY2hlY2tDb3Vwb24odm0uZm9ybS5jb3Vwb24pLnRoZW4oZnVuY3Rpb24gKHBlcnNlbnQpIHtcbiAgICAgICAgdm0ub3JkZXJzLmNvdXBvblBlcnNlbnQgPSBwZXJzZW50O1xuICAgICAgICBjYWxjdWxhdGVUb3RhbFByaWNlKCk7XG4gICAgICAgIGFsZXJ0U2VydmljZS5zdWNjZXNzKCdZb3VyIGNvdXBvbiBpcyBhY3RpdmF0ZWQuJyk7XG4gICAgICAgIHZtLmNvdXBvbkFwcGxpZWQgPSB0cnVlO1xuICAgICAgfSwgZnVuY3Rpb24gKCkge1xuICAgICAgICBhbGVydFNlcnZpY2UuZXJyb3IoJ0NvdXBvbiBub3QgZXhpc3RzIG9yIGV4cGlyZWQuJyk7XG4gICAgICB9KS5maW5hbGx5KGZ1bmN0aW9uICgpIHtcbiAgICAgICAgdm0ucmVxdWVzdCA9IGZhbHNlO1xuICAgICAgfSk7XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gY2FuY2VsQ291cG9uKCkge1xuICAgICAgLy9DbGVhciBkYXRhIGFib3V0IHByZXZpb3VzIGNvdXBvblxuICAgICAgdm0uZm9ybS5jb3Vwb24gPSAnJztcbiAgICAgIHZtLm9yZGVycy5jb3Vwb25QZXJzZW50ID0gdW5kZWZpbmVkO1xuICAgICAgdm0ub3JkZXJzLmNvdXBvbiA9IDA7XG4gICAgICB2bS5jb3Vwb25BcHBsaWVkID0gZmFsc2U7XG5cbiAgICAgIGNhbGN1bGF0ZVRvdGFsUHJpY2UoKTtcblxuICAgICAgYWxlcnRTZXJ2aWNlLnN1Y2Nlc3MoJ1lvdSBjb3Vwb24gd2FzIGRlYWN0aXZhdGVkLicpO1xuICAgIH1cblxuICAgIGZ1bmN0aW9uIHJlbW92ZShpZCkge1xuICAgICAgdmFyIG1vZGFsSW5zdGFuY2UgPSAkdWliTW9kYWwub3Blbih7XG4gICAgICAgIHRlbXBsYXRlVXJsOiAnL2Jvb2tpbmcvY29tcGxldGUvY29uZmlybS1yZW1vdmUtbW9kYWwuaHRtbCcsXG4gICAgICAgIGNvbnRyb2xsZXI6ICdDb25maXJtUmVtb3ZlT3JkZXJDb250cm9sbGVyJyxcbiAgICAgICAgY29udHJvbGxlckFzOiAnQ29uZmlybVJlbW92ZUN0cmwnLFxuICAgICAgICBzaXplOiAnbWQnXG4gICAgICB9KTtcblxuICAgICAgbW9kYWxJbnN0YW5jZS5yZXN1bHQudGhlbihmdW5jdGlvbiAoKSB7XG4gICAgICAgIG9yZGVyU2VydmljZS5yZW1vdmUoaWQpLnRoZW4oZnVuY3Rpb24gKCkge1xuICAgICAgICAgIC8vUmVmcmVzaCBkYXRhXG4gICAgICAgICAgb3JkZXJTZXJ2aWNlLmFsbCgpLnRoZW4oZnVuY3Rpb24gKG9yZGVycykge1xuICAgICAgICAgICAgYWxlcnRTZXJ2aWNlLnN1Y2Nlc3MoJ1RoZSBvcmRlciB3YXMgcmVtb3ZlZCcpO1xuXG4gICAgICAgICAgICBpZighIG9yZGVycy5sZW5ndGgpIHtcbiAgICAgICAgICAgICAgJHN0YXRlLmdvKCdib29raW5nLm9yZGVycy5jcmVhdGUnKTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgdmFyIF9vcmRlcnMgPSBoZWxwZXJTZXJ2aWNlLnByZXBhcmVPcmRlcnMob3JkZXJzKTtcbiAgICAgICAgICAgIGJvb2tpbmdTZXJ2aWNlLmNhbGN1bGF0ZShfb3JkZXJzKS50aGVuKGZ1bmN0aW9uIChwcmljZXMpIHtcbiAgICAgICAgICAgICAgdmFyIG9yZGVyc1dpdGhQcmljZSA9IFtdO1xuXG4gICAgICAgICAgICAgIGZvciAodmFyIGkgPSAwLCBvcmRlcjsgKG9yZGVyID0gb3JkZXJzW2ldKTsgaSsrKSB7XG4gICAgICAgICAgICAgICAgb3JkZXIuc3VtID0gMDtcblxuICAgICAgICAgICAgICAgIHZhciBwcmljZSA9ICRmaWx0ZXIoJ2ZpbHRlcicpKHByaWNlcywge19pZDogb3JkZXIuX2lkfSwgdHJ1ZSk7XG4gICAgICAgICAgICAgICAgaWYgKHByaWNlLmxlbmd0aCAmJiBwcmljZVswXS5hbW91bnQpIHtcbiAgICAgICAgICAgICAgICAgIG9yZGVyLnN1bSA9IHByaWNlWzBdLmFtb3VudDtcbiAgICAgICAgICAgICAgICAgIG9yZGVyLmRpc2NvdW50ID0gcHJpY2VbMF0uZGlzY291bnQ7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIG9yZGVyc1dpdGhQcmljZS5wdXNoKG9yZGVyKTtcbiAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgIHZtLm9yZGVycyA9IG9yZGVyc1dpdGhQcmljZTtcbiAgICAgICAgICAgICAgdm0ub3JkZXJzLnN1bSA9ICRmaWx0ZXIoJ3N1bUJ5S2V5Jykodm0ub3JkZXJzLCAnc3VtJyk7XG4gICAgICAgICAgICAgIHZtLm9yZGVycy5kaXNjb3VudCA9ICRmaWx0ZXIoJ3N1bUJ5S2V5Jykodm0ub3JkZXJzLCAnZGlzY291bnQnKTtcbiAgICAgICAgICAgICAgdm0ub3JkZXJzLmNvdXBvbiA9IDA7XG4gICAgICAgICAgICAgIGNhbGN1bGF0ZVRvdGFsUHJpY2UoKTtcbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgIH0pO1xuICAgICAgICB9KTtcbiAgICAgIH0pO1xuICAgIH1cblxuICAgIGZ1bmN0aW9uIGJvb2soKSB7XG4gICAgICBpZighIHZtLmFncmVlZCkge1xuICAgICAgICBhbGVydFNlcnZpY2UuZXJyb3IoJ1lvdSBzaG91bGQgd2l0aCBib29raW5nXFwncyB0ZXJtcyBhbmQgY29uZGl0aW9ucycpO1xuICAgICAgICByZXR1cm4gO1xuICAgICAgfVxuXG4gICAgICB2bS5yZXF1ZXN0ID0gdHJ1ZTtcblxuICAgICAgcmV0dXJuIGJvb2tpbmdTZXJ2aWNlLmJvb2sodm0uZm9ybSkudGhlbihmdW5jdGlvbiAoZGF0YSkge1xuICAgICAgICBhbGVydFNlcnZpY2Uuc3VjY2VzcygnWW91IGJvb2tlZCEnKTtcblxuICAgICAgICAvL0NsZWFyIGN1cnJlbnQgb3JkZXJzXG4gICAgICAgIG9yZGVyU2VydmljZS5jbGVhcigpO1xuXG4gICAgICAgIGhlbHBlclNlcnZpY2UucmVkaXJlY3QoZGF0YS5yZWRpcmVjdCwgZGF0YS5tZXRob2QsIGRhdGEuZmllbGRzKTtcbiAgICAgIH0sIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgdm0ucmVxdWVzdCA9IGZhbHNlO1xuICAgICAgfSk7XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gc2V0T3JkZXJzVG9Cb29raW5nRm9ybSgpIHtcbiAgICAgIHZtLmZvcm0ub3JkZXJzID0gaGVscGVyU2VydmljZS5wcmVwYXJlT3JkZXJzKHZtLm9yZGVycyk7XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gY2FsY3VsYXRlVG90YWxQcmljZSgpIHtcbiAgICAgIGlmKHZtLmZvcm0uY291cG9uICYmIHZtLm9yZGVycy5jb3Vwb25QZXJzZW50KSB7XG4gICAgICAgIHZtLm9yZGVycy5jb3Vwb24gPSB2bS5vcmRlcnMuc3VtICogdm0ub3JkZXJzLmNvdXBvblBlcnNlbnQgLyAxMDA7XG4gICAgICAgIHZtLm9yZGVycy50b3RhbCA9IHZtLm9yZGVycy5zdW0gLSB2bS5vcmRlcnMuY291cG9uO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgdm0ub3JkZXJzLnRvdGFsID0gdm0ub3JkZXJzLnN1bSAtIHZtLm9yZGVycy5kaXNjb3VudDtcbiAgICAgIH1cbiAgICAgIHZtLm9yZGVycy50b3RhbCA9IHZtLm9yZGVycy50b3RhbCArIHZtLnN1bV92YXJpYW50cztcbiAgICB9XG5cbiAgICBmdW5jdGlvbiBwcmVwYXJlUHJvZHVjdHMoKSB7XG4gICAgICBhbmd1bGFyLmZvckVhY2godm0ucHJvZHVjdHMsIGZ1bmN0aW9uIChwcm9kdWN0KSB7XG4gICAgICAgIGlmICghcHJvZHVjdC5zZWxlY3RlZFF1YW50aXR5KSB7XG4gICAgICAgICAgcHJvZHVjdC5zZWxlY3RlZFF1YW50aXR5ID0gMTtcbiAgICAgICAgfVxuXG4gICAgICAgIGlmICghcHJvZHVjdC5zZWxlY3RlZFZhcmlhbnQpIHtcbiAgICAgICAgICBwcm9kdWN0LnNlbGVjdGVkVmFyaWFudCA9IHByb2R1Y3QudmFyaWFudHMubGVuZ3RoID8gcHJvZHVjdC52YXJpYW50c1swXS5pZCA6IDA7XG4gICAgICAgIH1cblxuICAgICAgICBPYmplY3QuZGVmaW5lUHJvcGVydHkocHJvZHVjdCwgJ3ByaWNlJywge1xuICAgICAgICAgIGdldDogZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgdmFyIHByaWNlID0gMDtcblxuICAgICAgICAgICAgdmFyIHNlbGVjdGVkVmFyaWFudCA9IHRoaXMuc2VsZWN0ZWRWYXJpYW50O1xuICAgICAgICAgICAgdmFyIHZhcmlhbnQgPSB0aGlzLnZhcmlhbnRzLmZpbHRlcihmdW5jdGlvbiAodmFyaWFudCkge1xuICAgICAgICAgICAgICByZXR1cm4gc2VsZWN0ZWRWYXJpYW50ICYmIHNlbGVjdGVkVmFyaWFudCA9PT0gdmFyaWFudC5pZDtcbiAgICAgICAgICAgIH0pWzBdO1xuXG4gICAgICAgICAgICBpZiAodmFyaWFudCAmJiB0aGlzLnNlbGVjdGVkUXVhbnRpdHkpIHtcbiAgICAgICAgICAgICAgcHJpY2UgPSB2YXJpYW50LnByaWNlICogdGhpcy5zZWxlY3RlZFF1YW50aXR5O1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICByZXR1cm4gcHJpY2U7XG4gICAgICAgICAgfVxuICAgICAgICB9KTtcbiAgICAgICAgT2JqZWN0LmRlZmluZVByb3BlcnR5KHByb2R1Y3QsICdtYXhDb3VudCcsIHtcbiAgICAgICAgICBnZXQ6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgIHZhciBjb3VudE1heCA9IDA7XG5cbiAgICAgICAgICAgIHZhciBzZWxlY3RlZFZhcmlhbnQgPSB0aGlzLnNlbGVjdGVkVmFyaWFudDtcbiAgICAgICAgICAgIHZhciB2YXJpYW50ID0gdGhpcy52YXJpYW50cy5maWx0ZXIoZnVuY3Rpb24gKHZhcmlhbnQpIHtcbiAgICAgICAgICAgICAgcmV0dXJuIHNlbGVjdGVkVmFyaWFudCAmJiBzZWxlY3RlZFZhcmlhbnQgPT09IHZhcmlhbnQuaWQ7XG4gICAgICAgICAgICB9KVswXTtcblxuICAgICAgICAgICAgaWYgKHZhcmlhbnQgJiYgdGhpcy5zZWxlY3RlZFF1YW50aXR5KSB7XG4gICAgICAgICAgICAgIGNvdW50TWF4ID0gdmFyaWFudC5jb3VudDtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgcmV0dXJuIGNvdW50TWF4O1xuICAgICAgICAgIH1cbiAgICAgICAgfSk7XG5cbiAgICAgICAgY2FsY3VsYXRlUHJvZHVjdFByaWNlKHByb2R1Y3QsIHRydWUpO1xuICAgICAgfSk7XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gY2FsY3VsYXRlUHJvZHVjdFByaWNlKHByb2R1Y3QsIG5vdFNob3dDaGFuZ2VQcm9kdWN0cykge1xuICAgICAgdm0uY2hhbmdlZFByb2R1Y3RzID0gIW5vdFNob3dDaGFuZ2VQcm9kdWN0cztcbiAgICAgIC8vcHJvZHVjdC5wcmljZSA9IDA7XG4gICAgICAvL3Byb2R1Y3QubWF4Q291bnQgPSB1bmRlZmluZWQ7XG5cbiAgICAgIHZhciB2YXJpYW50ID0gcHJvZHVjdC52YXJpYW50cy5maWx0ZXIoZnVuY3Rpb24gKHZhcmlhbnQpIHtcbiAgICAgICAgcmV0dXJuIHByb2R1Y3Quc2VsZWN0ZWRWYXJpYW50ICYmIHByb2R1Y3Quc2VsZWN0ZWRWYXJpYW50ID09PSB2YXJpYW50LmlkO1xuICAgICAgfSlbMF07XG5cbiAgICAgIGlmICh2YXJpYW50ICYmIHByb2R1Y3Quc2VsZWN0ZWRRdWFudGl0eSkge1xuICAgICAgICAvL3Byb2R1Y3QucHJpY2UgPSB2YXJpYW50LnByaWNlICogcHJvZHVjdC5zZWxlY3RlZFF1YW50aXR5O1xuICAgICAgICAvL3Byb2R1Y3QubWF4Q291bnQgPSB2YXJpYW50LmNvdW50O1xuICAgICAgfVxuICAgIH1cblxuICAgIGZ1bmN0aW9uIHNhdmVQcm9kdWN0cygpIHtcbiAgICAgIHZhciBwcm9kdWN0cyA9IHZtLnByb2R1Y3RzLmZpbHRlcihmdW5jdGlvbiAocHJvZHVjdCkge1xuICAgICAgICByZXR1cm4gcHJvZHVjdC5wcmljZTtcbiAgICAgIH0pO1xuXG4gICAgICBvcmRlclNlcnZpY2Uuc2F2ZVByb2R1Y3RzKHByb2R1Y3RzKS50aGVuKGZ1bmN0aW9uICgpIHtcbiAgICAgICAgYWxlcnRTZXJ2aWNlLnN1Y2Nlc3MoJ1Byb2R1Y3RzIHNhdmVkJyk7XG5cbiAgICAgICAgdm0uZm9ybS5wcm9kdWN0cyA9IHByb2R1Y3RzO1xuXG4gICAgICAgIHZtLnN1bV92YXJpYW50cyA9ICRmaWx0ZXIoJ3N1bUJ5S2V5JykocHJvZHVjdHMsICdwcmljZScpO1xuXG4gICAgICAgIGNhbGN1bGF0ZVRvdGFsUHJpY2UoKTtcblxuICAgICAgICB2bS5jaGFuZ2VkUHJvZHVjdHMgPSBmYWxzZTtcbiAgICAgIH0pO1xuICAgIH1cblxuICAgIGZ1bmN0aW9uIGFkZFRvT3JkZXJQcm9kdWN0c0lmU3RvcmVkKCkge1xuICAgICAgdmFyIHByb2R1Y3RzID0gc3RvcmUuZ2V0KCdwcm9kdWN0cycpO1xuXG4gICAgICBpZiAoIXByb2R1Y3RzIHx8ICFwcm9kdWN0cy5sZW5ndGgpIHtcbiAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgfVxuXG4gICAgICB2bS5mb3JtLnByb2R1Y3RzID0gcHJvZHVjdHM7XG4gICAgfVxuXG4gIH1cbn0pKCk7XG4iLCIoZnVuY3Rpb24gKCkge1xuICAndXNlIHN0cmljdCc7XG5cbiAgYW5ndWxhclxuICAgIC5tb2R1bGUoJ2Jvb2tpbmcnKVxuICAgIC5jb250cm9sbGVyKCdDb25maXJtUmVtb3ZlT3JkZXJDb250cm9sbGVyJywgQ29uZmlybVJlbW92ZU9yZGVyQ29udHJvbGxlcik7XG5cbiAgQ29uZmlybVJlbW92ZU9yZGVyQ29udHJvbGxlci4kaW5qZWN0ID0gWyckdWliTW9kYWxJbnN0YW5jZSddO1xuXG4gIC8qIEBuZ0luamVjdCAqL1xuICBmdW5jdGlvbiBDb25maXJtUmVtb3ZlT3JkZXJDb250cm9sbGVyKCR1aWJNb2RhbEluc3RhbmNlKSB7XG4gICAgLyoganNoaW50IHZhbGlkdGhpczogdHJ1ZSAqL1xuICAgIHZhciB2bSA9IHRoaXM7XG5cbiAgICB2bS5vayA9IG9rO1xuICAgIHZtLmNhbmNlbCA9IGNhbmNlbDtcblxuICAgIGZ1bmN0aW9uIG9rKCkge1xuICAgICAgJHVpYk1vZGFsSW5zdGFuY2UuY2xvc2UoKTtcbiAgICB9XG5cbiAgICBmdW5jdGlvbiBjYW5jZWwoKSB7XG4gICAgICAkdWliTW9kYWxJbnN0YW5jZS5kaXNtaXNzKCdjYW5jZWwnKTtcbiAgICB9XG4gIH1cbn0pKCk7XG4iLCIoZnVuY3Rpb24oKXtcbiAgJ3VzZSBzdHJpY3QnO1xuXG4gIGFuZ3VsYXJcbiAgICAubW9kdWxlKCdib29raW5nJylcbiAgICAuZmlsdGVyKCdhZ2UnLCBhZ2UpO1xuXG4gIGFnZS4kaW5qZWN0ID0gW107XG5cbiAgZnVuY3Rpb24gYWdlKCkge1xuICAgIHJldHVybiBtYWluO1xuXG4gICAgZnVuY3Rpb24gbWFpbihwZXJzb24sIGRhdGUpIHtcbiAgICAgIGRhdGUgPSBkYXRlID8gbmV3IERhdGUoZGF0ZSkgOiBuZXcgRGF0ZSgpO1xuXG4gICAgICB2YXIgYmlydGhkYXkgPSBuZXcgRGF0ZShwZXJzb24uYmlydGhkYXkpO1xuICAgICAgdmFyIGFnZURpZk1zID0gZGF0ZS5nZXRUaW1lKCkgLSBiaXJ0aGRheS5nZXRUaW1lKCk7XG4gICAgICB2YXIgYWdlRGF0ZSA9IG5ldyBEYXRlKGFnZURpZk1zKTsgLy8gbWlsaXNlY29uZHMgZnJvbSBlcG9jaFxuICAgICAgcmV0dXJuIE1hdGguYWJzKGFnZURhdGUuZ2V0RnVsbFllYXIoKSAtIDE5NzApO1xuICAgIH1cbiAgfVxufSkoKTsiLCIoZnVuY3Rpb24oKXtcbiAgJ3VzZSBzdHJpY3QnO1xuXG4gIGFuZ3VsYXJcbiAgICAubW9kdWxlKCdib29raW5nJylcbiAgICAuZmlsdGVyKCdjdXJyZW5jeUxiJywgY3VycmVuY3lMYik7XG5cbiAgY3VycmVuY3lMYi4kaW5qZWN0ID0gWyckZmlsdGVyJ107XG5cbiAgZnVuY3Rpb24gY3VycmVuY3lMYigkZmlsdGVyKSB7XG4gICAgcmV0dXJuIG1haW47XG5cbiAgICBmdW5jdGlvbiBtYWluKHZhbHVlKSB7XG4gICAgICByZXR1cm4gJGZpbHRlcignY3VycmVuY3knKSh2YWx1ZSwgJ8KjJyk7XG4gICAgfVxuICB9XG59KSgpOyIsIihmdW5jdGlvbigpe1xuICAndXNlIHN0cmljdCc7XG5cbiAgYW5ndWxhclxuICAgIC5tb2R1bGUoJ2Jvb2tpbmcnKVxuICAgIC5maWx0ZXIoJ2V2ZW50JywgZXZlbnQpO1xuXG4gIGV2ZW50LiRpbmplY3QgPSBbXTtcblxuICBmdW5jdGlvbiBldmVudCgpIHtcbiAgICByZXR1cm4gbWFpbjtcblxuICAgIGZ1bmN0aW9uIG1haW4oZXZlbnQpIHtcbiAgICAgIHJldHVybiBldmVudC52ZW51ZS5uYW1lICsgJyAtICcgKyBldmVudC5uYW1lO1xuICAgIH1cbiAgfVxufSkoKTsiLCIoZnVuY3Rpb24oKXtcbiAgJ3VzZSBzdHJpY3QnO1xuXG4gIGFuZ3VsYXJcbiAgICAubW9kdWxlKCdib29raW5nJylcbiAgICAuZmlsdGVyKCdsb2dpY2FsJywgbG9naWNhbCk7XG5cbiAgbG9naWNhbC4kaW5qZWN0ID0gW107XG5cbiAgZnVuY3Rpb24gbG9naWNhbCgpIHtcbiAgICByZXR1cm4gbWFpbjtcblxuICAgIGZ1bmN0aW9uIG1haW4odmFsdWUpIHtcbiAgICAgIHJldHVybiB2YWx1ZSA/ICdZZXMnIDogJ05vJztcbiAgICB9XG4gIH1cbn0pKCk7IiwiKGZ1bmN0aW9uKCl7XG4gICd1c2Ugc3RyaWN0JztcblxuICBhbmd1bGFyXG4gICAgLm1vZHVsZSgnYm9va2luZycpXG4gICAgLmZpbHRlcigncHJvZmlsZScsIHByb2ZpbGUpO1xuXG4gIHByb2ZpbGUuJGluamVjdCA9IFtdO1xuXG4gIGZ1bmN0aW9uIHByb2ZpbGUoKSB7XG4gICAgcmV0dXJuIG1haW47XG5cbiAgICBmdW5jdGlvbiBtYWluKHVzZXIpIHtcbiAgICAgIHJldHVybiB1c2VyLmZpcnN0X25hbWUgKyAnICcgKyB1c2VyLmxhc3RfbmFtZTtcbiAgICB9XG4gIH1cbn0pKCk7IiwiKGZ1bmN0aW9uKCl7XG4gICd1c2Ugc3RyaWN0JztcblxuICBhbmd1bGFyXG4gICAgLm1vZHVsZSgnYm9va2luZycpXG4gICAgLmZpbHRlcignc2V4Jywgc2V4KTtcblxuICBzZXguJGluamVjdCA9IFtdO1xuXG4gIGZ1bmN0aW9uIHNleCgpIHtcbiAgICByZXR1cm4gbWFpbjtcblxuICAgIGZ1bmN0aW9uIG1haW4odmFsdWUpIHtcbiAgICAgIHJldHVybiB2YWx1ZSA/ICdNYWxlJyA6ICdGZW1hbGUnO1xuICAgIH1cbiAgfVxufSkoKTsiLCIoZnVuY3Rpb24gKCkge1xuICAndXNlIHN0cmljdCc7XG5cbiAgYW5ndWxhclxuICAgIC5tb2R1bGUoJ2Jvb2tpbmcnKVxuICAgIC5maWx0ZXIoJ3N1bUJ5S2V5JywgZnVuY3Rpb24gKCkge1xuICAgICAgcmV0dXJuIGZ1bmN0aW9uIChkYXRhLCBrZXkpIHtcbiAgICAgICAgaWYgKHR5cGVvZihkYXRhKSA9PT0gJ3VuZGVmaW5lZCcgfHwgdHlwZW9mKGtleSkgPT09ICd1bmRlZmluZWQnKSB7XG4gICAgICAgICAgcmV0dXJuIDA7XG4gICAgICAgIH1cblxuICAgICAgICB2YXIgc3VtID0gMDtcbiAgICAgICAgZm9yICh2YXIgaSA9IGRhdGEubGVuZ3RoIC0gMTsgaSA+PSAwOyBpLS0pIHtcbiAgICAgICAgICBzdW0gKz0gcGFyc2VGbG9hdChkYXRhW2ldW2tleV0pO1xuICAgICAgICB9XG5cbiAgICAgICAgcmV0dXJuIHN1bTtcbiAgICAgIH07XG4gICAgfSk7XG5cbn0pKCk7XG4iLCIoZnVuY3Rpb24gKCkge1xuICAndXNlIHN0cmljdCc7XG5cbiAgYW5ndWxhclxuICAgIC5tb2R1bGUoJ2Jvb2tpbmcnKVxuICAgIC5maWx0ZXIoJ3N3aW0nLCBzd2ltKTtcblxuICBzd2ltLiRpbmplY3QgPSBbXTtcblxuICBmdW5jdGlvbiBzd2ltKCkge1xuICAgIHJldHVybiBtYWluO1xuXG4gICAgZnVuY3Rpb24gbWFpbihjaGlsZCkge1xuICAgICAgcmV0dXJuIGNoaWxkLmNhbl9zd2ltID8gKGNoaWxkLmNhbl9zd2ltLm51bWJlciArICcgLSAnICsgY2hpbGQuY2FuX3N3aW0ub3B0aW9uKSA6ICcnO1xuICAgIH1cbiAgfVxufSkoKTsiLCIoZnVuY3Rpb24gKCkge1xuICAndXNlIHN0cmljdCc7XG5cbiAgYW5ndWxhclxuICAgIC5tb2R1bGUoJ2Jvb2tpbmcnKVxuICAgIC5maWx0ZXIoJ3dlZWtkYXknLCB3ZWVrZGF5KTtcblxuICB3ZWVrZGF5LiRpbmplY3QgPSBbXTtcblxuICBmdW5jdGlvbiB3ZWVrZGF5KCkge1xuICAgIHJldHVybiBtYWluO1xuXG4gICAgZnVuY3Rpb24gbWFpbihkYXRlKSB7XG4gICAgICBpZiAoIShkYXRlIGluc3RhbmNlb2YgRGF0ZSkpIHtcbiAgICAgICAgZGF0ZSA9IG5ldyBEYXRlKGRhdGUpO1xuICAgICAgfVxuXG4gICAgICB2YXIgd2Vla2RheTtcblxuICAgICAgc3dpdGNoIChkYXRlLmdldERheSgpKSB7XG4gICAgICAgIGNhc2UgMDpcbiAgICAgICAgICB3ZWVrZGF5ID0gJ1N1bmRheSc7XG4gICAgICAgICAgYnJlYWs7XG4gICAgICAgIGNhc2UgMTpcbiAgICAgICAgICB3ZWVrZGF5ID0gJ01vbmRheSc7XG4gICAgICAgICAgYnJlYWs7XG4gICAgICAgIGNhc2UgMjpcbiAgICAgICAgICB3ZWVrZGF5ID0gJ1R1ZXNkYXknO1xuICAgICAgICAgIGJyZWFrO1xuICAgICAgICBjYXNlIDM6XG4gICAgICAgICAgd2Vla2RheSA9ICdXZWRuZXNkYXknO1xuICAgICAgICAgIGJyZWFrO1xuICAgICAgICBjYXNlIDQ6XG4gICAgICAgICAgd2Vla2RheSA9ICdUaHVyc2RheSc7XG4gICAgICAgICAgYnJlYWs7XG4gICAgICAgIGNhc2UgNTpcbiAgICAgICAgICB3ZWVrZGF5ID0gJ0ZyaWRheSc7XG4gICAgICAgICAgYnJlYWs7XG4gICAgICAgIGNhc2UgNjpcbiAgICAgICAgICB3ZWVrZGF5ID0gJ1NhdHVyZGF5JztcbiAgICAgICAgICBicmVhaztcblxuICAgICAgfVxuXG4gICAgICByZXR1cm4gd2Vla2RheTtcbiAgICB9XG4gIH1cbn0pKCk7IiwiKGZ1bmN0aW9uICgpIHtcbiAgJ3VzZSBzdHJpY3QnO1xuXG4gIGFuZ3VsYXJcbiAgICAubW9kdWxlKCdib29raW5nJylcbiAgICAuY29udHJvbGxlcignQ3JlYXRlT3JkZXJDb250cm9sbGVyJywgQ3JlYXRlT3JkZXJDb250cm9sbGVyKTtcblxuICBDcmVhdGVPcmRlckNvbnRyb2xsZXIuJGluamVjdCA9IFsnY2hpbGRyZW4nLCAnZXZlbnRzJywgJ29yZGVyU2VydmljZScsICckc3RhdGUnLCAnYWxlcnRTZXJ2aWNlJywgJ2hlbHBlclNlcnZpY2UnLCAnJGZpbHRlcicsICdwcm9kdWN0cycsICdjb3VudE9yZGVycyddO1xuXG4gIC8qIEBuZ0luamVjdCAqL1xuICBmdW5jdGlvbiBDcmVhdGVPcmRlckNvbnRyb2xsZXIoY2hpbGRyZW4sIGV2ZW50cywgb3JkZXJTZXJ2aWNlLCAkc3RhdGUsIGFsZXJ0U2VydmljZSwgaGVscGVyU2VydmljZSwgJGZpbHRlciwgcHJvZHVjdHMsIGNvdW50T3JkZXJzKSB7XG4gICAgLyoganNoaW50IHZhbGlkdGhpczogdHJ1ZSAqL1xuICAgIHZhciB2bSA9IHRoaXM7XG5cbiAgICBpZiAoIWNoaWxkcmVuLmxlbmd0aCkge1xuICAgICAgYWxlcnRTZXJ2aWNlLmVycm9yKCdQbGVhc2UgYWRkIHlvdXIgY2hpbGRyZW4hJyk7XG4gICAgICAkc3RhdGUuZ28oJ2Jvb2tpbmcuY2hpbGRyZW4uY3JlYXRlJyk7XG4gICAgfVxuXG4gICAgdm0uY291bnRPcmRlcnMgPSBjb3VudE9yZGVycztcbiAgICB2bS5jaGlsZHJlbiA9IGNoaWxkcmVuO1xuICAgIHZtLnByb2R1Y3RzID0gcHJvZHVjdHM7XG4gICAgdm0uZXZlbnRzID0gW107Ly9ldmVudHM7XG5cbiAgICB2bS5yZXF1ZXN0ID0gZmFsc2U7XG5cbiAgICB2bS5mb3JtID0ge1xuICAgICAgY2hpbGRfaWQ6IHVuZGVmaW5lZCxcbiAgICAgIGV2ZW50X2lkOiB1bmRlZmluZWQsXG4gICAgICBkYXRlczogW10sXG4gICAgICBwcm9kdWN0czogW11cbiAgICB9O1xuICAgIHZtLnNlbGVjdGVkUHJvZHVjdCA9IHVuZGVmaW5lZDtcblxuICAgIHZtLmNyZWF0ZSA9IGNyZWF0ZTtcbiAgICB2bS5zZWxlY3RlZEV2ZW50ID0gc2VsZWN0ZWRFdmVudDtcbiAgICB2bS5zZWxlY3RlZFdlZWsgPSBzZWxlY3RlZFdlZWs7XG4gICAgdm0uc2VsZWN0ZWREYXkgPSBzZWxlY3RlZERheTtcbiAgICB2bS5zZWxlY3RlZENoaWxkID0gc2VsZWN0ZWRDaGlsZDtcbiAgICB2bS5hZGRQcm9kdWN0ID0gYWRkUHJvZHVjdDtcblxuICAgIC8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vXG5cbiAgICBmdW5jdGlvbiBhZGRQcm9kdWN0KCkge1xuXG4gICAgfVxuXG4gICAgZnVuY3Rpb24gc2VsZWN0ZWRDaGlsZCgpIHtcbiAgICAgIHZhciBjaGlsZCA9ICRmaWx0ZXIoJ2ZpbHRlcicpKGNoaWxkcmVuLCB7aWQ6IHZtLmZvcm0uY2hpbGRfaWR9KVswXTtcblxuICAgICAgdm0uZXZlbnRzID0gJGZpbHRlcignZmlsdGVyJykoZXZlbnRzLCBmdW5jdGlvbihldmVudCkge1xuICAgICAgICB2YXIgYWdlID0gJGZpbHRlcignYWdlJykoY2hpbGQsIGV2ZW50LnN0YXJ0X2RhdGUpO1xuICAgICAgICByZXR1cm4gZXZlbnQubWluX2FnZSA8PSBhZ2UgJiYgYWdlIDw9IGV2ZW50Lm1heF9hZ2U7XG4gICAgICB9KTtcbiAgICB9XG5cbiAgICBmdW5jdGlvbiBjcmVhdGUoKSB7XG4gICAgICBpZiAoaGVscGVyU2VydmljZS5TaG93VmFsaWRhdGlvbkVycm9ycyhvcmRlclNlcnZpY2UudmFsaWRhdGUodm0uZm9ybSwgY2hpbGRyZW4sIGV2ZW50cykpLmxlbmd0aCkge1xuICAgICAgICByZXR1cm47XG4gICAgICB9XG5cbiAgICAgIHZtLnJlcXVlc3QgPSB0cnVlO1xuXG4gICAgICBvcmRlclNlcnZpY2UuYWRkKHZtLmZvcm0pLnRoZW4oZnVuY3Rpb24gKCkge1xuICAgICAgICBhbGVydFNlcnZpY2Uuc3VjY2VzcygnT3JkZXIgd2FzIHNhdmVkJyk7XG5cbiAgICAgICAgJHN0YXRlLmdvKCdeLl4uY29tcGxldGUnKTtcbiAgICAgIH0pLmZpbmFsbHkoZnVuY3Rpb24gKCkge1xuICAgICAgICAvL3ZtLnJlcXVlc3QgPSBmYWxzZTtcbiAgICAgIH0pO1xuICAgIH1cblxuICAgIGZ1bmN0aW9uIHNlbGVjdGVkRXZlbnQoKSB7XG4gICAgICBpZih2bS5mb3JtLmV2ZW50X2lkKSB7XG4gICAgICAgIGhlbHBlclNlcnZpY2UucHVzaEV2ZW50RGF0ZXModm0uZXZlbnRzLCB2bS5mb3JtKTtcbiAgICAgIH1cbiAgICB9XG5cbiAgICBmdW5jdGlvbiBzZWxlY3RlZFdlZWsod2Vlaykge1xuICAgICAgaWYgKHdlZWsuc2VsZWN0ZWQpIHtcbiAgICAgICAgc2V0U2VsZWN0ZWREYXRlc09mV2Vlayh3ZWVrLCB0cnVlKTtcbiAgICAgICAgd2Vlay5zZWxlY3RlZEFueURhdGVzID0gdHJ1ZTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHdlZWsuc2VsZWN0ZWRBbnlEYXRlcyA9IGZhbHNlO1xuICAgICAgICBzZXRTZWxlY3RlZERhdGVzT2ZXZWVrKHdlZWssIGZhbHNlKTtcbiAgICAgIH1cbiAgICB9XG5cbiAgICBmdW5jdGlvbiBzZWxlY3RlZERheSh3ZWVrLCBkYXkpIHtcbiAgICAgIGlmICghZGF5LnNlbGVjdGVkICYmIHdlZWsuc2VsZWN0ZWQpIHtcbiAgICAgICAgd2Vlay5zZWxlY3RlZCA9IGZhbHNlO1xuICAgICAgfVxuXG4gICAgICBpZiAoZGF5LnNlbGVjdGVkKSB7XG4gICAgICAgIHdlZWsuc2VsZWN0ZWRBbnlEYXRlcyA9IHRydWU7XG4gICAgICAgIHZhciBhbGxEYXlzT2ZXZWVrU2VsZWN0ZWQgPSB0cnVlO1xuXG4gICAgICAgIGZvciAodmFyIGkgPSAwLCB3ZWVrZGF5OyAod2Vla2RheSA9IHdlZWsuZGF0ZXNbaV0pOyBpKyspIHtcbiAgICAgICAgICBpZiAod2Vla2RheS5hbGxvdyAmJiAhd2Vla2RheS5zZWxlY3RlZCkge1xuICAgICAgICAgICAgYWxsRGF5c09mV2Vla1NlbGVjdGVkID0gZmFsc2U7XG4gICAgICAgICAgICBicmVhaztcbiAgICAgICAgICB9XG4gICAgICAgIH1cblxuICAgICAgICBpZiAoYWxsRGF5c09mV2Vla1NlbGVjdGVkKSB7XG4gICAgICAgICAgd2Vlay5zZWxlY3RlZCA9IHRydWU7XG4gICAgICAgIH1cbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIC8vQ2hlY2sgdGhhdCBhbnkgZGF5IGlzIHNlbGVjdGVkXG4gICAgICAgIHZhciBzZWxlY3RlZEFueURhdGUgPSBmYWxzZTtcbiAgICAgICAgZm9yICh2YXIgaiA9IDAsIGRhdGU7IChkYXRlID0gd2Vlay5kYXRlc1tqXSk7IGorKykge1xuICAgICAgICAgIGlmIChkYXRlLnNlbGVjdGVkKSB7XG4gICAgICAgICAgICBzZWxlY3RlZEFueURhdGUgPSB0cnVlO1xuICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgfVxuICAgICAgICB9XG5cbiAgICAgICAgd2Vlay5zZWxlY3RlZEFueURhdGVzID0gc2VsZWN0ZWRBbnlEYXRlO1xuICAgICAgfVxuICAgIH1cblxuICAgIGZ1bmN0aW9uIHNldFNlbGVjdGVkRGF0ZXNPZldlZWsod2VlaywgdmFsdWUpIHtcbiAgICAgIGZvciAodmFyIGkgPSAwLCBkYXk7IChkYXkgPSB3ZWVrLmRhdGVzW2ldKTsgaSsrKSB7XG4gICAgICAgIGlmIChkYXkuYWxsb3cpIHtcbiAgICAgICAgICBkYXkuc2VsZWN0ZWQgPSB2YWx1ZTtcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH1cblxuICB9XG59KSgpO1xuIiwiKGZ1bmN0aW9uICgpIHtcbiAgJ3VzZSBzdHJpY3QnO1xuXG4gIGFuZ3VsYXJcbiAgICAubW9kdWxlKCdib29raW5nJylcbiAgICAuY29udHJvbGxlcignRWRpdE9yZGVyQ29udHJvbGxlcicsIEVkaXRPcmRlckNvbnRyb2xsZXIpO1xuXG4gIEVkaXRPcmRlckNvbnRyb2xsZXIuJGluamVjdCA9IFsnb3JkZXInLCAnY2hpbGRyZW4nLCAnZXZlbnRzJywgJ29yZGVyU2VydmljZScsICckc3RhdGUnLCAnYWxlcnRTZXJ2aWNlJywgJ2hlbHBlclNlcnZpY2UnXTtcblxuICAvKiBAbmdJbmplY3QgKi9cbiAgZnVuY3Rpb24gRWRpdE9yZGVyQ29udHJvbGxlcihvcmRlciwgY2hpbGRyZW4sIGV2ZW50cywgb3JkZXJTZXJ2aWNlLCAkc3RhdGUsIGFsZXJ0U2VydmljZSwgaGVscGVyU2VydmljZSkge1xuICAgIC8qIGpzaGludCB2YWxpZHRoaXM6IHRydWUgKi9cbiAgICB2YXIgdm0gPSB0aGlzO1xuXG4gICAgdm0uZm9ybSA9IGFuZ3VsYXIuY29weShvcmRlcik7XG4gICAgdm0uY2hpbGRyZW4gPSBjaGlsZHJlbjtcbiAgICB2bS5ldmVudHMgPSBldmVudHM7XG5cbiAgICB2bS51cGRhdGUgPSB1cGRhdGU7XG4gICAgdm0uc2VsZWN0ZWRFdmVudCA9IHNlbGVjdGVkRXZlbnQ7XG5cbiAgICAvLy8vLy8vLy8vLy8vLy8vLy8vLy8vL1xuXG4gICAgZnVuY3Rpb24gdXBkYXRlKCkge1xuICAgICAgaWYoaGVscGVyU2VydmljZS5TaG93VmFsaWRhdGlvbkVycm9ycyhvcmRlclNlcnZpY2UudmFsaWRhdGUodm0uZm9ybSkpLmxlbmd0aCkge1xuICAgICAgICByZXR1cm47XG4gICAgICB9XG5cbiAgICAgIG9yZGVyU2VydmljZS51cGRhdGUodm0uZm9ybS5faWQsIHZtLmZvcm0pLnRoZW4oZnVuY3Rpb24gKCkge1xuICAgICAgICBhbGVydFNlcnZpY2Uuc3VjY2VzcygnVGhlIG9yZGVyIHdhcyB1cGRhdGVkIScpO1xuXG4gICAgICAgICRzdGF0ZS5nbygnXi5pbmRleCcpO1xuICAgICAgfSk7XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gc2VsZWN0ZWRFdmVudCgpIHtcbiAgICAgIGhlbHBlclNlcnZpY2UucHVzaEV2ZW50RGF0ZXModm0uZXZlbnRzLCB2bS5mb3JtKTtcbiAgICB9XG5cblxuICB9XG59KSgpO1xuIiwiKGZ1bmN0aW9uICgpIHtcbiAgJ3VzZSBzdHJpY3QnO1xuXG4gIGFuZ3VsYXJcbiAgICAubW9kdWxlKCdib29raW5nJylcbiAgICAuY29udHJvbGxlcignT3JkZXJzQ29udHJvbGxlcicsIE9yZGVyc0NvbnRyb2xsZXIpO1xuXG4gIE9yZGVyc0NvbnRyb2xsZXIuJGluamVjdCA9IFsnb3JkZXJzJywgJ29yZGVyU2VydmljZScsICd0b2FzdGVyJ107XG5cbiAgLyogQG5nSW5qZWN0ICovXG4gIGZ1bmN0aW9uIE9yZGVyc0NvbnRyb2xsZXIob3JkZXJzLCBvcmRlclNlcnZpY2UsIHRvYXN0ZXIpIHtcbiAgICAvKiBqc2hpbnQgdmFsaWR0aGlzOiB0cnVlICovXG4gICAgdmFyIHZtID0gdGhpcztcblxuICAgIHZtLm9yZGVycyA9IG9yZGVycztcblxuICAgIHZtLnJlbW92ZSA9IHJlbW92ZTtcblxuICAgIC8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vXG5cbiAgICBmdW5jdGlvbiByZW1vdmUoaWQpIHtcbiAgICAgIG9yZGVyU2VydmljZS5yZW1vdmUoaWQpLnRoZW4oZnVuY3Rpb24gKCkge1xuICAgICAgICAvL1JlZnJlc2ggZGF0YVxuICAgICAgICBvcmRlclNlcnZpY2UuYWxsKCkudGhlbihmdW5jdGlvbiAob3JkZXJzKSB7XG4gICAgICAgICAgdm0ub3JkZXJzID0gb3JkZXJzO1xuXG4gICAgICAgICAgdG9hc3Rlci5zdWNjZXNzKCdUaGUgb3JkZXIgd2FzIHJlbW92ZWQnKTtcbiAgICAgICAgfSk7XG4gICAgICB9KTtcbiAgICB9XG5cbiAgfVxufSkoKTtcbiIsIihmdW5jdGlvbiAoKSB7XG4gICd1c2Ugc3RyaWN0JztcblxuICBhbmd1bGFyXG4gICAgLm1vZHVsZSgnYm9va2luZycpXG4gICAgLmNvbnRyb2xsZXIoJ1Nob3dPcmRlckNvbnRyb2xsZXInLCBTaG93T3JkZXJDb250cm9sbGVyKTtcblxuICBTaG93T3JkZXJDb250cm9sbGVyLiRpbmplY3QgPSBbJ29yZGVyJ107XG5cbiAgLyogQG5nSW5qZWN0ICovXG4gIGZ1bmN0aW9uIFNob3dPcmRlckNvbnRyb2xsZXIob3JkZXIpIHtcbiAgICAvKiBqc2hpbnQgdmFsaWR0aGlzOiB0cnVlICovXG4gICAgdmFyIHZtID0gdGhpcztcblxuICAgIHZtLm9yZGVyID0gb3JkZXI7XG4gIH1cbn0pKCk7XG4iLCIoZnVuY3Rpb24gKCkge1xuICAndXNlIHN0cmljdCc7XG5cbiAgYW5ndWxhclxuICAgIC5tb2R1bGUoJ2Jvb2tpbmcnKVxuICAgIC5jb250cm9sbGVyKCdQcm9maWxlQ29udHJvbGxlcicsIFByb2ZpbGVDb250cm9sbGVyKTtcblxuICBQcm9maWxlQ29udHJvbGxlci4kaW5qZWN0ID0gWydwcm9maWxlJywgJ3Byb2ZpbGVTZXJ2aWNlJywgJyRzdGF0ZScsICd0b2FzdGVyJywgJ2FjY29zdHMnLCAnYm9vbGVhblZhbHVlcycsICdjb3VudHJpZXMnLCAnJGZpbHRlciddO1xuXG4gIC8qIEBuZ0luamVjdCAqL1xuICBmdW5jdGlvbiBQcm9maWxlQ29udHJvbGxlcihwcm9maWxlLCBwcm9maWxlU2VydmljZSwgJHN0YXRlLCB0b2FzdGVyLCBhY2Nvc3RzLCBib29sZWFuVmFsdWVzLCBjb3VudHJpZXMsICRmaWx0ZXIpIHtcbiAgICAvKiBqc2hpbnQgdmFsaWR0aGlzOiB0cnVlICovXG4gICAgdmFyIHZtID0gdGhpcztcblxuICAgIHZtLnByb2ZpbGUgPSBhbmd1bGFyLmNvcHkocHJvZmlsZSk7XG4gICAgdm0uZm9ybSA9IGFuZ3VsYXIuY29weSh2bS5wcm9maWxlKTtcbiAgICB2bS5hY2Nvc3RzID0gYWNjb3N0cztcbiAgICB2bS5ib29sZWFuVmFsdWVzID0gYm9vbGVhblZhbHVlcztcbiAgICB2bS5jb3VudHJpZXMgPSBjb3VudHJpZXM7XG4gICAgdm0ucmVxdWVzdCA9IGZhbHNlO1xuXG4gICAgdm0udXBkYXRlID0gdXBkYXRlO1xuICAgIHZtLnJlc2V0ID0gcmVzZXQ7XG5cbiAgICAvLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vL1xuXG4gICAgZnVuY3Rpb24gdXBkYXRlKCkge1xuICAgICAgdmFyIGZvcm0gPSBhbmd1bGFyLmNvcHkodm0uZm9ybSk7XG4gICAgICBmb3JtLmNvdW50cnlfaWQgPSBmb3JtLmNvdW50cnkuaWQ7XG5cbiAgICAgIHZtLnJlcXVlc3QgPSB0cnVlO1xuXG4gICAgICBwcm9maWxlU2VydmljZS51cGRhdGUoZm9ybSkudGhlbihmdW5jdGlvbiAoKSB7XG4gICAgICAgIHZtLnByb2ZpbGUgPSB2bS5mb3JtO1xuXG4gICAgICAgIHRvYXN0ZXIuc3VjY2VzcygnUHJvZmlsZSB1cGRhdGVkIScpO1xuXG4gICAgICAgICRzdGF0ZS5nbygnXi5zaG93Jyk7XG4gICAgICB9KS5maW5hbGx5KGZ1bmN0aW9uICgpIHtcbiAgICAgICAgdm0ucmVxdWVzdCA9IGZhbHNlO1xuICAgICAgfSk7XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gcmVzZXQoKSB7XG4gICAgICB2bS5mb3JtID0gYW5ndWxhci5jb3B5KHZtLnByb2ZpbGUpO1xuICAgIH1cbiAgfVxufSkoKTtcbiIsIihmdW5jdGlvbigpIHtcbiAgJ3VzZSBzdHJpY3QnO1xuXG4gIGFuZ3VsYXJcbiAgICAubW9kdWxlKCdib29raW5nJylcbiAgICAuc2VydmljZSgnYWxlcnRTZXJ2aWNlJywgYWxlcnRTZXJ2aWNlKTtcblxuICBhbGVydFNlcnZpY2UuJGluamVjdCA9IFsndG9hc3RlciddO1xuXG4gIC8qIEBuZ0luamVjdCAqL1xuICBmdW5jdGlvbiBhbGVydFNlcnZpY2UgKHRvYXN0ZXIpIHtcbiAgICAvKiBqc2hpbnQgdmFsaWR0aGlzOiB0cnVlICovXG4gICAgdmFyIHZtID0gdGhpcztcblxuICAgIHZtLnBvcCA9IHBvcDtcbiAgICB2bS5zdWNjZXNzID0gc3VjY2VzcztcbiAgICB2bS5lcnJvciA9IGVycm9yO1xuICAgIHZtLmluZm8gPSBpbmZvO1xuICAgIHZtLnNob3dSZXNwb25zZUVycm9yID0gc2hvd1Jlc3BvbnNlRXJyb3I7XG5cbiAgICBmdW5jdGlvbiBwb3AoZGF0YSkge1xuICAgICAgdG9hc3Rlci5wb3AoZGF0YSk7XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gc3VjY2VzcyAoZGF0YSkge1xuICAgICAgZGF0YSA9IHByZXBhcmVEYXRhKGRhdGEsICdTdWNjZXNzJyk7XG4gICAgICB0b2FzdGVyLnN1Y2Nlc3MoZGF0YSk7XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gaW5mbyhkYXRhKSB7XG4gICAgICBwcmVwYXJlRGF0YShkYXRhLCAnSW5mbycpO1xuICAgICAgdG9hc3Rlci5pbmZvKGRhdGEpO1xuICAgIH1cblxuICAgIGZ1bmN0aW9uIGVycm9yKGRhdGEpIHtcbiAgICAgIHByZXBhcmVEYXRhKGRhdGEsICdFcnJvcicpO1xuICAgICAgdG9hc3Rlci5lcnJvcihkYXRhKTtcbiAgICB9XG5cbiAgICBmdW5jdGlvbiBzaG93UmVzcG9uc2VFcnJvcihyZXNwb25zZSwgdGl0bGUpIHtcbiAgICAgIGZvcih2YXIga2V5IGluIHJlc3BvbnNlKSB7XG4gICAgICAgIGlmIChyZXNwb25zZS5oYXNPd25Qcm9wZXJ0eShrZXkpICYmIChyZXNwb25zZVtrZXldIGluc3RhbmNlb2YgQXJyYXkpKSB7XG4gICAgICAgICAgZm9yKHZhciBpIGluIHJlc3BvbnNlW2tleV0pIHtcbiAgICAgICAgICAgIGlmIChyZXNwb25zZVtrZXldLmhhc093blByb3BlcnR5KGkpKSB7XG4gICAgICAgICAgICAgIGVycm9yKHtcbiAgICAgICAgICAgICAgICB0aXRsZTogdGl0bGUgPyB0aXRsZSA6ICdFcnJvcicsXG4gICAgICAgICAgICAgICAgYm9keTogcmVzcG9uc2Vba2V5XVtpXVxuICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgZXJyb3Ioe1xuICAgICAgICAgICAgdGl0bGU6IHRpdGxlLFxuICAgICAgICAgICAgYm9keTogcmVzcG9uc2Vba2V5XVxuICAgICAgICAgIH0pO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gcHJlcGFyZURhdGEoZGF0YSwgdGl0bGUpIHtcbiAgICAgIGlmICghKGRhdGEgaW5zdGFuY2VvZiBPYmplY3QpKSB7XG4gICAgICAgIGRhdGEgPSB7XG4gICAgICAgICAgYm9keTogZGF0YVxuICAgICAgICB9O1xuICAgICAgfVxuXG4gICAgICBkYXRhLnRpdGxlID0gZGF0YS50aXRsZSA/IGRhdGEudGl0bGUgOiB0aXRsZTtcblxuICAgICAgcmV0dXJuIGRhdGE7XG4gICAgfVxuICB9XG5cblxufSkoKTtcbiIsIihmdW5jdGlvbiAoKSB7XG4gICd1c2Ugc3RyaWN0JztcblxuICBhbmd1bGFyXG4gICAgLm1vZHVsZSgnYm9va2luZycpXG4gICAgLnNlcnZpY2UoJ2Jvb2tpbmdTZXJ2aWNlJywgYm9va2luZ1NlcnZpY2UpO1xuXG4gIGJvb2tpbmdTZXJ2aWNlLiRpbmplY3QgPSBbJyRodHRwJywgJ2hlbHBlclNlcnZpY2UnLCAnJHJvb3RTY29wZSddO1xuXG4gIC8qIEBuZ0luamVjdCAqL1xuICBmdW5jdGlvbiBib29raW5nU2VydmljZSgkaHR0cCwgaGVscGVyU2VydmljZSwgJHJvb3RTY29wZSkge1xuICAgIC8qIGpzaGludCB2YWxpZHRoaXM6IHRydWUgKi9cbiAgICB2YXIgdm0gPSB0aGlzO1xuXG4gICAgdm0uYm9vayA9IGJvb2s7XG4gICAgdm0uY2FsY3VsYXRlID0gY2FsY3VsYXRlO1xuICAgIHZtLmNoZWNrQ291cG9uID0gY2hlY2tDb3Vwb247XG5cbiAgICAvLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vXG5cbiAgICBmdW5jdGlvbiBib29rKGRhdGEpIHtcbiAgICAgIHJldHVybiAkaHR0cC5wb3N0KCcvYWRtaW4vYXBpLycgKyAkcm9vdFNjb3BlLmN1c3RvbWVySWQgKyAnL2Jvb2tpbmcvJywgaGVscGVyU2VydmljZS5wcmVwYXJlQm9va2luZ0ZvclJlcXVlc3QoZGF0YSkpLnRoZW4oaGVscGVyU2VydmljZS5mZXRjaFJlc3BvbnNlKTtcbiAgICB9XG5cbiAgICBmdW5jdGlvbiBjYWxjdWxhdGUob3JkZXJzKSB7XG4gICAgICByZXR1cm4gJGh0dHAucG9zdCgnL2FwaS9ib29raW5nL2NhbGN1bGF0ZScsIHtvcmRlcnM6IG9yZGVyc30pLnRoZW4oaGVscGVyU2VydmljZS5mZXRjaFJlc3BvbnNlKS50aGVuKGZ1bmN0aW9uIChkYXRhKSB7XG4gICAgICAgIHJldHVybiBkYXRhO1xuICAgICAgfSk7XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gY2hlY2tDb3Vwb24oY291cG9uKSB7XG4gICAgICByZXR1cm4gJGh0dHAuZ2V0KCcvYXBpL2Jvb2tpbmcvY291cG9ucy8nICsgY291cG9uKS50aGVuKGhlbHBlclNlcnZpY2UuZmV0Y2hSZXNwb25zZSk7XG4gICAgfVxuICB9XG59KSgpO1xuIiwiKGZ1bmN0aW9uICgpIHtcbiAgJ3VzZSBzdHJpY3QnO1xuXG4gIGFuZ3VsYXJcbiAgICAubW9kdWxlKCdib29raW5nJylcbiAgICAuc2VydmljZSgnY2hpbGRTZXJ2aWNlJywgY2hpbGRTZXJ2aWNlKTtcblxuICBjaGlsZFNlcnZpY2UuJGluamVjdCA9IFsnJHJlc291cmNlJywgJyRodHRwJywgJ2hlbHBlclNlcnZpY2UnLCAnJHJvb3RTY29wZScsICckdGltZW91dCcsICckcSddO1xuXG4gIC8qIEBuZ0luamVjdCAqL1xuICBmdW5jdGlvbiBjaGlsZFNlcnZpY2UoJHJlc291cmNlLCAkaHR0cCwgaGVscGVyU2VydmljZSwgJHJvb3RTY29wZSwgJHRpbWVvdXQsICRxKSB7XG4gICAgLyoganNoaW50IHZhbGlkdGhpczogdHJ1ZSAqL1xuICAgIHZhciB2bSA9IHRoaXM7XG5cbiAgICB2YXIgQ2hpbGQ7XG5cbiAgICB2YXIgcnVuID0gJHRpbWVvdXQoZnVuY3Rpb24gKCkge1xuICAgICAgQ2hpbGQgPSAkcmVzb3VyY2UoJy9hZG1pbi9hcGkvY3VzdG9tZXJzLycgKyAkcm9vdFNjb3BlLmN1c3RvbWVySWQgKyAnL2NoaWxkcmVuLzpjaGlsZElkJywge2NoaWxkSWQ6ICdAaWQnfSwge1xuICAgICAgICB1cGRhdGU6IHttZXRob2Q6ICdQVVQnfVxuICAgICAgfSk7XG4gICAgfSk7XG5cbiAgICB2bS5hbGwgPSBhbGw7XG4gICAgdm0uZ2V0ID0gZ2V0O1xuICAgIHZtLnN0b3JlID0gc3RvcmU7XG4gICAgdm0udXBkYXRlID0gdXBkYXRlO1xuICAgIHZtLnJlbW92ZSA9IHJlbW92ZTtcbiAgICB2bS5zd2ltT3B0aW9ucyA9IHN3aW1PcHRpb25zO1xuICAgIHZtLnllYXJzSW5TY2hvb2wgPSB5ZWFyc0luU2Nob29sO1xuXG4gICAgLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy9cblxuICAgIGZ1bmN0aW9uIHllYXJzSW5TY2hvb2woKSB7XG4gICAgICByZXR1cm4gJGh0dHAuZ2V0KCcvYXBpL3llYXJzLWluLXNjaG9vbCcpLnRoZW4oaGVscGVyU2VydmljZS5mZXRjaFJlc3BvbnNlKTtcbiAgICB9XG5cbiAgICBmdW5jdGlvbiBzd2ltT3B0aW9ucygpIHtcbiAgICAgIHJldHVybiAkaHR0cC5nZXQoJy9hcGkvc3dpbS1vcHRpb25zJykudGhlbihoZWxwZXJTZXJ2aWNlLmZldGNoUmVzcG9uc2UpO1xuICAgIH1cblxuICAgIGZ1bmN0aW9uIGFsbCgpIHtcbiAgICAgIHJldHVybiAkcS5hbGwoW3J1bl0pLnRoZW4oZnVuY3Rpb24gKCkge1xuICAgICAgICByZXR1cm4gQ2hpbGQucXVlcnkoKVxuICAgICAgICAgICAgLiRwcm9taXNlXG4gICAgICAgICAgICAudGhlbihmdW5jdGlvbiAoY2hpbGRyZW4pIHtcbiAgICAgICAgICAgICAgZm9yKHZhciBpID0gMCwgY2hpbGQ7IChjaGlsZCA9IGNoaWxkcmVuW2ldKTsgaSsrKSB7XG4gICAgICAgICAgICAgICAgY2hpbGQuYmlydGhkYXkgPSBtb21lbnQoY2hpbGQuYmlydGhkYXkpLnRvRGF0ZSgpO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgIHJldHVybiBjaGlsZHJlbjtcbiAgICAgICAgICAgIH0pO1xuICAgICAgfSk7XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gZ2V0KGlkKSB7XG4gICAgICByZXR1cm4gJHEuYWxsKFtydW5dKS50aGVuKGZ1bmN0aW9uICgpIHtcbiAgICAgICAgcmV0dXJuIENoaWxkLmdldCh7Y2hpbGRJZDogaWR9KVxuICAgICAgICAgIC4kcHJvbWlzZVxuICAgICAgICAgIC50aGVuKGZ1bmN0aW9uIChjaGlsZCkge1xuICAgICAgICAgICAgY2hpbGQuYmlydGhkYXkgPSBtb21lbnQoY2hpbGQuYmlydGhkYXkpLnRvRGF0ZSgpO1xuXG4gICAgICAgICAgICByZXR1cm4gY2hpbGQ7XG4gICAgICAgICAgfSk7XG4gICAgICB9KTtcbiAgICB9XG5cbiAgICBmdW5jdGlvbiBzdG9yZShkYXRhKSB7XG4gICAgICByZXR1cm4gQ2hpbGQuc2F2ZShkYXRhKS4kcHJvbWlzZTtcbiAgICB9XG5cbiAgICBmdW5jdGlvbiB1cGRhdGUoZGF0YSkge1xuICAgICAgcmV0dXJuIENoaWxkLnVwZGF0ZShkYXRhKS4kcHJvbWlzZTtcbiAgICB9XG5cbiAgICBmdW5jdGlvbiByZW1vdmUoaWQpIHtcbiAgICAgIHJldHVybiBDaGlsZC5kZWxldGUoe2NoaWxkSWQ6IGlkfSkuJHByb21pc2U7XG4gICAgfVxuICB9XG59KSgpO1xuIiwiKGZ1bmN0aW9uKCkge1xuICAndXNlIHN0cmljdCc7XG5cbiAgYW5ndWxhclxuICAgIC5tb2R1bGUoJ2Jvb2tpbmcnKVxuICAgIC5zZXJ2aWNlKCdldmVudFNlcnZpY2UnLCBldmVudFNlcnZpY2UpO1xuXG4gIGV2ZW50U2VydmljZS4kaW5qZWN0ID0gWyckaHR0cCcsICdoZWxwZXJTZXJ2aWNlJ107XG5cbiAgLyogQG5nSW5qZWN0ICovXG4gIGZ1bmN0aW9uIGV2ZW50U2VydmljZSgkaHR0cCwgaGVscGVyU2VydmljZSkge1xuICAgIC8qIGpzaGludCB2YWxpZHRoaXM6IHRydWUgKi9cbiAgICB2YXIgdm0gPSB0aGlzO1xuXG4gICAgdm0uYWxsID0gYWxsO1xuXG4gICAgLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vXG5cbiAgICBmdW5jdGlvbiBhbGwoKSB7XG4gICAgICByZXR1cm4gJGh0dHAuZ2V0KCcvYXBpL2V2ZW50cycpXG4gICAgICAgIC50aGVuKGhlbHBlclNlcnZpY2UuZmV0Y2hSZXNwb25zZSwgaGVscGVyU2VydmljZS5yZWplY3RSZXNwb25zZSlcbiAgICAgICAgLnRoZW4oZnVuY3Rpb24gKGV2ZW50cykge1xuICAgICAgICAgIGZvcih2YXIgaSA9IDAsIGV2ZW50OyAoZXZlbnQgPSBldmVudHNbaV0pOyBpKyspIHtcbiAgICAgICAgICAgIGV2ZW50LnN0YXJ0X2RhdGUgPSBtb21lbnQoZXZlbnQuc3RhcnRfZGF0ZSk7XG4gICAgICAgICAgICBldmVudC5lbmRfZGF0ZSA9IG1vbWVudChldmVudC5lbmRfZGF0ZSk7XG4gICAgICAgICAgfVxuXG4gICAgICAgICAgcmV0dXJuIGV2ZW50cztcbiAgICAgICAgfSk7XG4gICAgfVxuICB9XG59KSgpO1xuIiwiKGZ1bmN0aW9uICgpIHtcbiAgJ3VzZSBzdHJpY3QnO1xuXG4gIGFuZ3VsYXJcbiAgICAubW9kdWxlKCdib29raW5nJylcbiAgICAuc2VydmljZSgnaGVscGVyU2VydmljZScsIGhlbHBlclNlcnZpY2UpO1xuXG4gIC8qIEBuZ0luamVjdCAqL1xuICBmdW5jdGlvbiBoZWxwZXJTZXJ2aWNlKCRmaWx0ZXIsIGFsZXJ0U2VydmljZSwgJHEsIHN0b3JlKSB7XG4gICAgLyoganNoaW50IHZhbGlkdGhpczogdHJ1ZSAqL1xuICAgIHZhciB2bSA9IHRoaXM7XG5cbiAgICB2bS5mZXRjaFJlc3BvbnNlID0gZmV0Y2hSZXNwb25zZTtcbiAgICB2bS5yZWplY3RSZXNwb25zZSA9IHJlamVjdFJlc3BvbnNlO1xuICAgIHZtLnB1c2hFdmVudERhdGVzID0gcHVzaEV2ZW50RGF0ZXM7XG4gICAgdm0uU2hvd1ZhbGlkYXRpb25FcnJvcnMgPSBTaG93VmFsaWRhdGlvbkVycm9ycztcbiAgICB2bS5yZWRpcmVjdCA9IHJlZGlyZWN0O1xuICAgIHZtLnByZXBhcmVPcmRlcnMgPSBwcmVwYXJlT3JkZXJzO1xuICAgIHZtLnByZXBhcmVQcm9kdWN0cyA9IHByZXBhcmVQcm9kdWN0cztcbiAgICB2bS5wcmVwYXJlQm9va2luZ0ZvclJlcXVlc3QgPSBwcmVwYXJlQm9va2luZ0ZvclJlcXVlc3Q7XG5cbiAgICAvLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vXG5cbiAgICBmdW5jdGlvbiBwcmVwYXJlT3JkZXJzKG9yZGVycylcbiAgICB7XG4gICAgICB2YXIgX29yZGVycyA9IFtdO1xuXG4gICAgICBmb3IgKHZhciBpID0gMCwgb3JkZXI7IChvcmRlciA9IG9yZGVyc1tpXSk7IGkrKykge1xuICAgICAgICB2YXIgc2VsZWN0ZWREYXRlcyA9IFtdO1xuICAgICAgICBmb3IgKHZhciBqID0gMCwgd2VlazsgKHdlZWsgPSBvcmRlci53ZWVrc1tqXSk7IGorKykge1xuICAgICAgICAgIGZvciAodmFyIGsgPSAwLCBkYXk7IChkYXkgPSB3ZWVrLmRhdGVzW2tdKTsgaysrKSB7XG4gICAgICAgICAgICBpZiAoZGF5LnNlbGVjdGVkKSB7XG4gICAgICAgICAgICAgIHZhciBkYXRlID0gbmV3IERhdGUoZGF5LmRhdGUpO1xuICAgICAgICAgICAgICBpZighIChkYXRlLmdldE1pbnV0ZXMoKSB8fCBkYXRlLmdldEhvdXJzKCkpKSB7XG4gICAgICAgICAgICAgICAgZGF0ZS5zZXRNaW51dGVzKGRhdGUuZ2V0TWludXRlcygpIC0gZGF0ZS5nZXRUaW1lem9uZU9mZnNldCgpKTtcbiAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgIHNlbGVjdGVkRGF0ZXMucHVzaChkYXRlKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9XG4gICAgICAgIH1cblxuICAgICAgICBfb3JkZXJzLnB1c2goe1xuICAgICAgICAgIF9pZDogb3JkZXIuX2lkLFxuICAgICAgICAgIGNoaWxkX2lkOiBvcmRlci5jaGlsZF9pZCxcbiAgICAgICAgICBldmVudF9pZDogb3JkZXIuZXZlbnRfaWQsXG4gICAgICAgICAgc2VsZWN0ZWREYXRlczogc2VsZWN0ZWREYXRlcyxcbiAgICAgICAgICB2YXJpYW50czogb3JkZXIudmFyaWFudHNcbiAgICAgICAgfSk7XG4gICAgICB9XG5cbiAgICAgIHJldHVybiBfb3JkZXJzO1xuICAgIH1cblxuICAgIGZ1bmN0aW9uIHByZXBhcmVQcm9kdWN0cyhwcm9kdWN0cykge1xuICAgICAgdmFyIGxvY2FsUHJvZHVjdHMgPSBzdG9yZS5nZXQoJ3Byb2R1Y3RzJyk7XG5cbiAgICAgIHJldHVybiBhbmd1bGFyLmZvckVhY2gocHJvZHVjdHMsIGZ1bmN0aW9uIChwcm9kdWN0KSB7XG4gICAgICAgIHByb2R1Y3QuaW1hZ2VQYXRoID0gcHJvZHVjdC5pbWFnZV90aHVtYm5haWwgPyAnLycgKyBwcm9kdWN0LmltYWdlX3RodW1ibmFpbC5wYXRoIDogJyc7XG5cbiAgICAgICAgaWYgKGxvY2FsUHJvZHVjdHMpIHtcbiAgICAgICAgICB2YXIgbG9jYWxQcm9kdWN0ID0gJGZpbHRlcignZmlsdGVyJykobG9jYWxQcm9kdWN0cywge2lkOiBwcm9kdWN0LmlkfSlbMF07XG5cbiAgICAgICAgICBpZiAobG9jYWxQcm9kdWN0KSB7XG4gICAgICAgICAgICBwcm9kdWN0LnNlbGVjdGVkVmFyaWFudCA9IGxvY2FsUHJvZHVjdC5zZWxlY3RlZFZhcmlhbnQ7XG4gICAgICAgICAgICBwcm9kdWN0LnNlbGVjdGVkUXVhbnRpdHkgPSBsb2NhbFByb2R1Y3Quc2VsZWN0ZWRRdWFudGl0eTtcbiAgICAgICAgICB9XG4gICAgICAgIH1cblxuICAgICAgICBwcm9kdWN0LnByaWNlID0gMDtcblxuICAgICAgICB2YXIgdmFyaWFudCA9IHByb2R1Y3QudmFyaWFudHMuZmlsdGVyKGZ1bmN0aW9uICh2YXJpYW50KSB7XG4gICAgICAgICAgcmV0dXJuIHByb2R1Y3Quc2VsZWN0ZWRWYXJpYW50ICYmIHByb2R1Y3Quc2VsZWN0ZWRWYXJpYW50ID09PSB2YXJpYW50LmlkO1xuICAgICAgICB9KVswXTtcblxuICAgICAgICBpZiAodmFyaWFudCAmJiBwcm9kdWN0LnNlbGVjdGVkUXVhbnRpdHkpIHtcbiAgICAgICAgICBwcm9kdWN0LnByaWNlID0gdmFyaWFudC5wcmljZSAqIHByb2R1Y3Quc2VsZWN0ZWRRdWFudGl0eTtcbiAgICAgICAgfVxuICAgICAgfSk7XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gcmVkaXJlY3QodXJsLCBtZXRob2QsIGZpZWxkcykge1xuICAgICAgJCgnPGZvcm0+Jywge1xuICAgICAgICBtZXRob2Q6IG1ldGhvZCxcbiAgICAgICAgYWN0aW9uOiB1cmxcbiAgICAgIH0pLmFwcGVuZChmaWVsZHMpLmFwcGVuZFRvKCdib2R5Jykuc3VibWl0KCk7XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gcHJlcGFyZUJvb2tpbmdGb3JSZXF1ZXN0KF9kYXRhKSB7XG4gICAgICB2YXIgZGF0YSA9IGFuZ3VsYXIuY29weShfZGF0YSk7XG5cbiAgICAgIGRhdGEudmFyaWFudHMgPSBbXTtcblxuICAgICAgYW5ndWxhci5mb3JFYWNoKGRhdGEucHJvZHVjdHMsIGZ1bmN0aW9uIChwcm9kdWN0KSB7XG4gICAgICAgIGlmIChwcm9kdWN0LnNlbGVjdGVkVmFyaWFudCAmJiBwcm9kdWN0LnNlbGVjdGVkUXVhbnRpdHkpIHtcbiAgICAgICAgICBkYXRhLnZhcmlhbnRzLnB1c2goe1xuICAgICAgICAgICAgaWQ6IHByb2R1Y3Quc2VsZWN0ZWRWYXJpYW50LFxuICAgICAgICAgICAgY291bnQ6IHByb2R1Y3Quc2VsZWN0ZWRRdWFudGl0eVxuICAgICAgICAgIH0pO1xuICAgICAgICB9XG4gICAgICB9KTtcblxuICAgICAgZGVsZXRlIGRhdGEucHJvZHVjdHM7XG5cbiAgICAgIHJldHVybiBkYXRhO1xuICAgIH1cblxuICAgIGZ1bmN0aW9uIGZldGNoUmVzcG9uc2UoZGF0YSkge1xuICAgICAgcmV0dXJuIGRhdGEuZGF0YTtcbiAgICB9XG5cbiAgICBmdW5jdGlvbiByZWplY3RSZXNwb25zZShkYXRhKSB7XG4gICAgICByZXR1cm4gJHEucmVqZWN0KGRhdGEuZGF0YSk7XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gU2hvd1ZhbGlkYXRpb25FcnJvcnMoZXJyb3JzKSB7XG4gICAgICBpZiAoZXJyb3JzLmxlbmd0aCkge1xuICAgICAgICBmb3IgKHZhciBpID0gMCwgZXJyb3I7IChlcnJvciA9IGVycm9yc1tpXSk7IGkrKykge1xuICAgICAgICAgIGFsZXJ0U2VydmljZS5lcnJvcihlcnJvci5tZXNzYWdlKTtcbiAgICAgICAgfVxuICAgICAgfVxuXG4gICAgICByZXR1cm4gZXJyb3JzO1xuICAgIH1cblxuICAgIGZ1bmN0aW9uIGV4aXN0c0RheShkYXRlcywgZGF0ZSkge1xuICAgICAgZGF0ZS5zZXRIb3VycygwKTtcbiAgICAgIGRhdGUuc2V0TWludXRlcygwKTtcblxuICAgICAgcmV0dXJuICRmaWx0ZXIoJ2ZpbHRlcicpKGRhdGVzLCBmdW5jdGlvbiAoZGF5KSB7XG4gICAgICAgIHJldHVybiBtb21lbnQoZGF0ZSkuZGlmZihtb21lbnQoZGF5LmRhdGUpKSA9PT0gMDtcbiAgICAgIH0pLmxlbmd0aDtcbiAgICB9XG5cbiAgICBmdW5jdGlvbiBmaW5kRXZlbnQoZXZlbnRzLCBldmVudElkKSB7XG4gICAgICBmb3IodmFyIGkgPSAwLCBldmVudDsgKGV2ZW50ID0gZXZlbnRzW2ldKTsgaSsrKSB7XG4gICAgICAgIGlmKGV2ZW50LmlkID09PSBldmVudElkKSB7XG4gICAgICAgICAgcmV0dXJuIGFuZ3VsYXIuY29weShldmVudCk7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG5cbiAgICBmdW5jdGlvbiBwdXNoRXZlbnREYXRlcyhldmVudHMsIGZvcm0pIHtcbiAgICAgIC8vUHJlcGFyYXRpb24gZm9yIGVkaXQgb3JkZXJcbiAgICAgIC8qaWYgKGZvcm0ud2Vla3MpIHtcbiAgICAgICB2YXIgc2VsZWN0ZWREYXRlcyA9IFtdO1xuXG4gICAgICAgZm9yICh2YXIgayA9IDAsIHByZXZpb3VzV2VlazsgKHByZXZpb3VzV2VlayA9IGZvcm0ud2Vla3Nba10pOyBrKyspIHtcbiAgICAgICBmb3IgKHZhciBwID0gMCwgZGF5OyAoZGF5ID0gcHJldmlvdXNXZWVrLmRhdGVzW3BdKTsgcCsrKSB7XG4gICAgICAgaWYgKGRheS5zZWxlY3RlZCkge1xuICAgICAgIHNlbGVjdGVkRGF0ZXMucHVzaChkYXkpO1xuICAgICAgIH1cbiAgICAgICB9XG4gICAgICAgfVxuICAgICAgIH0qL1xuXG4gICAgICAvL0ZpbmQgdGhlIGV2ZW50XG4gICAgICB2YXIgZXZlbnQgPSBmaW5kRXZlbnQoZXZlbnRzLCBmb3JtLmV2ZW50X2lkKTtcblxuICAgICAgZm9ybS5ldmVudCA9IGV2ZW50O1xuICAgICAgZm9ybS53ZWVrcyA9IFtdO1xuXG4gICAgICB2YXIgc3RhcnREYXRlID0gYW5ndWxhci5jb3B5KGV2ZW50LnN0YXJ0X2RhdGUpO1xuXG4gICAgICAvLyBSZW1vdmUgc2F0dXJkYXkgYW5kIHN1bmRheSBmcm9tIHBlcmlvZCBpZiBmcm9tIHRoaXMgc3RhcnRcbiAgICAgIGlmIChzdGFydERhdGUud2Vla2RheSgpID09PSAwKSB7XG4gICAgICAgIHN0YXJ0RGF0ZS5hZGQoMSwgJ2QnKTtcbiAgICAgIH0gZWxzZSBpZiAoc3RhcnREYXRlLndlZWtkYXkoKSA9PT0gNikge1xuICAgICAgICBzdGFydERhdGUuYWRkKDIsICdkJyk7XG4gICAgICB9XG5cblxuICAgICAgZm9yICh2YXIgc3RhcnRXZWVrID0gc3RhcnREYXRlLCBtb25kYXlPZldlZWsgPSBuZXcgRGF0ZShzdGFydERhdGUpOyAoc3RhcnRXZWVrIDw9IGV2ZW50LmVuZF9kYXRlKTsgc3RhcnRXZWVrID0gbmV3IERhdGUoKG5ldyBEYXRlKG1vbmRheU9mV2VlaykpLnNldERhdGUoKG5ldyBEYXRlKG1vbmRheU9mV2VlaykpLmdldERhdGUoKSArIDcpKSwgc3RhcnREYXRlID0gbmV3IERhdGUoc3RhcnRXZWVrKSkge1xuICAgICAgICAvLyBHZXQgZGlmZmVyZW5jZSBiZXR3ZWVuIHJlYWwgc3RhcnQgZXZlbnQgYW5kIG1vbmRheSAoMSAtIGJlY2F1c2UgTW9uZGF5IGlzIDEpXG4gICAgICAgIHZhciBkaWZmZXJlbmNlID0gbW9tZW50KHN0YXJ0V2Vlaykud2Vla2RheSgpIC0gMTtcblxuICAgICAgICAvL0dldCBtb25kYXkgb2Ygd2Vla1xuICAgICAgICBtb25kYXlPZldlZWsgPSBuZXcgRGF0ZShzdGFydFdlZWspO1xuICAgICAgICBtb25kYXlPZldlZWsuc2V0RGF0ZShtb25kYXlPZldlZWsuZ2V0RGF0ZSgpIC0gZGlmZmVyZW5jZSk7XG5cbiAgICAgICAgLy9HZXQgZW5kIG9mIHdlZWsobm93IGZyaWRheSlcbiAgICAgICAgdmFyIGZyaWRheU9mV2VlayA9IG5ldyBEYXRlKG1vbmRheU9mV2Vlayk7XG4gICAgICAgIGZyaWRheU9mV2Vlay5zZXREYXRlKGZyaWRheU9mV2Vlay5nZXREYXRlKCkgKyA0KTtcbiAgICAgICAgdmFyIGVuZE9mV2VlayA9IG5ldyBEYXRlKGZyaWRheU9mV2Vlayk7XG5cbiAgICAgICAgdmFyIHdlZWsgPSB7XG4gICAgICAgICAgbW9uZGF5OiBuZXcgRGF0ZShtb25kYXlPZldlZWspLFxuICAgICAgICAgIGZyaWRheTogbmV3IERhdGUoZW5kT2ZXZWVrKSxcbiAgICAgICAgICBzZWxlY3RlZDogZmFsc2UsXG4gICAgICAgICAgYWxsb3c6IHRydWUsLy9BbGxvdyBzZWxlY3QgZnVsbCB3ZWVrIG9yIG5vdC4gTXVzdCBiZSAnZmFsc2UnIGlmIGF0IGxlYXN0IG9uZSB3ZWVrZGF5IGlzIGZ1bGx5XG4gICAgICAgICAgZnVsbHk6IHRydWUsXG4gICAgICAgICAgZGF0ZXM6IFtdLFxuICAgICAgICAgIHNlbGVjdGVkQW55RGF0ZXM6IGZhbHNlXG4gICAgICAgIH07XG5cbiAgICAgICAgLy9JZiBldmVudCBmaW5pc2ggYmVmb3JlIGZyaWRheVxuICAgICAgICBpZiAoZW5kT2ZXZWVrID4gZXZlbnQuZW5kX2RhdGUpIHtcbiAgICAgICAgICBlbmRPZldlZWsgPSBldmVudC5lbmRfZGF0ZTtcbiAgICAgICAgfVxuXG4gICAgICAgIGlmIChtb25kYXlPZldlZWsgPCBzdGFydERhdGUpIHtcbiAgICAgICAgICBmb3IgKHZhciB0RGF0ZSA9IG5ldyBEYXRlKG1vbmRheU9mV2Vlayk7IHREYXRlIDwgc3RhcnREYXRlOyB0RGF0ZS5zZXREYXRlKHREYXRlLmdldERhdGUoKSArIDEpKSB7XG4gICAgICAgICAgICB3ZWVrLmRhdGVzLnB1c2goe1xuICAgICAgICAgICAgICBkYXRlOiBhbmd1bGFyLmNvcHkodERhdGUpLFxuICAgICAgICAgICAgICBhbGxvdzogZmFsc2UsXG4gICAgICAgICAgICAgIHNlbGVjdGVkOiBmYWxzZSxcbiAgICAgICAgICAgICAgZnVsbHk6IGZhbHNlXG4gICAgICAgICAgICB9KTtcbiAgICAgICAgICB9XG4gICAgICAgIH1cblxuICAgICAgICBmb3IgKHZhciBkYXRlID0gbmV3IERhdGUoc3RhcnREYXRlKTsgZGF0ZSA8PSBlbmRPZldlZWs7IGRhdGUuc2V0RGF0ZShkYXRlLmdldERhdGUoKSArIDEpKSB7XG4gICAgICAgICAgLypqc2hpbnQgLVcwODMgKi9cbiAgICAgICAgICB2YXIgZXhjZXB0ID0gJGZpbHRlcignZmlsdGVyJykoZXZlbnQuZXhjZXB0aW9uX2RhdGVzLCAoZnVuY3Rpb24gKGV4Y2VwdGlvbkRhdGUpIHtcbiAgICAgICAgICAgICAgZXhjZXB0aW9uRGF0ZSA9IG1vbWVudChleGNlcHRpb25EYXRlLmRhdGUpLnRvRGF0ZSgpO1xuICAgICAgICAgICAgICByZXR1cm4gZXhjZXB0aW9uRGF0ZS50b0RhdGVTdHJpbmcoKSA9PT0gZGF0ZS50b0RhdGVTdHJpbmcoKTtcbiAgICAgICAgICAgIH0pKSxcbiAgICAgICAgICAgIGFsbG93ID0gKGV4Y2VwdC5sZW5ndGggPiAwKSA/IGZhbHNlIDogdHJ1ZTtcblxuICAgICAgICAgIHdlZWsuZGF0ZXMucHVzaCh7XG4gICAgICAgICAgICBkYXRlOiBhbmd1bGFyLmNvcHkoZGF0ZSksXG4gICAgICAgICAgICBhbGxvdzogYWxsb3csXG4gICAgICAgICAgICBzZWxlY3RlZDogZmFsc2UsXG4gICAgICAgICAgICBmdWxseTogYWxsb3cgPyBleGlzdHNEYXkoZm9ybS5ldmVudC5idXN5X2RhdGVzLCBkYXRlKSA6IGZhbHNlXG4gICAgICAgICAgfSk7XG4gICAgICAgIH1cblxuICAgICAgICBpZiAoZnJpZGF5T2ZXZWVrID4gZW5kT2ZXZWVrKSB7XG4gICAgICAgICAgZm9yIChkYXRlID0gbmV3IERhdGUoZW5kT2ZXZWVrKSwgZGF0ZS5zZXREYXRlKGRhdGUuZ2V0RGF0ZSgpICsgMSk7IGRhdGUgPD0gZnJpZGF5T2ZXZWVrOyBkYXRlLnNldERhdGUoZGF0ZS5nZXREYXRlKCkgKyAxKSkge1xuICAgICAgICAgICAgd2Vlay5kYXRlcy5wdXNoKHtcbiAgICAgICAgICAgICAgZGF0ZTogYW5ndWxhci5jb3B5KGRhdGUpLFxuICAgICAgICAgICAgICBhbGxvdzogZmFsc2UsXG4gICAgICAgICAgICAgIHNlbGVjdGVkOiBmYWxzZSxcbiAgICAgICAgICAgICAgZnVsbHk6IGZhbHNlXG4gICAgICAgICAgICB9KTtcbiAgICAgICAgICB9XG4gICAgICAgIH1cblxuICAgICAgICBmb3IodmFyIGsgPSAwLCBkYXlPZldlZWs7IChkYXlPZldlZWsgPSB3ZWVrLmRhdGVzW2tdKTsgaysrKSB7XG4gICAgICAgICAgaWYoZGF5T2ZXZWVrLmFsbG93ICYmICFkYXlPZldlZWsuZnVsbHkpIHtcbiAgICAgICAgICAgIHdlZWsuZnVsbHkgPSBmYWxzZTtcbiAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgIH1cbiAgICAgICAgfVxuXG4gICAgICAgIGZvcm0ud2Vla3MucHVzaCh3ZWVrKTtcbiAgICAgIH1cbiAgICB9XG4gIH1cbn0pKCk7XG4iLCIoZnVuY3Rpb24gKCkge1xuICAndXNlIHN0cmljdCc7XG5cbiAgYW5ndWxhclxuICAgIC5tb2R1bGUoJ2Jvb2tpbmcnKVxuICAgIC5zZXJ2aWNlKCdvcmRlclNlcnZpY2UnLCBvcmRlclNlcnZpY2UpO1xuXG4gIG9yZGVyU2VydmljZS4kaW5qZWN0ID0gWyckZmlsdGVyJywgJyRxJywgJ3N0b3JlJywgJ2NoaWxkU2VydmljZScsICdldmVudFNlcnZpY2UnLCAnJGh0dHAnLCAnaGVscGVyU2VydmljZScsICckdGltZW91dCcsICckcm9vdFNjb3BlJ107XG5cbiAgLyogQG5nSW5qZWN0ICovXG4gIGZ1bmN0aW9uIG9yZGVyU2VydmljZSgkZmlsdGVyLCAkcSwgc3RvcmUsIGNoaWxkU2VydmljZSwgZXZlbnRTZXJ2aWNlLCAkaHR0cCwgaGVscGVyU2VydmljZSwgJHRpbWVvdXQsICRyb290U2NvcGUpIHtcbiAgICAvKiBqc2hpbnQgdmFsaWR0aGlzOiB0cnVlICovXG4gICAgdmFyIHZtID0gdGhpcztcblxuICAgIHZtLmN1c3RvbWVySWQgPSBzdG9yZS5nZXQoJ2N1c3RvbWVyLWlkJyk7XG5cbiAgICB2YXIgcnVuID0gJHRpbWVvdXQoZnVuY3Rpb24gKCkge1xuICAgICAgaWYodm0uY3VzdG9tZXJJZCAhPSAkcm9vdFNjb3BlLmN1c3RvbWVySWQpIHtcbiAgICAgICAgdm0uY3VzdG9tZXJJZCA9ICRyb290U2NvcGUuY3VzdG9tZXJJZDtcbiAgICAgICAgc3RvcmUuc2V0KCdjdXN0b21lci1pZCcsIHZtLmN1c3RvbWVySWQpO1xuXG4gICAgICAgIHN0b3JlLnJlbW92ZSgnb3JkZXJzJyk7XG4gICAgICAgIHN0b3JlLnJlbW92ZSgnaW5kZXgnKTtcbiAgICAgIH1cblxuICAgICAgdm0ub3JkZXJzID0gc3RvcmUuZ2V0KCdvcmRlcnMnKTtcbiAgICAgIHZtLmluZGV4ID0gc3RvcmUuZ2V0KCdpbmRleCcpO1xuXG4gICAgICBpZiAoIXZtLm9yZGVycykge1xuICAgICAgICB2bS5vcmRlcnMgPSBbXTtcbiAgICAgICAgdm0uaW5kZXggPSAxO1xuICAgICAgfVxuXG4gICAgICB2bS5wcm9taXNlcyA9ICRxLmFsbChbY2hpbGRTZXJ2aWNlLmFsbCgpLCBldmVudFNlcnZpY2UuYWxsKCldKS50aGVuKGZ1bmN0aW9uIChkYXRhKSB7XG4gICAgICAgIHZtLmNoaWxkcmVuID0gZGF0YVswXTtcbiAgICAgICAgdm0uZXZlbnRzID0gZGF0YVsxXTtcbiAgICAgIH0pO1xuICAgIH0pO1xuXG4gICAgdm0udmFsaWRhdGUgPSB2YWxpZGF0ZTtcblxuICAgIHZtLmNvdW50ID0gY291bnQ7XG4gICAgdm0uYWxsID0gYWxsO1xuICAgIHZtLmFkZCA9IGFkZDtcbiAgICB2bS5nZXQgPSBnZXQ7XG4gICAgdm0udXBkYXRlID0gdXBkYXRlO1xuICAgIHZtLnJlbW92ZSA9IHJlbW92ZTtcbiAgICB2bS5jYWxjdWxhdGUgPSBjYWxjdWxhdGU7XG4gICAgdm0uY2xlYXIgPSBjbGVhcjtcbiAgICB2bS5wcm9kdWN0cyA9IHByb2R1Y3RzO1xuICAgIHZtLnNhdmVQcm9kdWN0cyA9IHNhdmVQcm9kdWN0cztcblxuICAgIC8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vXG5cbiAgICBmdW5jdGlvbiBwcm9kdWN0cygpIHtcbiAgICAgIHJldHVybiAkaHR0cC5nZXQoJy9hcGkvcHJvZHVjdHMnKS50aGVuKGhlbHBlclNlcnZpY2UuZmV0Y2hSZXNwb25zZSkudGhlbihoZWxwZXJTZXJ2aWNlLnByZXBhcmVQcm9kdWN0cyk7XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gc2F2ZVByb2R1Y3RzKHByb2R1Y3RzKSB7XG4gICAgICB2YXIgbG9jYWxQcm9taXNlID0gJHEuZGVmZXIoKTtcblxuICAgICAgdm0ucHJvbWlzZXMudGhlbihmdW5jdGlvbiAoKSB7XG4gICAgICAgIHN0b3JlLnNldCgncHJvZHVjdHMnLCBwcm9kdWN0cyk7XG5cbiAgICAgICAgbG9jYWxQcm9taXNlLnJlc29sdmUodHJ1ZSk7XG4gICAgICB9KTtcblxuICAgICAgcmV0dXJuIGxvY2FsUHJvbWlzZS5wcm9taXNlO1xuICAgIH1cblxuICAgIGZ1bmN0aW9uIGNvdW50KCkge1xuICAgICAgcmV0dXJuIHZtLm9yZGVycy5sZW5ndGg7XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gY2xlYXIoKSB7XG4gICAgICB2YXIgb3JkZXJzID0gYW5ndWxhci5jb3B5KHZtLm9yZGVycyk7XG5cbiAgICAgIHZhciBsb2NhbFByb21pc2UgPSAkcS5kZWZlcigpO1xuXG4gICAgICB2bS5wcm9taXNlcy50aGVuKGZ1bmN0aW9uICgpIHtcbiAgICAgICAgc3RvcmUucmVtb3ZlKCdvcmRlcnMnKTtcbiAgICAgICAgc3RvcmUucmVtb3ZlKCdpbmRleCcpO1xuICAgICAgICBzdG9yZS5yZW1vdmUoJ3Byb2R1Y3RzJyk7XG5cbiAgICAgICAgbG9jYWxQcm9taXNlLnJlc29sdmUodHJ1ZSk7XG4gICAgICB9KTtcblxuICAgICAgcmV0dXJuIGxvY2FsUHJvbWlzZS5wcm9taXNlO1xuICAgIH1cblxuICAgIGZ1bmN0aW9uIGNhbGN1bGF0ZShvcmRlcikge1xuICAgICAgdmFyIHN1bSA9IDA7XG5cbiAgICAgIGZvciAodmFyIGkgPSAwLCB3ZWVrOyAod2VlayA9IG9yZGVyLndlZWtzW2ldKTsgaSsrKSB7XG4gICAgICAgIGlmICh3ZWVrLnNlbGVjdGVkKSB7XG4gICAgICAgICAgc3VtICs9IG9yZGVyLmV2ZW50LmNvc3RfcGVyX3dlZWs7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgZm9yICh2YXIgaiA9IDAsIHdlZWtkYXk7ICh3ZWVrZGF5ID0gd2Vlay5kYXRlc1tqXSk7IGorKykge1xuICAgICAgICAgICAgaWYod2Vla2RheS5zZWxlY3RlZCkge1xuICAgICAgICAgICAgICBzdW0gKz0gb3JkZXIuZXZlbnQuY29zdF9wZXJfZGF5O1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgfVxuXG4gICAgICBhbmd1bGFyLmZvckVhY2gob3JkZXIudmFyaWFudHMsIGZ1bmN0aW9uICh2YXJpYW50KSB7XG4gICAgICAgIHN1bSArPSB2YXJpYW50LnByaWNlICogdmFyaWFudC5jb3VudDtcbiAgICAgIH0pO1xuXG4gICAgICByZXR1cm4gc3VtO1xuICAgIH1cblxuICAgIGZ1bmN0aW9uIHZhbGlkYXRlKG9yZGVyLCBjaGlsZHJlbiwgZXZlbnRzKSB7XG4gICAgICB2YXIgZXJyb3JzID0gW107XG5cbiAgICAgIHZhciBjaGlsZCA9ICRmaWx0ZXIoJ2ZpbHRlcicpKGNoaWxkcmVuLCB7aWQ6IG9yZGVyLmNoaWxkX2lkfSk7XG4gICAgICBpZiAoIShvcmRlci5jaGlsZF9pZCAmJiBjaGlsZC5sZW5ndGgpKSB7XG4gICAgICAgIGVycm9ycy5wdXNoKHtcbiAgICAgICAgICB0aXRsZTogJ2NoaWxkJyxcbiAgICAgICAgICBtZXNzYWdlOiAnTmVjZXNzYXJ5IHNlbGVjdCBjaGlsZCdcbiAgICAgICAgfSk7XG4gICAgICB9XG5cbiAgICAgIHZhciBldmVudCA9ICRmaWx0ZXIoJ2ZpbHRlcicpKGV2ZW50cywge2lkOiBvcmRlci5ldmVudF9pZH0pO1xuICAgICAgaWYgKCEob3JkZXIuZXZlbnRfaWQgJiYgZXZlbnQubGVuZ3RoKSkge1xuICAgICAgICBlcnJvcnMucHVzaCh7XG4gICAgICAgICAgdGl0bGU6ICdldmVudCcsXG4gICAgICAgICAgbWVzc2FnZTogJ05lY2Vzc2FyeSBzZWxlY3QgZXZlbnQnXG4gICAgICAgIH0pO1xuICAgICAgfVxuXG4gICAgICAvL0NoZWNrIHRoYXQgc2VsZWN0ZWQgYXMgbWluaW11bSBvbmUgZGF5XG4gICAgICB2YXIgc2VsZWN0ZWQgPSBmYWxzZTtcbiAgICAgIGZvciAodmFyIGkgPSAwLCB3ZWVrOyBvcmRlci53ZWVrcyAmJiAod2VlayA9IG9yZGVyLndlZWtzW2ldKTsgaSsrKSB7XG4gICAgICAgIGZvciAodmFyIGogPSAwLCBkYXk7IChkYXkgPSB3ZWVrLmRhdGVzW2pdKTsgaisrKSB7XG4gICAgICAgICAgaWYgKGRheS5zZWxlY3RlZCkge1xuICAgICAgICAgICAgc2VsZWN0ZWQgPSB0cnVlO1xuICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgfVxuICAgICAgICB9XG5cbiAgICAgICAgaWYgKHNlbGVjdGVkKSB7XG4gICAgICAgICAgYnJlYWs7XG4gICAgICAgIH1cbiAgICAgIH1cblxuICAgICAgaWYgKCFzZWxlY3RlZCkge1xuICAgICAgICBlcnJvcnMucHVzaCh7XG4gICAgICAgICAgdGl0bGU6ICdkYXRlcycsXG4gICAgICAgICAgbWVzc2FnZTogJ05lY2Vzc2FyeSBzZWxlY3QgYXQgbGVhc3Qgb25lIGRheSdcbiAgICAgICAgfSk7XG4gICAgICB9XG5cbiAgICAgIHJldHVybiBlcnJvcnMucmV2ZXJzZSgpO1xuICAgIH1cblxuICAgIGZ1bmN0aW9uIGFsbCgpIHtcbiAgICAgIHJldHVybiAkcS5hbGwoW3J1bl0pLnRoZW4oZnVuY3Rpb24gKCkge1xuICAgICAgICB2YXIgb3JkZXJzID0gYW5ndWxhci5jb3B5KHZtLm9yZGVycyk7XG5cbiAgICAgICAgdmFyIGxvY2FsUHJvbWlzZSA9ICRxLmRlZmVyKCk7XG5cbiAgICAgICAgdm0ucHJvbWlzZXMudGhlbihmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgZm9yICh2YXIgaSA9IDAsIG9yZGVyOyAob3JkZXIgPSBvcmRlcnNbaV0pOyBpKyspIHtcbiAgICAgICAgICAgIHNldENoaWxkKG9yZGVyKTtcbiAgICAgICAgICAgIHNldEV2ZW50KG9yZGVyKTtcbiAgICAgICAgICB9XG5cbiAgICAgICAgICBsb2NhbFByb21pc2UucmVzb2x2ZShvcmRlcnMpO1xuICAgICAgICB9KTtcblxuICAgICAgICByZXR1cm4gbG9jYWxQcm9taXNlLnByb21pc2U7XG4gICAgICB9KTtcbiAgICB9XG5cbiAgICBmdW5jdGlvbiBhZGQob3JkZXIpIHtcbiAgICAgIHZhciBsb2NhbFByb21pc2UgPSAkcS5kZWZlcigpO1xuXG4gICAgICB2bS5wcm9taXNlcy50aGVuKGZ1bmN0aW9uICgpIHtcbiAgICAgICAgb3JkZXIuX2lkID0gdm0uaW5kZXgrKztcbiAgICAgICAgdm0ub3JkZXJzLnB1c2gob3JkZXIpO1xuICAgICAgICBzYXZlT3JkZXJzKCk7XG5cbiAgICAgICAgbG9jYWxQcm9taXNlLnJlc29sdmUoXG4gICAgICAgICAgYW5ndWxhci5jb3B5KG9yZGVyKVxuICAgICAgICApO1xuICAgICAgfSk7XG5cblxuICAgICAgcmV0dXJuIGxvY2FsUHJvbWlzZS5wcm9taXNlO1xuICAgIH1cblxuICAgIGZ1bmN0aW9uIGdldChpZCkge1xuICAgICAgdmFyIGxvY2FsUHJvbWlzZSA9ICRxLmRlZmVyKCk7XG5cbiAgICAgIHZtLnByb21pc2VzLnRoZW4oZnVuY3Rpb24gKCkge1xuICAgICAgICB2YXIgb3JkZXIgPSBhbmd1bGFyLmNvcHkoJGZpbHRlcignZmlsdGVyJykodm0ub3JkZXJzLCB7X2lkOiBpZH0pWzBdKTtcblxuICAgICAgICBzZXRDaGlsZChvcmRlcik7XG4gICAgICAgIHNldEV2ZW50KG9yZGVyKTtcblxuICAgICAgICBsb2NhbFByb21pc2UucmVzb2x2ZShvcmRlcik7XG4gICAgICB9KTtcblxuXG4gICAgICByZXR1cm4gbG9jYWxQcm9taXNlLnByb21pc2U7XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gdXBkYXRlKGlkLCBvcmRlcikge1xuICAgICAgdmFyIGxvY2FsUHJvbWlzZSA9ICRxLmRlZmVyKCk7XG5cbiAgICAgIHZhciBfb3JkZXIgPSAkZmlsdGVyKCdmaWx0ZXInKSh2bS5vcmRlcnMsIHtfaWQ6IGlkfSlbMF07XG5cbiAgICAgIF9vcmRlci5jaGlsZF9pZCA9IG9yZGVyLmNoaWxkX2lkO1xuICAgICAgX29yZGVyLmNoaWxkID0gb3JkZXIuY2hpbGQ7XG4gICAgICBfb3JkZXIuZXZlbnRfaWQgPSBvcmRlci5ldmVudF9pZDtcbiAgICAgIF9vcmRlci5ldmVudCA9IG9yZGVyLmV2ZW50O1xuICAgICAgX29yZGVyLmRhdGVzID0gb3JkZXIuZGF0ZXM7XG5cbiAgICAgIHNhdmVPcmRlcnMoKTtcblxuICAgICAgbG9jYWxQcm9taXNlLnJlc29sdmUoX29yZGVyKTtcblxuICAgICAgcmV0dXJuIGxvY2FsUHJvbWlzZS5wcm9taXNlO1xuICAgIH1cblxuICAgIGZ1bmN0aW9uIHJlbW92ZShpZCkge1xuICAgICAgdmFyIGxvY2FsUHJvbWlzZSA9ICRxLmRlZmVyKCk7XG5cbiAgICAgIHZhciBvcmRlciA9ICRmaWx0ZXIoJ2ZpbHRlcicpKHZtLm9yZGVycywge19pZDogaWR9KVswXTtcblxuICAgICAgdm0ub3JkZXJzLnNwbGljZSh2bS5vcmRlcnMuaW5kZXhPZihvcmRlciksIDEpO1xuXG4gICAgICBzYXZlT3JkZXJzKCk7XG5cbiAgICAgIGxvY2FsUHJvbWlzZS5yZXNvbHZlKHRydWUpO1xuXG4gICAgICByZXR1cm4gbG9jYWxQcm9taXNlLnByb21pc2U7XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gc2F2ZU9yZGVycygpIHtcbiAgICAgIHN0b3JlLnNldCgnb3JkZXJzJywgdm0ub3JkZXJzKTtcbiAgICAgIHN0b3JlLnNldCgnaW5kZXgnLCB2bS5pbmRleCk7XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gc2V0Q2hpbGQob3JkZXIpIHtcbiAgICAgIG9yZGVyLmNoaWxkID0gJGZpbHRlcignZmlsdGVyJykodm0uY2hpbGRyZW4sIHtpZDogb3JkZXIuY2hpbGRfaWR9KVswXTtcbiAgICB9XG5cbiAgICBmdW5jdGlvbiBzZXRFdmVudChvcmRlcikge1xuICAgICAgb3JkZXIuZXZlbnQgPSAkZmlsdGVyKCdmaWx0ZXInKSh2bS5ldmVudHMsIHtpZDogb3JkZXIuZXZlbnRfaWR9KVswXTtcbiAgICB9XG5cbiAgfVxufSkoKTtcbiIsIihmdW5jdGlvbiAoKSB7XG4gICd1c2Ugc3RyaWN0JztcblxuICBhbmd1bGFyXG4gICAgLm1vZHVsZSgnYm9va2luZycpXG4gICAgLnNlcnZpY2UoJ3Byb2ZpbGVTZXJ2aWNlJywgcHJvZmlsZSk7XG5cbiAgcHJvZmlsZS4kaW5qZWN0ID0gWyckaHR0cCcsICdoZWxwZXJTZXJ2aWNlJywgJyRyb290U2NvcGUnXTtcblxuICAvKiBAbmdJbmplY3QgKi9cbiAgZnVuY3Rpb24gcHJvZmlsZSgkaHR0cCwgaGVscGVyU2VydmljZSwgJHJvb3RTY29wZSkge1xuICAgIC8qIGpzaGludCB2YWxpZHRoaXM6IHRydWUgKi9cbiAgICB2YXIgdm0gPSB0aGlzO1xuXG4gICAgdm0uZ2V0ID0gZ2V0O1xuICAgIHZtLnVwZGF0ZSA9IHVwZGF0ZTtcbiAgICB2bS5jb3VudHJpZXMgPSBjb3VudHJpZXM7XG5cbiAgICAvLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vXG5cbiAgICBmdW5jdGlvbiBnZXQoKSB7XG4gICAgICByZXR1cm4gJGh0dHAuZ2V0KCcvYWRtaW4vYXBpL2N1c3RvbWVycy8nICsgJHJvb3RTY29wZS5jdXN0b21lcklkKVxuICAgICAgICAudGhlbihoZWxwZXJTZXJ2aWNlLmZldGNoUmVzcG9uc2UpXG4gICAgICAgIC50aGVuKGZ1bmN0aW9uIChkYXRhKSB7XG4gICAgICAgICAgZGF0YS5lbWFpbCA9IGRhdGEudXNlci5lbWFpbDtcbiAgICAgICAgICByZXR1cm4gZGF0YTtcbiAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gdXBkYXRlKGRhdGEpIHtcbiAgICAgIHJldHVybiAkaHR0cC5wdXQoJy9hZG1pbi9hcGkvY3VzdG9tZXJzLycgKyAkcm9vdFNjb3BlLmN1c3RvbWVySWQsIGRhdGEpXG4gICAgICAgIC50aGVuKGhlbHBlclNlcnZpY2UuZmV0Y2hSZXNwb25zZSlcbiAgICAgICAgLnRoZW4oZnVuY3Rpb24gKGRhdGEpIHtcbiAgICAgICAgICBkYXRhLmVtYWlsID0gZGF0YS51c2VyLmVtYWlsO1xuICAgICAgICAgIHJldHVybiBkYXRhO1xuICAgICAgICB9KTtcbiAgICB9XG5cbiAgICBmdW5jdGlvbiBjb3VudHJpZXMoKSB7XG4gICAgICByZXR1cm4gJGh0dHAuZ2V0KCcvYXBpL2NvdW50cmllcycpLnRoZW4oaGVscGVyU2VydmljZS5mZXRjaFJlc3BvbnNlKTtcbiAgICB9XG4gIH1cbn0pKCk7XG4iXSwic291cmNlUm9vdCI6Ii9zb3VyY2UvIn0=
