(function () {
  'use strict';

  angular.module('booking', [
    'ngResource',
    'ui.router',
    'toaster',
    'angular-storage',
    'ui.bootstrap',
    'ui.select',
    'ngSanitize'
  ]);

})();

(function() {
  'use strict';

  angular
    .module('booking')
    .config(config);

  config.$inject = ['$httpProvider'];

  function config($httpProvider) {

    $httpProvider.interceptors.push('slInterceptor');
  }

})();

(function () {

  'use strict';

  angular
    .module('booking')
    .constant('accosts', ['Mr', 'Miss', 'Mrs', 'Ms', 'Dr'])
    .constant('booleanValues', [
      {
        title: 'Yes',
        value: 1
      },
      {
        title: 'No',
        value: 0
      }
    ])
    .constant('paymentTypes', [
      {
        title: 'Pay Online by Credit / Debit Card',
        value: 'online'
      },
      {
        title: 'Using childcare vouchers',
        value: 'voucher'
      },
      //{
      //  title: 'By cheque',
      //  value: 'cheque'
      //}
    ])
    .constant('months', [
      {
        value: 0,
        name: 'January'
      },
      {
        value: 1,
        name: 'February'
      },
      {
        value: 2,
        name: 'March'
      },
      {
        value: 3,
        name: 'April'
      },
      {
        value: 4,
        name: 'May'
      },
      {
        value: 5,
        name: 'June'
      },
      {
        value: 6,
        name: 'July'
      },
      {
        value: 7,
        name: 'August'
      },
      {
        value: 8,
        name: 'September'
      },
      {
        value: 9,
        name: 'October'
      },
      {
        value: 10,
        name: 'November'
      },
      {
        value: 11,
        name: 'December'
      }
    ]);

})();

(function () {
  'use strict';

  angular
    .module('booking')
    .factory('slInterceptor', slInterceptor);

  slInterceptor.$inject = ['alertService', '$q'];

  function slInterceptor(alertService, $q) {
    return {
      responseError: function (res) {
        var status = res.status,
          fetchedResponse = res.data;

        if(fetchedResponse === 'Unauthorized.') {
          alertService.error('Your session was expired. Please login again.');

          window.location.href = '/login';
        } else if(fetchedResponse.length === undefined || fetchedResponse.length < 256) {
          alertService.showResponseError(fetchedResponse, titleByCodeStatus(status));
        } else  {
          alertService.showResponseError(['Server Error'], titleByCodeStatus(status));
        }

        return $q.reject(fetchedResponse);
      }
    };

    function titleByCodeStatus(status) {
      var title;

      switch (status) {
        case 422:
          title = 'Validation Fail!';
          break;
        case 403:
          title = 'Access Denied!';
          break;
        case 500:
          title = 'Sorry. Server Error.';
          break;
      }

      return title;
    }
  }
})();

(function () {
  'use strict';

  angular
    .module('booking')
    .config(config);

  config.$inject = ['$stateProvider', '$urlRouterProvider'];

  function config($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise('/profile/show');

    var uiView = '<ui-view></ui-view>';

    var dependecies = {
      orders: ['orderService', function (orderService) {
        return orderService.all();
      }],
      order: ['orderService', '$stateParams', function (orderService, $stateParams) {
        return orderService.get($stateParams.orderId);
      }],
      children: ['childService', function (childService) {
        return childService.all();
      }],
      child: ['childService', '$stateParams', function (childService, $stateParams) {
        return childService.get($stateParams.childId);
      }],
      events: ['eventService', function (eventService) {
        return eventService.all();
      }],
      swimOptions: ['childService', function(childService) {
        return childService.swimOptions();
      }],
      yearsInSchool: ['childService', function(childService) {
        return childService.yearsInSchool();
      }],
      products: ['orderService', function(orderService) {
        return orderService.products();
      }],
      countries: ['profileService', function (profileService) {
        return profileService.countries();
      }],
      profile: ['profileService', function (profileService) {
        return profileService.get();
      }]
    };

    $stateProvider
      .state('booking', {
        abstract: true,
        templateUrl: '/booking/template.html'
      })


      .state('booking.profile', {
        abstract: true,
        url: '/profile',
        data: {
          tab: 'profile'
        },
        template: uiView,
        controller: 'ProfileController',
        controllerAs: 'ProfileCtrl',
        resolve: {
          profile: dependecies.profile,
          countries: dependecies.countries
        }
      })
      .state('booking.profile.show', {
        url: '/show',
        templateUrl: '/booking/profile/show.html',
      })
      .state('booking.profile.edit', {
        url: '/edit',
        templateUrl: '/booking/profile/edit.html',
      })


      .state('booking.children', {
        abstract: true,
        url: '/children',
        template: uiView,
        data: {
          tab: 'children'
        },
      })
      .state('booking.children.index', {
        url: '/index',
        templateUrl: '/booking/children/index.html',
        controller: 'ChildrenController',
        controllerAs: 'ChildrenCtrl',
        resolve: {
          children: dependecies.children
        }
      })
      .state('booking.children.create', {
        url: '/create',
        templateUrl: '/booking/children/create.html',
        controller: 'CreateChildController',
        controllerAs: 'CreateChildCtrl',
        resolve: {
          swimOptions: dependecies.swimOptions,
          yearsInSchool: dependecies.yearsInSchool
        }
      })
      .state('booking.children.show', {
        url: '/:childId/show',
        templateUrl: '/booking/children/show.html',
        controller: 'ShowChildController',
        controllerAs: 'ShowChildCtrl',
        resolve: {
          child: dependecies.child
        }
      })
      .state('booking.children.edit', {
        url: '/:childId/edit',
        templateUrl: '/booking/children/edit.html',
        controller: 'EditChildController',
        controllerAs: 'EditChildCtrl',
        resolve: {
          child: dependecies.child,
          swimOptions: dependecies.swimOptions,
          yearsInSchool: dependecies.yearsInSchool
        }
      })

      .state('booking.orders', {
        abstract: true,
        url: '/orders',
        template: uiView,
        data: {
          tab: 'orders'
        },
      })
      /*
       .state('booking.orders.index', {
       url: '/index',
       templateUrl: '/booking/orders/index.html',
       controller: 'OrdersController',
       controllerAs: 'OrdersCtrl',
       resolve: {
       orders: dependecies.orders
       }
       })
       */
      .state('booking.orders.create', {
        url: '/create',
        templateUrl: '/booking/orders/create.html',
        controller: 'CreateOrderController',
        controllerAs: 'CreateOrderCtrl',
        resolve: {
          children: dependecies.children,
          events: dependecies.events,
          countOrders: ['orderService', function (orderService) {
            return orderService.count();
          }]
        }
      })
      /*
       .state('booking.orders.show', {
       url: '/:orderId/show',
       templateUrl: '/booking/orders/show.html',
       controller: 'ShowOrderController',
       controllerAs: 'ShowOrderCtrl',
       resolve: {
       order: dependecies.order,
       }
       })
       */
      /*
       .state('booking.orders.edit', {
       url: '/:orderId/edit',
       templateUrl: '/booking/orders/edit.html',
       controller: 'EditOrderController',
       controllerAs: 'EditOrderCtrl',
       resolve: {
       order: dependecies.order,
       children: dependecies.children,
       events: dependecies.events
       }
       })
       */
      // Showing total info, need select payment type and submit action
      // At the end create Booking and go to All Bookings page(not SPA)
      .state('booking.complete', {
        url: '/complete',
        templateUrl: '/booking/complete/complete.html',
        data: {
          tab: 'complete'
        },
        controller: 'CompleteController',
        controllerAs: 'CompleteCtrl',
        resolve: {
          products: dependecies.products,
          orders: ['orderService', function (orderService) {
            return orderService.all();
          }],
          prices: ['orders', 'bookingService', 'helperService', function(orders, bookingService, helperService) {
            var _orders = helperService.prepareOrders(orders);

            return bookingService.calculate(_orders);
          }],
          ordersWithPrices: ['orders', 'prices', '$filter', function (orders, prices, $filter) {
            var ordersWithPrice = [];

            for (var i = 0, order; (order = orders[i]); i++) {
              order.sum = 0;

              var price = $filter('filter')(prices, {_id: order._id}, true);
              if(price.length && price[0].amount) {
                order.sum = price[0].amount;
                order.discount = price[0].discount;
                order.price_variants = price[0].price_variants;
              }

              order.variants = price[0].variants;

              ordersWithPrice.push(order);
            }

            return ordersWithPrice;
          }],
          paymentSystems: ['bookingService', function (bookingService) {
            return bookingService.paymentSystems();
          }]
        }
      });
  }
})();

(function () {
  'use strict';

  angular
    .module('booking')
    .run(run);

  run.$inject = ['$state', '$rootScope', 'orderService'];

  function run($state, $rootScope, orderService) {

    $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
      var preventEvent = false,
        stateName;

      if(toState.name === 'booking.complete' && !orderService.count()) {
        preventEvent = true;
        stateName = 'booking.orders.create';
      }

      if(preventEvent) {
        event.preventDefault();
        if(stateName) {
          $state.go(stateName);
        }
      } else {
        $rootScope.tab = toState.data.tab;
      }

    });
  }
})();

(function () {
  'use strict';

  angular
    .module('booking')
    .controller('BookingController', BookingController);

  BookingController.$inject = ['$state'];

  /* @ngInject */
  function BookingController($state) {
    /* jshint validthis: true */
    var vm = this;

    var tab = $state.current.data.tab;
  }
})();

(function () {
  'use strict';

  angular
    .module('booking')
    .controller('ChildrenController', ChildrenController);

  ChildrenController.$inject = ['children', 'childService', 'alertService', '$state'];

  /* @ngInject */
  function ChildrenController(children, childService, alertService, $state) {
    /* jshint validthis: true */
    var vm = this;

    vm.children = children;
    vm.confirmedUpdated = false;
    vm.confirmedLegal = false;

    vm.remove = remove;
    vm.next = next;

    ///////////////////

    function next() {
      if (!vm.confirmedUpdated) {
        alertService.error('Please confirm that you have updated all of your children\'s contact and medical information before booking.');

        return;
      }
      if (!vm.confirmedLegal) {
        alertService.error('Please confirm that you are the legal guardian of the children above.');

        return;
      }

      $state.go('booking.orders.create');
    }

    function remove(child) {
      childService.remove(child.id).then(function () {
        alertService.success('The child was removed!');

        vm.children.splice(vm.children.indexOf(child), 1);
      });
    }
  }
})();

(function () {
  'use strict';

  angular
    .module('booking')
    .controller('CreateChildController', CreateChildController);

  CreateChildController.$inject = ['childService', '$state', 'toaster', 'swimOptions', 'yearsInSchool'];

  /* @ngInject */
  function CreateChildController(childService, $state, toaster, swimOptions, yearsInSchool) {
    /* jshint validthis: true */
    var vm = this;

    vm.swimOptions = swimOptions;
    vm.yearsInSchool = yearsInSchool;

    vm.form = {
      first_name: undefined,
      last_name: undefined,
      email: undefined,
      birthday: undefined,
      sex: undefined,
      relationship: undefined,
      school: undefined,
      year_in_school: undefined,
      doctor_name: undefined,
      doctor_telephone_number: undefined,
      medical_advice_and_treatment: undefined,
      epi_pen: undefined,
      medication: undefined,
      special_requirements: undefined,
      can_swim: undefined,
      emergency_contact_number: undefined,
    };

    vm.create = create;

    vm.datePickerOpen = false;

    //////////////////////////////

    function create() {
      childService.store(vm.form).then(function () {
        toaster.success('The child was created!');

        $state.go('^.index');
      });
    }

  }
})();

(function () {
  'use strict';

  angular
    .module('booking')
    .controller('EditChildController', EditChildController);

  EditChildController.$inject = ['child', 'childService', '$state', 'toaster', 'swimOptions', 'yearsInSchool'];

  /* @ngInject */
  function EditChildController(child, childService, $state, toaster, swimOptions, yearsInSchool) {
    /* jshint validthis: true */
    var vm = this;

    vm.swimOptions = swimOptions;
    vm.yearsInSchool = yearsInSchool;

    vm.form = child;

    vm.update = update;

    vm.datePickerOpen = false;

    ////////////

    function update() {
      childService.update(vm.form).then(function () {
        toaster.success('The child was updated!');

        $state.go('^.index');
      });
    }

  }
})();

(function () {
  'use strict';

  angular
    .module('booking')
    .controller('ShowChildController', ShowChildController);

  ShowChildController.$inject = ['child'];

  /* @ngInject */
  function ShowChildController(child) {
    /* jshint validthis: true */
    var vm = this;

    vm.child = child;
  }
})();

(function () {
  'use strict';

  CompleteController.$inject = ["paymentSystems", "ordersWithPrices", "bookingService", "paymentTypes", "alertService", "orderService", "helperService", "$filter", "$state", "$uibModal", "products", "store"];
  angular
    .module('booking')
    .controller('CompleteController', CompleteController);

  /* @ngInject */
  function CompleteController(paymentSystems, ordersWithPrices, bookingService, paymentTypes, alertService, orderService, helperService, $filter, $state, $uibModal, products, store) {
    /* jshint validthis: true */
    var vm = this;

    vm.orders = ordersWithPrices;
    vm.orders.sum = $filter('sumByKey')(vm.orders, 'sum');
    vm.orders.discount = $filter('sumByKey')(vm.orders, 'discount');
    vm.sum_variants = $filter('sumByKey')(products, 'price');
    vm.orders.coupon = 0;

    vm.paymentTypes = paymentSystems.length === 0 ? paymentTypes.splice(1, 1) : paymentTypes;
    vm.paymentSystems = paymentSystems;
    vm.couponApplied = false;
    vm.products = products;

    vm.form = {
      payment_type: undefined,
      payment_system: paymentSystems.length === 1 ? paymentSystems[0].system : undefined,
      orders: [],
      coupon: undefined,
    };
    vm.agreed = false;
    vm.request = false;

    prepareProducts();
    calculateTotalPrice();
    setOrdersToBookingForm();
    addToOrderProductsIfStored();

    vm.book = book;
    vm.remove = remove;
    vm.applyCoupon = applyCoupon;
    vm.cancelCoupon = cancelCoupon;
    vm.calculateProductPrice = calculateProductPrice;
    vm.saveProducts = saveProducts;

    ////////////////////////////////

    function applyCoupon() {
      if (!vm.form.coupon) {
        return;
      }

      vm.request = true;

      bookingService.checkCoupon(vm.form.coupon).then(function (persent) {
        vm.orders.couponPersent = persent;
        calculateTotalPrice();
        alertService.success('Your coupon is activated.');
        vm.couponApplied = true;
      }, function () {
        alertService.error('Coupon not exists or expired.');
      }).finally(function () {
        vm.request = false;
      });
    }

    function cancelCoupon() {
      //Clear data about previous coupon
      vm.form.coupon = '';
      vm.orders.couponPersent = undefined;
      vm.orders.coupon = 0;
      vm.couponApplied = false;

      calculateTotalPrice();

      alertService.success('You coupon was deactivated.');
    }

    function remove(id) {
      var modalInstance = $uibModal.open({
        templateUrl: '/booking/complete/confirm-remove-modal.html',
        controller: 'ConfirmRemoveOrderController',
        controllerAs: 'ConfirmRemoveCtrl',
        size: 'md'
      });

      modalInstance.result.then(function () {
        orderService.remove(id).then(function () {
          //Refresh data
          orderService.all().then(function (orders) {
            alertService.success('The order was removed');

            if (!orders.length) {
              $state.go('booking.orders.create');
            }

            var _orders = helperService.prepareOrders(orders);
            bookingService.calculate(_orders).then(function (prices) {
              var ordersWithPrice = [];

              for (var i = 0, order; (order = orders[i]); i++) {
                order.sum = 0;

                var price = $filter('filter')(prices, {_id: order._id}, true);
                if (price.length && price[0].amount) {
                  order.sum = price[0].amount;
                  order.discount = price[0].discount;
                  order.price_variants = price[0].price_variants;
                }
                order.variants = price[0].variants;
                ordersWithPrice.push(order);
              }

              vm.orders = ordersWithPrice;
              vm.orders.sum = $filter('sumByKey')(vm.orders, 'sum');
              vm.orders.discount = $filter('sumByKey')(vm.orders, 'discount');
              vm.sum_variants = $filter('sumByKey')(vm.orders, 'price_variants');
              vm.orders.coupon = 0;
              calculateTotalPrice();
            });

            // vm.orders = orders;
          });
        });
      });
    }

    function book() {
      if (!vm.agreed) {
        alertService.error('You should with booking\'s terms and conditions');
        return;
      }

      vm.request = true;

      return bookingService.book(vm.form).then(function (data) {
        alertService.success('You booked!');

        //Clear current orders
        orderService.clear();

        helperService.redirect(data.redirect, data.method, data.fields);
      }, function () {
        vm.request = false;
      });
    }

    function setOrdersToBookingForm() {
      vm.form.orders = helperService.prepareOrders(vm.orders);
    }

    function calculateTotalPrice() {
      if (vm.form.coupon && vm.orders.couponPersent) {
        vm.orders.coupon = vm.orders.sum * vm.orders.couponPersent / 100;
        vm.orders.total = vm.orders.sum - vm.orders.coupon;
      } else {
        vm.orders.total = vm.orders.sum - vm.orders.discount;
      }
      vm.orders.total = vm.orders.total + vm.sum_variants;
    }

    function prepareProducts() {
      angular.forEach(vm.products, function (product) {
        if (!product.selectedQuantity) {
          product.selectedQuantity = 1;
        }

        if (!product.selectedVariant) {
          product.selectedVariant = product.variants.length ? product.variants[0].id : 0;
        }

        Object.defineProperty(product, 'price', {
          get: function () {
            var price = 0;

            var selectedVariant = this.selectedVariant;
            var variant = this.variants.filter(function (variant) {
              return selectedVariant && selectedVariant === variant.id;
            })[0];

            if (variant && this.selectedQuantity) {
              price = variant.price * this.selectedQuantity;
            }

            return price;
          }
        });
        Object.defineProperty(product, 'maxCount', {
          get: function () {
            var countMax = 0;

            var selectedVariant = this.selectedVariant;
            var variant = this.variants.filter(function (variant) {
              return selectedVariant && selectedVariant === variant.id;
            })[0];

            if (variant && this.selectedQuantity) {
              countMax = variant.count;
            }

            return countMax;
          }
        });

        calculateProductPrice(product, true);
      });
    }

    function calculateProductPrice(product, notShowChangeProducts) {
      vm.changedProducts = !notShowChangeProducts;
      //product.price = 0;
      //product.maxCount = undefined;

      var variant = product.variants.filter(function (variant) {
        return product.selectedVariant && product.selectedVariant === variant.id;
      })[0];

      if (variant && product.selectedQuantity) {
        //product.price = variant.price * product.selectedQuantity;
        //product.maxCount = variant.count;
      }
    }

    function saveProducts() {
      var products = vm.products.filter(function (product) {
        return product.price;
      });

      orderService.saveProducts(products).then(function () {
        alertService.success('Products saved');

        vm.form.products = products;

        vm.sum_variants = $filter('sumByKey')(products, 'price');

        calculateTotalPrice();

        vm.changedProducts = false;
      });
    }

    function addToOrderProductsIfStored() {
      var products = store.get('products');

      if (!products || !products.length) {
        return false;
      }

      vm.form.products = products;
    }


  }
})();

(function () {
  'use strict';

  angular
    .module('booking')
    .controller('ConfirmRemoveOrderController', ConfirmRemoveOrderController);

  ConfirmRemoveOrderController.$inject = ['$uibModalInstance'];

  /* @ngInject */
  function ConfirmRemoveOrderController($uibModalInstance) {
    /* jshint validthis: true */
    var vm = this;

    vm.ok = ok;
    vm.cancel = cancel;

    function ok() {
      $uibModalInstance.close();
    }

    function cancel() {
      $uibModalInstance.dismiss('cancel');
    }
  }
})();

(function(){
  'use strict';

  angular
    .module('booking')
    .filter('age', age);

  age.$inject = [];

  function age() {
    return main;

    function main(person, date) {
      date = date ? moment(date) : moment();

      var birthday = moment(person.birthday);
      return date.diff(birthday, 'years');
    }
  }
})();
(function(){
  'use strict';

  angular
    .module('booking')
    .filter('currencyLb', currencyLb);

  currencyLb.$inject = ['$filter'];

  function currencyLb($filter) {
    return main;

    function main(value) {
      return $filter('currency')(value, '£');
    }
  }
})();
(function(){
  'use strict';

  angular
    .module('booking')
    .filter('event', event);

  event.$inject = [];

  function event() {
    return main;

    function main(event) {
      return event.venue.name + ' - ' + event.name;
    }
  }
})();
(function(){
  'use strict';

  angular
    .module('booking')
    .filter('logical', logical);

  logical.$inject = [];

  function logical() {
    return main;

    function main(value) {
      return value ? 'Yes' : 'No';
    }
  }
})();
(function(){
  'use strict';

  angular
    .module('booking')
    .filter('profile', profile);

  profile.$inject = [];

  function profile() {
    return main;

    function main(user) {
      return user.first_name + ' ' + user.last_name;
    }
  }
})();
(function () {
  'use strict';

  angular
    .module('booking')
    .filter('range', range);

  range.$inject = [];

  function range() {
    return main;

    function main(input, min, max, order) {
      min = parseInt(min);
      max = parseInt(max);

      if (order === 'desc') {
        for (var i = max; i >= min; i--) {
          input.push(i);
        }
      } else {
        for (var j = min; j <= max; j++) {
          input.push(j);
        }
      }

      return input;
    }
  }
})();
(function(){
  'use strict';

  angular
    .module('booking')
    .filter('sex', sex);

  sex.$inject = [];

  function sex() {
    return main;

    function main(value) {
      return value ? 'Male' : 'Female';
    }
  }
})();
(function () {
  'use strict';

  angular
    .module('booking')
    .filter('sumByKey', function () {
      return function (data, key) {
        if (typeof(data) === 'undefined' || typeof(key) === 'undefined') {
          return 0;
        }

        var sum = 0;
        for (var i = data.length - 1; i >= 0; i--) {
          sum += parseFloat(data[i][key]);
        }

        return sum;
      };
    });

})();

(function(){
  'use strict';

  angular
    .module('booking')
    .filter('swim', swim);

  swim.$inject = [];

  function swim() {
    return main;

    function main(child) {
      return child.can_swim ? (child.can_swim.number + ' - '  + child.can_swim.option) : '';
    }
  }
})();
(function () {
  'use strict';

  angular
    .module('booking')
    .filter('weekday', weekday);

  weekday.$inject = [];

  function weekday() {
    return main;

    function main(date) {
      if (!(date instanceof Date)) {
        date = new Date(date);
      }

      var weekday;

      switch (date.getDay()) {
        case 0:
          weekday = 'Sunday';
          break;
        case 1:
          weekday = 'Monday';
          break;
        case 2:
          weekday = 'Tuesday';
          break;
        case 3:
          weekday = 'Wednesday';
          break;
        case 4:
          weekday = 'Thursday';
          break;
        case 5:
          weekday = 'Friday';
          break;
        case 6:
          weekday = 'Saturday';
          break;

      }

      return weekday;
    }
  }
})();
(function () {
  'use strict';

  angular
    .module('booking')
    .controller('ChildDetailsModalController', ChildDetailsModalController);

  ChildDetailsModalController.$inject = ['$uibModalInstance', 'child'];

  /* @ngInject */
  function ChildDetailsModalController($uibModalInstance, child) {
    /* jshint validthis: true */
    var vm = this;

    vm.child = child;

    vm.close = close;

    /////////////////////

    function close() {
      $uibModalInstance.close();
    }

  }
})();

(function () {
  'use strict';

  angular
    .module('booking')
    .controller('CreateOrderController', CreateOrderController);

  CreateOrderController.$inject = ['$uibModal', 'children', 'events', 'orderService', '$state', 'alertService', 'helperService', '$filter', 'countOrders'];

  /* @ngInject */
  function CreateOrderController($uibModal, children, events, orderService, $state, alertService, helperService, $filter, countOrders) {
    /* jshint validthis: true */
    var vm = this;

    if (!children.length) {
      alertService.error('Please add your children!');
      $state.go('booking.children.create');
    }

    vm.countOrders = countOrders;
    vm.children = children;
    // vm.products = products;
    // vm.allVariants = initVariants();
    vm.events = [];//events;

    vm.request = false;

    vm.form = {
      child_id: undefined,
      event_id: undefined,
      dates: []
    };

    vm.create = create;
    vm.selectedEvent = selectedEvent;
    vm.selectedWeek = selectedWeek;
    vm.selectedDay = selectedDay;
    vm.selectedChild = selectedChild;

    ///////////////////////

    function selectedChild() {
      var child = $filter('filter')(children, {id: vm.form.child_id})[0];

      $uibModal.open({
        templateUrl: '/booking/orders/child-details-modal.html',
        controller: 'ChildDetailsModalController',
        controllerAs: 'ShowChildCtrl',
        size: 'lg',
        resolve: {
          child: function () {
            return child;
          }
        }
      });

      vm.events = $filter('filter')(events, function (event) {
        var age = $filter('age')(child, event.start_date);
        return event.min_age <= age && age <= event.max_age;
      });
    }

    function create() {
      if (helperService.ShowValidationErrors(orderService.validate(vm.form, children, events)).length) {
        return;
      }

      vm.request = true;

      orderService.add(vm.form).then(function () {
        alertService.success('Order was saved');

        $state.go('^.^.complete');
      }).finally(function () {
        //vm.request = false;
      });
    }

    function selectedEvent() {
      if (!vm.form.event_id) {
        return;
      }

      var event = $filter('filter')(vm.events, {id: vm.form.event_id})[0];

      if (!event.excerpt) {
        setEvent();
        return;
      }

      $uibModal.open({
        templateUrl: '/booking/orders/event-info-modal.html',
        controller: 'EventInfoController',
        controllerAs: 'EventInfoCtrl',
        size: 'lg',
        resolve: {
          event: function () {
            return event;
          }
        }
      })
        .result
        .then(function () {
          setEvent();
        }, function () {
          vm.form.event_id = undefined;
        });
    }

    function setEvent() {
      helperService.pushEventDates(vm.events, vm.form);
      alertService.success('Change event\'s dates to event dates');
    }

    function selectedWeek(week) {
      if (week.selected) {
        setSelectedDatesOfWeek(week, true);
        week.selectedAnyDates = true;
      } else {
        week.selectedAnyDates = false;
        setSelectedDatesOfWeek(week, false);
      }
    }

    function selectedDay(week, day) {
      if (!day.selected && week.selected) {
        week.selected = false;
      }

      if (day.selected) {
        week.selectedAnyDates = true;
        var allDaysOfWeekSelected = true;

        for (var i = 0, weekday; (weekday = week.dates[i]); i++) {
          if (weekday.allow && !weekday.selected) {
            allDaysOfWeekSelected = false;
            break;
          }
        }

        if (allDaysOfWeekSelected) {
          week.selected = true;
        }
      } else {
        //Check that any day is selected
        var selectedAnyDate = false;
        for (var j = 0, date; (date = week.dates[j]); j++) {
          if (date.selected) {
            selectedAnyDate = true;
            break;
          }
        }

        week.selectedAnyDates = selectedAnyDate;
      }
    }

    function setSelectedDatesOfWeek(week, value) {
      for (var i = 0, day; (day = week.dates[i]); i++) {
        if (day.allow && !day.fully) {
          day.selected = value;
        }
      }
    }
  }
})();

(function () {
  'use strict';

  angular
    .module('booking')
    .controller('EditOrderController', EditOrderController);

  EditOrderController.$inject = ['order', 'children', 'events', 'orderService', '$state', 'alertService', 'helperService'];

  /* @ngInject */
  function EditOrderController(order, children, events, orderService, $state, alertService, helperService) {
    /* jshint validthis: true */
    var vm = this;

    vm.form = angular.copy(order);
    vm.children = children;
    vm.events = events;

    vm.update = update;
    vm.selectedEvent = selectedEvent;

    ///////////////////////

    function update() {
      if(helperService.ShowValidationErrors(orderService.validate(vm.form)).length) {
        return;
      }

      orderService.update(vm.form._id, vm.form).then(function () {
        alertService.success('The order was updated!');

        $state.go('^.index');
      });
    }

    function selectedEvent() {
      helperService.pushEventDates(vm.events, vm.form);
    }


  }
})();

(function () {
  'use strict';

  angular
    .module('booking')
    .controller('EventInfoController', EventInfoController);

  EventInfoController.$inject = ['$uibModalInstance', 'event'];

  /* @ngInject */
  function EventInfoController($uibModalInstance, event) {
    /* jshint validthis: true */
    var vm = this;

    vm.event = event;

    vm.confirm = confirm;
    vm.close = close;

    //////////

    function confirm() {
      $uibModalInstance.close();
    }

    function close() {
      $uibModalInstance.dismiss();
    }
  }
})();

(function () {
  'use strict';

  angular
    .module('booking')
    .controller('OrdersController', OrdersController);

  OrdersController.$inject = ['orders', 'orderService', 'toaster'];

  /* @ngInject */
  function OrdersController(orders, orderService, toaster) {
    /* jshint validthis: true */
    var vm = this;

    vm.orders = orders;

    vm.remove = remove;

    //////////////////////////

    function remove(id) {
      orderService.remove(id).then(function () {
        //Refresh data
        orderService.all().then(function (orders) {
          vm.orders = orders;

          toaster.success('The order was removed');
        });
      });
    }

  }
})();

(function () {
  'use strict';

  angular
    .module('booking')
    .controller('ShowOrderController', ShowOrderController);

  ShowOrderController.$inject = ['order'];

  /* @ngInject */
  function ShowOrderController(order) {
    /* jshint validthis: true */
    var vm = this;

    vm.order = order;
  }
})();

(function () {
  'use strict';

  angular
    .module('booking')
    .controller('ProfileController', ProfileController);

  ProfileController.$inject = ['profile', 'profileService', '$state', 'toaster', 'accosts', 'booleanValues', 'countries', '$filter'];

  /* @ngInject */
  function ProfileController(profile, profileService, $state, toaster, accosts, booleanValues, countries, $filter) {
    /* jshint validthis: true */
    var vm = this;

    vm.profile = angular.copy(profile);
    vm.form = angular.copy(vm.profile);
    vm.accosts = accosts;
    vm.booleanValues = booleanValues;
    vm.countries = countries;
    vm.request = false;

    vm.update = update;
    vm.reset = reset;

    ////////////////////////////////

    function update() {
      var form = angular.copy(vm.form);
      form.country_id = form.country.id;

      vm.request = true;

      profileService.update(form).then(function () {
        vm.profile = vm.form;

        toaster.success('Profile updated!');

        $state.go('^.show');
      }).finally(function () {
        vm.request = false;
      });
    }

    function reset() {
      vm.form = angular.copy(vm.profile);
    }
  }
})();

(function() {
  'use strict';

  angular
    .module('booking')
    .service('alertService', alertService);

  alertService.$inject = ['toaster'];

  /* @ngInject */
  function alertService (toaster) {
    /* jshint validthis: true */
    var vm = this;

    vm.pop = pop;
    vm.success = success;
    vm.error = error;
    vm.info = info;
    vm.showResponseError = showResponseError;

    function pop(data) {
      toaster.pop(data);
    }

    function success (data) {
      data = prepareData(data, 'Success');
      toaster.success(data);
    }

    function info(data) {
      prepareData(data, 'Info');
      toaster.info(data);
    }

    function error(data) {
      prepareData(data, 'Error');
      toaster.error(data);
    }

    function showResponseError(response, title) {
      for(var key in response) {
        if (response.hasOwnProperty(key) && (response[key] instanceof Array)) {
          for(var i in response[key]) {
            if (response[key].hasOwnProperty(i)) {
              error({
                title: title ? title : 'Error',
                body: response[key][i]
              });
            }
          }
        } else {
          error({
            title: title,
            body: response[key]
          });
        }
      }
    }

    function prepareData(data, title) {
      if (!(data instanceof Object)) {
        data = {
          body: data
        };
      }

      data.title = data.title ? data.title : title;

      return data;
    }
  }


})();

(function () {
  'use strict';

  angular
    .module('booking')
    .service('bookingService', bookingService);

  bookingService.$inject = ['$http', 'helperService'];

  /* @ngInject */
  function bookingService($http, helperService) {
    /* jshint validthis: true */
    var vm = this;

    vm.book = book;
    vm.calculate = calculate;
    vm.checkCoupon = checkCoupon;
    vm.paymentSystems = paymentSystems;

    /////////////////////////

    function book(data) {
      return $http.post('/api/booking', helperService.prepareBookingForRequest(data)).then(helperService.fetchResponse);
    }

    function calculate(orders) {
      return $http.post('/api/booking/calculate', {orders: orders}).then(helperService.fetchResponse).then(function (data) {
        return data;
      });
    }

    function checkCoupon(coupon) {
      return $http.get('/api/booking/coupons/' + coupon).then(helperService.fetchResponse);
    }
    
    function paymentSystems() {
      return $http.get('/api/payment-systems').then(helperService.fetchResponse);
    }
  }
})();

(function () {
  'use strict';

  angular
    .module('booking')
    .service('childService', childService);

  childService.$inject = ['$resource', '$http', 'helperService'];

  /* @ngInject */
  function childService($resource, $http, helperService) {
    /* jshint validthis: true */
    var vm = this;

    var Child = $resource('/api/children/:childId', {childId: '@id'}, {
      update: {method: 'PUT'}
    });

    vm.all = all;
    vm.get = get;
    vm.store = store;
    vm.update = update;
    vm.remove = remove;
    vm.swimOptions = swimOptions;
    vm.yearsInSchool = yearsInSchool;

    ///////////////////////

    function yearsInSchool() {
      return $http.get('/api/years-in-school').then(helperService.fetchResponse);
    }

    function swimOptions() {
      return $http.get('/api/swim-options').then(helperService.fetchResponse);
    }

    function all() {
      return Child.query()
          .$promise
          .then(function (children) {
            for(var i = 0, child; (child = children[i]); i++) {
              child.birthday = moment(child.birthday).toDate();
            }

            return children;
          });
    }

    function get(id) {
      return Child.get({childId: id})
        .$promise
        .then(function (child) {
          child.birthday = moment(child.birthday).toDate();

          return child;
        });
    }

    function store(data) {
      return Child.save(helperService.prepareDatesForRequest(data, ['birthday'], true)).$promise;
    }

    function update(data) {
      return Child.update(helperService.prepareDatesForRequest(data, ['birthday'], true)).$promise;
    }

    function remove(id) {
      return Child.delete({childId: id}).$promise;
    }
  }
})();

(function() {
  'use strict';

  angular
    .module('booking')
    .service('eventService', eventService);

  eventService.$inject = ['$http', 'helperService'];

  /* @ngInject */
  function eventService($http, helperService) {
    /* jshint validthis: true */
    var vm = this;

    vm.all = all;

    ///////////////////////////

    function all() {
      return $http.get('/api/events')
        .then(helperService.fetchResponse)
        .then(function (events) {
          for(var i = 0, event; (event = events[i]); i++) {
            event.start_date = moment(event.start_date);
            event.end_date = moment(event.end_date);
          }

          return events;
        });
    }
  }
})();

(function () {
  'use strict';

  helperService.$inject = ["$filter", "alertService", "store"];
  angular
    .module('booking')
    .service('helperService', helperService);

  /* @ngInject */
  function helperService($filter, alertService, store) {
    /* jshint validthis: true */
    var vm = this;

    vm.fetchResponse = fetchResponse;
    vm.pushEventDates = pushEventDates;
    vm.ShowValidationErrors = ShowValidationErrors;
    vm.redirect = redirect;
    vm.prepareOrders = prepareOrders;
    vm.prepareProducts = prepareProducts;
    vm.prepareBookingForRequest = prepareBookingForRequest;
    vm.prepareDatesForRequest = prepareDatesForRequest;

    /////////////////////////

    function prepareOrders(orders)
    {
      var _orders = [];

      for (var i = 0, order; (order = orders[i]); i++) {
        var selectedDates = [];
        for (var j = 0, week; (week = order.weeks[j]); j++) {
          for (var k = 0, day; (day = week.dates[k]); k++) {
            if (day.selected) {
              var date = new Date(day.date);
              if(! (date.getMinutes() || date.getHours())) {
                date.setMinutes(date.getMinutes() - date.getTimezoneOffset());
              }

              selectedDates.push(date);
            }
          }
        }

        _orders.push({
          _id: order._id,
          child_id: order.child_id,
          event_id: order.event_id,
          selectedDates: selectedDates,
          variants: order.variants
        });
      }

      return _orders;
    }

    function redirect(url, method, fields) {
      $('<form>', {
        method: method,
        action: url
      }).append(fields).appendTo('body').submit();
    }

    function fetchResponse(data) {
      return data.data;
    }

    function ShowValidationErrors(errors) {
      if (errors.length) {
        for (var i = 0, error; (error = errors[i]); i++) {
          alertService.error(error.message);
        }
      }

      return errors;
    }

    function existsDay(dates, date) {
      date.setHours(0);
      date.setMinutes(0);

      return $filter('filter')(dates, function (day) {
        return moment(date).diff(moment(day.date)) === 0;
      }).length;
    }

    function findEvent(events, eventId) {
      for(var i = 0, event; (event = events[i]); i++) {
        if(event.id === eventId) {
          return angular.copy(event);
        }
      }
    }

    function pushEventDates(events, form) {
      //Preparation for edit order
      /*if (form.weeks) {
       var selectedDates = [];

       for (var k = 0, previousWeek; (previousWeek = form.weeks[k]); k++) {
       for (var p = 0, day; (day = previousWeek.dates[p]); p++) {
       if (day.selected) {
       selectedDates.push(day);
       }
       }
       }
       }*/

      //Find the event
      var event = findEvent(events, form.event_id);

      form.event = event;
      form.weeks = [];

      var startDate = angular.copy(event.start_date);

      // Remove saturday and sunday from period if from this start. Sunday is 0
      if (startDate.weekday() === 0) {
        startDate.add(1, 'd');
      } else if (startDate.weekday() === 6) {
        startDate.add(2, 'd');
      }


      for (var startWeek = startDate, mondayOfWeek = new Date(startDate); (startWeek <= event.end_date); startWeek = new Date((new Date(mondayOfWeek)).setDate((new Date(mondayOfWeek)).getDate() + 7)), startDate = new Date(startWeek)) {
        // Get difference between real start event and monday (1 - because Monday is 1)
        var difference = moment(startWeek).weekday() - 1;

        //Get monday of week
        mondayOfWeek = new Date(startWeek);
        mondayOfWeek.setDate(mondayOfWeek.getDate() - difference);

        //Get end of week(now friday)
        var fridayOfWeek = new Date(mondayOfWeek);
        fridayOfWeek.setDate(fridayOfWeek.getDate() + 4);
        var endOfWeek = new Date(fridayOfWeek);

        var week = {
          monday: new Date(mondayOfWeek),
          friday: new Date(endOfWeek),
          selected: false,
          allow: true,//Allow select full week or not. Must be 'false' if at least one weekday is fully
          fully: true,
          dates: [],
          selectedAnyDates: false
        };

        //If event finish before friday
        if (endOfWeek > event.end_date) {
          endOfWeek = event.end_date;
        }

        if (mondayOfWeek < startDate) {
          for (var tDate = new Date(mondayOfWeek); tDate < startDate; tDate.setDate(tDate.getDate() + 1)) {
            week.dates.push({
              date: angular.copy(tDate),
              allow: false,
              selected: false,
              fully: false
            });
          }
        }

        for (var date = new Date(startDate); date <= endOfWeek; date.setDate(date.getDate() + 1)) {
          /*jshint -W083 */
          var except = $filter('filter')(event.exception_dates, (function (exceptionDate) {
              exceptionDate = moment(exceptionDate.date).toDate();
              return exceptionDate.toDateString() === date.toDateString();
            })),
            allow = (except.length > 0) ? false : true;

          week.dates.push({
            date: angular.copy(date),
            allow: allow,
            selected: false,
            fully: allow ? existsDay(form.event.busy_dates, date) : false
          });
        }

        if (fridayOfWeek > endOfWeek) {
          for (date = new Date(endOfWeek), date.setDate(date.getDate() + 1); date <= fridayOfWeek; date.setDate(date.getDate() + 1)) {
            week.dates.push({
              date: angular.copy(date),
              allow: false,
              selected: false,
              fully: false
            });
          }
        }

        for(var k = 0, dayOfWeek; (dayOfWeek = week.dates[k]); k++) {
          //Ha-ha what is it?!
          if(dayOfWeek.allow && !dayOfWeek.fully) {
            week.fully = false;
          }

          if(dayOfWeek.allow && dayOfWeek.fully) {
            week.allow = false;
          }
        }

        form.weeks.push(week);
      }
    }

    function prepareProducts(products) {
      var localProducts = store.get('products');

      return angular.forEach(products, function (product) {
        product.imagePath = product.image_thumbnail ? '/' + product.image_thumbnail.path : '';

        if (localProducts) {
          var localProduct = $filter('filter')(localProducts, {id: product.id})[0];

          if (localProduct) {
            product.selectedVariant = localProduct.selectedVariant;
            product.selectedQuantity = localProduct.selectedQuantity;
          }
        }

        product.price = 0;

        var variant = product.variants.filter(function (variant) {
          return product.selectedVariant && product.selectedVariant === variant.id;
        })[0];

        if (variant && product.selectedQuantity) {
          product.price = variant.price * product.selectedQuantity;
        }
      });
    }

    function prepareBookingForRequest(_data) {
      var data = angular.copy(_data);

      data.variants = [];

      angular.forEach(data.products, function (product) {
        if (product.selectedVariant && product.selectedQuantity) {
          data.variants.push({
            id: product.selectedVariant,
            count: product.selectedQuantity
          });
        }
      });

      delete data.products;

      return data;
    }

    function prepareDatesForRequest(object, properties, needClone) {
      var localObject = needClone ? angular.copy(object) : object;

      properties.forEach(function (property) {
        localObject[property] = moment(localObject[property]).format('YYYY-MM-DD');
      });

      return localObject;
    }
  }
})();

(function () {
  'use strict';

  angular
    .module('booking')
    .service('orderService', orderService);

  orderService.$inject = ['$filter', '$q', 'store', 'childService', 'eventService', '$http', 'helperService'];

  /* @ngInject */
  function orderService($filter, $q, store, childService, eventService, $http, helperService) {
    /* jshint validthis: true */
    var vm = this;

    vm.promises = $q.all([childService.all(), eventService.all()]).then(function (data) {
      vm.children = data[0];
      vm.events = data[1];
    });

    vm.customerId = store.get('customer-id');

    if(vm.customerId) {
      store.remove('customer-id');
      store.remove('orders');
      store.remove('index');
    }

    vm.orders = store.get('orders');
    vm.index = store.get('index');

    if (!vm.orders) {
      vm.orders = [];
      vm.index = 1;
    }

    vm.validate = validate;

    vm.count = count;
    vm.all = all;
    vm.add = add;
    vm.get = get;
    vm.update = update;
    vm.remove = remove;
    vm.calculate = calculate;
    vm.clear = clear;
    vm.products = products;
    vm.saveProducts = saveProducts;

    ///////////////////////

    function products() {
      return $http.get('/api/products').then(helperService.fetchResponse).then(helperService.prepareProducts);
    }

    function count() {
      return vm.orders.length;
    }

    function clear() {
      var orders = angular.copy(vm.orders);

      var localPromise = $q.defer();

      vm.promises.then(function () {
        store.remove('orders');
        store.remove('index');
        store.remove('products');

        localPromise.resolve(true);
      });

      return localPromise.promise;
    }

    function saveProducts(products) {
      var localPromise = $q.defer();

      vm.promises.then(function () {
        store.set('products', products);

        localPromise.resolve(true);
      });

      return localPromise.promise;
    }

    function calculate(order) {
      var sum = 0;

      for (var i = 0, week; (week = order.weeks[i]); i++) {
        if (week.selected) {
          sum += order.event.cost_per_week;
        } else {
          for (var j = 0, weekday; (weekday = week.dates[j]); j++) {
            if(weekday.selected) {
              sum += order.event.cost_per_day;
            }
          }
        }
      }

      angular.forEach(order.variants, function (variant) {
        sum += variant.price * variant.count;
      });

      return sum;
    }

    function validate(order, children, events) {
      var errors = [];

      var child = $filter('filter')(children, {id: order.child_id});
      if (!(order.child_id && child.length)) {
        errors.push({
          title: 'child',
          message: 'Necessary select child'
        });
      }

      var event = $filter('filter')(events, {id: order.event_id});
      if (!(order.event_id && event.length)) {
        errors.push({
          title: 'event',
          message: 'Necessary select event'
        });
      }

      //Check that selected as minimum one day
      var selected = false;
      for (var i = 0, week; order.weeks && (week = order.weeks[i]); i++) {
        for (var j = 0, day; (day = week.dates[j]); j++) {
          if (day.selected) {
            selected = true;
            break;
          }
        }

        if (selected) {
          break;
        }
      }

      if (!selected) {
        errors.push({
          title: 'dates',
          message: 'Necessary select at least one day'
        });
      }

      return errors.reverse();
    }

    function all() {
      var orders = angular.copy(vm.orders);

      var localPromise = $q.defer();

      vm.promises.then(function () {
        for (var i = 0, order; (order = orders[i]); i++) {
          setChild(order);
          setEvent(order);
        }

        localPromise.resolve(orders);
      });

      return localPromise.promise;
    }

    function add(order) {
      var localPromise = $q.defer();

      vm.promises.then(function () {
        order._id = vm.index++;
        vm.orders.push(order);
        saveOrders();

        localPromise.resolve(
          angular.copy(order)
        );
      });


      return localPromise.promise;
    }

    function get(id) {
      var localPromise = $q.defer();

      vm.promises.then(function () {
        var order = angular.copy($filter('filter')(vm.orders, {_id: id})[0]);

        setChild(order);
        setEvent(order);

        localPromise.resolve(order);
      });


      return localPromise.promise;
    }

    function update(id, order) {
      var localPromise = $q.defer();

      var _order = $filter('filter')(vm.orders, {_id: id})[0];

      _order.child_id = order.child_id;
      _order.child = order.child;
      _order.event_id = order.event_id;
      _order.event = order.event;
      _order.dates = order.dates;

      saveOrders();

      localPromise.resolve(_order);

      return localPromise.promise;
    }

    function remove(id) {
      var localPromise = $q.defer();

      var order = $filter('filter')(vm.orders, {_id: id})[0];

      vm.orders.splice(vm.orders.indexOf(order), 1);

      saveOrders();

      localPromise.resolve(true);

      return localPromise.promise;
    }

    function saveOrders() {
      store.set('orders', vm.orders);
      store.set('index', vm.index);
    }

    function setChild(order) {
      order.child = $filter('filter')(vm.children, {id: order.child_id})[0];
    }

    function setEvent(order) {
      order.event = $filter('filter')(vm.events, {id: order.event_id})[0];
    }

  }
})();

(function () {
  'use strict';

  angular
    .module('booking')
    .service('profileService', profile);

  profile.$inject = ['$http', 'helperService'];

  /* @ngInject */
  function profile($http, helperService) {
    /* jshint validthis: true */
    var vm = this;

    vm.get = get;
    vm.update = update;
    vm.countries = countries;

    ////////////////////////////

    function get() {
      return $http.get('/api/profile')
        .then(helperService.fetchResponse)
        .then(function (data) {
          data.email = data.user.email;
          return data;
        });
    }

    function update(data) {
      return $http.put('/api/profile', data)
        .then(helperService.fetchResponse)
        .then(function (data) {
          data.email = data.user.email;
          return data;
        });
    }

    function countries() {
      return $http.get('/api/countries').then(helperService.fetchResponse);
    }
  }
})();

(function () {
  'use strict';

  angular
    .module('booking')
    .component('selectDate', {
      bindings: {
        date: '=ngModel'
      },
      templateUrl: '/booking/components/select-date/select-date.html',
      controllerAs: 'SelectDateCtrl',
      controller: 'SelectDateController'
    });
})();
(function () {
  'use strict';

  SelectDateController.$inject = ["$filter", "months"];
  angular
    .module('booking')
    .controller('SelectDateController', SelectDateController);

  /* @ngInject */
  function SelectDateController($filter, months) {
    /* jshint validthis: true */
    var vm = this;

    vm.days = $filter('range')([], 1, 31);
    vm.months = months;
    vm.years = $filter('range')([], moment().get('year') - 100, moment().get('year'), 'desc');

    initDate();

    vm.change = change;

    //////////////////////////////

    function change() {
      if (!vm.day || vm.month === undefined || !vm.year) {
        return;
      }

      while (!moment([vm.year, vm.month, vm.day]).isValid()) {
        --vm.day;
      }

      vm.date = moment([vm.year, vm.month, vm.day]);
    }

    function initDate() {
      var date = moment(vm.date);
      if (date.isValid()) {
        vm.day = parseInt(date.format('D'));
        vm.month = date.month();
        vm.year = date.year();
      }
    }

  }
})();

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC5tb2R1bGUuanMiLCJhcHAuY29uZmlnLmpzIiwiYXBwLmNvbnN0YW50cy5qcyIsImFwcC5pbnRlcmNlcHRvci5qcyIsImFwcC5yb3V0ZXMuanMiLCJhcHAucnVuLmpzIiwiYm9va2luZy5jb250cm9sbGVyLmpzIiwiY2hpbGRyZW4vY2hpbGRyZW4uY29udHJvbGxlci5qcyIsImNoaWxkcmVuL2NyZWF0ZS5jb250cm9sbGVyLmpzIiwiY2hpbGRyZW4vZWRpdC5jb250cm9sbGVyLmpzIiwiY2hpbGRyZW4vc2hvdy5jb250cm9sbGVyLmpzIiwiY29tcGxldGUvY29tcGxldGUuY29udHJvbGxlci5qcyIsImNvbXBsZXRlL2NvbmZpcm0tcmVtb3ZlLW9yZGVyLmNvbnRyb2xsZXIuanMiLCJmaWx0ZXJzL2FnZS5maWx0ZXIuanMiLCJmaWx0ZXJzL2N1cnJlbmN5LWxiLmZpbHRlci5qcyIsImZpbHRlcnMvZXZlbnQuanMiLCJmaWx0ZXJzL2xvZ2ljYWwuZmlsdGVyLmpzIiwiZmlsdGVycy9wcm9maWxlLmZpbHRlci5qcyIsImZpbHRlcnMvcmFuZ2UuZmlsdGVyLmpzIiwiZmlsdGVycy9zZXguZmlsdGVyLmpzIiwiZmlsdGVycy9zdW1CeUtleS5qcyIsImZpbHRlcnMvc3dpbS5maWx0ZXIuanMiLCJmaWx0ZXJzL3dlZWtkYXkuanMiLCJvcmRlcnMvY2hpbGQtZGV0YWlscy1tb2RhbC5jb250cm9sbGVyLmpzIiwib3JkZXJzL2NyZWF0ZS5jb250cm9sbGVyLmpzIiwib3JkZXJzL2VkaXQuY29udHJvbGxlci5qcyIsIm9yZGVycy9ldmVudC1pbmZvLW1vZGFsLmNvbnRyb2xsZXIuanMiLCJvcmRlcnMvb3JkZXJzLmNvbnRyb2xsZXIuanMiLCJvcmRlcnMvc2hvdy5jb250cm9sbGVyLmpzIiwicHJvZmlsZS9wcm9maWxlLmNvbnRyb2xsZXIuanMiLCJzZXJ2aWNlcy9hbGVydC5zZXJ2aWNlLmpzIiwic2VydmljZXMvYm9va2luZy5zZXJ2aWNlLmpzIiwic2VydmljZXMvY2hpbGRyZW4uc2VydmljZS5qcyIsInNlcnZpY2VzL2V2ZW50cy5zZXJ2aWNlLmpzIiwic2VydmljZXMvaGVscGVyLnNlcnZpY2UuanMiLCJzZXJ2aWNlcy9vcmRlcnMuc2VydmljZS5qcyIsInNlcnZpY2VzL3Byb2ZpbGUuc2VydmljZS5qcyIsImNvbXBvbmVudHMvc2VsZWN0LWRhdGUvc2VsZWN0LWRhdGUuY29tcG9uZW50LmpzIiwiY29tcG9uZW50cy9zZWxlY3QtZGF0ZS9zZWxlY3QtZGF0ZS5jb250cm9sbGVyLmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLENBQUEsWUFBQTtFQUNBOztFQUVBLFFBQUEsT0FBQSxXQUFBO0lBQ0E7SUFDQTtJQUNBO0lBQ0E7SUFDQTtJQUNBO0lBQ0E7Ozs7O0FDVkEsQ0FBQSxXQUFBO0VBQ0E7O0VBRUE7S0FDQSxPQUFBO0tBQ0EsT0FBQTs7RUFFQSxPQUFBLFVBQUEsQ0FBQTs7RUFFQSxTQUFBLE9BQUEsZUFBQTs7SUFFQSxjQUFBLGFBQUEsS0FBQTs7Ozs7QUNYQSxDQUFBLFlBQUE7O0VBRUE7O0VBRUE7S0FDQSxPQUFBO0tBQ0EsU0FBQSxXQUFBLENBQUEsTUFBQSxRQUFBLE9BQUEsTUFBQTtLQUNBLFNBQUEsaUJBQUE7TUFDQTtRQUNBLE9BQUE7UUFDQSxPQUFBOztNQUVBO1FBQ0EsT0FBQTtRQUNBLE9BQUE7OztLQUdBLFNBQUEsZ0JBQUE7TUFDQTtRQUNBLE9BQUE7UUFDQSxPQUFBOztNQUVBO1FBQ0EsT0FBQTtRQUNBLE9BQUE7Ozs7Ozs7S0FPQSxTQUFBLFVBQUE7TUFDQTtRQUNBLE9BQUE7UUFDQSxNQUFBOztNQUVBO1FBQ0EsT0FBQTtRQUNBLE1BQUE7O01BRUE7UUFDQSxPQUFBO1FBQ0EsTUFBQTs7TUFFQTtRQUNBLE9BQUE7UUFDQSxNQUFBOztNQUVBO1FBQ0EsT0FBQTtRQUNBLE1BQUE7O01BRUE7UUFDQSxPQUFBO1FBQ0EsTUFBQTs7TUFFQTtRQUNBLE9BQUE7UUFDQSxNQUFBOztNQUVBO1FBQ0EsT0FBQTtRQUNBLE1BQUE7O01BRUE7UUFDQSxPQUFBO1FBQ0EsTUFBQTs7TUFFQTtRQUNBLE9BQUE7UUFDQSxNQUFBOztNQUVBO1FBQ0EsT0FBQTtRQUNBLE1BQUE7O01BRUE7UUFDQSxPQUFBO1FBQ0EsTUFBQTs7Ozs7O0FDOUVBLENBQUEsWUFBQTtFQUNBOztFQUVBO0tBQ0EsT0FBQTtLQUNBLFFBQUEsaUJBQUE7O0VBRUEsY0FBQSxVQUFBLENBQUEsZ0JBQUE7O0VBRUEsU0FBQSxjQUFBLGNBQUEsSUFBQTtJQUNBLE9BQUE7TUFDQSxlQUFBLFVBQUEsS0FBQTtRQUNBLElBQUEsU0FBQSxJQUFBO1VBQ0Esa0JBQUEsSUFBQTs7UUFFQSxHQUFBLG9CQUFBLGlCQUFBO1VBQ0EsYUFBQSxNQUFBOztVQUVBLE9BQUEsU0FBQSxPQUFBO2VBQ0EsR0FBQSxnQkFBQSxXQUFBLGFBQUEsZ0JBQUEsU0FBQSxLQUFBO1VBQ0EsYUFBQSxrQkFBQSxpQkFBQSxrQkFBQTtnQkFDQTtVQUNBLGFBQUEsa0JBQUEsQ0FBQSxpQkFBQSxrQkFBQTs7O1FBR0EsT0FBQSxHQUFBLE9BQUE7Ozs7SUFJQSxTQUFBLGtCQUFBLFFBQUE7TUFDQSxJQUFBOztNQUVBLFFBQUE7UUFDQSxLQUFBO1VBQ0EsUUFBQTtVQUNBO1FBQ0EsS0FBQTtVQUNBLFFBQUE7VUFDQTtRQUNBLEtBQUE7VUFDQSxRQUFBO1VBQ0E7OztNQUdBLE9BQUE7Ozs7O0FDNUNBLENBQUEsWUFBQTtFQUNBOztFQUVBO0tBQ0EsT0FBQTtLQUNBLE9BQUE7O0VBRUEsT0FBQSxVQUFBLENBQUEsa0JBQUE7O0VBRUEsU0FBQSxPQUFBLGdCQUFBLG9CQUFBOztJQUVBLG1CQUFBLFVBQUE7O0lBRUEsSUFBQSxTQUFBOztJQUVBLElBQUEsY0FBQTtNQUNBLFFBQUEsQ0FBQSxnQkFBQSxVQUFBLGNBQUE7UUFDQSxPQUFBLGFBQUE7O01BRUEsT0FBQSxDQUFBLGdCQUFBLGdCQUFBLFVBQUEsY0FBQSxjQUFBO1FBQ0EsT0FBQSxhQUFBLElBQUEsYUFBQTs7TUFFQSxVQUFBLENBQUEsZ0JBQUEsVUFBQSxjQUFBO1FBQ0EsT0FBQSxhQUFBOztNQUVBLE9BQUEsQ0FBQSxnQkFBQSxnQkFBQSxVQUFBLGNBQUEsY0FBQTtRQUNBLE9BQUEsYUFBQSxJQUFBLGFBQUE7O01BRUEsUUFBQSxDQUFBLGdCQUFBLFVBQUEsY0FBQTtRQUNBLE9BQUEsYUFBQTs7TUFFQSxhQUFBLENBQUEsZ0JBQUEsU0FBQSxjQUFBO1FBQ0EsT0FBQSxhQUFBOztNQUVBLGVBQUEsQ0FBQSxnQkFBQSxTQUFBLGNBQUE7UUFDQSxPQUFBLGFBQUE7O01BRUEsVUFBQSxDQUFBLGdCQUFBLFNBQUEsY0FBQTtRQUNBLE9BQUEsYUFBQTs7TUFFQSxXQUFBLENBQUEsa0JBQUEsVUFBQSxnQkFBQTtRQUNBLE9BQUEsZUFBQTs7TUFFQSxTQUFBLENBQUEsa0JBQUEsVUFBQSxnQkFBQTtRQUNBLE9BQUEsZUFBQTs7OztJQUlBO09BQ0EsTUFBQSxXQUFBO1FBQ0EsVUFBQTtRQUNBLGFBQUE7Ozs7T0FJQSxNQUFBLG1CQUFBO1FBQ0EsVUFBQTtRQUNBLEtBQUE7UUFDQSxNQUFBO1VBQ0EsS0FBQTs7UUFFQSxVQUFBO1FBQ0EsWUFBQTtRQUNBLGNBQUE7UUFDQSxTQUFBO1VBQ0EsU0FBQSxZQUFBO1VBQ0EsV0FBQSxZQUFBOzs7T0FHQSxNQUFBLHdCQUFBO1FBQ0EsS0FBQTtRQUNBLGFBQUE7O09BRUEsTUFBQSx3QkFBQTtRQUNBLEtBQUE7UUFDQSxhQUFBOzs7O09BSUEsTUFBQSxvQkFBQTtRQUNBLFVBQUE7UUFDQSxLQUFBO1FBQ0EsVUFBQTtRQUNBLE1BQUE7VUFDQSxLQUFBOzs7T0FHQSxNQUFBLDBCQUFBO1FBQ0EsS0FBQTtRQUNBLGFBQUE7UUFDQSxZQUFBO1FBQ0EsY0FBQTtRQUNBLFNBQUE7VUFDQSxVQUFBLFlBQUE7OztPQUdBLE1BQUEsMkJBQUE7UUFDQSxLQUFBO1FBQ0EsYUFBQTtRQUNBLFlBQUE7UUFDQSxjQUFBO1FBQ0EsU0FBQTtVQUNBLGFBQUEsWUFBQTtVQUNBLGVBQUEsWUFBQTs7O09BR0EsTUFBQSx5QkFBQTtRQUNBLEtBQUE7UUFDQSxhQUFBO1FBQ0EsWUFBQTtRQUNBLGNBQUE7UUFDQSxTQUFBO1VBQ0EsT0FBQSxZQUFBOzs7T0FHQSxNQUFBLHlCQUFBO1FBQ0EsS0FBQTtRQUNBLGFBQUE7UUFDQSxZQUFBO1FBQ0EsY0FBQTtRQUNBLFNBQUE7VUFDQSxPQUFBLFlBQUE7VUFDQSxhQUFBLFlBQUE7VUFDQSxlQUFBLFlBQUE7Ozs7T0FJQSxNQUFBLGtCQUFBO1FBQ0EsVUFBQTtRQUNBLEtBQUE7UUFDQSxVQUFBO1FBQ0EsTUFBQTtVQUNBLEtBQUE7Ozs7Ozs7Ozs7Ozs7O09BY0EsTUFBQSx5QkFBQTtRQUNBLEtBQUE7UUFDQSxhQUFBO1FBQ0EsWUFBQTtRQUNBLGNBQUE7UUFDQSxTQUFBO1VBQ0EsVUFBQSxZQUFBO1VBQ0EsUUFBQSxZQUFBO1VBQ0EsYUFBQSxDQUFBLGdCQUFBLFVBQUEsY0FBQTtZQUNBLE9BQUEsYUFBQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O09BOEJBLE1BQUEsb0JBQUE7UUFDQSxLQUFBO1FBQ0EsYUFBQTtRQUNBLE1BQUE7VUFDQSxLQUFBOztRQUVBLFlBQUE7UUFDQSxjQUFBO1FBQ0EsU0FBQTtVQUNBLFVBQUEsWUFBQTtVQUNBLFFBQUEsQ0FBQSxnQkFBQSxVQUFBLGNBQUE7WUFDQSxPQUFBLGFBQUE7O1VBRUEsUUFBQSxDQUFBLFVBQUEsa0JBQUEsaUJBQUEsU0FBQSxRQUFBLGdCQUFBLGVBQUE7WUFDQSxJQUFBLFVBQUEsY0FBQSxjQUFBOztZQUVBLE9BQUEsZUFBQSxVQUFBOztVQUVBLGtCQUFBLENBQUEsVUFBQSxVQUFBLFdBQUEsVUFBQSxRQUFBLFFBQUEsU0FBQTtZQUNBLElBQUEsa0JBQUE7O1lBRUEsS0FBQSxJQUFBLElBQUEsR0FBQSxRQUFBLFFBQUEsT0FBQSxLQUFBLEtBQUE7Y0FDQSxNQUFBLE1BQUE7O2NBRUEsSUFBQSxRQUFBLFFBQUEsVUFBQSxRQUFBLENBQUEsS0FBQSxNQUFBLE1BQUE7Y0FDQSxHQUFBLE1BQUEsVUFBQSxNQUFBLEdBQUEsUUFBQTtnQkFDQSxNQUFBLE1BQUEsTUFBQSxHQUFBO2dCQUNBLE1BQUEsV0FBQSxNQUFBLEdBQUE7Z0JBQ0EsTUFBQSxpQkFBQSxNQUFBLEdBQUE7OztjQUdBLE1BQUEsV0FBQSxNQUFBLEdBQUE7O2NBRUEsZ0JBQUEsS0FBQTs7O1lBR0EsT0FBQTs7VUFFQSxnQkFBQSxDQUFBLGtCQUFBLFVBQUEsZ0JBQUE7WUFDQSxPQUFBLGVBQUE7Ozs7Ozs7QUNoT0EsQ0FBQSxZQUFBO0VBQ0E7O0VBRUE7S0FDQSxPQUFBO0tBQ0EsSUFBQTs7RUFFQSxJQUFBLFVBQUEsQ0FBQSxVQUFBLGNBQUE7O0VBRUEsU0FBQSxJQUFBLFFBQUEsWUFBQSxjQUFBOztJQUVBLFdBQUEsSUFBQSxxQkFBQSxVQUFBLE9BQUEsU0FBQSxVQUFBLFdBQUEsWUFBQTtNQUNBLElBQUEsZUFBQTtRQUNBOztNQUVBLEdBQUEsUUFBQSxTQUFBLHNCQUFBLENBQUEsYUFBQSxTQUFBO1FBQ0EsZUFBQTtRQUNBLFlBQUE7OztNQUdBLEdBQUEsY0FBQTtRQUNBLE1BQUE7UUFDQSxHQUFBLFdBQUE7VUFDQSxPQUFBLEdBQUE7O2FBRUE7UUFDQSxXQUFBLE1BQUEsUUFBQSxLQUFBOzs7Ozs7O0FDMUJBLENBQUEsWUFBQTtFQUNBOztFQUVBO0tBQ0EsT0FBQTtLQUNBLFdBQUEscUJBQUE7O0VBRUEsa0JBQUEsVUFBQSxDQUFBOzs7RUFHQSxTQUFBLGtCQUFBLFFBQUE7O0lBRUEsSUFBQSxLQUFBOztJQUVBLElBQUEsTUFBQSxPQUFBLFFBQUEsS0FBQTs7OztBQ2RBLENBQUEsWUFBQTtFQUNBOztFQUVBO0tBQ0EsT0FBQTtLQUNBLFdBQUEsc0JBQUE7O0VBRUEsbUJBQUEsVUFBQSxDQUFBLFlBQUEsZ0JBQUEsZ0JBQUE7OztFQUdBLFNBQUEsbUJBQUEsVUFBQSxjQUFBLGNBQUEsUUFBQTs7SUFFQSxJQUFBLEtBQUE7O0lBRUEsR0FBQSxXQUFBO0lBQ0EsR0FBQSxtQkFBQTtJQUNBLEdBQUEsaUJBQUE7O0lBRUEsR0FBQSxTQUFBO0lBQ0EsR0FBQSxPQUFBOzs7O0lBSUEsU0FBQSxPQUFBO01BQ0EsSUFBQSxDQUFBLEdBQUEsa0JBQUE7UUFDQSxhQUFBLE1BQUE7O1FBRUE7O01BRUEsSUFBQSxDQUFBLEdBQUEsZ0JBQUE7UUFDQSxhQUFBLE1BQUE7O1FBRUE7OztNQUdBLE9BQUEsR0FBQTs7O0lBR0EsU0FBQSxPQUFBLE9BQUE7TUFDQSxhQUFBLE9BQUEsTUFBQSxJQUFBLEtBQUEsWUFBQTtRQUNBLGFBQUEsUUFBQTs7UUFFQSxHQUFBLFNBQUEsT0FBQSxHQUFBLFNBQUEsUUFBQSxRQUFBOzs7Ozs7QUMxQ0EsQ0FBQSxZQUFBO0VBQ0E7O0VBRUE7S0FDQSxPQUFBO0tBQ0EsV0FBQSx5QkFBQTs7RUFFQSxzQkFBQSxVQUFBLENBQUEsZ0JBQUEsVUFBQSxXQUFBLGVBQUE7OztFQUdBLFNBQUEsc0JBQUEsY0FBQSxRQUFBLFNBQUEsYUFBQSxlQUFBOztJQUVBLElBQUEsS0FBQTs7SUFFQSxHQUFBLGNBQUE7SUFDQSxHQUFBLGdCQUFBOztJQUVBLEdBQUEsT0FBQTtNQUNBLFlBQUE7TUFDQSxXQUFBO01BQ0EsT0FBQTtNQUNBLFVBQUE7TUFDQSxLQUFBO01BQ0EsY0FBQTtNQUNBLFFBQUE7TUFDQSxnQkFBQTtNQUNBLGFBQUE7TUFDQSx5QkFBQTtNQUNBLDhCQUFBO01BQ0EsU0FBQTtNQUNBLFlBQUE7TUFDQSxzQkFBQTtNQUNBLFVBQUE7TUFDQSwwQkFBQTs7O0lBR0EsR0FBQSxTQUFBOztJQUVBLEdBQUEsaUJBQUE7Ozs7SUFJQSxTQUFBLFNBQUE7TUFDQSxhQUFBLE1BQUEsR0FBQSxNQUFBLEtBQUEsWUFBQTtRQUNBLFFBQUEsUUFBQTs7UUFFQSxPQUFBLEdBQUE7Ozs7Ozs7QUM5Q0EsQ0FBQSxZQUFBO0VBQ0E7O0VBRUE7S0FDQSxPQUFBO0tBQ0EsV0FBQSx1QkFBQTs7RUFFQSxvQkFBQSxVQUFBLENBQUEsU0FBQSxnQkFBQSxVQUFBLFdBQUEsZUFBQTs7O0VBR0EsU0FBQSxvQkFBQSxPQUFBLGNBQUEsUUFBQSxTQUFBLGFBQUEsZUFBQTs7SUFFQSxJQUFBLEtBQUE7O0lBRUEsR0FBQSxjQUFBO0lBQ0EsR0FBQSxnQkFBQTs7SUFFQSxHQUFBLE9BQUE7O0lBRUEsR0FBQSxTQUFBOztJQUVBLEdBQUEsaUJBQUE7Ozs7SUFJQSxTQUFBLFNBQUE7TUFDQSxhQUFBLE9BQUEsR0FBQSxNQUFBLEtBQUEsWUFBQTtRQUNBLFFBQUEsUUFBQTs7UUFFQSxPQUFBLEdBQUE7Ozs7Ozs7QUM3QkEsQ0FBQSxZQUFBO0VBQ0E7O0VBRUE7S0FDQSxPQUFBO0tBQ0EsV0FBQSx1QkFBQTs7RUFFQSxvQkFBQSxVQUFBLENBQUE7OztFQUdBLFNBQUEsb0JBQUEsT0FBQTs7SUFFQSxJQUFBLEtBQUE7O0lBRUEsR0FBQSxRQUFBOzs7O0FDZEEsQ0FBQSxZQUFBO0VBQ0E7OztFQUVBO0tBQ0EsT0FBQTtLQUNBLFdBQUEsc0JBQUE7OztFQUdBLFNBQUEsbUJBQUEsZ0JBQUEsa0JBQUEsZ0JBQUEsY0FBQSxjQUFBLGNBQUEsZUFBQSxTQUFBLFFBQUEsV0FBQSxVQUFBLE9BQUE7O0lBRUEsSUFBQSxLQUFBOztJQUVBLEdBQUEsU0FBQTtJQUNBLEdBQUEsT0FBQSxNQUFBLFFBQUEsWUFBQSxHQUFBLFFBQUE7SUFDQSxHQUFBLE9BQUEsV0FBQSxRQUFBLFlBQUEsR0FBQSxRQUFBO0lBQ0EsR0FBQSxlQUFBLFFBQUEsWUFBQSxVQUFBO0lBQ0EsR0FBQSxPQUFBLFNBQUE7O0lBRUEsR0FBQSxlQUFBLGVBQUEsV0FBQSxJQUFBLGFBQUEsT0FBQSxHQUFBLEtBQUE7SUFDQSxHQUFBLGlCQUFBO0lBQ0EsR0FBQSxnQkFBQTtJQUNBLEdBQUEsV0FBQTs7SUFFQSxHQUFBLE9BQUE7TUFDQSxjQUFBO01BQ0EsZ0JBQUEsZUFBQSxXQUFBLElBQUEsZUFBQSxHQUFBLFNBQUE7TUFDQSxRQUFBO01BQ0EsUUFBQTs7SUFFQSxHQUFBLFNBQUE7SUFDQSxHQUFBLFVBQUE7O0lBRUE7SUFDQTtJQUNBO0lBQ0E7O0lBRUEsR0FBQSxPQUFBO0lBQ0EsR0FBQSxTQUFBO0lBQ0EsR0FBQSxjQUFBO0lBQ0EsR0FBQSxlQUFBO0lBQ0EsR0FBQSx3QkFBQTtJQUNBLEdBQUEsZUFBQTs7OztJQUlBLFNBQUEsY0FBQTtNQUNBLElBQUEsQ0FBQSxHQUFBLEtBQUEsUUFBQTtRQUNBOzs7TUFHQSxHQUFBLFVBQUE7O01BRUEsZUFBQSxZQUFBLEdBQUEsS0FBQSxRQUFBLEtBQUEsVUFBQSxTQUFBO1FBQ0EsR0FBQSxPQUFBLGdCQUFBO1FBQ0E7UUFDQSxhQUFBLFFBQUE7UUFDQSxHQUFBLGdCQUFBO1NBQ0EsWUFBQTtRQUNBLGFBQUEsTUFBQTtTQUNBLFFBQUEsWUFBQTtRQUNBLEdBQUEsVUFBQTs7OztJQUlBLFNBQUEsZUFBQTs7TUFFQSxHQUFBLEtBQUEsU0FBQTtNQUNBLEdBQUEsT0FBQSxnQkFBQTtNQUNBLEdBQUEsT0FBQSxTQUFBO01BQ0EsR0FBQSxnQkFBQTs7TUFFQTs7TUFFQSxhQUFBLFFBQUE7OztJQUdBLFNBQUEsT0FBQSxJQUFBO01BQ0EsSUFBQSxnQkFBQSxVQUFBLEtBQUE7UUFDQSxhQUFBO1FBQ0EsWUFBQTtRQUNBLGNBQUE7UUFDQSxNQUFBOzs7TUFHQSxjQUFBLE9BQUEsS0FBQSxZQUFBO1FBQ0EsYUFBQSxPQUFBLElBQUEsS0FBQSxZQUFBOztVQUVBLGFBQUEsTUFBQSxLQUFBLFVBQUEsUUFBQTtZQUNBLGFBQUEsUUFBQTs7WUFFQSxJQUFBLENBQUEsT0FBQSxRQUFBO2NBQ0EsT0FBQSxHQUFBOzs7WUFHQSxJQUFBLFVBQUEsY0FBQSxjQUFBO1lBQ0EsZUFBQSxVQUFBLFNBQUEsS0FBQSxVQUFBLFFBQUE7Y0FDQSxJQUFBLGtCQUFBOztjQUVBLEtBQUEsSUFBQSxJQUFBLEdBQUEsUUFBQSxRQUFBLE9BQUEsS0FBQSxLQUFBO2dCQUNBLE1BQUEsTUFBQTs7Z0JBRUEsSUFBQSxRQUFBLFFBQUEsVUFBQSxRQUFBLENBQUEsS0FBQSxNQUFBLE1BQUE7Z0JBQ0EsSUFBQSxNQUFBLFVBQUEsTUFBQSxHQUFBLFFBQUE7a0JBQ0EsTUFBQSxNQUFBLE1BQUEsR0FBQTtrQkFDQSxNQUFBLFdBQUEsTUFBQSxHQUFBO2tCQUNBLE1BQUEsaUJBQUEsTUFBQSxHQUFBOztnQkFFQSxNQUFBLFdBQUEsTUFBQSxHQUFBO2dCQUNBLGdCQUFBLEtBQUE7OztjQUdBLEdBQUEsU0FBQTtjQUNBLEdBQUEsT0FBQSxNQUFBLFFBQUEsWUFBQSxHQUFBLFFBQUE7Y0FDQSxHQUFBLE9BQUEsV0FBQSxRQUFBLFlBQUEsR0FBQSxRQUFBO2NBQ0EsR0FBQSxlQUFBLFFBQUEsWUFBQSxHQUFBLFFBQUE7Y0FDQSxHQUFBLE9BQUEsU0FBQTtjQUNBOzs7Ozs7Ozs7SUFTQSxTQUFBLE9BQUE7TUFDQSxJQUFBLENBQUEsR0FBQSxRQUFBO1FBQ0EsYUFBQSxNQUFBO1FBQ0E7OztNQUdBLEdBQUEsVUFBQTs7TUFFQSxPQUFBLGVBQUEsS0FBQSxHQUFBLE1BQUEsS0FBQSxVQUFBLE1BQUE7UUFDQSxhQUFBLFFBQUE7OztRQUdBLGFBQUE7O1FBRUEsY0FBQSxTQUFBLEtBQUEsVUFBQSxLQUFBLFFBQUEsS0FBQTtTQUNBLFlBQUE7UUFDQSxHQUFBLFVBQUE7Ozs7SUFJQSxTQUFBLHlCQUFBO01BQ0EsR0FBQSxLQUFBLFNBQUEsY0FBQSxjQUFBLEdBQUE7OztJQUdBLFNBQUEsc0JBQUE7TUFDQSxJQUFBLEdBQUEsS0FBQSxVQUFBLEdBQUEsT0FBQSxlQUFBO1FBQ0EsR0FBQSxPQUFBLFNBQUEsR0FBQSxPQUFBLE1BQUEsR0FBQSxPQUFBLGdCQUFBO1FBQ0EsR0FBQSxPQUFBLFFBQUEsR0FBQSxPQUFBLE1BQUEsR0FBQSxPQUFBO2FBQ0E7UUFDQSxHQUFBLE9BQUEsUUFBQSxHQUFBLE9BQUEsTUFBQSxHQUFBLE9BQUE7O01BRUEsR0FBQSxPQUFBLFFBQUEsR0FBQSxPQUFBLFFBQUEsR0FBQTs7O0lBR0EsU0FBQSxrQkFBQTtNQUNBLFFBQUEsUUFBQSxHQUFBLFVBQUEsVUFBQSxTQUFBO1FBQ0EsSUFBQSxDQUFBLFFBQUEsa0JBQUE7VUFDQSxRQUFBLG1CQUFBOzs7UUFHQSxJQUFBLENBQUEsUUFBQSxpQkFBQTtVQUNBLFFBQUEsa0JBQUEsUUFBQSxTQUFBLFNBQUEsUUFBQSxTQUFBLEdBQUEsS0FBQTs7O1FBR0EsT0FBQSxlQUFBLFNBQUEsU0FBQTtVQUNBLEtBQUEsWUFBQTtZQUNBLElBQUEsUUFBQTs7WUFFQSxJQUFBLGtCQUFBLEtBQUE7WUFDQSxJQUFBLFVBQUEsS0FBQSxTQUFBLE9BQUEsVUFBQSxTQUFBO2NBQ0EsT0FBQSxtQkFBQSxvQkFBQSxRQUFBO2VBQ0E7O1lBRUEsSUFBQSxXQUFBLEtBQUEsa0JBQUE7Y0FDQSxRQUFBLFFBQUEsUUFBQSxLQUFBOzs7WUFHQSxPQUFBOzs7UUFHQSxPQUFBLGVBQUEsU0FBQSxZQUFBO1VBQ0EsS0FBQSxZQUFBO1lBQ0EsSUFBQSxXQUFBOztZQUVBLElBQUEsa0JBQUEsS0FBQTtZQUNBLElBQUEsVUFBQSxLQUFBLFNBQUEsT0FBQSxVQUFBLFNBQUE7Y0FDQSxPQUFBLG1CQUFBLG9CQUFBLFFBQUE7ZUFDQTs7WUFFQSxJQUFBLFdBQUEsS0FBQSxrQkFBQTtjQUNBLFdBQUEsUUFBQTs7O1lBR0EsT0FBQTs7OztRQUlBLHNCQUFBLFNBQUE7Ozs7SUFJQSxTQUFBLHNCQUFBLFNBQUEsdUJBQUE7TUFDQSxHQUFBLGtCQUFBLENBQUE7Ozs7TUFJQSxJQUFBLFVBQUEsUUFBQSxTQUFBLE9BQUEsVUFBQSxTQUFBO1FBQ0EsT0FBQSxRQUFBLG1CQUFBLFFBQUEsb0JBQUEsUUFBQTtTQUNBOztNQUVBLElBQUEsV0FBQSxRQUFBLGtCQUFBOzs7Ozs7SUFNQSxTQUFBLGVBQUE7TUFDQSxJQUFBLFdBQUEsR0FBQSxTQUFBLE9BQUEsVUFBQSxTQUFBO1FBQ0EsT0FBQSxRQUFBOzs7TUFHQSxhQUFBLGFBQUEsVUFBQSxLQUFBLFlBQUE7UUFDQSxhQUFBLFFBQUE7O1FBRUEsR0FBQSxLQUFBLFdBQUE7O1FBRUEsR0FBQSxlQUFBLFFBQUEsWUFBQSxVQUFBOztRQUVBOztRQUVBLEdBQUEsa0JBQUE7Ozs7SUFJQSxTQUFBLDZCQUFBO01BQ0EsSUFBQSxXQUFBLE1BQUEsSUFBQTs7TUFFQSxJQUFBLENBQUEsWUFBQSxDQUFBLFNBQUEsUUFBQTtRQUNBLE9BQUE7OztNQUdBLEdBQUEsS0FBQSxXQUFBOzs7Ozs7O0FDdlBBLENBQUEsWUFBQTtFQUNBOztFQUVBO0tBQ0EsT0FBQTtLQUNBLFdBQUEsZ0NBQUE7O0VBRUEsNkJBQUEsVUFBQSxDQUFBOzs7RUFHQSxTQUFBLDZCQUFBLG1CQUFBOztJQUVBLElBQUEsS0FBQTs7SUFFQSxHQUFBLEtBQUE7SUFDQSxHQUFBLFNBQUE7O0lBRUEsU0FBQSxLQUFBO01BQ0Esa0JBQUE7OztJQUdBLFNBQUEsU0FBQTtNQUNBLGtCQUFBLFFBQUE7Ozs7O0FDdEJBLENBQUEsVUFBQTtFQUNBOztFQUVBO0tBQ0EsT0FBQTtLQUNBLE9BQUEsT0FBQTs7RUFFQSxJQUFBLFVBQUE7O0VBRUEsU0FBQSxNQUFBO0lBQ0EsT0FBQTs7SUFFQSxTQUFBLEtBQUEsUUFBQSxNQUFBO01BQ0EsT0FBQSxPQUFBLE9BQUEsUUFBQTs7TUFFQSxJQUFBLFdBQUEsT0FBQSxPQUFBO01BQ0EsT0FBQSxLQUFBLEtBQUEsVUFBQTs7OztBQ2hCQSxDQUFBLFVBQUE7RUFDQTs7RUFFQTtLQUNBLE9BQUE7S0FDQSxPQUFBLGNBQUE7O0VBRUEsV0FBQSxVQUFBLENBQUE7O0VBRUEsU0FBQSxXQUFBLFNBQUE7SUFDQSxPQUFBOztJQUVBLFNBQUEsS0FBQSxPQUFBO01BQ0EsT0FBQSxRQUFBLFlBQUEsT0FBQTs7OztBQ2JBLENBQUEsVUFBQTtFQUNBOztFQUVBO0tBQ0EsT0FBQTtLQUNBLE9BQUEsU0FBQTs7RUFFQSxNQUFBLFVBQUE7O0VBRUEsU0FBQSxRQUFBO0lBQ0EsT0FBQTs7SUFFQSxTQUFBLEtBQUEsT0FBQTtNQUNBLE9BQUEsTUFBQSxNQUFBLE9BQUEsUUFBQSxNQUFBOzs7O0FDYkEsQ0FBQSxVQUFBO0VBQ0E7O0VBRUE7S0FDQSxPQUFBO0tBQ0EsT0FBQSxXQUFBOztFQUVBLFFBQUEsVUFBQTs7RUFFQSxTQUFBLFVBQUE7SUFDQSxPQUFBOztJQUVBLFNBQUEsS0FBQSxPQUFBO01BQ0EsT0FBQSxRQUFBLFFBQUE7Ozs7QUNiQSxDQUFBLFVBQUE7RUFDQTs7RUFFQTtLQUNBLE9BQUE7S0FDQSxPQUFBLFdBQUE7O0VBRUEsUUFBQSxVQUFBOztFQUVBLFNBQUEsVUFBQTtJQUNBLE9BQUE7O0lBRUEsU0FBQSxLQUFBLE1BQUE7TUFDQSxPQUFBLEtBQUEsYUFBQSxNQUFBLEtBQUE7Ozs7QUNiQSxDQUFBLFlBQUE7RUFDQTs7RUFFQTtLQUNBLE9BQUE7S0FDQSxPQUFBLFNBQUE7O0VBRUEsTUFBQSxVQUFBOztFQUVBLFNBQUEsUUFBQTtJQUNBLE9BQUE7O0lBRUEsU0FBQSxLQUFBLE9BQUEsS0FBQSxLQUFBLE9BQUE7TUFDQSxNQUFBLFNBQUE7TUFDQSxNQUFBLFNBQUE7O01BRUEsSUFBQSxVQUFBLFFBQUE7UUFDQSxLQUFBLElBQUEsSUFBQSxLQUFBLEtBQUEsS0FBQSxLQUFBO1VBQ0EsTUFBQSxLQUFBOzthQUVBO1FBQ0EsS0FBQSxJQUFBLElBQUEsS0FBQSxLQUFBLEtBQUEsS0FBQTtVQUNBLE1BQUEsS0FBQTs7OztNQUlBLE9BQUE7Ozs7QUMxQkEsQ0FBQSxVQUFBO0VBQ0E7O0VBRUE7S0FDQSxPQUFBO0tBQ0EsT0FBQSxPQUFBOztFQUVBLElBQUEsVUFBQTs7RUFFQSxTQUFBLE1BQUE7SUFDQSxPQUFBOztJQUVBLFNBQUEsS0FBQSxPQUFBO01BQ0EsT0FBQSxRQUFBLFNBQUE7Ozs7QUNiQSxDQUFBLFlBQUE7RUFDQTs7RUFFQTtLQUNBLE9BQUE7S0FDQSxPQUFBLFlBQUEsWUFBQTtNQUNBLE9BQUEsVUFBQSxNQUFBLEtBQUE7UUFDQSxJQUFBLE9BQUEsVUFBQSxlQUFBLE9BQUEsU0FBQSxhQUFBO1VBQ0EsT0FBQTs7O1FBR0EsSUFBQSxNQUFBO1FBQ0EsS0FBQSxJQUFBLElBQUEsS0FBQSxTQUFBLEdBQUEsS0FBQSxHQUFBLEtBQUE7VUFDQSxPQUFBLFdBQUEsS0FBQSxHQUFBOzs7UUFHQSxPQUFBOzs7Ozs7QUNoQkEsQ0FBQSxVQUFBO0VBQ0E7O0VBRUE7S0FDQSxPQUFBO0tBQ0EsT0FBQSxRQUFBOztFQUVBLEtBQUEsVUFBQTs7RUFFQSxTQUFBLE9BQUE7SUFDQSxPQUFBOztJQUVBLFNBQUEsS0FBQSxPQUFBO01BQ0EsT0FBQSxNQUFBLFlBQUEsTUFBQSxTQUFBLFNBQUEsU0FBQSxNQUFBLFNBQUEsVUFBQTs7OztBQ2JBLENBQUEsWUFBQTtFQUNBOztFQUVBO0tBQ0EsT0FBQTtLQUNBLE9BQUEsV0FBQTs7RUFFQSxRQUFBLFVBQUE7O0VBRUEsU0FBQSxVQUFBO0lBQ0EsT0FBQTs7SUFFQSxTQUFBLEtBQUEsTUFBQTtNQUNBLElBQUEsRUFBQSxnQkFBQSxPQUFBO1FBQ0EsT0FBQSxJQUFBLEtBQUE7OztNQUdBLElBQUE7O01BRUEsUUFBQSxLQUFBO1FBQ0EsS0FBQTtVQUNBLFVBQUE7VUFDQTtRQUNBLEtBQUE7VUFDQSxVQUFBO1VBQ0E7UUFDQSxLQUFBO1VBQ0EsVUFBQTtVQUNBO1FBQ0EsS0FBQTtVQUNBLFVBQUE7VUFDQTtRQUNBLEtBQUE7VUFDQSxVQUFBO1VBQ0E7UUFDQSxLQUFBO1VBQ0EsVUFBQTtVQUNBO1FBQ0EsS0FBQTtVQUNBLFVBQUE7VUFDQTs7OztNQUlBLE9BQUE7Ozs7QUM1Q0EsQ0FBQSxZQUFBO0VBQ0E7O0VBRUE7S0FDQSxPQUFBO0tBQ0EsV0FBQSwrQkFBQTs7RUFFQSw0QkFBQSxVQUFBLENBQUEscUJBQUE7OztFQUdBLFNBQUEsNEJBQUEsbUJBQUEsT0FBQTs7SUFFQSxJQUFBLEtBQUE7O0lBRUEsR0FBQSxRQUFBOztJQUVBLEdBQUEsUUFBQTs7OztJQUlBLFNBQUEsUUFBQTtNQUNBLGtCQUFBOzs7Ozs7QUNyQkEsQ0FBQSxZQUFBO0VBQ0E7O0VBRUE7S0FDQSxPQUFBO0tBQ0EsV0FBQSx5QkFBQTs7RUFFQSxzQkFBQSxVQUFBLENBQUEsYUFBQSxZQUFBLFVBQUEsZ0JBQUEsVUFBQSxnQkFBQSxpQkFBQSxXQUFBOzs7RUFHQSxTQUFBLHNCQUFBLFdBQUEsVUFBQSxRQUFBLGNBQUEsUUFBQSxjQUFBLGVBQUEsU0FBQSxhQUFBOztJQUVBLElBQUEsS0FBQTs7SUFFQSxJQUFBLENBQUEsU0FBQSxRQUFBO01BQ0EsYUFBQSxNQUFBO01BQ0EsT0FBQSxHQUFBOzs7SUFHQSxHQUFBLGNBQUE7SUFDQSxHQUFBLFdBQUE7OztJQUdBLEdBQUEsU0FBQTs7SUFFQSxHQUFBLFVBQUE7O0lBRUEsR0FBQSxPQUFBO01BQ0EsVUFBQTtNQUNBLFVBQUE7TUFDQSxPQUFBOzs7SUFHQSxHQUFBLFNBQUE7SUFDQSxHQUFBLGdCQUFBO0lBQ0EsR0FBQSxlQUFBO0lBQ0EsR0FBQSxjQUFBO0lBQ0EsR0FBQSxnQkFBQTs7OztJQUlBLFNBQUEsZ0JBQUE7TUFDQSxJQUFBLFFBQUEsUUFBQSxVQUFBLFVBQUEsQ0FBQSxJQUFBLEdBQUEsS0FBQSxXQUFBOztNQUVBLFVBQUEsS0FBQTtRQUNBLGFBQUE7UUFDQSxZQUFBO1FBQ0EsY0FBQTtRQUNBLE1BQUE7UUFDQSxTQUFBO1VBQ0EsT0FBQSxZQUFBO1lBQ0EsT0FBQTs7Ozs7TUFLQSxHQUFBLFNBQUEsUUFBQSxVQUFBLFFBQUEsVUFBQSxPQUFBO1FBQ0EsSUFBQSxNQUFBLFFBQUEsT0FBQSxPQUFBLE1BQUE7UUFDQSxPQUFBLE1BQUEsV0FBQSxPQUFBLE9BQUEsTUFBQTs7OztJQUlBLFNBQUEsU0FBQTtNQUNBLElBQUEsY0FBQSxxQkFBQSxhQUFBLFNBQUEsR0FBQSxNQUFBLFVBQUEsU0FBQSxRQUFBO1FBQ0E7OztNQUdBLEdBQUEsVUFBQTs7TUFFQSxhQUFBLElBQUEsR0FBQSxNQUFBLEtBQUEsWUFBQTtRQUNBLGFBQUEsUUFBQTs7UUFFQSxPQUFBLEdBQUE7U0FDQSxRQUFBLFlBQUE7Ozs7O0lBS0EsU0FBQSxnQkFBQTtNQUNBLElBQUEsQ0FBQSxHQUFBLEtBQUEsVUFBQTtRQUNBOzs7TUFHQSxJQUFBLFFBQUEsUUFBQSxVQUFBLEdBQUEsUUFBQSxDQUFBLElBQUEsR0FBQSxLQUFBLFdBQUE7O01BRUEsSUFBQSxDQUFBLE1BQUEsU0FBQTtRQUNBO1FBQ0E7OztNQUdBLFVBQUEsS0FBQTtRQUNBLGFBQUE7UUFDQSxZQUFBO1FBQ0EsY0FBQTtRQUNBLE1BQUE7UUFDQSxTQUFBO1VBQ0EsT0FBQSxZQUFBO1lBQ0EsT0FBQTs7OztTQUlBO1NBQ0EsS0FBQSxZQUFBO1VBQ0E7V0FDQSxZQUFBO1VBQ0EsR0FBQSxLQUFBLFdBQUE7Ozs7SUFJQSxTQUFBLFdBQUE7TUFDQSxjQUFBLGVBQUEsR0FBQSxRQUFBLEdBQUE7TUFDQSxhQUFBLFFBQUE7OztJQUdBLFNBQUEsYUFBQSxNQUFBO01BQ0EsSUFBQSxLQUFBLFVBQUE7UUFDQSx1QkFBQSxNQUFBO1FBQ0EsS0FBQSxtQkFBQTthQUNBO1FBQ0EsS0FBQSxtQkFBQTtRQUNBLHVCQUFBLE1BQUE7Ozs7SUFJQSxTQUFBLFlBQUEsTUFBQSxLQUFBO01BQ0EsSUFBQSxDQUFBLElBQUEsWUFBQSxLQUFBLFVBQUE7UUFDQSxLQUFBLFdBQUE7OztNQUdBLElBQUEsSUFBQSxVQUFBO1FBQ0EsS0FBQSxtQkFBQTtRQUNBLElBQUEsd0JBQUE7O1FBRUEsS0FBQSxJQUFBLElBQUEsR0FBQSxVQUFBLFVBQUEsS0FBQSxNQUFBLEtBQUEsS0FBQTtVQUNBLElBQUEsUUFBQSxTQUFBLENBQUEsUUFBQSxVQUFBO1lBQ0Esd0JBQUE7WUFDQTs7OztRQUlBLElBQUEsdUJBQUE7VUFDQSxLQUFBLFdBQUE7O2FBRUE7O1FBRUEsSUFBQSxrQkFBQTtRQUNBLEtBQUEsSUFBQSxJQUFBLEdBQUEsT0FBQSxPQUFBLEtBQUEsTUFBQSxLQUFBLEtBQUE7VUFDQSxJQUFBLEtBQUEsVUFBQTtZQUNBLGtCQUFBO1lBQ0E7Ozs7UUFJQSxLQUFBLG1CQUFBOzs7O0lBSUEsU0FBQSx1QkFBQSxNQUFBLE9BQUE7TUFDQSxLQUFBLElBQUEsSUFBQSxHQUFBLE1BQUEsTUFBQSxLQUFBLE1BQUEsS0FBQSxLQUFBO1FBQ0EsSUFBQSxJQUFBLFNBQUEsQ0FBQSxJQUFBLE9BQUE7VUFDQSxJQUFBLFdBQUE7Ozs7Ozs7QUNoS0EsQ0FBQSxZQUFBO0VBQ0E7O0VBRUE7S0FDQSxPQUFBO0tBQ0EsV0FBQSx1QkFBQTs7RUFFQSxvQkFBQSxVQUFBLENBQUEsU0FBQSxZQUFBLFVBQUEsZ0JBQUEsVUFBQSxnQkFBQTs7O0VBR0EsU0FBQSxvQkFBQSxPQUFBLFVBQUEsUUFBQSxjQUFBLFFBQUEsY0FBQSxlQUFBOztJQUVBLElBQUEsS0FBQTs7SUFFQSxHQUFBLE9BQUEsUUFBQSxLQUFBO0lBQ0EsR0FBQSxXQUFBO0lBQ0EsR0FBQSxTQUFBOztJQUVBLEdBQUEsU0FBQTtJQUNBLEdBQUEsZ0JBQUE7Ozs7SUFJQSxTQUFBLFNBQUE7TUFDQSxHQUFBLGNBQUEscUJBQUEsYUFBQSxTQUFBLEdBQUEsT0FBQSxRQUFBO1FBQ0E7OztNQUdBLGFBQUEsT0FBQSxHQUFBLEtBQUEsS0FBQSxHQUFBLE1BQUEsS0FBQSxZQUFBO1FBQ0EsYUFBQSxRQUFBOztRQUVBLE9BQUEsR0FBQTs7OztJQUlBLFNBQUEsZ0JBQUE7TUFDQSxjQUFBLGVBQUEsR0FBQSxRQUFBLEdBQUE7Ozs7Ozs7QUNwQ0EsQ0FBQSxZQUFBO0VBQ0E7O0VBRUE7S0FDQSxPQUFBO0tBQ0EsV0FBQSx1QkFBQTs7RUFFQSxvQkFBQSxVQUFBLENBQUEscUJBQUE7OztFQUdBLFNBQUEsb0JBQUEsbUJBQUEsT0FBQTs7SUFFQSxJQUFBLEtBQUE7O0lBRUEsR0FBQSxRQUFBOztJQUVBLEdBQUEsVUFBQTtJQUNBLEdBQUEsUUFBQTs7OztJQUlBLFNBQUEsVUFBQTtNQUNBLGtCQUFBOzs7SUFHQSxTQUFBLFFBQUE7TUFDQSxrQkFBQTs7Ozs7QUMxQkEsQ0FBQSxZQUFBO0VBQ0E7O0VBRUE7S0FDQSxPQUFBO0tBQ0EsV0FBQSxvQkFBQTs7RUFFQSxpQkFBQSxVQUFBLENBQUEsVUFBQSxnQkFBQTs7O0VBR0EsU0FBQSxpQkFBQSxRQUFBLGNBQUEsU0FBQTs7SUFFQSxJQUFBLEtBQUE7O0lBRUEsR0FBQSxTQUFBOztJQUVBLEdBQUEsU0FBQTs7OztJQUlBLFNBQUEsT0FBQSxJQUFBO01BQ0EsYUFBQSxPQUFBLElBQUEsS0FBQSxZQUFBOztRQUVBLGFBQUEsTUFBQSxLQUFBLFVBQUEsUUFBQTtVQUNBLEdBQUEsU0FBQTs7VUFFQSxRQUFBLFFBQUE7Ozs7Ozs7O0FDMUJBLENBQUEsWUFBQTtFQUNBOztFQUVBO0tBQ0EsT0FBQTtLQUNBLFdBQUEsdUJBQUE7O0VBRUEsb0JBQUEsVUFBQSxDQUFBOzs7RUFHQSxTQUFBLG9CQUFBLE9BQUE7O0lBRUEsSUFBQSxLQUFBOztJQUVBLEdBQUEsUUFBQTs7OztBQ2RBLENBQUEsWUFBQTtFQUNBOztFQUVBO0tBQ0EsT0FBQTtLQUNBLFdBQUEscUJBQUE7O0VBRUEsa0JBQUEsVUFBQSxDQUFBLFdBQUEsa0JBQUEsVUFBQSxXQUFBLFdBQUEsaUJBQUEsYUFBQTs7O0VBR0EsU0FBQSxrQkFBQSxTQUFBLGdCQUFBLFFBQUEsU0FBQSxTQUFBLGVBQUEsV0FBQSxTQUFBOztJQUVBLElBQUEsS0FBQTs7SUFFQSxHQUFBLFVBQUEsUUFBQSxLQUFBO0lBQ0EsR0FBQSxPQUFBLFFBQUEsS0FBQSxHQUFBO0lBQ0EsR0FBQSxVQUFBO0lBQ0EsR0FBQSxnQkFBQTtJQUNBLEdBQUEsWUFBQTtJQUNBLEdBQUEsVUFBQTs7SUFFQSxHQUFBLFNBQUE7SUFDQSxHQUFBLFFBQUE7Ozs7SUFJQSxTQUFBLFNBQUE7TUFDQSxJQUFBLE9BQUEsUUFBQSxLQUFBLEdBQUE7TUFDQSxLQUFBLGFBQUEsS0FBQSxRQUFBOztNQUVBLEdBQUEsVUFBQTs7TUFFQSxlQUFBLE9BQUEsTUFBQSxLQUFBLFlBQUE7UUFDQSxHQUFBLFVBQUEsR0FBQTs7UUFFQSxRQUFBLFFBQUE7O1FBRUEsT0FBQSxHQUFBO1NBQ0EsUUFBQSxZQUFBO1FBQ0EsR0FBQSxVQUFBOzs7O0lBSUEsU0FBQSxRQUFBO01BQ0EsR0FBQSxPQUFBLFFBQUEsS0FBQSxHQUFBOzs7OztBQzVDQSxDQUFBLFdBQUE7RUFDQTs7RUFFQTtLQUNBLE9BQUE7S0FDQSxRQUFBLGdCQUFBOztFQUVBLGFBQUEsVUFBQSxDQUFBOzs7RUFHQSxTQUFBLGNBQUEsU0FBQTs7SUFFQSxJQUFBLEtBQUE7O0lBRUEsR0FBQSxNQUFBO0lBQ0EsR0FBQSxVQUFBO0lBQ0EsR0FBQSxRQUFBO0lBQ0EsR0FBQSxPQUFBO0lBQ0EsR0FBQSxvQkFBQTs7SUFFQSxTQUFBLElBQUEsTUFBQTtNQUNBLFFBQUEsSUFBQTs7O0lBR0EsU0FBQSxTQUFBLE1BQUE7TUFDQSxPQUFBLFlBQUEsTUFBQTtNQUNBLFFBQUEsUUFBQTs7O0lBR0EsU0FBQSxLQUFBLE1BQUE7TUFDQSxZQUFBLE1BQUE7TUFDQSxRQUFBLEtBQUE7OztJQUdBLFNBQUEsTUFBQSxNQUFBO01BQ0EsWUFBQSxNQUFBO01BQ0EsUUFBQSxNQUFBOzs7SUFHQSxTQUFBLGtCQUFBLFVBQUEsT0FBQTtNQUNBLElBQUEsSUFBQSxPQUFBLFVBQUE7UUFDQSxJQUFBLFNBQUEsZUFBQSxTQUFBLFNBQUEsZ0JBQUEsUUFBQTtVQUNBLElBQUEsSUFBQSxLQUFBLFNBQUEsTUFBQTtZQUNBLElBQUEsU0FBQSxLQUFBLGVBQUEsSUFBQTtjQUNBLE1BQUE7Z0JBQ0EsT0FBQSxRQUFBLFFBQUE7Z0JBQ0EsTUFBQSxTQUFBLEtBQUE7Ozs7ZUFJQTtVQUNBLE1BQUE7WUFDQSxPQUFBO1lBQ0EsTUFBQSxTQUFBOzs7Ozs7SUFNQSxTQUFBLFlBQUEsTUFBQSxPQUFBO01BQ0EsSUFBQSxFQUFBLGdCQUFBLFNBQUE7UUFDQSxPQUFBO1VBQ0EsTUFBQTs7OztNQUlBLEtBQUEsUUFBQSxLQUFBLFFBQUEsS0FBQSxRQUFBOztNQUVBLE9BQUE7Ozs7Ozs7QUNwRUEsQ0FBQSxZQUFBO0VBQ0E7O0VBRUE7S0FDQSxPQUFBO0tBQ0EsUUFBQSxrQkFBQTs7RUFFQSxlQUFBLFVBQUEsQ0FBQSxTQUFBOzs7RUFHQSxTQUFBLGVBQUEsT0FBQSxlQUFBOztJQUVBLElBQUEsS0FBQTs7SUFFQSxHQUFBLE9BQUE7SUFDQSxHQUFBLFlBQUE7SUFDQSxHQUFBLGNBQUE7SUFDQSxHQUFBLGlCQUFBOzs7O0lBSUEsU0FBQSxLQUFBLE1BQUE7TUFDQSxPQUFBLE1BQUEsS0FBQSxnQkFBQSxjQUFBLHlCQUFBLE9BQUEsS0FBQSxjQUFBOzs7SUFHQSxTQUFBLFVBQUEsUUFBQTtNQUNBLE9BQUEsTUFBQSxLQUFBLDBCQUFBLENBQUEsUUFBQSxTQUFBLEtBQUEsY0FBQSxlQUFBLEtBQUEsVUFBQSxNQUFBO1FBQ0EsT0FBQTs7OztJQUlBLFNBQUEsWUFBQSxRQUFBO01BQ0EsT0FBQSxNQUFBLElBQUEsMEJBQUEsUUFBQSxLQUFBLGNBQUE7OztJQUdBLFNBQUEsaUJBQUE7TUFDQSxPQUFBLE1BQUEsSUFBQSx3QkFBQSxLQUFBLGNBQUE7Ozs7O0FDcENBLENBQUEsWUFBQTtFQUNBOztFQUVBO0tBQ0EsT0FBQTtLQUNBLFFBQUEsZ0JBQUE7O0VBRUEsYUFBQSxVQUFBLENBQUEsYUFBQSxTQUFBOzs7RUFHQSxTQUFBLGFBQUEsV0FBQSxPQUFBLGVBQUE7O0lBRUEsSUFBQSxLQUFBOztJQUVBLElBQUEsUUFBQSxVQUFBLDBCQUFBLENBQUEsU0FBQSxRQUFBO01BQ0EsUUFBQSxDQUFBLFFBQUE7OztJQUdBLEdBQUEsTUFBQTtJQUNBLEdBQUEsTUFBQTtJQUNBLEdBQUEsUUFBQTtJQUNBLEdBQUEsU0FBQTtJQUNBLEdBQUEsU0FBQTtJQUNBLEdBQUEsY0FBQTtJQUNBLEdBQUEsZ0JBQUE7Ozs7SUFJQSxTQUFBLGdCQUFBO01BQ0EsT0FBQSxNQUFBLElBQUEsd0JBQUEsS0FBQSxjQUFBOzs7SUFHQSxTQUFBLGNBQUE7TUFDQSxPQUFBLE1BQUEsSUFBQSxxQkFBQSxLQUFBLGNBQUE7OztJQUdBLFNBQUEsTUFBQTtNQUNBLE9BQUEsTUFBQTtXQUNBO1dBQ0EsS0FBQSxVQUFBLFVBQUE7WUFDQSxJQUFBLElBQUEsSUFBQSxHQUFBLFFBQUEsUUFBQSxTQUFBLEtBQUEsS0FBQTtjQUNBLE1BQUEsV0FBQSxPQUFBLE1BQUEsVUFBQTs7O1lBR0EsT0FBQTs7OztJQUlBLFNBQUEsSUFBQSxJQUFBO01BQ0EsT0FBQSxNQUFBLElBQUEsQ0FBQSxTQUFBO1NBQ0E7U0FDQSxLQUFBLFVBQUEsT0FBQTtVQUNBLE1BQUEsV0FBQSxPQUFBLE1BQUEsVUFBQTs7VUFFQSxPQUFBOzs7O0lBSUEsU0FBQSxNQUFBLE1BQUE7TUFDQSxPQUFBLE1BQUEsS0FBQSxjQUFBLHVCQUFBLE1BQUEsQ0FBQSxhQUFBLE9BQUE7OztJQUdBLFNBQUEsT0FBQSxNQUFBO01BQ0EsT0FBQSxNQUFBLE9BQUEsY0FBQSx1QkFBQSxNQUFBLENBQUEsYUFBQSxPQUFBOzs7SUFHQSxTQUFBLE9BQUEsSUFBQTtNQUNBLE9BQUEsTUFBQSxPQUFBLENBQUEsU0FBQSxLQUFBOzs7OztBQ25FQSxDQUFBLFdBQUE7RUFDQTs7RUFFQTtLQUNBLE9BQUE7S0FDQSxRQUFBLGdCQUFBOztFQUVBLGFBQUEsVUFBQSxDQUFBLFNBQUE7OztFQUdBLFNBQUEsYUFBQSxPQUFBLGVBQUE7O0lBRUEsSUFBQSxLQUFBOztJQUVBLEdBQUEsTUFBQTs7OztJQUlBLFNBQUEsTUFBQTtNQUNBLE9BQUEsTUFBQSxJQUFBO1NBQ0EsS0FBQSxjQUFBO1NBQ0EsS0FBQSxVQUFBLFFBQUE7VUFDQSxJQUFBLElBQUEsSUFBQSxHQUFBLFFBQUEsUUFBQSxPQUFBLEtBQUEsS0FBQTtZQUNBLE1BQUEsYUFBQSxPQUFBLE1BQUE7WUFDQSxNQUFBLFdBQUEsT0FBQSxNQUFBOzs7VUFHQSxPQUFBOzs7Ozs7QUMzQkEsQ0FBQSxZQUFBO0VBQ0E7OztFQUVBO0tBQ0EsT0FBQTtLQUNBLFFBQUEsaUJBQUE7OztFQUdBLFNBQUEsY0FBQSxTQUFBLGNBQUEsT0FBQTs7SUFFQSxJQUFBLEtBQUE7O0lBRUEsR0FBQSxnQkFBQTtJQUNBLEdBQUEsaUJBQUE7SUFDQSxHQUFBLHVCQUFBO0lBQ0EsR0FBQSxXQUFBO0lBQ0EsR0FBQSxnQkFBQTtJQUNBLEdBQUEsa0JBQUE7SUFDQSxHQUFBLDJCQUFBO0lBQ0EsR0FBQSx5QkFBQTs7OztJQUlBLFNBQUEsY0FBQTtJQUNBO01BQ0EsSUFBQSxVQUFBOztNQUVBLEtBQUEsSUFBQSxJQUFBLEdBQUEsUUFBQSxRQUFBLE9BQUEsS0FBQSxLQUFBO1FBQ0EsSUFBQSxnQkFBQTtRQUNBLEtBQUEsSUFBQSxJQUFBLEdBQUEsT0FBQSxPQUFBLE1BQUEsTUFBQSxLQUFBLEtBQUE7VUFDQSxLQUFBLElBQUEsSUFBQSxHQUFBLE1BQUEsTUFBQSxLQUFBLE1BQUEsS0FBQSxLQUFBO1lBQ0EsSUFBQSxJQUFBLFVBQUE7Y0FDQSxJQUFBLE9BQUEsSUFBQSxLQUFBLElBQUE7Y0FDQSxHQUFBLEdBQUEsS0FBQSxnQkFBQSxLQUFBLGFBQUE7Z0JBQ0EsS0FBQSxXQUFBLEtBQUEsZUFBQSxLQUFBOzs7Y0FHQSxjQUFBLEtBQUE7Ozs7O1FBS0EsUUFBQSxLQUFBO1VBQ0EsS0FBQSxNQUFBO1VBQ0EsVUFBQSxNQUFBO1VBQ0EsVUFBQSxNQUFBO1VBQ0EsZUFBQTtVQUNBLFVBQUEsTUFBQTs7OztNQUlBLE9BQUE7OztJQUdBLFNBQUEsU0FBQSxLQUFBLFFBQUEsUUFBQTtNQUNBLEVBQUEsVUFBQTtRQUNBLFFBQUE7UUFDQSxRQUFBO1NBQ0EsT0FBQSxRQUFBLFNBQUEsUUFBQTs7O0lBR0EsU0FBQSxjQUFBLE1BQUE7TUFDQSxPQUFBLEtBQUE7OztJQUdBLFNBQUEscUJBQUEsUUFBQTtNQUNBLElBQUEsT0FBQSxRQUFBO1FBQ0EsS0FBQSxJQUFBLElBQUEsR0FBQSxRQUFBLFFBQUEsT0FBQSxLQUFBLEtBQUE7VUFDQSxhQUFBLE1BQUEsTUFBQTs7OztNQUlBLE9BQUE7OztJQUdBLFNBQUEsVUFBQSxPQUFBLE1BQUE7TUFDQSxLQUFBLFNBQUE7TUFDQSxLQUFBLFdBQUE7O01BRUEsT0FBQSxRQUFBLFVBQUEsT0FBQSxVQUFBLEtBQUE7UUFDQSxPQUFBLE9BQUEsTUFBQSxLQUFBLE9BQUEsSUFBQSxXQUFBO1NBQ0E7OztJQUdBLFNBQUEsVUFBQSxRQUFBLFNBQUE7TUFDQSxJQUFBLElBQUEsSUFBQSxHQUFBLFFBQUEsUUFBQSxPQUFBLEtBQUEsS0FBQTtRQUNBLEdBQUEsTUFBQSxPQUFBLFNBQUE7VUFDQSxPQUFBLFFBQUEsS0FBQTs7Ozs7SUFLQSxTQUFBLGVBQUEsUUFBQSxNQUFBOzs7Ozs7Ozs7Ozs7Ozs7TUFlQSxJQUFBLFFBQUEsVUFBQSxRQUFBLEtBQUE7O01BRUEsS0FBQSxRQUFBO01BQ0EsS0FBQSxRQUFBOztNQUVBLElBQUEsWUFBQSxRQUFBLEtBQUEsTUFBQTs7O01BR0EsSUFBQSxVQUFBLGNBQUEsR0FBQTtRQUNBLFVBQUEsSUFBQSxHQUFBO2FBQ0EsSUFBQSxVQUFBLGNBQUEsR0FBQTtRQUNBLFVBQUEsSUFBQSxHQUFBOzs7O01BSUEsS0FBQSxJQUFBLFlBQUEsV0FBQSxlQUFBLElBQUEsS0FBQSxhQUFBLGFBQUEsTUFBQSxXQUFBLFlBQUEsSUFBQSxLQUFBLENBQUEsSUFBQSxLQUFBLGVBQUEsUUFBQSxDQUFBLElBQUEsS0FBQSxlQUFBLFlBQUEsS0FBQSxZQUFBLElBQUEsS0FBQSxZQUFBOztRQUVBLElBQUEsYUFBQSxPQUFBLFdBQUEsWUFBQTs7O1FBR0EsZUFBQSxJQUFBLEtBQUE7UUFDQSxhQUFBLFFBQUEsYUFBQSxZQUFBOzs7UUFHQSxJQUFBLGVBQUEsSUFBQSxLQUFBO1FBQ0EsYUFBQSxRQUFBLGFBQUEsWUFBQTtRQUNBLElBQUEsWUFBQSxJQUFBLEtBQUE7O1FBRUEsSUFBQSxPQUFBO1VBQ0EsUUFBQSxJQUFBLEtBQUE7VUFDQSxRQUFBLElBQUEsS0FBQTtVQUNBLFVBQUE7VUFDQSxPQUFBO1VBQ0EsT0FBQTtVQUNBLE9BQUE7VUFDQSxrQkFBQTs7OztRQUlBLElBQUEsWUFBQSxNQUFBLFVBQUE7VUFDQSxZQUFBLE1BQUE7OztRQUdBLElBQUEsZUFBQSxXQUFBO1VBQ0EsS0FBQSxJQUFBLFFBQUEsSUFBQSxLQUFBLGVBQUEsUUFBQSxXQUFBLE1BQUEsUUFBQSxNQUFBLFlBQUEsSUFBQTtZQUNBLEtBQUEsTUFBQSxLQUFBO2NBQ0EsTUFBQSxRQUFBLEtBQUE7Y0FDQSxPQUFBO2NBQ0EsVUFBQTtjQUNBLE9BQUE7Ozs7O1FBS0EsS0FBQSxJQUFBLE9BQUEsSUFBQSxLQUFBLFlBQUEsUUFBQSxXQUFBLEtBQUEsUUFBQSxLQUFBLFlBQUEsSUFBQTs7VUFFQSxJQUFBLFNBQUEsUUFBQSxVQUFBLE1BQUEsa0JBQUEsVUFBQSxlQUFBO2NBQ0EsZ0JBQUEsT0FBQSxjQUFBLE1BQUE7Y0FDQSxPQUFBLGNBQUEsbUJBQUEsS0FBQTs7WUFFQSxRQUFBLENBQUEsT0FBQSxTQUFBLEtBQUEsUUFBQTs7VUFFQSxLQUFBLE1BQUEsS0FBQTtZQUNBLE1BQUEsUUFBQSxLQUFBO1lBQ0EsT0FBQTtZQUNBLFVBQUE7WUFDQSxPQUFBLFFBQUEsVUFBQSxLQUFBLE1BQUEsWUFBQSxRQUFBOzs7O1FBSUEsSUFBQSxlQUFBLFdBQUE7VUFDQSxLQUFBLE9BQUEsSUFBQSxLQUFBLFlBQUEsS0FBQSxRQUFBLEtBQUEsWUFBQSxJQUFBLFFBQUEsY0FBQSxLQUFBLFFBQUEsS0FBQSxZQUFBLElBQUE7WUFDQSxLQUFBLE1BQUEsS0FBQTtjQUNBLE1BQUEsUUFBQSxLQUFBO2NBQ0EsT0FBQTtjQUNBLFVBQUE7Y0FDQSxPQUFBOzs7OztRQUtBLElBQUEsSUFBQSxJQUFBLEdBQUEsWUFBQSxZQUFBLEtBQUEsTUFBQSxLQUFBLEtBQUE7O1VBRUEsR0FBQSxVQUFBLFNBQUEsQ0FBQSxVQUFBLE9BQUE7WUFDQSxLQUFBLFFBQUE7OztVQUdBLEdBQUEsVUFBQSxTQUFBLFVBQUEsT0FBQTtZQUNBLEtBQUEsUUFBQTs7OztRQUlBLEtBQUEsTUFBQSxLQUFBOzs7O0lBSUEsU0FBQSxnQkFBQSxVQUFBO01BQ0EsSUFBQSxnQkFBQSxNQUFBLElBQUE7O01BRUEsT0FBQSxRQUFBLFFBQUEsVUFBQSxVQUFBLFNBQUE7UUFDQSxRQUFBLFlBQUEsUUFBQSxrQkFBQSxNQUFBLFFBQUEsZ0JBQUEsT0FBQTs7UUFFQSxJQUFBLGVBQUE7VUFDQSxJQUFBLGVBQUEsUUFBQSxVQUFBLGVBQUEsQ0FBQSxJQUFBLFFBQUEsS0FBQTs7VUFFQSxJQUFBLGNBQUE7WUFDQSxRQUFBLGtCQUFBLGFBQUE7WUFDQSxRQUFBLG1CQUFBLGFBQUE7Ozs7UUFJQSxRQUFBLFFBQUE7O1FBRUEsSUFBQSxVQUFBLFFBQUEsU0FBQSxPQUFBLFVBQUEsU0FBQTtVQUNBLE9BQUEsUUFBQSxtQkFBQSxRQUFBLG9CQUFBLFFBQUE7V0FDQTs7UUFFQSxJQUFBLFdBQUEsUUFBQSxrQkFBQTtVQUNBLFFBQUEsUUFBQSxRQUFBLFFBQUEsUUFBQTs7Ozs7SUFLQSxTQUFBLHlCQUFBLE9BQUE7TUFDQSxJQUFBLE9BQUEsUUFBQSxLQUFBOztNQUVBLEtBQUEsV0FBQTs7TUFFQSxRQUFBLFFBQUEsS0FBQSxVQUFBLFVBQUEsU0FBQTtRQUNBLElBQUEsUUFBQSxtQkFBQSxRQUFBLGtCQUFBO1VBQ0EsS0FBQSxTQUFBLEtBQUE7WUFDQSxJQUFBLFFBQUE7WUFDQSxPQUFBLFFBQUE7Ozs7O01BS0EsT0FBQSxLQUFBOztNQUVBLE9BQUE7OztJQUdBLFNBQUEsdUJBQUEsUUFBQSxZQUFBLFdBQUE7TUFDQSxJQUFBLGNBQUEsWUFBQSxRQUFBLEtBQUEsVUFBQTs7TUFFQSxXQUFBLFFBQUEsVUFBQSxVQUFBO1FBQ0EsWUFBQSxZQUFBLE9BQUEsWUFBQSxXQUFBLE9BQUE7OztNQUdBLE9BQUE7Ozs7O0FDaFFBLENBQUEsWUFBQTtFQUNBOztFQUVBO0tBQ0EsT0FBQTtLQUNBLFFBQUEsZ0JBQUE7O0VBRUEsYUFBQSxVQUFBLENBQUEsV0FBQSxNQUFBLFNBQUEsZ0JBQUEsZ0JBQUEsU0FBQTs7O0VBR0EsU0FBQSxhQUFBLFNBQUEsSUFBQSxPQUFBLGNBQUEsY0FBQSxPQUFBLGVBQUE7O0lBRUEsSUFBQSxLQUFBOztJQUVBLEdBQUEsV0FBQSxHQUFBLElBQUEsQ0FBQSxhQUFBLE9BQUEsYUFBQSxRQUFBLEtBQUEsVUFBQSxNQUFBO01BQ0EsR0FBQSxXQUFBLEtBQUE7TUFDQSxHQUFBLFNBQUEsS0FBQTs7O0lBR0EsR0FBQSxhQUFBLE1BQUEsSUFBQTs7SUFFQSxHQUFBLEdBQUEsWUFBQTtNQUNBLE1BQUEsT0FBQTtNQUNBLE1BQUEsT0FBQTtNQUNBLE1BQUEsT0FBQTs7O0lBR0EsR0FBQSxTQUFBLE1BQUEsSUFBQTtJQUNBLEdBQUEsUUFBQSxNQUFBLElBQUE7O0lBRUEsSUFBQSxDQUFBLEdBQUEsUUFBQTtNQUNBLEdBQUEsU0FBQTtNQUNBLEdBQUEsUUFBQTs7O0lBR0EsR0FBQSxXQUFBOztJQUVBLEdBQUEsUUFBQTtJQUNBLEdBQUEsTUFBQTtJQUNBLEdBQUEsTUFBQTtJQUNBLEdBQUEsTUFBQTtJQUNBLEdBQUEsU0FBQTtJQUNBLEdBQUEsU0FBQTtJQUNBLEdBQUEsWUFBQTtJQUNBLEdBQUEsUUFBQTtJQUNBLEdBQUEsV0FBQTtJQUNBLEdBQUEsZUFBQTs7OztJQUlBLFNBQUEsV0FBQTtNQUNBLE9BQUEsTUFBQSxJQUFBLGlCQUFBLEtBQUEsY0FBQSxlQUFBLEtBQUEsY0FBQTs7O0lBR0EsU0FBQSxRQUFBO01BQ0EsT0FBQSxHQUFBLE9BQUE7OztJQUdBLFNBQUEsUUFBQTtNQUNBLElBQUEsU0FBQSxRQUFBLEtBQUEsR0FBQTs7TUFFQSxJQUFBLGVBQUEsR0FBQTs7TUFFQSxHQUFBLFNBQUEsS0FBQSxZQUFBO1FBQ0EsTUFBQSxPQUFBO1FBQ0EsTUFBQSxPQUFBO1FBQ0EsTUFBQSxPQUFBOztRQUVBLGFBQUEsUUFBQTs7O01BR0EsT0FBQSxhQUFBOzs7SUFHQSxTQUFBLGFBQUEsVUFBQTtNQUNBLElBQUEsZUFBQSxHQUFBOztNQUVBLEdBQUEsU0FBQSxLQUFBLFlBQUE7UUFDQSxNQUFBLElBQUEsWUFBQTs7UUFFQSxhQUFBLFFBQUE7OztNQUdBLE9BQUEsYUFBQTs7O0lBR0EsU0FBQSxVQUFBLE9BQUE7TUFDQSxJQUFBLE1BQUE7O01BRUEsS0FBQSxJQUFBLElBQUEsR0FBQSxPQUFBLE9BQUEsTUFBQSxNQUFBLEtBQUEsS0FBQTtRQUNBLElBQUEsS0FBQSxVQUFBO1VBQ0EsT0FBQSxNQUFBLE1BQUE7ZUFDQTtVQUNBLEtBQUEsSUFBQSxJQUFBLEdBQUEsVUFBQSxVQUFBLEtBQUEsTUFBQSxLQUFBLEtBQUE7WUFDQSxHQUFBLFFBQUEsVUFBQTtjQUNBLE9BQUEsTUFBQSxNQUFBOzs7Ozs7TUFNQSxRQUFBLFFBQUEsTUFBQSxVQUFBLFVBQUEsU0FBQTtRQUNBLE9BQUEsUUFBQSxRQUFBLFFBQUE7OztNQUdBLE9BQUE7OztJQUdBLFNBQUEsU0FBQSxPQUFBLFVBQUEsUUFBQTtNQUNBLElBQUEsU0FBQTs7TUFFQSxJQUFBLFFBQUEsUUFBQSxVQUFBLFVBQUEsQ0FBQSxJQUFBLE1BQUE7TUFDQSxJQUFBLEVBQUEsTUFBQSxZQUFBLE1BQUEsU0FBQTtRQUNBLE9BQUEsS0FBQTtVQUNBLE9BQUE7VUFDQSxTQUFBOzs7O01BSUEsSUFBQSxRQUFBLFFBQUEsVUFBQSxRQUFBLENBQUEsSUFBQSxNQUFBO01BQ0EsSUFBQSxFQUFBLE1BQUEsWUFBQSxNQUFBLFNBQUE7UUFDQSxPQUFBLEtBQUE7VUFDQSxPQUFBO1VBQ0EsU0FBQTs7Ozs7TUFLQSxJQUFBLFdBQUE7TUFDQSxLQUFBLElBQUEsSUFBQSxHQUFBLE1BQUEsTUFBQSxVQUFBLE9BQUEsTUFBQSxNQUFBLEtBQUEsS0FBQTtRQUNBLEtBQUEsSUFBQSxJQUFBLEdBQUEsTUFBQSxNQUFBLEtBQUEsTUFBQSxLQUFBLEtBQUE7VUFDQSxJQUFBLElBQUEsVUFBQTtZQUNBLFdBQUE7WUFDQTs7OztRQUlBLElBQUEsVUFBQTtVQUNBOzs7O01BSUEsSUFBQSxDQUFBLFVBQUE7UUFDQSxPQUFBLEtBQUE7VUFDQSxPQUFBO1VBQ0EsU0FBQTs7OztNQUlBLE9BQUEsT0FBQTs7O0lBR0EsU0FBQSxNQUFBO01BQ0EsSUFBQSxTQUFBLFFBQUEsS0FBQSxHQUFBOztNQUVBLElBQUEsZUFBQSxHQUFBOztNQUVBLEdBQUEsU0FBQSxLQUFBLFlBQUE7UUFDQSxLQUFBLElBQUEsSUFBQSxHQUFBLFFBQUEsUUFBQSxPQUFBLEtBQUEsS0FBQTtVQUNBLFNBQUE7VUFDQSxTQUFBOzs7UUFHQSxhQUFBLFFBQUE7OztNQUdBLE9BQUEsYUFBQTs7O0lBR0EsU0FBQSxJQUFBLE9BQUE7TUFDQSxJQUFBLGVBQUEsR0FBQTs7TUFFQSxHQUFBLFNBQUEsS0FBQSxZQUFBO1FBQ0EsTUFBQSxNQUFBLEdBQUE7UUFDQSxHQUFBLE9BQUEsS0FBQTtRQUNBOztRQUVBLGFBQUE7VUFDQSxRQUFBLEtBQUE7Ozs7O01BS0EsT0FBQSxhQUFBOzs7SUFHQSxTQUFBLElBQUEsSUFBQTtNQUNBLElBQUEsZUFBQSxHQUFBOztNQUVBLEdBQUEsU0FBQSxLQUFBLFlBQUE7UUFDQSxJQUFBLFFBQUEsUUFBQSxLQUFBLFFBQUEsVUFBQSxHQUFBLFFBQUEsQ0FBQSxLQUFBLEtBQUE7O1FBRUEsU0FBQTtRQUNBLFNBQUE7O1FBRUEsYUFBQSxRQUFBOzs7O01BSUEsT0FBQSxhQUFBOzs7SUFHQSxTQUFBLE9BQUEsSUFBQSxPQUFBO01BQ0EsSUFBQSxlQUFBLEdBQUE7O01BRUEsSUFBQSxTQUFBLFFBQUEsVUFBQSxHQUFBLFFBQUEsQ0FBQSxLQUFBLEtBQUE7O01BRUEsT0FBQSxXQUFBLE1BQUE7TUFDQSxPQUFBLFFBQUEsTUFBQTtNQUNBLE9BQUEsV0FBQSxNQUFBO01BQ0EsT0FBQSxRQUFBLE1BQUE7TUFDQSxPQUFBLFFBQUEsTUFBQTs7TUFFQTs7TUFFQSxhQUFBLFFBQUE7O01BRUEsT0FBQSxhQUFBOzs7SUFHQSxTQUFBLE9BQUEsSUFBQTtNQUNBLElBQUEsZUFBQSxHQUFBOztNQUVBLElBQUEsUUFBQSxRQUFBLFVBQUEsR0FBQSxRQUFBLENBQUEsS0FBQSxLQUFBOztNQUVBLEdBQUEsT0FBQSxPQUFBLEdBQUEsT0FBQSxRQUFBLFFBQUE7O01BRUE7O01BRUEsYUFBQSxRQUFBOztNQUVBLE9BQUEsYUFBQTs7O0lBR0EsU0FBQSxhQUFBO01BQ0EsTUFBQSxJQUFBLFVBQUEsR0FBQTtNQUNBLE1BQUEsSUFBQSxTQUFBLEdBQUE7OztJQUdBLFNBQUEsU0FBQSxPQUFBO01BQ0EsTUFBQSxRQUFBLFFBQUEsVUFBQSxHQUFBLFVBQUEsQ0FBQSxJQUFBLE1BQUEsV0FBQTs7O0lBR0EsU0FBQSxTQUFBLE9BQUE7TUFDQSxNQUFBLFFBQUEsUUFBQSxVQUFBLEdBQUEsUUFBQSxDQUFBLElBQUEsTUFBQSxXQUFBOzs7Ozs7QUNwUEEsQ0FBQSxZQUFBO0VBQ0E7O0VBRUE7S0FDQSxPQUFBO0tBQ0EsUUFBQSxrQkFBQTs7RUFFQSxRQUFBLFVBQUEsQ0FBQSxTQUFBOzs7RUFHQSxTQUFBLFFBQUEsT0FBQSxlQUFBOztJQUVBLElBQUEsS0FBQTs7SUFFQSxHQUFBLE1BQUE7SUFDQSxHQUFBLFNBQUE7SUFDQSxHQUFBLFlBQUE7Ozs7SUFJQSxTQUFBLE1BQUE7TUFDQSxPQUFBLE1BQUEsSUFBQTtTQUNBLEtBQUEsY0FBQTtTQUNBLEtBQUEsVUFBQSxNQUFBO1VBQ0EsS0FBQSxRQUFBLEtBQUEsS0FBQTtVQUNBLE9BQUE7Ozs7SUFJQSxTQUFBLE9BQUEsTUFBQTtNQUNBLE9BQUEsTUFBQSxJQUFBLGdCQUFBO1NBQ0EsS0FBQSxjQUFBO1NBQ0EsS0FBQSxVQUFBLE1BQUE7VUFDQSxLQUFBLFFBQUEsS0FBQSxLQUFBO1VBQ0EsT0FBQTs7OztJQUlBLFNBQUEsWUFBQTtNQUNBLE9BQUEsTUFBQSxJQUFBLGtCQUFBLEtBQUEsY0FBQTs7Ozs7QUN2Q0EsQ0FBQSxZQUFBO0VBQ0E7O0VBRUE7S0FDQSxPQUFBO0tBQ0EsVUFBQSxjQUFBO01BQ0EsVUFBQTtRQUNBLE1BQUE7O01BRUEsYUFBQTtNQUNBLGNBQUE7TUFDQSxZQUFBOzs7QUNYQSxDQUFBLFlBQUE7RUFDQTs7O0VBRUE7S0FDQSxPQUFBO0tBQ0EsV0FBQSx3QkFBQTs7O0VBR0EsU0FBQSxxQkFBQSxTQUFBLFFBQUE7O0lBRUEsSUFBQSxLQUFBOztJQUVBLEdBQUEsT0FBQSxRQUFBLFNBQUEsSUFBQSxHQUFBO0lBQ0EsR0FBQSxTQUFBO0lBQ0EsR0FBQSxRQUFBLFFBQUEsU0FBQSxJQUFBLFNBQUEsSUFBQSxVQUFBLEtBQUEsU0FBQSxJQUFBLFNBQUE7O0lBRUE7O0lBRUEsR0FBQSxTQUFBOzs7O0lBSUEsU0FBQSxTQUFBO01BQ0EsSUFBQSxDQUFBLEdBQUEsT0FBQSxHQUFBLFVBQUEsYUFBQSxDQUFBLEdBQUEsTUFBQTtRQUNBOzs7TUFHQSxPQUFBLENBQUEsT0FBQSxDQUFBLEdBQUEsTUFBQSxHQUFBLE9BQUEsR0FBQSxNQUFBLFdBQUE7UUFDQSxFQUFBLEdBQUE7OztNQUdBLEdBQUEsT0FBQSxPQUFBLENBQUEsR0FBQSxNQUFBLEdBQUEsT0FBQSxHQUFBOzs7SUFHQSxTQUFBLFdBQUE7TUFDQSxJQUFBLE9BQUEsT0FBQSxHQUFBO01BQ0EsSUFBQSxLQUFBLFdBQUE7UUFDQSxHQUFBLE1BQUEsU0FBQSxLQUFBLE9BQUE7UUFDQSxHQUFBLFFBQUEsS0FBQTtRQUNBLEdBQUEsT0FBQSxLQUFBOzs7Ozs7QUFNQSIsImZpbGUiOiJhcHAuanMiLCJzb3VyY2VzQ29udGVudCI6WyIoZnVuY3Rpb24gKCkge1xuICAndXNlIHN0cmljdCc7XG5cbiAgYW5ndWxhci5tb2R1bGUoJ2Jvb2tpbmcnLCBbXG4gICAgJ25nUmVzb3VyY2UnLFxuICAgICd1aS5yb3V0ZXInLFxuICAgICd0b2FzdGVyJyxcbiAgICAnYW5ndWxhci1zdG9yYWdlJyxcbiAgICAndWkuYm9vdHN0cmFwJyxcbiAgICAndWkuc2VsZWN0JyxcbiAgICAnbmdTYW5pdGl6ZSdcbiAgXSk7XG5cbn0pKCk7XG4iLCIoZnVuY3Rpb24oKSB7XG4gICd1c2Ugc3RyaWN0JztcblxuICBhbmd1bGFyXG4gICAgLm1vZHVsZSgnYm9va2luZycpXG4gICAgLmNvbmZpZyhjb25maWcpO1xuXG4gIGNvbmZpZy4kaW5qZWN0ID0gWyckaHR0cFByb3ZpZGVyJ107XG5cbiAgZnVuY3Rpb24gY29uZmlnKCRodHRwUHJvdmlkZXIpIHtcblxuICAgICRodHRwUHJvdmlkZXIuaW50ZXJjZXB0b3JzLnB1c2goJ3NsSW50ZXJjZXB0b3InKTtcbiAgfVxuXG59KSgpO1xuIiwiKGZ1bmN0aW9uICgpIHtcblxuICAndXNlIHN0cmljdCc7XG5cbiAgYW5ndWxhclxuICAgIC5tb2R1bGUoJ2Jvb2tpbmcnKVxuICAgIC5jb25zdGFudCgnYWNjb3N0cycsIFsnTXInLCAnTWlzcycsICdNcnMnLCAnTXMnLCAnRHInXSlcbiAgICAuY29uc3RhbnQoJ2Jvb2xlYW5WYWx1ZXMnLCBbXG4gICAgICB7XG4gICAgICAgIHRpdGxlOiAnWWVzJyxcbiAgICAgICAgdmFsdWU6IDFcbiAgICAgIH0sXG4gICAgICB7XG4gICAgICAgIHRpdGxlOiAnTm8nLFxuICAgICAgICB2YWx1ZTogMFxuICAgICAgfVxuICAgIF0pXG4gICAgLmNvbnN0YW50KCdwYXltZW50VHlwZXMnLCBbXG4gICAgICB7XG4gICAgICAgIHRpdGxlOiAnUGF5IE9ubGluZSBieSBDcmVkaXQgLyBEZWJpdCBDYXJkJyxcbiAgICAgICAgdmFsdWU6ICdvbmxpbmUnXG4gICAgICB9LFxuICAgICAge1xuICAgICAgICB0aXRsZTogJ1VzaW5nIGNoaWxkY2FyZSB2b3VjaGVycycsXG4gICAgICAgIHZhbHVlOiAndm91Y2hlcidcbiAgICAgIH0sXG4gICAgICAvL3tcbiAgICAgIC8vICB0aXRsZTogJ0J5IGNoZXF1ZScsXG4gICAgICAvLyAgdmFsdWU6ICdjaGVxdWUnXG4gICAgICAvL31cbiAgICBdKVxuICAgIC5jb25zdGFudCgnbW9udGhzJywgW1xuICAgICAge1xuICAgICAgICB2YWx1ZTogMCxcbiAgICAgICAgbmFtZTogJ0phbnVhcnknXG4gICAgICB9LFxuICAgICAge1xuICAgICAgICB2YWx1ZTogMSxcbiAgICAgICAgbmFtZTogJ0ZlYnJ1YXJ5J1xuICAgICAgfSxcbiAgICAgIHtcbiAgICAgICAgdmFsdWU6IDIsXG4gICAgICAgIG5hbWU6ICdNYXJjaCdcbiAgICAgIH0sXG4gICAgICB7XG4gICAgICAgIHZhbHVlOiAzLFxuICAgICAgICBuYW1lOiAnQXByaWwnXG4gICAgICB9LFxuICAgICAge1xuICAgICAgICB2YWx1ZTogNCxcbiAgICAgICAgbmFtZTogJ01heSdcbiAgICAgIH0sXG4gICAgICB7XG4gICAgICAgIHZhbHVlOiA1LFxuICAgICAgICBuYW1lOiAnSnVuZSdcbiAgICAgIH0sXG4gICAgICB7XG4gICAgICAgIHZhbHVlOiA2LFxuICAgICAgICBuYW1lOiAnSnVseSdcbiAgICAgIH0sXG4gICAgICB7XG4gICAgICAgIHZhbHVlOiA3LFxuICAgICAgICBuYW1lOiAnQXVndXN0J1xuICAgICAgfSxcbiAgICAgIHtcbiAgICAgICAgdmFsdWU6IDgsXG4gICAgICAgIG5hbWU6ICdTZXB0ZW1iZXInXG4gICAgICB9LFxuICAgICAge1xuICAgICAgICB2YWx1ZTogOSxcbiAgICAgICAgbmFtZTogJ09jdG9iZXInXG4gICAgICB9LFxuICAgICAge1xuICAgICAgICB2YWx1ZTogMTAsXG4gICAgICAgIG5hbWU6ICdOb3ZlbWJlcidcbiAgICAgIH0sXG4gICAgICB7XG4gICAgICAgIHZhbHVlOiAxMSxcbiAgICAgICAgbmFtZTogJ0RlY2VtYmVyJ1xuICAgICAgfVxuICAgIF0pO1xuXG59KSgpO1xuIiwiKGZ1bmN0aW9uICgpIHtcbiAgJ3VzZSBzdHJpY3QnO1xuXG4gIGFuZ3VsYXJcbiAgICAubW9kdWxlKCdib29raW5nJylcbiAgICAuZmFjdG9yeSgnc2xJbnRlcmNlcHRvcicsIHNsSW50ZXJjZXB0b3IpO1xuXG4gIHNsSW50ZXJjZXB0b3IuJGluamVjdCA9IFsnYWxlcnRTZXJ2aWNlJywgJyRxJ107XG5cbiAgZnVuY3Rpb24gc2xJbnRlcmNlcHRvcihhbGVydFNlcnZpY2UsICRxKSB7XG4gICAgcmV0dXJuIHtcbiAgICAgIHJlc3BvbnNlRXJyb3I6IGZ1bmN0aW9uIChyZXMpIHtcbiAgICAgICAgdmFyIHN0YXR1cyA9IHJlcy5zdGF0dXMsXG4gICAgICAgICAgZmV0Y2hlZFJlc3BvbnNlID0gcmVzLmRhdGE7XG5cbiAgICAgICAgaWYoZmV0Y2hlZFJlc3BvbnNlID09PSAnVW5hdXRob3JpemVkLicpIHtcbiAgICAgICAgICBhbGVydFNlcnZpY2UuZXJyb3IoJ1lvdXIgc2Vzc2lvbiB3YXMgZXhwaXJlZC4gUGxlYXNlIGxvZ2luIGFnYWluLicpO1xuXG4gICAgICAgICAgd2luZG93LmxvY2F0aW9uLmhyZWYgPSAnL2xvZ2luJztcbiAgICAgICAgfSBlbHNlIGlmKGZldGNoZWRSZXNwb25zZS5sZW5ndGggPT09IHVuZGVmaW5lZCB8fCBmZXRjaGVkUmVzcG9uc2UubGVuZ3RoIDwgMjU2KSB7XG4gICAgICAgICAgYWxlcnRTZXJ2aWNlLnNob3dSZXNwb25zZUVycm9yKGZldGNoZWRSZXNwb25zZSwgdGl0bGVCeUNvZGVTdGF0dXMoc3RhdHVzKSk7XG4gICAgICAgIH0gZWxzZSAge1xuICAgICAgICAgIGFsZXJ0U2VydmljZS5zaG93UmVzcG9uc2VFcnJvcihbJ1NlcnZlciBFcnJvciddLCB0aXRsZUJ5Q29kZVN0YXR1cyhzdGF0dXMpKTtcbiAgICAgICAgfVxuXG4gICAgICAgIHJldHVybiAkcS5yZWplY3QoZmV0Y2hlZFJlc3BvbnNlKTtcbiAgICAgIH1cbiAgICB9O1xuXG4gICAgZnVuY3Rpb24gdGl0bGVCeUNvZGVTdGF0dXMoc3RhdHVzKSB7XG4gICAgICB2YXIgdGl0bGU7XG5cbiAgICAgIHN3aXRjaCAoc3RhdHVzKSB7XG4gICAgICAgIGNhc2UgNDIyOlxuICAgICAgICAgIHRpdGxlID0gJ1ZhbGlkYXRpb24gRmFpbCEnO1xuICAgICAgICAgIGJyZWFrO1xuICAgICAgICBjYXNlIDQwMzpcbiAgICAgICAgICB0aXRsZSA9ICdBY2Nlc3MgRGVuaWVkISc7XG4gICAgICAgICAgYnJlYWs7XG4gICAgICAgIGNhc2UgNTAwOlxuICAgICAgICAgIHRpdGxlID0gJ1NvcnJ5LiBTZXJ2ZXIgRXJyb3IuJztcbiAgICAgICAgICBicmVhaztcbiAgICAgIH1cblxuICAgICAgcmV0dXJuIHRpdGxlO1xuICAgIH1cbiAgfVxufSkoKTtcbiIsIihmdW5jdGlvbiAoKSB7XG4gICd1c2Ugc3RyaWN0JztcblxuICBhbmd1bGFyXG4gICAgLm1vZHVsZSgnYm9va2luZycpXG4gICAgLmNvbmZpZyhjb25maWcpO1xuXG4gIGNvbmZpZy4kaW5qZWN0ID0gWyckc3RhdGVQcm92aWRlcicsICckdXJsUm91dGVyUHJvdmlkZXInXTtcblxuICBmdW5jdGlvbiBjb25maWcoJHN0YXRlUHJvdmlkZXIsICR1cmxSb3V0ZXJQcm92aWRlcikge1xuXG4gICAgJHVybFJvdXRlclByb3ZpZGVyLm90aGVyd2lzZSgnL3Byb2ZpbGUvc2hvdycpO1xuXG4gICAgdmFyIHVpVmlldyA9ICc8dWktdmlldz48L3VpLXZpZXc+JztcblxuICAgIHZhciBkZXBlbmRlY2llcyA9IHtcbiAgICAgIG9yZGVyczogWydvcmRlclNlcnZpY2UnLCBmdW5jdGlvbiAob3JkZXJTZXJ2aWNlKSB7XG4gICAgICAgIHJldHVybiBvcmRlclNlcnZpY2UuYWxsKCk7XG4gICAgICB9XSxcbiAgICAgIG9yZGVyOiBbJ29yZGVyU2VydmljZScsICckc3RhdGVQYXJhbXMnLCBmdW5jdGlvbiAob3JkZXJTZXJ2aWNlLCAkc3RhdGVQYXJhbXMpIHtcbiAgICAgICAgcmV0dXJuIG9yZGVyU2VydmljZS5nZXQoJHN0YXRlUGFyYW1zLm9yZGVySWQpO1xuICAgICAgfV0sXG4gICAgICBjaGlsZHJlbjogWydjaGlsZFNlcnZpY2UnLCBmdW5jdGlvbiAoY2hpbGRTZXJ2aWNlKSB7XG4gICAgICAgIHJldHVybiBjaGlsZFNlcnZpY2UuYWxsKCk7XG4gICAgICB9XSxcbiAgICAgIGNoaWxkOiBbJ2NoaWxkU2VydmljZScsICckc3RhdGVQYXJhbXMnLCBmdW5jdGlvbiAoY2hpbGRTZXJ2aWNlLCAkc3RhdGVQYXJhbXMpIHtcbiAgICAgICAgcmV0dXJuIGNoaWxkU2VydmljZS5nZXQoJHN0YXRlUGFyYW1zLmNoaWxkSWQpO1xuICAgICAgfV0sXG4gICAgICBldmVudHM6IFsnZXZlbnRTZXJ2aWNlJywgZnVuY3Rpb24gKGV2ZW50U2VydmljZSkge1xuICAgICAgICByZXR1cm4gZXZlbnRTZXJ2aWNlLmFsbCgpO1xuICAgICAgfV0sXG4gICAgICBzd2ltT3B0aW9uczogWydjaGlsZFNlcnZpY2UnLCBmdW5jdGlvbihjaGlsZFNlcnZpY2UpIHtcbiAgICAgICAgcmV0dXJuIGNoaWxkU2VydmljZS5zd2ltT3B0aW9ucygpO1xuICAgICAgfV0sXG4gICAgICB5ZWFyc0luU2Nob29sOiBbJ2NoaWxkU2VydmljZScsIGZ1bmN0aW9uKGNoaWxkU2VydmljZSkge1xuICAgICAgICByZXR1cm4gY2hpbGRTZXJ2aWNlLnllYXJzSW5TY2hvb2woKTtcbiAgICAgIH1dLFxuICAgICAgcHJvZHVjdHM6IFsnb3JkZXJTZXJ2aWNlJywgZnVuY3Rpb24ob3JkZXJTZXJ2aWNlKSB7XG4gICAgICAgIHJldHVybiBvcmRlclNlcnZpY2UucHJvZHVjdHMoKTtcbiAgICAgIH1dLFxuICAgICAgY291bnRyaWVzOiBbJ3Byb2ZpbGVTZXJ2aWNlJywgZnVuY3Rpb24gKHByb2ZpbGVTZXJ2aWNlKSB7XG4gICAgICAgIHJldHVybiBwcm9maWxlU2VydmljZS5jb3VudHJpZXMoKTtcbiAgICAgIH1dLFxuICAgICAgcHJvZmlsZTogWydwcm9maWxlU2VydmljZScsIGZ1bmN0aW9uIChwcm9maWxlU2VydmljZSkge1xuICAgICAgICByZXR1cm4gcHJvZmlsZVNlcnZpY2UuZ2V0KCk7XG4gICAgICB9XVxuICAgIH07XG5cbiAgICAkc3RhdGVQcm92aWRlclxuICAgICAgLnN0YXRlKCdib29raW5nJywge1xuICAgICAgICBhYnN0cmFjdDogdHJ1ZSxcbiAgICAgICAgdGVtcGxhdGVVcmw6ICcvYm9va2luZy90ZW1wbGF0ZS5odG1sJ1xuICAgICAgfSlcblxuXG4gICAgICAuc3RhdGUoJ2Jvb2tpbmcucHJvZmlsZScsIHtcbiAgICAgICAgYWJzdHJhY3Q6IHRydWUsXG4gICAgICAgIHVybDogJy9wcm9maWxlJyxcbiAgICAgICAgZGF0YToge1xuICAgICAgICAgIHRhYjogJ3Byb2ZpbGUnXG4gICAgICAgIH0sXG4gICAgICAgIHRlbXBsYXRlOiB1aVZpZXcsXG4gICAgICAgIGNvbnRyb2xsZXI6ICdQcm9maWxlQ29udHJvbGxlcicsXG4gICAgICAgIGNvbnRyb2xsZXJBczogJ1Byb2ZpbGVDdHJsJyxcbiAgICAgICAgcmVzb2x2ZToge1xuICAgICAgICAgIHByb2ZpbGU6IGRlcGVuZGVjaWVzLnByb2ZpbGUsXG4gICAgICAgICAgY291bnRyaWVzOiBkZXBlbmRlY2llcy5jb3VudHJpZXNcbiAgICAgICAgfVxuICAgICAgfSlcbiAgICAgIC5zdGF0ZSgnYm9va2luZy5wcm9maWxlLnNob3cnLCB7XG4gICAgICAgIHVybDogJy9zaG93JyxcbiAgICAgICAgdGVtcGxhdGVVcmw6ICcvYm9va2luZy9wcm9maWxlL3Nob3cuaHRtbCcsXG4gICAgICB9KVxuICAgICAgLnN0YXRlKCdib29raW5nLnByb2ZpbGUuZWRpdCcsIHtcbiAgICAgICAgdXJsOiAnL2VkaXQnLFxuICAgICAgICB0ZW1wbGF0ZVVybDogJy9ib29raW5nL3Byb2ZpbGUvZWRpdC5odG1sJyxcbiAgICAgIH0pXG5cblxuICAgICAgLnN0YXRlKCdib29raW5nLmNoaWxkcmVuJywge1xuICAgICAgICBhYnN0cmFjdDogdHJ1ZSxcbiAgICAgICAgdXJsOiAnL2NoaWxkcmVuJyxcbiAgICAgICAgdGVtcGxhdGU6IHVpVmlldyxcbiAgICAgICAgZGF0YToge1xuICAgICAgICAgIHRhYjogJ2NoaWxkcmVuJ1xuICAgICAgICB9LFxuICAgICAgfSlcbiAgICAgIC5zdGF0ZSgnYm9va2luZy5jaGlsZHJlbi5pbmRleCcsIHtcbiAgICAgICAgdXJsOiAnL2luZGV4JyxcbiAgICAgICAgdGVtcGxhdGVVcmw6ICcvYm9va2luZy9jaGlsZHJlbi9pbmRleC5odG1sJyxcbiAgICAgICAgY29udHJvbGxlcjogJ0NoaWxkcmVuQ29udHJvbGxlcicsXG4gICAgICAgIGNvbnRyb2xsZXJBczogJ0NoaWxkcmVuQ3RybCcsXG4gICAgICAgIHJlc29sdmU6IHtcbiAgICAgICAgICBjaGlsZHJlbjogZGVwZW5kZWNpZXMuY2hpbGRyZW5cbiAgICAgICAgfVxuICAgICAgfSlcbiAgICAgIC5zdGF0ZSgnYm9va2luZy5jaGlsZHJlbi5jcmVhdGUnLCB7XG4gICAgICAgIHVybDogJy9jcmVhdGUnLFxuICAgICAgICB0ZW1wbGF0ZVVybDogJy9ib29raW5nL2NoaWxkcmVuL2NyZWF0ZS5odG1sJyxcbiAgICAgICAgY29udHJvbGxlcjogJ0NyZWF0ZUNoaWxkQ29udHJvbGxlcicsXG4gICAgICAgIGNvbnRyb2xsZXJBczogJ0NyZWF0ZUNoaWxkQ3RybCcsXG4gICAgICAgIHJlc29sdmU6IHtcbiAgICAgICAgICBzd2ltT3B0aW9uczogZGVwZW5kZWNpZXMuc3dpbU9wdGlvbnMsXG4gICAgICAgICAgeWVhcnNJblNjaG9vbDogZGVwZW5kZWNpZXMueWVhcnNJblNjaG9vbFxuICAgICAgICB9XG4gICAgICB9KVxuICAgICAgLnN0YXRlKCdib29raW5nLmNoaWxkcmVuLnNob3cnLCB7XG4gICAgICAgIHVybDogJy86Y2hpbGRJZC9zaG93JyxcbiAgICAgICAgdGVtcGxhdGVVcmw6ICcvYm9va2luZy9jaGlsZHJlbi9zaG93Lmh0bWwnLFxuICAgICAgICBjb250cm9sbGVyOiAnU2hvd0NoaWxkQ29udHJvbGxlcicsXG4gICAgICAgIGNvbnRyb2xsZXJBczogJ1Nob3dDaGlsZEN0cmwnLFxuICAgICAgICByZXNvbHZlOiB7XG4gICAgICAgICAgY2hpbGQ6IGRlcGVuZGVjaWVzLmNoaWxkXG4gICAgICAgIH1cbiAgICAgIH0pXG4gICAgICAuc3RhdGUoJ2Jvb2tpbmcuY2hpbGRyZW4uZWRpdCcsIHtcbiAgICAgICAgdXJsOiAnLzpjaGlsZElkL2VkaXQnLFxuICAgICAgICB0ZW1wbGF0ZVVybDogJy9ib29raW5nL2NoaWxkcmVuL2VkaXQuaHRtbCcsXG4gICAgICAgIGNvbnRyb2xsZXI6ICdFZGl0Q2hpbGRDb250cm9sbGVyJyxcbiAgICAgICAgY29udHJvbGxlckFzOiAnRWRpdENoaWxkQ3RybCcsXG4gICAgICAgIHJlc29sdmU6IHtcbiAgICAgICAgICBjaGlsZDogZGVwZW5kZWNpZXMuY2hpbGQsXG4gICAgICAgICAgc3dpbU9wdGlvbnM6IGRlcGVuZGVjaWVzLnN3aW1PcHRpb25zLFxuICAgICAgICAgIHllYXJzSW5TY2hvb2w6IGRlcGVuZGVjaWVzLnllYXJzSW5TY2hvb2xcbiAgICAgICAgfVxuICAgICAgfSlcblxuICAgICAgLnN0YXRlKCdib29raW5nLm9yZGVycycsIHtcbiAgICAgICAgYWJzdHJhY3Q6IHRydWUsXG4gICAgICAgIHVybDogJy9vcmRlcnMnLFxuICAgICAgICB0ZW1wbGF0ZTogdWlWaWV3LFxuICAgICAgICBkYXRhOiB7XG4gICAgICAgICAgdGFiOiAnb3JkZXJzJ1xuICAgICAgICB9LFxuICAgICAgfSlcbiAgICAgIC8qXG4gICAgICAgLnN0YXRlKCdib29raW5nLm9yZGVycy5pbmRleCcsIHtcbiAgICAgICB1cmw6ICcvaW5kZXgnLFxuICAgICAgIHRlbXBsYXRlVXJsOiAnL2Jvb2tpbmcvb3JkZXJzL2luZGV4Lmh0bWwnLFxuICAgICAgIGNvbnRyb2xsZXI6ICdPcmRlcnNDb250cm9sbGVyJyxcbiAgICAgICBjb250cm9sbGVyQXM6ICdPcmRlcnNDdHJsJyxcbiAgICAgICByZXNvbHZlOiB7XG4gICAgICAgb3JkZXJzOiBkZXBlbmRlY2llcy5vcmRlcnNcbiAgICAgICB9XG4gICAgICAgfSlcbiAgICAgICAqL1xuICAgICAgLnN0YXRlKCdib29raW5nLm9yZGVycy5jcmVhdGUnLCB7XG4gICAgICAgIHVybDogJy9jcmVhdGUnLFxuICAgICAgICB0ZW1wbGF0ZVVybDogJy9ib29raW5nL29yZGVycy9jcmVhdGUuaHRtbCcsXG4gICAgICAgIGNvbnRyb2xsZXI6ICdDcmVhdGVPcmRlckNvbnRyb2xsZXInLFxuICAgICAgICBjb250cm9sbGVyQXM6ICdDcmVhdGVPcmRlckN0cmwnLFxuICAgICAgICByZXNvbHZlOiB7XG4gICAgICAgICAgY2hpbGRyZW46IGRlcGVuZGVjaWVzLmNoaWxkcmVuLFxuICAgICAgICAgIGV2ZW50czogZGVwZW5kZWNpZXMuZXZlbnRzLFxuICAgICAgICAgIGNvdW50T3JkZXJzOiBbJ29yZGVyU2VydmljZScsIGZ1bmN0aW9uIChvcmRlclNlcnZpY2UpIHtcbiAgICAgICAgICAgIHJldHVybiBvcmRlclNlcnZpY2UuY291bnQoKTtcbiAgICAgICAgICB9XVxuICAgICAgICB9XG4gICAgICB9KVxuICAgICAgLypcbiAgICAgICAuc3RhdGUoJ2Jvb2tpbmcub3JkZXJzLnNob3cnLCB7XG4gICAgICAgdXJsOiAnLzpvcmRlcklkL3Nob3cnLFxuICAgICAgIHRlbXBsYXRlVXJsOiAnL2Jvb2tpbmcvb3JkZXJzL3Nob3cuaHRtbCcsXG4gICAgICAgY29udHJvbGxlcjogJ1Nob3dPcmRlckNvbnRyb2xsZXInLFxuICAgICAgIGNvbnRyb2xsZXJBczogJ1Nob3dPcmRlckN0cmwnLFxuICAgICAgIHJlc29sdmU6IHtcbiAgICAgICBvcmRlcjogZGVwZW5kZWNpZXMub3JkZXIsXG4gICAgICAgfVxuICAgICAgIH0pXG4gICAgICAgKi9cbiAgICAgIC8qXG4gICAgICAgLnN0YXRlKCdib29raW5nLm9yZGVycy5lZGl0Jywge1xuICAgICAgIHVybDogJy86b3JkZXJJZC9lZGl0JyxcbiAgICAgICB0ZW1wbGF0ZVVybDogJy9ib29raW5nL29yZGVycy9lZGl0Lmh0bWwnLFxuICAgICAgIGNvbnRyb2xsZXI6ICdFZGl0T3JkZXJDb250cm9sbGVyJyxcbiAgICAgICBjb250cm9sbGVyQXM6ICdFZGl0T3JkZXJDdHJsJyxcbiAgICAgICByZXNvbHZlOiB7XG4gICAgICAgb3JkZXI6IGRlcGVuZGVjaWVzLm9yZGVyLFxuICAgICAgIGNoaWxkcmVuOiBkZXBlbmRlY2llcy5jaGlsZHJlbixcbiAgICAgICBldmVudHM6IGRlcGVuZGVjaWVzLmV2ZW50c1xuICAgICAgIH1cbiAgICAgICB9KVxuICAgICAgICovXG4gICAgICAvLyBTaG93aW5nIHRvdGFsIGluZm8sIG5lZWQgc2VsZWN0IHBheW1lbnQgdHlwZSBhbmQgc3VibWl0IGFjdGlvblxuICAgICAgLy8gQXQgdGhlIGVuZCBjcmVhdGUgQm9va2luZyBhbmQgZ28gdG8gQWxsIEJvb2tpbmdzIHBhZ2Uobm90IFNQQSlcbiAgICAgIC5zdGF0ZSgnYm9va2luZy5jb21wbGV0ZScsIHtcbiAgICAgICAgdXJsOiAnL2NvbXBsZXRlJyxcbiAgICAgICAgdGVtcGxhdGVVcmw6ICcvYm9va2luZy9jb21wbGV0ZS9jb21wbGV0ZS5odG1sJyxcbiAgICAgICAgZGF0YToge1xuICAgICAgICAgIHRhYjogJ2NvbXBsZXRlJ1xuICAgICAgICB9LFxuICAgICAgICBjb250cm9sbGVyOiAnQ29tcGxldGVDb250cm9sbGVyJyxcbiAgICAgICAgY29udHJvbGxlckFzOiAnQ29tcGxldGVDdHJsJyxcbiAgICAgICAgcmVzb2x2ZToge1xuICAgICAgICAgIHByb2R1Y3RzOiBkZXBlbmRlY2llcy5wcm9kdWN0cyxcbiAgICAgICAgICBvcmRlcnM6IFsnb3JkZXJTZXJ2aWNlJywgZnVuY3Rpb24gKG9yZGVyU2VydmljZSkge1xuICAgICAgICAgICAgcmV0dXJuIG9yZGVyU2VydmljZS5hbGwoKTtcbiAgICAgICAgICB9XSxcbiAgICAgICAgICBwcmljZXM6IFsnb3JkZXJzJywgJ2Jvb2tpbmdTZXJ2aWNlJywgJ2hlbHBlclNlcnZpY2UnLCBmdW5jdGlvbihvcmRlcnMsIGJvb2tpbmdTZXJ2aWNlLCBoZWxwZXJTZXJ2aWNlKSB7XG4gICAgICAgICAgICB2YXIgX29yZGVycyA9IGhlbHBlclNlcnZpY2UucHJlcGFyZU9yZGVycyhvcmRlcnMpO1xuXG4gICAgICAgICAgICByZXR1cm4gYm9va2luZ1NlcnZpY2UuY2FsY3VsYXRlKF9vcmRlcnMpO1xuICAgICAgICAgIH1dLFxuICAgICAgICAgIG9yZGVyc1dpdGhQcmljZXM6IFsnb3JkZXJzJywgJ3ByaWNlcycsICckZmlsdGVyJywgZnVuY3Rpb24gKG9yZGVycywgcHJpY2VzLCAkZmlsdGVyKSB7XG4gICAgICAgICAgICB2YXIgb3JkZXJzV2l0aFByaWNlID0gW107XG5cbiAgICAgICAgICAgIGZvciAodmFyIGkgPSAwLCBvcmRlcjsgKG9yZGVyID0gb3JkZXJzW2ldKTsgaSsrKSB7XG4gICAgICAgICAgICAgIG9yZGVyLnN1bSA9IDA7XG5cbiAgICAgICAgICAgICAgdmFyIHByaWNlID0gJGZpbHRlcignZmlsdGVyJykocHJpY2VzLCB7X2lkOiBvcmRlci5faWR9LCB0cnVlKTtcbiAgICAgICAgICAgICAgaWYocHJpY2UubGVuZ3RoICYmIHByaWNlWzBdLmFtb3VudCkge1xuICAgICAgICAgICAgICAgIG9yZGVyLnN1bSA9IHByaWNlWzBdLmFtb3VudDtcbiAgICAgICAgICAgICAgICBvcmRlci5kaXNjb3VudCA9IHByaWNlWzBdLmRpc2NvdW50O1xuICAgICAgICAgICAgICAgIG9yZGVyLnByaWNlX3ZhcmlhbnRzID0gcHJpY2VbMF0ucHJpY2VfdmFyaWFudHM7XG4gICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICBvcmRlci52YXJpYW50cyA9IHByaWNlWzBdLnZhcmlhbnRzO1xuXG4gICAgICAgICAgICAgIG9yZGVyc1dpdGhQcmljZS5wdXNoKG9yZGVyKTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgcmV0dXJuIG9yZGVyc1dpdGhQcmljZTtcbiAgICAgICAgICB9XSxcbiAgICAgICAgICBwYXltZW50U3lzdGVtczogWydib29raW5nU2VydmljZScsIGZ1bmN0aW9uIChib29raW5nU2VydmljZSkge1xuICAgICAgICAgICAgcmV0dXJuIGJvb2tpbmdTZXJ2aWNlLnBheW1lbnRTeXN0ZW1zKCk7XG4gICAgICAgICAgfV1cbiAgICAgICAgfVxuICAgICAgfSk7XG4gIH1cbn0pKCk7XG4iLCIoZnVuY3Rpb24gKCkge1xuICAndXNlIHN0cmljdCc7XG5cbiAgYW5ndWxhclxuICAgIC5tb2R1bGUoJ2Jvb2tpbmcnKVxuICAgIC5ydW4ocnVuKTtcblxuICBydW4uJGluamVjdCA9IFsnJHN0YXRlJywgJyRyb290U2NvcGUnLCAnb3JkZXJTZXJ2aWNlJ107XG5cbiAgZnVuY3Rpb24gcnVuKCRzdGF0ZSwgJHJvb3RTY29wZSwgb3JkZXJTZXJ2aWNlKSB7XG5cbiAgICAkcm9vdFNjb3BlLiRvbignJHN0YXRlQ2hhbmdlU3RhcnQnLCBmdW5jdGlvbiAoZXZlbnQsIHRvU3RhdGUsIHRvUGFyYW1zLCBmcm9tU3RhdGUsIGZyb21QYXJhbXMpIHtcbiAgICAgIHZhciBwcmV2ZW50RXZlbnQgPSBmYWxzZSxcbiAgICAgICAgc3RhdGVOYW1lO1xuXG4gICAgICBpZih0b1N0YXRlLm5hbWUgPT09ICdib29raW5nLmNvbXBsZXRlJyAmJiAhb3JkZXJTZXJ2aWNlLmNvdW50KCkpIHtcbiAgICAgICAgcHJldmVudEV2ZW50ID0gdHJ1ZTtcbiAgICAgICAgc3RhdGVOYW1lID0gJ2Jvb2tpbmcub3JkZXJzLmNyZWF0ZSc7XG4gICAgICB9XG5cbiAgICAgIGlmKHByZXZlbnRFdmVudCkge1xuICAgICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgICBpZihzdGF0ZU5hbWUpIHtcbiAgICAgICAgICAkc3RhdGUuZ28oc3RhdGVOYW1lKTtcbiAgICAgICAgfVxuICAgICAgfSBlbHNlIHtcbiAgICAgICAgJHJvb3RTY29wZS50YWIgPSB0b1N0YXRlLmRhdGEudGFiO1xuICAgICAgfVxuXG4gICAgfSk7XG4gIH1cbn0pKCk7XG4iLCIoZnVuY3Rpb24gKCkge1xuICAndXNlIHN0cmljdCc7XG5cbiAgYW5ndWxhclxuICAgIC5tb2R1bGUoJ2Jvb2tpbmcnKVxuICAgIC5jb250cm9sbGVyKCdCb29raW5nQ29udHJvbGxlcicsIEJvb2tpbmdDb250cm9sbGVyKTtcblxuICBCb29raW5nQ29udHJvbGxlci4kaW5qZWN0ID0gWyckc3RhdGUnXTtcblxuICAvKiBAbmdJbmplY3QgKi9cbiAgZnVuY3Rpb24gQm9va2luZ0NvbnRyb2xsZXIoJHN0YXRlKSB7XG4gICAgLyoganNoaW50IHZhbGlkdGhpczogdHJ1ZSAqL1xuICAgIHZhciB2bSA9IHRoaXM7XG5cbiAgICB2YXIgdGFiID0gJHN0YXRlLmN1cnJlbnQuZGF0YS50YWI7XG4gIH1cbn0pKCk7XG4iLCIoZnVuY3Rpb24gKCkge1xuICAndXNlIHN0cmljdCc7XG5cbiAgYW5ndWxhclxuICAgIC5tb2R1bGUoJ2Jvb2tpbmcnKVxuICAgIC5jb250cm9sbGVyKCdDaGlsZHJlbkNvbnRyb2xsZXInLCBDaGlsZHJlbkNvbnRyb2xsZXIpO1xuXG4gIENoaWxkcmVuQ29udHJvbGxlci4kaW5qZWN0ID0gWydjaGlsZHJlbicsICdjaGlsZFNlcnZpY2UnLCAnYWxlcnRTZXJ2aWNlJywgJyRzdGF0ZSddO1xuXG4gIC8qIEBuZ0luamVjdCAqL1xuICBmdW5jdGlvbiBDaGlsZHJlbkNvbnRyb2xsZXIoY2hpbGRyZW4sIGNoaWxkU2VydmljZSwgYWxlcnRTZXJ2aWNlLCAkc3RhdGUpIHtcbiAgICAvKiBqc2hpbnQgdmFsaWR0aGlzOiB0cnVlICovXG4gICAgdmFyIHZtID0gdGhpcztcblxuICAgIHZtLmNoaWxkcmVuID0gY2hpbGRyZW47XG4gICAgdm0uY29uZmlybWVkVXBkYXRlZCA9IGZhbHNlO1xuICAgIHZtLmNvbmZpcm1lZExlZ2FsID0gZmFsc2U7XG5cbiAgICB2bS5yZW1vdmUgPSByZW1vdmU7XG4gICAgdm0ubmV4dCA9IG5leHQ7XG5cbiAgICAvLy8vLy8vLy8vLy8vLy8vLy8vXG5cbiAgICBmdW5jdGlvbiBuZXh0KCkge1xuICAgICAgaWYgKCF2bS5jb25maXJtZWRVcGRhdGVkKSB7XG4gICAgICAgIGFsZXJ0U2VydmljZS5lcnJvcignUGxlYXNlIGNvbmZpcm0gdGhhdCB5b3UgaGF2ZSB1cGRhdGVkIGFsbCBvZiB5b3VyIGNoaWxkcmVuXFwncyBjb250YWN0IGFuZCBtZWRpY2FsIGluZm9ybWF0aW9uIGJlZm9yZSBib29raW5nLicpO1xuXG4gICAgICAgIHJldHVybjtcbiAgICAgIH1cbiAgICAgIGlmICghdm0uY29uZmlybWVkTGVnYWwpIHtcbiAgICAgICAgYWxlcnRTZXJ2aWNlLmVycm9yKCdQbGVhc2UgY29uZmlybSB0aGF0IHlvdSBhcmUgdGhlIGxlZ2FsIGd1YXJkaWFuIG9mIHRoZSBjaGlsZHJlbiBhYm92ZS4nKTtcblxuICAgICAgICByZXR1cm47XG4gICAgICB9XG5cbiAgICAgICRzdGF0ZS5nbygnYm9va2luZy5vcmRlcnMuY3JlYXRlJyk7XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gcmVtb3ZlKGNoaWxkKSB7XG4gICAgICBjaGlsZFNlcnZpY2UucmVtb3ZlKGNoaWxkLmlkKS50aGVuKGZ1bmN0aW9uICgpIHtcbiAgICAgICAgYWxlcnRTZXJ2aWNlLnN1Y2Nlc3MoJ1RoZSBjaGlsZCB3YXMgcmVtb3ZlZCEnKTtcblxuICAgICAgICB2bS5jaGlsZHJlbi5zcGxpY2Uodm0uY2hpbGRyZW4uaW5kZXhPZihjaGlsZCksIDEpO1xuICAgICAgfSk7XG4gICAgfVxuICB9XG59KSgpO1xuIiwiKGZ1bmN0aW9uICgpIHtcbiAgJ3VzZSBzdHJpY3QnO1xuXG4gIGFuZ3VsYXJcbiAgICAubW9kdWxlKCdib29raW5nJylcbiAgICAuY29udHJvbGxlcignQ3JlYXRlQ2hpbGRDb250cm9sbGVyJywgQ3JlYXRlQ2hpbGRDb250cm9sbGVyKTtcblxuICBDcmVhdGVDaGlsZENvbnRyb2xsZXIuJGluamVjdCA9IFsnY2hpbGRTZXJ2aWNlJywgJyRzdGF0ZScsICd0b2FzdGVyJywgJ3N3aW1PcHRpb25zJywgJ3llYXJzSW5TY2hvb2wnXTtcblxuICAvKiBAbmdJbmplY3QgKi9cbiAgZnVuY3Rpb24gQ3JlYXRlQ2hpbGRDb250cm9sbGVyKGNoaWxkU2VydmljZSwgJHN0YXRlLCB0b2FzdGVyLCBzd2ltT3B0aW9ucywgeWVhcnNJblNjaG9vbCkge1xuICAgIC8qIGpzaGludCB2YWxpZHRoaXM6IHRydWUgKi9cbiAgICB2YXIgdm0gPSB0aGlzO1xuXG4gICAgdm0uc3dpbU9wdGlvbnMgPSBzd2ltT3B0aW9ucztcbiAgICB2bS55ZWFyc0luU2Nob29sID0geWVhcnNJblNjaG9vbDtcblxuICAgIHZtLmZvcm0gPSB7XG4gICAgICBmaXJzdF9uYW1lOiB1bmRlZmluZWQsXG4gICAgICBsYXN0X25hbWU6IHVuZGVmaW5lZCxcbiAgICAgIGVtYWlsOiB1bmRlZmluZWQsXG4gICAgICBiaXJ0aGRheTogdW5kZWZpbmVkLFxuICAgICAgc2V4OiB1bmRlZmluZWQsXG4gICAgICByZWxhdGlvbnNoaXA6IHVuZGVmaW5lZCxcbiAgICAgIHNjaG9vbDogdW5kZWZpbmVkLFxuICAgICAgeWVhcl9pbl9zY2hvb2w6IHVuZGVmaW5lZCxcbiAgICAgIGRvY3Rvcl9uYW1lOiB1bmRlZmluZWQsXG4gICAgICBkb2N0b3JfdGVsZXBob25lX251bWJlcjogdW5kZWZpbmVkLFxuICAgICAgbWVkaWNhbF9hZHZpY2VfYW5kX3RyZWF0bWVudDogdW5kZWZpbmVkLFxuICAgICAgZXBpX3BlbjogdW5kZWZpbmVkLFxuICAgICAgbWVkaWNhdGlvbjogdW5kZWZpbmVkLFxuICAgICAgc3BlY2lhbF9yZXF1aXJlbWVudHM6IHVuZGVmaW5lZCxcbiAgICAgIGNhbl9zd2ltOiB1bmRlZmluZWQsXG4gICAgICBlbWVyZ2VuY3lfY29udGFjdF9udW1iZXI6IHVuZGVmaW5lZCxcbiAgICB9O1xuXG4gICAgdm0uY3JlYXRlID0gY3JlYXRlO1xuXG4gICAgdm0uZGF0ZVBpY2tlck9wZW4gPSBmYWxzZTtcblxuICAgIC8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vL1xuXG4gICAgZnVuY3Rpb24gY3JlYXRlKCkge1xuICAgICAgY2hpbGRTZXJ2aWNlLnN0b3JlKHZtLmZvcm0pLnRoZW4oZnVuY3Rpb24gKCkge1xuICAgICAgICB0b2FzdGVyLnN1Y2Nlc3MoJ1RoZSBjaGlsZCB3YXMgY3JlYXRlZCEnKTtcblxuICAgICAgICAkc3RhdGUuZ28oJ14uaW5kZXgnKTtcbiAgICAgIH0pO1xuICAgIH1cblxuICB9XG59KSgpO1xuIiwiKGZ1bmN0aW9uICgpIHtcbiAgJ3VzZSBzdHJpY3QnO1xuXG4gIGFuZ3VsYXJcbiAgICAubW9kdWxlKCdib29raW5nJylcbiAgICAuY29udHJvbGxlcignRWRpdENoaWxkQ29udHJvbGxlcicsIEVkaXRDaGlsZENvbnRyb2xsZXIpO1xuXG4gIEVkaXRDaGlsZENvbnRyb2xsZXIuJGluamVjdCA9IFsnY2hpbGQnLCAnY2hpbGRTZXJ2aWNlJywgJyRzdGF0ZScsICd0b2FzdGVyJywgJ3N3aW1PcHRpb25zJywgJ3llYXJzSW5TY2hvb2wnXTtcblxuICAvKiBAbmdJbmplY3QgKi9cbiAgZnVuY3Rpb24gRWRpdENoaWxkQ29udHJvbGxlcihjaGlsZCwgY2hpbGRTZXJ2aWNlLCAkc3RhdGUsIHRvYXN0ZXIsIHN3aW1PcHRpb25zLCB5ZWFyc0luU2Nob29sKSB7XG4gICAgLyoganNoaW50IHZhbGlkdGhpczogdHJ1ZSAqL1xuICAgIHZhciB2bSA9IHRoaXM7XG5cbiAgICB2bS5zd2ltT3B0aW9ucyA9IHN3aW1PcHRpb25zO1xuICAgIHZtLnllYXJzSW5TY2hvb2wgPSB5ZWFyc0luU2Nob29sO1xuXG4gICAgdm0uZm9ybSA9IGNoaWxkO1xuXG4gICAgdm0udXBkYXRlID0gdXBkYXRlO1xuXG4gICAgdm0uZGF0ZVBpY2tlck9wZW4gPSBmYWxzZTtcblxuICAgIC8vLy8vLy8vLy8vL1xuXG4gICAgZnVuY3Rpb24gdXBkYXRlKCkge1xuICAgICAgY2hpbGRTZXJ2aWNlLnVwZGF0ZSh2bS5mb3JtKS50aGVuKGZ1bmN0aW9uICgpIHtcbiAgICAgICAgdG9hc3Rlci5zdWNjZXNzKCdUaGUgY2hpbGQgd2FzIHVwZGF0ZWQhJyk7XG5cbiAgICAgICAgJHN0YXRlLmdvKCdeLmluZGV4Jyk7XG4gICAgICB9KTtcbiAgICB9XG5cbiAgfVxufSkoKTtcbiIsIihmdW5jdGlvbiAoKSB7XG4gICd1c2Ugc3RyaWN0JztcblxuICBhbmd1bGFyXG4gICAgLm1vZHVsZSgnYm9va2luZycpXG4gICAgLmNvbnRyb2xsZXIoJ1Nob3dDaGlsZENvbnRyb2xsZXInLCBTaG93Q2hpbGRDb250cm9sbGVyKTtcblxuICBTaG93Q2hpbGRDb250cm9sbGVyLiRpbmplY3QgPSBbJ2NoaWxkJ107XG5cbiAgLyogQG5nSW5qZWN0ICovXG4gIGZ1bmN0aW9uIFNob3dDaGlsZENvbnRyb2xsZXIoY2hpbGQpIHtcbiAgICAvKiBqc2hpbnQgdmFsaWR0aGlzOiB0cnVlICovXG4gICAgdmFyIHZtID0gdGhpcztcblxuICAgIHZtLmNoaWxkID0gY2hpbGQ7XG4gIH1cbn0pKCk7XG4iLCIoZnVuY3Rpb24gKCkge1xuICAndXNlIHN0cmljdCc7XG5cbiAgYW5ndWxhclxuICAgIC5tb2R1bGUoJ2Jvb2tpbmcnKVxuICAgIC5jb250cm9sbGVyKCdDb21wbGV0ZUNvbnRyb2xsZXInLCBDb21wbGV0ZUNvbnRyb2xsZXIpO1xuXG4gIC8qIEBuZ0luamVjdCAqL1xuICBmdW5jdGlvbiBDb21wbGV0ZUNvbnRyb2xsZXIocGF5bWVudFN5c3RlbXMsIG9yZGVyc1dpdGhQcmljZXMsIGJvb2tpbmdTZXJ2aWNlLCBwYXltZW50VHlwZXMsIGFsZXJ0U2VydmljZSwgb3JkZXJTZXJ2aWNlLCBoZWxwZXJTZXJ2aWNlLCAkZmlsdGVyLCAkc3RhdGUsICR1aWJNb2RhbCwgcHJvZHVjdHMsIHN0b3JlKSB7XG4gICAgLyoganNoaW50IHZhbGlkdGhpczogdHJ1ZSAqL1xuICAgIHZhciB2bSA9IHRoaXM7XG5cbiAgICB2bS5vcmRlcnMgPSBvcmRlcnNXaXRoUHJpY2VzO1xuICAgIHZtLm9yZGVycy5zdW0gPSAkZmlsdGVyKCdzdW1CeUtleScpKHZtLm9yZGVycywgJ3N1bScpO1xuICAgIHZtLm9yZGVycy5kaXNjb3VudCA9ICRmaWx0ZXIoJ3N1bUJ5S2V5Jykodm0ub3JkZXJzLCAnZGlzY291bnQnKTtcbiAgICB2bS5zdW1fdmFyaWFudHMgPSAkZmlsdGVyKCdzdW1CeUtleScpKHByb2R1Y3RzLCAncHJpY2UnKTtcbiAgICB2bS5vcmRlcnMuY291cG9uID0gMDtcblxuICAgIHZtLnBheW1lbnRUeXBlcyA9IHBheW1lbnRTeXN0ZW1zLmxlbmd0aCA9PT0gMCA/IHBheW1lbnRUeXBlcy5zcGxpY2UoMSwgMSkgOiBwYXltZW50VHlwZXM7XG4gICAgdm0ucGF5bWVudFN5c3RlbXMgPSBwYXltZW50U3lzdGVtcztcbiAgICB2bS5jb3Vwb25BcHBsaWVkID0gZmFsc2U7XG4gICAgdm0ucHJvZHVjdHMgPSBwcm9kdWN0cztcblxuICAgIHZtLmZvcm0gPSB7XG4gICAgICBwYXltZW50X3R5cGU6IHVuZGVmaW5lZCxcbiAgICAgIHBheW1lbnRfc3lzdGVtOiBwYXltZW50U3lzdGVtcy5sZW5ndGggPT09IDEgPyBwYXltZW50U3lzdGVtc1swXS5zeXN0ZW0gOiB1bmRlZmluZWQsXG4gICAgICBvcmRlcnM6IFtdLFxuICAgICAgY291cG9uOiB1bmRlZmluZWQsXG4gICAgfTtcbiAgICB2bS5hZ3JlZWQgPSBmYWxzZTtcbiAgICB2bS5yZXF1ZXN0ID0gZmFsc2U7XG5cbiAgICBwcmVwYXJlUHJvZHVjdHMoKTtcbiAgICBjYWxjdWxhdGVUb3RhbFByaWNlKCk7XG4gICAgc2V0T3JkZXJzVG9Cb29raW5nRm9ybSgpO1xuICAgIGFkZFRvT3JkZXJQcm9kdWN0c0lmU3RvcmVkKCk7XG5cbiAgICB2bS5ib29rID0gYm9vaztcbiAgICB2bS5yZW1vdmUgPSByZW1vdmU7XG4gICAgdm0uYXBwbHlDb3Vwb24gPSBhcHBseUNvdXBvbjtcbiAgICB2bS5jYW5jZWxDb3Vwb24gPSBjYW5jZWxDb3Vwb247XG4gICAgdm0uY2FsY3VsYXRlUHJvZHVjdFByaWNlID0gY2FsY3VsYXRlUHJvZHVjdFByaWNlO1xuICAgIHZtLnNhdmVQcm9kdWN0cyA9IHNhdmVQcm9kdWN0cztcblxuICAgIC8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vXG5cbiAgICBmdW5jdGlvbiBhcHBseUNvdXBvbigpIHtcbiAgICAgIGlmICghdm0uZm9ybS5jb3Vwb24pIHtcbiAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuXG4gICAgICB2bS5yZXF1ZXN0ID0gdHJ1ZTtcblxuICAgICAgYm9va2luZ1NlcnZpY2UuY2hlY2tDb3Vwb24odm0uZm9ybS5jb3Vwb24pLnRoZW4oZnVuY3Rpb24gKHBlcnNlbnQpIHtcbiAgICAgICAgdm0ub3JkZXJzLmNvdXBvblBlcnNlbnQgPSBwZXJzZW50O1xuICAgICAgICBjYWxjdWxhdGVUb3RhbFByaWNlKCk7XG4gICAgICAgIGFsZXJ0U2VydmljZS5zdWNjZXNzKCdZb3VyIGNvdXBvbiBpcyBhY3RpdmF0ZWQuJyk7XG4gICAgICAgIHZtLmNvdXBvbkFwcGxpZWQgPSB0cnVlO1xuICAgICAgfSwgZnVuY3Rpb24gKCkge1xuICAgICAgICBhbGVydFNlcnZpY2UuZXJyb3IoJ0NvdXBvbiBub3QgZXhpc3RzIG9yIGV4cGlyZWQuJyk7XG4gICAgICB9KS5maW5hbGx5KGZ1bmN0aW9uICgpIHtcbiAgICAgICAgdm0ucmVxdWVzdCA9IGZhbHNlO1xuICAgICAgfSk7XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gY2FuY2VsQ291cG9uKCkge1xuICAgICAgLy9DbGVhciBkYXRhIGFib3V0IHByZXZpb3VzIGNvdXBvblxuICAgICAgdm0uZm9ybS5jb3Vwb24gPSAnJztcbiAgICAgIHZtLm9yZGVycy5jb3Vwb25QZXJzZW50ID0gdW5kZWZpbmVkO1xuICAgICAgdm0ub3JkZXJzLmNvdXBvbiA9IDA7XG4gICAgICB2bS5jb3Vwb25BcHBsaWVkID0gZmFsc2U7XG5cbiAgICAgIGNhbGN1bGF0ZVRvdGFsUHJpY2UoKTtcblxuICAgICAgYWxlcnRTZXJ2aWNlLnN1Y2Nlc3MoJ1lvdSBjb3Vwb24gd2FzIGRlYWN0aXZhdGVkLicpO1xuICAgIH1cblxuICAgIGZ1bmN0aW9uIHJlbW92ZShpZCkge1xuICAgICAgdmFyIG1vZGFsSW5zdGFuY2UgPSAkdWliTW9kYWwub3Blbih7XG4gICAgICAgIHRlbXBsYXRlVXJsOiAnL2Jvb2tpbmcvY29tcGxldGUvY29uZmlybS1yZW1vdmUtbW9kYWwuaHRtbCcsXG4gICAgICAgIGNvbnRyb2xsZXI6ICdDb25maXJtUmVtb3ZlT3JkZXJDb250cm9sbGVyJyxcbiAgICAgICAgY29udHJvbGxlckFzOiAnQ29uZmlybVJlbW92ZUN0cmwnLFxuICAgICAgICBzaXplOiAnbWQnXG4gICAgICB9KTtcblxuICAgICAgbW9kYWxJbnN0YW5jZS5yZXN1bHQudGhlbihmdW5jdGlvbiAoKSB7XG4gICAgICAgIG9yZGVyU2VydmljZS5yZW1vdmUoaWQpLnRoZW4oZnVuY3Rpb24gKCkge1xuICAgICAgICAgIC8vUmVmcmVzaCBkYXRhXG4gICAgICAgICAgb3JkZXJTZXJ2aWNlLmFsbCgpLnRoZW4oZnVuY3Rpb24gKG9yZGVycykge1xuICAgICAgICAgICAgYWxlcnRTZXJ2aWNlLnN1Y2Nlc3MoJ1RoZSBvcmRlciB3YXMgcmVtb3ZlZCcpO1xuXG4gICAgICAgICAgICBpZiAoIW9yZGVycy5sZW5ndGgpIHtcbiAgICAgICAgICAgICAgJHN0YXRlLmdvKCdib29raW5nLm9yZGVycy5jcmVhdGUnKTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgdmFyIF9vcmRlcnMgPSBoZWxwZXJTZXJ2aWNlLnByZXBhcmVPcmRlcnMob3JkZXJzKTtcbiAgICAgICAgICAgIGJvb2tpbmdTZXJ2aWNlLmNhbGN1bGF0ZShfb3JkZXJzKS50aGVuKGZ1bmN0aW9uIChwcmljZXMpIHtcbiAgICAgICAgICAgICAgdmFyIG9yZGVyc1dpdGhQcmljZSA9IFtdO1xuXG4gICAgICAgICAgICAgIGZvciAodmFyIGkgPSAwLCBvcmRlcjsgKG9yZGVyID0gb3JkZXJzW2ldKTsgaSsrKSB7XG4gICAgICAgICAgICAgICAgb3JkZXIuc3VtID0gMDtcblxuICAgICAgICAgICAgICAgIHZhciBwcmljZSA9ICRmaWx0ZXIoJ2ZpbHRlcicpKHByaWNlcywge19pZDogb3JkZXIuX2lkfSwgdHJ1ZSk7XG4gICAgICAgICAgICAgICAgaWYgKHByaWNlLmxlbmd0aCAmJiBwcmljZVswXS5hbW91bnQpIHtcbiAgICAgICAgICAgICAgICAgIG9yZGVyLnN1bSA9IHByaWNlWzBdLmFtb3VudDtcbiAgICAgICAgICAgICAgICAgIG9yZGVyLmRpc2NvdW50ID0gcHJpY2VbMF0uZGlzY291bnQ7XG4gICAgICAgICAgICAgICAgICBvcmRlci5wcmljZV92YXJpYW50cyA9IHByaWNlWzBdLnByaWNlX3ZhcmlhbnRzO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBvcmRlci52YXJpYW50cyA9IHByaWNlWzBdLnZhcmlhbnRzO1xuICAgICAgICAgICAgICAgIG9yZGVyc1dpdGhQcmljZS5wdXNoKG9yZGVyKTtcbiAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgIHZtLm9yZGVycyA9IG9yZGVyc1dpdGhQcmljZTtcbiAgICAgICAgICAgICAgdm0ub3JkZXJzLnN1bSA9ICRmaWx0ZXIoJ3N1bUJ5S2V5Jykodm0ub3JkZXJzLCAnc3VtJyk7XG4gICAgICAgICAgICAgIHZtLm9yZGVycy5kaXNjb3VudCA9ICRmaWx0ZXIoJ3N1bUJ5S2V5Jykodm0ub3JkZXJzLCAnZGlzY291bnQnKTtcbiAgICAgICAgICAgICAgdm0uc3VtX3ZhcmlhbnRzID0gJGZpbHRlcignc3VtQnlLZXknKSh2bS5vcmRlcnMsICdwcmljZV92YXJpYW50cycpO1xuICAgICAgICAgICAgICB2bS5vcmRlcnMuY291cG9uID0gMDtcbiAgICAgICAgICAgICAgY2FsY3VsYXRlVG90YWxQcmljZSgpO1xuICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgICAgIC8vIHZtLm9yZGVycyA9IG9yZGVycztcbiAgICAgICAgICB9KTtcbiAgICAgICAgfSk7XG4gICAgICB9KTtcbiAgICB9XG5cbiAgICBmdW5jdGlvbiBib29rKCkge1xuICAgICAgaWYgKCF2bS5hZ3JlZWQpIHtcbiAgICAgICAgYWxlcnRTZXJ2aWNlLmVycm9yKCdZb3Ugc2hvdWxkIHdpdGggYm9va2luZ1xcJ3MgdGVybXMgYW5kIGNvbmRpdGlvbnMnKTtcbiAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuXG4gICAgICB2bS5yZXF1ZXN0ID0gdHJ1ZTtcblxuICAgICAgcmV0dXJuIGJvb2tpbmdTZXJ2aWNlLmJvb2sodm0uZm9ybSkudGhlbihmdW5jdGlvbiAoZGF0YSkge1xuICAgICAgICBhbGVydFNlcnZpY2Uuc3VjY2VzcygnWW91IGJvb2tlZCEnKTtcblxuICAgICAgICAvL0NsZWFyIGN1cnJlbnQgb3JkZXJzXG4gICAgICAgIG9yZGVyU2VydmljZS5jbGVhcigpO1xuXG4gICAgICAgIGhlbHBlclNlcnZpY2UucmVkaXJlY3QoZGF0YS5yZWRpcmVjdCwgZGF0YS5tZXRob2QsIGRhdGEuZmllbGRzKTtcbiAgICAgIH0sIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgdm0ucmVxdWVzdCA9IGZhbHNlO1xuICAgICAgfSk7XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gc2V0T3JkZXJzVG9Cb29raW5nRm9ybSgpIHtcbiAgICAgIHZtLmZvcm0ub3JkZXJzID0gaGVscGVyU2VydmljZS5wcmVwYXJlT3JkZXJzKHZtLm9yZGVycyk7XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gY2FsY3VsYXRlVG90YWxQcmljZSgpIHtcbiAgICAgIGlmICh2bS5mb3JtLmNvdXBvbiAmJiB2bS5vcmRlcnMuY291cG9uUGVyc2VudCkge1xuICAgICAgICB2bS5vcmRlcnMuY291cG9uID0gdm0ub3JkZXJzLnN1bSAqIHZtLm9yZGVycy5jb3Vwb25QZXJzZW50IC8gMTAwO1xuICAgICAgICB2bS5vcmRlcnMudG90YWwgPSB2bS5vcmRlcnMuc3VtIC0gdm0ub3JkZXJzLmNvdXBvbjtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHZtLm9yZGVycy50b3RhbCA9IHZtLm9yZGVycy5zdW0gLSB2bS5vcmRlcnMuZGlzY291bnQ7XG4gICAgICB9XG4gICAgICB2bS5vcmRlcnMudG90YWwgPSB2bS5vcmRlcnMudG90YWwgKyB2bS5zdW1fdmFyaWFudHM7XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gcHJlcGFyZVByb2R1Y3RzKCkge1xuICAgICAgYW5ndWxhci5mb3JFYWNoKHZtLnByb2R1Y3RzLCBmdW5jdGlvbiAocHJvZHVjdCkge1xuICAgICAgICBpZiAoIXByb2R1Y3Quc2VsZWN0ZWRRdWFudGl0eSkge1xuICAgICAgICAgIHByb2R1Y3Quc2VsZWN0ZWRRdWFudGl0eSA9IDE7XG4gICAgICAgIH1cblxuICAgICAgICBpZiAoIXByb2R1Y3Quc2VsZWN0ZWRWYXJpYW50KSB7XG4gICAgICAgICAgcHJvZHVjdC5zZWxlY3RlZFZhcmlhbnQgPSBwcm9kdWN0LnZhcmlhbnRzLmxlbmd0aCA/IHByb2R1Y3QudmFyaWFudHNbMF0uaWQgOiAwO1xuICAgICAgICB9XG5cbiAgICAgICAgT2JqZWN0LmRlZmluZVByb3BlcnR5KHByb2R1Y3QsICdwcmljZScsIHtcbiAgICAgICAgICBnZXQ6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgIHZhciBwcmljZSA9IDA7XG5cbiAgICAgICAgICAgIHZhciBzZWxlY3RlZFZhcmlhbnQgPSB0aGlzLnNlbGVjdGVkVmFyaWFudDtcbiAgICAgICAgICAgIHZhciB2YXJpYW50ID0gdGhpcy52YXJpYW50cy5maWx0ZXIoZnVuY3Rpb24gKHZhcmlhbnQpIHtcbiAgICAgICAgICAgICAgcmV0dXJuIHNlbGVjdGVkVmFyaWFudCAmJiBzZWxlY3RlZFZhcmlhbnQgPT09IHZhcmlhbnQuaWQ7XG4gICAgICAgICAgICB9KVswXTtcblxuICAgICAgICAgICAgaWYgKHZhcmlhbnQgJiYgdGhpcy5zZWxlY3RlZFF1YW50aXR5KSB7XG4gICAgICAgICAgICAgIHByaWNlID0gdmFyaWFudC5wcmljZSAqIHRoaXMuc2VsZWN0ZWRRdWFudGl0eTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgcmV0dXJuIHByaWNlO1xuICAgICAgICAgIH1cbiAgICAgICAgfSk7XG4gICAgICAgIE9iamVjdC5kZWZpbmVQcm9wZXJ0eShwcm9kdWN0LCAnbWF4Q291bnQnLCB7XG4gICAgICAgICAgZ2V0OiBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICB2YXIgY291bnRNYXggPSAwO1xuXG4gICAgICAgICAgICB2YXIgc2VsZWN0ZWRWYXJpYW50ID0gdGhpcy5zZWxlY3RlZFZhcmlhbnQ7XG4gICAgICAgICAgICB2YXIgdmFyaWFudCA9IHRoaXMudmFyaWFudHMuZmlsdGVyKGZ1bmN0aW9uICh2YXJpYW50KSB7XG4gICAgICAgICAgICAgIHJldHVybiBzZWxlY3RlZFZhcmlhbnQgJiYgc2VsZWN0ZWRWYXJpYW50ID09PSB2YXJpYW50LmlkO1xuICAgICAgICAgICAgfSlbMF07XG5cbiAgICAgICAgICAgIGlmICh2YXJpYW50ICYmIHRoaXMuc2VsZWN0ZWRRdWFudGl0eSkge1xuICAgICAgICAgICAgICBjb3VudE1heCA9IHZhcmlhbnQuY291bnQ7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIHJldHVybiBjb3VudE1heDtcbiAgICAgICAgICB9XG4gICAgICAgIH0pO1xuXG4gICAgICAgIGNhbGN1bGF0ZVByb2R1Y3RQcmljZShwcm9kdWN0LCB0cnVlKTtcbiAgICAgIH0pO1xuICAgIH1cblxuICAgIGZ1bmN0aW9uIGNhbGN1bGF0ZVByb2R1Y3RQcmljZShwcm9kdWN0LCBub3RTaG93Q2hhbmdlUHJvZHVjdHMpIHtcbiAgICAgIHZtLmNoYW5nZWRQcm9kdWN0cyA9ICFub3RTaG93Q2hhbmdlUHJvZHVjdHM7XG4gICAgICAvL3Byb2R1Y3QucHJpY2UgPSAwO1xuICAgICAgLy9wcm9kdWN0Lm1heENvdW50ID0gdW5kZWZpbmVkO1xuXG4gICAgICB2YXIgdmFyaWFudCA9IHByb2R1Y3QudmFyaWFudHMuZmlsdGVyKGZ1bmN0aW9uICh2YXJpYW50KSB7XG4gICAgICAgIHJldHVybiBwcm9kdWN0LnNlbGVjdGVkVmFyaWFudCAmJiBwcm9kdWN0LnNlbGVjdGVkVmFyaWFudCA9PT0gdmFyaWFudC5pZDtcbiAgICAgIH0pWzBdO1xuXG4gICAgICBpZiAodmFyaWFudCAmJiBwcm9kdWN0LnNlbGVjdGVkUXVhbnRpdHkpIHtcbiAgICAgICAgLy9wcm9kdWN0LnByaWNlID0gdmFyaWFudC5wcmljZSAqIHByb2R1Y3Quc2VsZWN0ZWRRdWFudGl0eTtcbiAgICAgICAgLy9wcm9kdWN0Lm1heENvdW50ID0gdmFyaWFudC5jb3VudDtcbiAgICAgIH1cbiAgICB9XG5cbiAgICBmdW5jdGlvbiBzYXZlUHJvZHVjdHMoKSB7XG4gICAgICB2YXIgcHJvZHVjdHMgPSB2bS5wcm9kdWN0cy5maWx0ZXIoZnVuY3Rpb24gKHByb2R1Y3QpIHtcbiAgICAgICAgcmV0dXJuIHByb2R1Y3QucHJpY2U7XG4gICAgICB9KTtcblxuICAgICAgb3JkZXJTZXJ2aWNlLnNhdmVQcm9kdWN0cyhwcm9kdWN0cykudGhlbihmdW5jdGlvbiAoKSB7XG4gICAgICAgIGFsZXJ0U2VydmljZS5zdWNjZXNzKCdQcm9kdWN0cyBzYXZlZCcpO1xuXG4gICAgICAgIHZtLmZvcm0ucHJvZHVjdHMgPSBwcm9kdWN0cztcblxuICAgICAgICB2bS5zdW1fdmFyaWFudHMgPSAkZmlsdGVyKCdzdW1CeUtleScpKHByb2R1Y3RzLCAncHJpY2UnKTtcblxuICAgICAgICBjYWxjdWxhdGVUb3RhbFByaWNlKCk7XG5cbiAgICAgICAgdm0uY2hhbmdlZFByb2R1Y3RzID0gZmFsc2U7XG4gICAgICB9KTtcbiAgICB9XG5cbiAgICBmdW5jdGlvbiBhZGRUb09yZGVyUHJvZHVjdHNJZlN0b3JlZCgpIHtcbiAgICAgIHZhciBwcm9kdWN0cyA9IHN0b3JlLmdldCgncHJvZHVjdHMnKTtcblxuICAgICAgaWYgKCFwcm9kdWN0cyB8fCAhcHJvZHVjdHMubGVuZ3RoKSB7XG4gICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgIH1cblxuICAgICAgdm0uZm9ybS5wcm9kdWN0cyA9IHByb2R1Y3RzO1xuICAgIH1cblxuXG4gIH1cbn0pKCk7XG4iLCIoZnVuY3Rpb24gKCkge1xuICAndXNlIHN0cmljdCc7XG5cbiAgYW5ndWxhclxuICAgIC5tb2R1bGUoJ2Jvb2tpbmcnKVxuICAgIC5jb250cm9sbGVyKCdDb25maXJtUmVtb3ZlT3JkZXJDb250cm9sbGVyJywgQ29uZmlybVJlbW92ZU9yZGVyQ29udHJvbGxlcik7XG5cbiAgQ29uZmlybVJlbW92ZU9yZGVyQ29udHJvbGxlci4kaW5qZWN0ID0gWyckdWliTW9kYWxJbnN0YW5jZSddO1xuXG4gIC8qIEBuZ0luamVjdCAqL1xuICBmdW5jdGlvbiBDb25maXJtUmVtb3ZlT3JkZXJDb250cm9sbGVyKCR1aWJNb2RhbEluc3RhbmNlKSB7XG4gICAgLyoganNoaW50IHZhbGlkdGhpczogdHJ1ZSAqL1xuICAgIHZhciB2bSA9IHRoaXM7XG5cbiAgICB2bS5vayA9IG9rO1xuICAgIHZtLmNhbmNlbCA9IGNhbmNlbDtcblxuICAgIGZ1bmN0aW9uIG9rKCkge1xuICAgICAgJHVpYk1vZGFsSW5zdGFuY2UuY2xvc2UoKTtcbiAgICB9XG5cbiAgICBmdW5jdGlvbiBjYW5jZWwoKSB7XG4gICAgICAkdWliTW9kYWxJbnN0YW5jZS5kaXNtaXNzKCdjYW5jZWwnKTtcbiAgICB9XG4gIH1cbn0pKCk7XG4iLCIoZnVuY3Rpb24oKXtcbiAgJ3VzZSBzdHJpY3QnO1xuXG4gIGFuZ3VsYXJcbiAgICAubW9kdWxlKCdib29raW5nJylcbiAgICAuZmlsdGVyKCdhZ2UnLCBhZ2UpO1xuXG4gIGFnZS4kaW5qZWN0ID0gW107XG5cbiAgZnVuY3Rpb24gYWdlKCkge1xuICAgIHJldHVybiBtYWluO1xuXG4gICAgZnVuY3Rpb24gbWFpbihwZXJzb24sIGRhdGUpIHtcbiAgICAgIGRhdGUgPSBkYXRlID8gbW9tZW50KGRhdGUpIDogbW9tZW50KCk7XG5cbiAgICAgIHZhciBiaXJ0aGRheSA9IG1vbWVudChwZXJzb24uYmlydGhkYXkpO1xuICAgICAgcmV0dXJuIGRhdGUuZGlmZihiaXJ0aGRheSwgJ3llYXJzJyk7XG4gICAgfVxuICB9XG59KSgpOyIsIihmdW5jdGlvbigpe1xuICAndXNlIHN0cmljdCc7XG5cbiAgYW5ndWxhclxuICAgIC5tb2R1bGUoJ2Jvb2tpbmcnKVxuICAgIC5maWx0ZXIoJ2N1cnJlbmN5TGInLCBjdXJyZW5jeUxiKTtcblxuICBjdXJyZW5jeUxiLiRpbmplY3QgPSBbJyRmaWx0ZXInXTtcblxuICBmdW5jdGlvbiBjdXJyZW5jeUxiKCRmaWx0ZXIpIHtcbiAgICByZXR1cm4gbWFpbjtcblxuICAgIGZ1bmN0aW9uIG1haW4odmFsdWUpIHtcbiAgICAgIHJldHVybiAkZmlsdGVyKCdjdXJyZW5jeScpKHZhbHVlLCAnwqMnKTtcbiAgICB9XG4gIH1cbn0pKCk7IiwiKGZ1bmN0aW9uKCl7XG4gICd1c2Ugc3RyaWN0JztcblxuICBhbmd1bGFyXG4gICAgLm1vZHVsZSgnYm9va2luZycpXG4gICAgLmZpbHRlcignZXZlbnQnLCBldmVudCk7XG5cbiAgZXZlbnQuJGluamVjdCA9IFtdO1xuXG4gIGZ1bmN0aW9uIGV2ZW50KCkge1xuICAgIHJldHVybiBtYWluO1xuXG4gICAgZnVuY3Rpb24gbWFpbihldmVudCkge1xuICAgICAgcmV0dXJuIGV2ZW50LnZlbnVlLm5hbWUgKyAnIC0gJyArIGV2ZW50Lm5hbWU7XG4gICAgfVxuICB9XG59KSgpOyIsIihmdW5jdGlvbigpe1xuICAndXNlIHN0cmljdCc7XG5cbiAgYW5ndWxhclxuICAgIC5tb2R1bGUoJ2Jvb2tpbmcnKVxuICAgIC5maWx0ZXIoJ2xvZ2ljYWwnLCBsb2dpY2FsKTtcblxuICBsb2dpY2FsLiRpbmplY3QgPSBbXTtcblxuICBmdW5jdGlvbiBsb2dpY2FsKCkge1xuICAgIHJldHVybiBtYWluO1xuXG4gICAgZnVuY3Rpb24gbWFpbih2YWx1ZSkge1xuICAgICAgcmV0dXJuIHZhbHVlID8gJ1llcycgOiAnTm8nO1xuICAgIH1cbiAgfVxufSkoKTsiLCIoZnVuY3Rpb24oKXtcbiAgJ3VzZSBzdHJpY3QnO1xuXG4gIGFuZ3VsYXJcbiAgICAubW9kdWxlKCdib29raW5nJylcbiAgICAuZmlsdGVyKCdwcm9maWxlJywgcHJvZmlsZSk7XG5cbiAgcHJvZmlsZS4kaW5qZWN0ID0gW107XG5cbiAgZnVuY3Rpb24gcHJvZmlsZSgpIHtcbiAgICByZXR1cm4gbWFpbjtcblxuICAgIGZ1bmN0aW9uIG1haW4odXNlcikge1xuICAgICAgcmV0dXJuIHVzZXIuZmlyc3RfbmFtZSArICcgJyArIHVzZXIubGFzdF9uYW1lO1xuICAgIH1cbiAgfVxufSkoKTsiLCIoZnVuY3Rpb24gKCkge1xuICAndXNlIHN0cmljdCc7XG5cbiAgYW5ndWxhclxuICAgIC5tb2R1bGUoJ2Jvb2tpbmcnKVxuICAgIC5maWx0ZXIoJ3JhbmdlJywgcmFuZ2UpO1xuXG4gIHJhbmdlLiRpbmplY3QgPSBbXTtcblxuICBmdW5jdGlvbiByYW5nZSgpIHtcbiAgICByZXR1cm4gbWFpbjtcblxuICAgIGZ1bmN0aW9uIG1haW4oaW5wdXQsIG1pbiwgbWF4LCBvcmRlcikge1xuICAgICAgbWluID0gcGFyc2VJbnQobWluKTtcbiAgICAgIG1heCA9IHBhcnNlSW50KG1heCk7XG5cbiAgICAgIGlmIChvcmRlciA9PT0gJ2Rlc2MnKSB7XG4gICAgICAgIGZvciAodmFyIGkgPSBtYXg7IGkgPj0gbWluOyBpLS0pIHtcbiAgICAgICAgICBpbnB1dC5wdXNoKGkpO1xuICAgICAgICB9XG4gICAgICB9IGVsc2Uge1xuICAgICAgICBmb3IgKHZhciBqID0gbWluOyBqIDw9IG1heDsgaisrKSB7XG4gICAgICAgICAgaW5wdXQucHVzaChqKTtcbiAgICAgICAgfVxuICAgICAgfVxuXG4gICAgICByZXR1cm4gaW5wdXQ7XG4gICAgfVxuICB9XG59KSgpOyIsIihmdW5jdGlvbigpe1xuICAndXNlIHN0cmljdCc7XG5cbiAgYW5ndWxhclxuICAgIC5tb2R1bGUoJ2Jvb2tpbmcnKVxuICAgIC5maWx0ZXIoJ3NleCcsIHNleCk7XG5cbiAgc2V4LiRpbmplY3QgPSBbXTtcblxuICBmdW5jdGlvbiBzZXgoKSB7XG4gICAgcmV0dXJuIG1haW47XG5cbiAgICBmdW5jdGlvbiBtYWluKHZhbHVlKSB7XG4gICAgICByZXR1cm4gdmFsdWUgPyAnTWFsZScgOiAnRmVtYWxlJztcbiAgICB9XG4gIH1cbn0pKCk7IiwiKGZ1bmN0aW9uICgpIHtcbiAgJ3VzZSBzdHJpY3QnO1xuXG4gIGFuZ3VsYXJcbiAgICAubW9kdWxlKCdib29raW5nJylcbiAgICAuZmlsdGVyKCdzdW1CeUtleScsIGZ1bmN0aW9uICgpIHtcbiAgICAgIHJldHVybiBmdW5jdGlvbiAoZGF0YSwga2V5KSB7XG4gICAgICAgIGlmICh0eXBlb2YoZGF0YSkgPT09ICd1bmRlZmluZWQnIHx8IHR5cGVvZihrZXkpID09PSAndW5kZWZpbmVkJykge1xuICAgICAgICAgIHJldHVybiAwO1xuICAgICAgICB9XG5cbiAgICAgICAgdmFyIHN1bSA9IDA7XG4gICAgICAgIGZvciAodmFyIGkgPSBkYXRhLmxlbmd0aCAtIDE7IGkgPj0gMDsgaS0tKSB7XG4gICAgICAgICAgc3VtICs9IHBhcnNlRmxvYXQoZGF0YVtpXVtrZXldKTtcbiAgICAgICAgfVxuXG4gICAgICAgIHJldHVybiBzdW07XG4gICAgICB9O1xuICAgIH0pO1xuXG59KSgpO1xuIiwiKGZ1bmN0aW9uKCl7XG4gICd1c2Ugc3RyaWN0JztcblxuICBhbmd1bGFyXG4gICAgLm1vZHVsZSgnYm9va2luZycpXG4gICAgLmZpbHRlcignc3dpbScsIHN3aW0pO1xuXG4gIHN3aW0uJGluamVjdCA9IFtdO1xuXG4gIGZ1bmN0aW9uIHN3aW0oKSB7XG4gICAgcmV0dXJuIG1haW47XG5cbiAgICBmdW5jdGlvbiBtYWluKGNoaWxkKSB7XG4gICAgICByZXR1cm4gY2hpbGQuY2FuX3N3aW0gPyAoY2hpbGQuY2FuX3N3aW0ubnVtYmVyICsgJyAtICcgICsgY2hpbGQuY2FuX3N3aW0ub3B0aW9uKSA6ICcnO1xuICAgIH1cbiAgfVxufSkoKTsiLCIoZnVuY3Rpb24gKCkge1xuICAndXNlIHN0cmljdCc7XG5cbiAgYW5ndWxhclxuICAgIC5tb2R1bGUoJ2Jvb2tpbmcnKVxuICAgIC5maWx0ZXIoJ3dlZWtkYXknLCB3ZWVrZGF5KTtcblxuICB3ZWVrZGF5LiRpbmplY3QgPSBbXTtcblxuICBmdW5jdGlvbiB3ZWVrZGF5KCkge1xuICAgIHJldHVybiBtYWluO1xuXG4gICAgZnVuY3Rpb24gbWFpbihkYXRlKSB7XG4gICAgICBpZiAoIShkYXRlIGluc3RhbmNlb2YgRGF0ZSkpIHtcbiAgICAgICAgZGF0ZSA9IG5ldyBEYXRlKGRhdGUpO1xuICAgICAgfVxuXG4gICAgICB2YXIgd2Vla2RheTtcblxuICAgICAgc3dpdGNoIChkYXRlLmdldERheSgpKSB7XG4gICAgICAgIGNhc2UgMDpcbiAgICAgICAgICB3ZWVrZGF5ID0gJ1N1bmRheSc7XG4gICAgICAgICAgYnJlYWs7XG4gICAgICAgIGNhc2UgMTpcbiAgICAgICAgICB3ZWVrZGF5ID0gJ01vbmRheSc7XG4gICAgICAgICAgYnJlYWs7XG4gICAgICAgIGNhc2UgMjpcbiAgICAgICAgICB3ZWVrZGF5ID0gJ1R1ZXNkYXknO1xuICAgICAgICAgIGJyZWFrO1xuICAgICAgICBjYXNlIDM6XG4gICAgICAgICAgd2Vla2RheSA9ICdXZWRuZXNkYXknO1xuICAgICAgICAgIGJyZWFrO1xuICAgICAgICBjYXNlIDQ6XG4gICAgICAgICAgd2Vla2RheSA9ICdUaHVyc2RheSc7XG4gICAgICAgICAgYnJlYWs7XG4gICAgICAgIGNhc2UgNTpcbiAgICAgICAgICB3ZWVrZGF5ID0gJ0ZyaWRheSc7XG4gICAgICAgICAgYnJlYWs7XG4gICAgICAgIGNhc2UgNjpcbiAgICAgICAgICB3ZWVrZGF5ID0gJ1NhdHVyZGF5JztcbiAgICAgICAgICBicmVhaztcblxuICAgICAgfVxuXG4gICAgICByZXR1cm4gd2Vla2RheTtcbiAgICB9XG4gIH1cbn0pKCk7IiwiKGZ1bmN0aW9uICgpIHtcbiAgJ3VzZSBzdHJpY3QnO1xuXG4gIGFuZ3VsYXJcbiAgICAubW9kdWxlKCdib29raW5nJylcbiAgICAuY29udHJvbGxlcignQ2hpbGREZXRhaWxzTW9kYWxDb250cm9sbGVyJywgQ2hpbGREZXRhaWxzTW9kYWxDb250cm9sbGVyKTtcblxuICBDaGlsZERldGFpbHNNb2RhbENvbnRyb2xsZXIuJGluamVjdCA9IFsnJHVpYk1vZGFsSW5zdGFuY2UnLCAnY2hpbGQnXTtcblxuICAvKiBAbmdJbmplY3QgKi9cbiAgZnVuY3Rpb24gQ2hpbGREZXRhaWxzTW9kYWxDb250cm9sbGVyKCR1aWJNb2RhbEluc3RhbmNlLCBjaGlsZCkge1xuICAgIC8qIGpzaGludCB2YWxpZHRoaXM6IHRydWUgKi9cbiAgICB2YXIgdm0gPSB0aGlzO1xuXG4gICAgdm0uY2hpbGQgPSBjaGlsZDtcblxuICAgIHZtLmNsb3NlID0gY2xvc2U7XG5cbiAgICAvLy8vLy8vLy8vLy8vLy8vLy8vLy9cblxuICAgIGZ1bmN0aW9uIGNsb3NlKCkge1xuICAgICAgJHVpYk1vZGFsSW5zdGFuY2UuY2xvc2UoKTtcbiAgICB9XG5cbiAgfVxufSkoKTtcbiIsIihmdW5jdGlvbiAoKSB7XG4gICd1c2Ugc3RyaWN0JztcblxuICBhbmd1bGFyXG4gICAgLm1vZHVsZSgnYm9va2luZycpXG4gICAgLmNvbnRyb2xsZXIoJ0NyZWF0ZU9yZGVyQ29udHJvbGxlcicsIENyZWF0ZU9yZGVyQ29udHJvbGxlcik7XG5cbiAgQ3JlYXRlT3JkZXJDb250cm9sbGVyLiRpbmplY3QgPSBbJyR1aWJNb2RhbCcsICdjaGlsZHJlbicsICdldmVudHMnLCAnb3JkZXJTZXJ2aWNlJywgJyRzdGF0ZScsICdhbGVydFNlcnZpY2UnLCAnaGVscGVyU2VydmljZScsICckZmlsdGVyJywgJ2NvdW50T3JkZXJzJ107XG5cbiAgLyogQG5nSW5qZWN0ICovXG4gIGZ1bmN0aW9uIENyZWF0ZU9yZGVyQ29udHJvbGxlcigkdWliTW9kYWwsIGNoaWxkcmVuLCBldmVudHMsIG9yZGVyU2VydmljZSwgJHN0YXRlLCBhbGVydFNlcnZpY2UsIGhlbHBlclNlcnZpY2UsICRmaWx0ZXIsIGNvdW50T3JkZXJzKSB7XG4gICAgLyoganNoaW50IHZhbGlkdGhpczogdHJ1ZSAqL1xuICAgIHZhciB2bSA9IHRoaXM7XG5cbiAgICBpZiAoIWNoaWxkcmVuLmxlbmd0aCkge1xuICAgICAgYWxlcnRTZXJ2aWNlLmVycm9yKCdQbGVhc2UgYWRkIHlvdXIgY2hpbGRyZW4hJyk7XG4gICAgICAkc3RhdGUuZ28oJ2Jvb2tpbmcuY2hpbGRyZW4uY3JlYXRlJyk7XG4gICAgfVxuXG4gICAgdm0uY291bnRPcmRlcnMgPSBjb3VudE9yZGVycztcbiAgICB2bS5jaGlsZHJlbiA9IGNoaWxkcmVuO1xuICAgIC8vIHZtLnByb2R1Y3RzID0gcHJvZHVjdHM7XG4gICAgLy8gdm0uYWxsVmFyaWFudHMgPSBpbml0VmFyaWFudHMoKTtcbiAgICB2bS5ldmVudHMgPSBbXTsvL2V2ZW50cztcblxuICAgIHZtLnJlcXVlc3QgPSBmYWxzZTtcblxuICAgIHZtLmZvcm0gPSB7XG4gICAgICBjaGlsZF9pZDogdW5kZWZpbmVkLFxuICAgICAgZXZlbnRfaWQ6IHVuZGVmaW5lZCxcbiAgICAgIGRhdGVzOiBbXVxuICAgIH07XG5cbiAgICB2bS5jcmVhdGUgPSBjcmVhdGU7XG4gICAgdm0uc2VsZWN0ZWRFdmVudCA9IHNlbGVjdGVkRXZlbnQ7XG4gICAgdm0uc2VsZWN0ZWRXZWVrID0gc2VsZWN0ZWRXZWVrO1xuICAgIHZtLnNlbGVjdGVkRGF5ID0gc2VsZWN0ZWREYXk7XG4gICAgdm0uc2VsZWN0ZWRDaGlsZCA9IHNlbGVjdGVkQ2hpbGQ7XG5cbiAgICAvLy8vLy8vLy8vLy8vLy8vLy8vLy8vL1xuXG4gICAgZnVuY3Rpb24gc2VsZWN0ZWRDaGlsZCgpIHtcbiAgICAgIHZhciBjaGlsZCA9ICRmaWx0ZXIoJ2ZpbHRlcicpKGNoaWxkcmVuLCB7aWQ6IHZtLmZvcm0uY2hpbGRfaWR9KVswXTtcblxuICAgICAgJHVpYk1vZGFsLm9wZW4oe1xuICAgICAgICB0ZW1wbGF0ZVVybDogJy9ib29raW5nL29yZGVycy9jaGlsZC1kZXRhaWxzLW1vZGFsLmh0bWwnLFxuICAgICAgICBjb250cm9sbGVyOiAnQ2hpbGREZXRhaWxzTW9kYWxDb250cm9sbGVyJyxcbiAgICAgICAgY29udHJvbGxlckFzOiAnU2hvd0NoaWxkQ3RybCcsXG4gICAgICAgIHNpemU6ICdsZycsXG4gICAgICAgIHJlc29sdmU6IHtcbiAgICAgICAgICBjaGlsZDogZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgcmV0dXJuIGNoaWxkO1xuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgfSk7XG5cbiAgICAgIHZtLmV2ZW50cyA9ICRmaWx0ZXIoJ2ZpbHRlcicpKGV2ZW50cywgZnVuY3Rpb24gKGV2ZW50KSB7XG4gICAgICAgIHZhciBhZ2UgPSAkZmlsdGVyKCdhZ2UnKShjaGlsZCwgZXZlbnQuc3RhcnRfZGF0ZSk7XG4gICAgICAgIHJldHVybiBldmVudC5taW5fYWdlIDw9IGFnZSAmJiBhZ2UgPD0gZXZlbnQubWF4X2FnZTtcbiAgICAgIH0pO1xuICAgIH1cblxuICAgIGZ1bmN0aW9uIGNyZWF0ZSgpIHtcbiAgICAgIGlmIChoZWxwZXJTZXJ2aWNlLlNob3dWYWxpZGF0aW9uRXJyb3JzKG9yZGVyU2VydmljZS52YWxpZGF0ZSh2bS5mb3JtLCBjaGlsZHJlbiwgZXZlbnRzKSkubGVuZ3RoKSB7XG4gICAgICAgIHJldHVybjtcbiAgICAgIH1cblxuICAgICAgdm0ucmVxdWVzdCA9IHRydWU7XG5cbiAgICAgIG9yZGVyU2VydmljZS5hZGQodm0uZm9ybSkudGhlbihmdW5jdGlvbiAoKSB7XG4gICAgICAgIGFsZXJ0U2VydmljZS5zdWNjZXNzKCdPcmRlciB3YXMgc2F2ZWQnKTtcblxuICAgICAgICAkc3RhdGUuZ28oJ14uXi5jb21wbGV0ZScpO1xuICAgICAgfSkuZmluYWxseShmdW5jdGlvbiAoKSB7XG4gICAgICAgIC8vdm0ucmVxdWVzdCA9IGZhbHNlO1xuICAgICAgfSk7XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gc2VsZWN0ZWRFdmVudCgpIHtcbiAgICAgIGlmICghdm0uZm9ybS5ldmVudF9pZCkge1xuICAgICAgICByZXR1cm47XG4gICAgICB9XG5cbiAgICAgIHZhciBldmVudCA9ICRmaWx0ZXIoJ2ZpbHRlcicpKHZtLmV2ZW50cywge2lkOiB2bS5mb3JtLmV2ZW50X2lkfSlbMF07XG5cbiAgICAgIGlmICghZXZlbnQuZXhjZXJwdCkge1xuICAgICAgICBzZXRFdmVudCgpO1xuICAgICAgICByZXR1cm47XG4gICAgICB9XG5cbiAgICAgICR1aWJNb2RhbC5vcGVuKHtcbiAgICAgICAgdGVtcGxhdGVVcmw6ICcvYm9va2luZy9vcmRlcnMvZXZlbnQtaW5mby1tb2RhbC5odG1sJyxcbiAgICAgICAgY29udHJvbGxlcjogJ0V2ZW50SW5mb0NvbnRyb2xsZXInLFxuICAgICAgICBjb250cm9sbGVyQXM6ICdFdmVudEluZm9DdHJsJyxcbiAgICAgICAgc2l6ZTogJ2xnJyxcbiAgICAgICAgcmVzb2x2ZToge1xuICAgICAgICAgIGV2ZW50OiBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICByZXR1cm4gZXZlbnQ7XG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICB9KVxuICAgICAgICAucmVzdWx0XG4gICAgICAgIC50aGVuKGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICBzZXRFdmVudCgpO1xuICAgICAgICB9LCBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgdm0uZm9ybS5ldmVudF9pZCA9IHVuZGVmaW5lZDtcbiAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gc2V0RXZlbnQoKSB7XG4gICAgICBoZWxwZXJTZXJ2aWNlLnB1c2hFdmVudERhdGVzKHZtLmV2ZW50cywgdm0uZm9ybSk7XG4gICAgICBhbGVydFNlcnZpY2Uuc3VjY2VzcygnQ2hhbmdlIGV2ZW50XFwncyBkYXRlcyB0byBldmVudCBkYXRlcycpO1xuICAgIH1cblxuICAgIGZ1bmN0aW9uIHNlbGVjdGVkV2Vlayh3ZWVrKSB7XG4gICAgICBpZiAod2Vlay5zZWxlY3RlZCkge1xuICAgICAgICBzZXRTZWxlY3RlZERhdGVzT2ZXZWVrKHdlZWssIHRydWUpO1xuICAgICAgICB3ZWVrLnNlbGVjdGVkQW55RGF0ZXMgPSB0cnVlO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgd2Vlay5zZWxlY3RlZEFueURhdGVzID0gZmFsc2U7XG4gICAgICAgIHNldFNlbGVjdGVkRGF0ZXNPZldlZWsod2VlaywgZmFsc2UpO1xuICAgICAgfVxuICAgIH1cblxuICAgIGZ1bmN0aW9uIHNlbGVjdGVkRGF5KHdlZWssIGRheSkge1xuICAgICAgaWYgKCFkYXkuc2VsZWN0ZWQgJiYgd2Vlay5zZWxlY3RlZCkge1xuICAgICAgICB3ZWVrLnNlbGVjdGVkID0gZmFsc2U7XG4gICAgICB9XG5cbiAgICAgIGlmIChkYXkuc2VsZWN0ZWQpIHtcbiAgICAgICAgd2Vlay5zZWxlY3RlZEFueURhdGVzID0gdHJ1ZTtcbiAgICAgICAgdmFyIGFsbERheXNPZldlZWtTZWxlY3RlZCA9IHRydWU7XG5cbiAgICAgICAgZm9yICh2YXIgaSA9IDAsIHdlZWtkYXk7ICh3ZWVrZGF5ID0gd2Vlay5kYXRlc1tpXSk7IGkrKykge1xuICAgICAgICAgIGlmICh3ZWVrZGF5LmFsbG93ICYmICF3ZWVrZGF5LnNlbGVjdGVkKSB7XG4gICAgICAgICAgICBhbGxEYXlzT2ZXZWVrU2VsZWN0ZWQgPSBmYWxzZTtcbiAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgIH1cbiAgICAgICAgfVxuXG4gICAgICAgIGlmIChhbGxEYXlzT2ZXZWVrU2VsZWN0ZWQpIHtcbiAgICAgICAgICB3ZWVrLnNlbGVjdGVkID0gdHJ1ZTtcbiAgICAgICAgfVxuICAgICAgfSBlbHNlIHtcbiAgICAgICAgLy9DaGVjayB0aGF0IGFueSBkYXkgaXMgc2VsZWN0ZWRcbiAgICAgICAgdmFyIHNlbGVjdGVkQW55RGF0ZSA9IGZhbHNlO1xuICAgICAgICBmb3IgKHZhciBqID0gMCwgZGF0ZTsgKGRhdGUgPSB3ZWVrLmRhdGVzW2pdKTsgaisrKSB7XG4gICAgICAgICAgaWYgKGRhdGUuc2VsZWN0ZWQpIHtcbiAgICAgICAgICAgIHNlbGVjdGVkQW55RGF0ZSA9IHRydWU7XG4gICAgICAgICAgICBicmVhaztcbiAgICAgICAgICB9XG4gICAgICAgIH1cblxuICAgICAgICB3ZWVrLnNlbGVjdGVkQW55RGF0ZXMgPSBzZWxlY3RlZEFueURhdGU7XG4gICAgICB9XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gc2V0U2VsZWN0ZWREYXRlc09mV2Vlayh3ZWVrLCB2YWx1ZSkge1xuICAgICAgZm9yICh2YXIgaSA9IDAsIGRheTsgKGRheSA9IHdlZWsuZGF0ZXNbaV0pOyBpKyspIHtcbiAgICAgICAgaWYgKGRheS5hbGxvdyAmJiAhZGF5LmZ1bGx5KSB7XG4gICAgICAgICAgZGF5LnNlbGVjdGVkID0gdmFsdWU7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG4gIH1cbn0pKCk7XG4iLCIoZnVuY3Rpb24gKCkge1xuICAndXNlIHN0cmljdCc7XG5cbiAgYW5ndWxhclxuICAgIC5tb2R1bGUoJ2Jvb2tpbmcnKVxuICAgIC5jb250cm9sbGVyKCdFZGl0T3JkZXJDb250cm9sbGVyJywgRWRpdE9yZGVyQ29udHJvbGxlcik7XG5cbiAgRWRpdE9yZGVyQ29udHJvbGxlci4kaW5qZWN0ID0gWydvcmRlcicsICdjaGlsZHJlbicsICdldmVudHMnLCAnb3JkZXJTZXJ2aWNlJywgJyRzdGF0ZScsICdhbGVydFNlcnZpY2UnLCAnaGVscGVyU2VydmljZSddO1xuXG4gIC8qIEBuZ0luamVjdCAqL1xuICBmdW5jdGlvbiBFZGl0T3JkZXJDb250cm9sbGVyKG9yZGVyLCBjaGlsZHJlbiwgZXZlbnRzLCBvcmRlclNlcnZpY2UsICRzdGF0ZSwgYWxlcnRTZXJ2aWNlLCBoZWxwZXJTZXJ2aWNlKSB7XG4gICAgLyoganNoaW50IHZhbGlkdGhpczogdHJ1ZSAqL1xuICAgIHZhciB2bSA9IHRoaXM7XG5cbiAgICB2bS5mb3JtID0gYW5ndWxhci5jb3B5KG9yZGVyKTtcbiAgICB2bS5jaGlsZHJlbiA9IGNoaWxkcmVuO1xuICAgIHZtLmV2ZW50cyA9IGV2ZW50cztcblxuICAgIHZtLnVwZGF0ZSA9IHVwZGF0ZTtcbiAgICB2bS5zZWxlY3RlZEV2ZW50ID0gc2VsZWN0ZWRFdmVudDtcblxuICAgIC8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vXG5cbiAgICBmdW5jdGlvbiB1cGRhdGUoKSB7XG4gICAgICBpZihoZWxwZXJTZXJ2aWNlLlNob3dWYWxpZGF0aW9uRXJyb3JzKG9yZGVyU2VydmljZS52YWxpZGF0ZSh2bS5mb3JtKSkubGVuZ3RoKSB7XG4gICAgICAgIHJldHVybjtcbiAgICAgIH1cblxuICAgICAgb3JkZXJTZXJ2aWNlLnVwZGF0ZSh2bS5mb3JtLl9pZCwgdm0uZm9ybSkudGhlbihmdW5jdGlvbiAoKSB7XG4gICAgICAgIGFsZXJ0U2VydmljZS5zdWNjZXNzKCdUaGUgb3JkZXIgd2FzIHVwZGF0ZWQhJyk7XG5cbiAgICAgICAgJHN0YXRlLmdvKCdeLmluZGV4Jyk7XG4gICAgICB9KTtcbiAgICB9XG5cbiAgICBmdW5jdGlvbiBzZWxlY3RlZEV2ZW50KCkge1xuICAgICAgaGVscGVyU2VydmljZS5wdXNoRXZlbnREYXRlcyh2bS5ldmVudHMsIHZtLmZvcm0pO1xuICAgIH1cblxuXG4gIH1cbn0pKCk7XG4iLCIoZnVuY3Rpb24gKCkge1xuICAndXNlIHN0cmljdCc7XG5cbiAgYW5ndWxhclxuICAgIC5tb2R1bGUoJ2Jvb2tpbmcnKVxuICAgIC5jb250cm9sbGVyKCdFdmVudEluZm9Db250cm9sbGVyJywgRXZlbnRJbmZvQ29udHJvbGxlcik7XG5cbiAgRXZlbnRJbmZvQ29udHJvbGxlci4kaW5qZWN0ID0gWyckdWliTW9kYWxJbnN0YW5jZScsICdldmVudCddO1xuXG4gIC8qIEBuZ0luamVjdCAqL1xuICBmdW5jdGlvbiBFdmVudEluZm9Db250cm9sbGVyKCR1aWJNb2RhbEluc3RhbmNlLCBldmVudCkge1xuICAgIC8qIGpzaGludCB2YWxpZHRoaXM6IHRydWUgKi9cbiAgICB2YXIgdm0gPSB0aGlzO1xuXG4gICAgdm0uZXZlbnQgPSBldmVudDtcblxuICAgIHZtLmNvbmZpcm0gPSBjb25maXJtO1xuICAgIHZtLmNsb3NlID0gY2xvc2U7XG5cbiAgICAvLy8vLy8vLy8vXG5cbiAgICBmdW5jdGlvbiBjb25maXJtKCkge1xuICAgICAgJHVpYk1vZGFsSW5zdGFuY2UuY2xvc2UoKTtcbiAgICB9XG5cbiAgICBmdW5jdGlvbiBjbG9zZSgpIHtcbiAgICAgICR1aWJNb2RhbEluc3RhbmNlLmRpc21pc3MoKTtcbiAgICB9XG4gIH1cbn0pKCk7XG4iLCIoZnVuY3Rpb24gKCkge1xuICAndXNlIHN0cmljdCc7XG5cbiAgYW5ndWxhclxuICAgIC5tb2R1bGUoJ2Jvb2tpbmcnKVxuICAgIC5jb250cm9sbGVyKCdPcmRlcnNDb250cm9sbGVyJywgT3JkZXJzQ29udHJvbGxlcik7XG5cbiAgT3JkZXJzQ29udHJvbGxlci4kaW5qZWN0ID0gWydvcmRlcnMnLCAnb3JkZXJTZXJ2aWNlJywgJ3RvYXN0ZXInXTtcblxuICAvKiBAbmdJbmplY3QgKi9cbiAgZnVuY3Rpb24gT3JkZXJzQ29udHJvbGxlcihvcmRlcnMsIG9yZGVyU2VydmljZSwgdG9hc3Rlcikge1xuICAgIC8qIGpzaGludCB2YWxpZHRoaXM6IHRydWUgKi9cbiAgICB2YXIgdm0gPSB0aGlzO1xuXG4gICAgdm0ub3JkZXJzID0gb3JkZXJzO1xuXG4gICAgdm0ucmVtb3ZlID0gcmVtb3ZlO1xuXG4gICAgLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy9cblxuICAgIGZ1bmN0aW9uIHJlbW92ZShpZCkge1xuICAgICAgb3JkZXJTZXJ2aWNlLnJlbW92ZShpZCkudGhlbihmdW5jdGlvbiAoKSB7XG4gICAgICAgIC8vUmVmcmVzaCBkYXRhXG4gICAgICAgIG9yZGVyU2VydmljZS5hbGwoKS50aGVuKGZ1bmN0aW9uIChvcmRlcnMpIHtcbiAgICAgICAgICB2bS5vcmRlcnMgPSBvcmRlcnM7XG5cbiAgICAgICAgICB0b2FzdGVyLnN1Y2Nlc3MoJ1RoZSBvcmRlciB3YXMgcmVtb3ZlZCcpO1xuICAgICAgICB9KTtcbiAgICAgIH0pO1xuICAgIH1cblxuICB9XG59KSgpO1xuIiwiKGZ1bmN0aW9uICgpIHtcbiAgJ3VzZSBzdHJpY3QnO1xuXG4gIGFuZ3VsYXJcbiAgICAubW9kdWxlKCdib29raW5nJylcbiAgICAuY29udHJvbGxlcignU2hvd09yZGVyQ29udHJvbGxlcicsIFNob3dPcmRlckNvbnRyb2xsZXIpO1xuXG4gIFNob3dPcmRlckNvbnRyb2xsZXIuJGluamVjdCA9IFsnb3JkZXInXTtcblxuICAvKiBAbmdJbmplY3QgKi9cbiAgZnVuY3Rpb24gU2hvd09yZGVyQ29udHJvbGxlcihvcmRlcikge1xuICAgIC8qIGpzaGludCB2YWxpZHRoaXM6IHRydWUgKi9cbiAgICB2YXIgdm0gPSB0aGlzO1xuXG4gICAgdm0ub3JkZXIgPSBvcmRlcjtcbiAgfVxufSkoKTtcbiIsIihmdW5jdGlvbiAoKSB7XG4gICd1c2Ugc3RyaWN0JztcblxuICBhbmd1bGFyXG4gICAgLm1vZHVsZSgnYm9va2luZycpXG4gICAgLmNvbnRyb2xsZXIoJ1Byb2ZpbGVDb250cm9sbGVyJywgUHJvZmlsZUNvbnRyb2xsZXIpO1xuXG4gIFByb2ZpbGVDb250cm9sbGVyLiRpbmplY3QgPSBbJ3Byb2ZpbGUnLCAncHJvZmlsZVNlcnZpY2UnLCAnJHN0YXRlJywgJ3RvYXN0ZXInLCAnYWNjb3N0cycsICdib29sZWFuVmFsdWVzJywgJ2NvdW50cmllcycsICckZmlsdGVyJ107XG5cbiAgLyogQG5nSW5qZWN0ICovXG4gIGZ1bmN0aW9uIFByb2ZpbGVDb250cm9sbGVyKHByb2ZpbGUsIHByb2ZpbGVTZXJ2aWNlLCAkc3RhdGUsIHRvYXN0ZXIsIGFjY29zdHMsIGJvb2xlYW5WYWx1ZXMsIGNvdW50cmllcywgJGZpbHRlcikge1xuICAgIC8qIGpzaGludCB2YWxpZHRoaXM6IHRydWUgKi9cbiAgICB2YXIgdm0gPSB0aGlzO1xuXG4gICAgdm0ucHJvZmlsZSA9IGFuZ3VsYXIuY29weShwcm9maWxlKTtcbiAgICB2bS5mb3JtID0gYW5ndWxhci5jb3B5KHZtLnByb2ZpbGUpO1xuICAgIHZtLmFjY29zdHMgPSBhY2Nvc3RzO1xuICAgIHZtLmJvb2xlYW5WYWx1ZXMgPSBib29sZWFuVmFsdWVzO1xuICAgIHZtLmNvdW50cmllcyA9IGNvdW50cmllcztcbiAgICB2bS5yZXF1ZXN0ID0gZmFsc2U7XG5cbiAgICB2bS51cGRhdGUgPSB1cGRhdGU7XG4gICAgdm0ucmVzZXQgPSByZXNldDtcblxuICAgIC8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vXG5cbiAgICBmdW5jdGlvbiB1cGRhdGUoKSB7XG4gICAgICB2YXIgZm9ybSA9IGFuZ3VsYXIuY29weSh2bS5mb3JtKTtcbiAgICAgIGZvcm0uY291bnRyeV9pZCA9IGZvcm0uY291bnRyeS5pZDtcblxuICAgICAgdm0ucmVxdWVzdCA9IHRydWU7XG5cbiAgICAgIHByb2ZpbGVTZXJ2aWNlLnVwZGF0ZShmb3JtKS50aGVuKGZ1bmN0aW9uICgpIHtcbiAgICAgICAgdm0ucHJvZmlsZSA9IHZtLmZvcm07XG5cbiAgICAgICAgdG9hc3Rlci5zdWNjZXNzKCdQcm9maWxlIHVwZGF0ZWQhJyk7XG5cbiAgICAgICAgJHN0YXRlLmdvKCdeLnNob3cnKTtcbiAgICAgIH0pLmZpbmFsbHkoZnVuY3Rpb24gKCkge1xuICAgICAgICB2bS5yZXF1ZXN0ID0gZmFsc2U7XG4gICAgICB9KTtcbiAgICB9XG5cbiAgICBmdW5jdGlvbiByZXNldCgpIHtcbiAgICAgIHZtLmZvcm0gPSBhbmd1bGFyLmNvcHkodm0ucHJvZmlsZSk7XG4gICAgfVxuICB9XG59KSgpO1xuIiwiKGZ1bmN0aW9uKCkge1xuICAndXNlIHN0cmljdCc7XG5cbiAgYW5ndWxhclxuICAgIC5tb2R1bGUoJ2Jvb2tpbmcnKVxuICAgIC5zZXJ2aWNlKCdhbGVydFNlcnZpY2UnLCBhbGVydFNlcnZpY2UpO1xuXG4gIGFsZXJ0U2VydmljZS4kaW5qZWN0ID0gWyd0b2FzdGVyJ107XG5cbiAgLyogQG5nSW5qZWN0ICovXG4gIGZ1bmN0aW9uIGFsZXJ0U2VydmljZSAodG9hc3Rlcikge1xuICAgIC8qIGpzaGludCB2YWxpZHRoaXM6IHRydWUgKi9cbiAgICB2YXIgdm0gPSB0aGlzO1xuXG4gICAgdm0ucG9wID0gcG9wO1xuICAgIHZtLnN1Y2Nlc3MgPSBzdWNjZXNzO1xuICAgIHZtLmVycm9yID0gZXJyb3I7XG4gICAgdm0uaW5mbyA9IGluZm87XG4gICAgdm0uc2hvd1Jlc3BvbnNlRXJyb3IgPSBzaG93UmVzcG9uc2VFcnJvcjtcblxuICAgIGZ1bmN0aW9uIHBvcChkYXRhKSB7XG4gICAgICB0b2FzdGVyLnBvcChkYXRhKTtcbiAgICB9XG5cbiAgICBmdW5jdGlvbiBzdWNjZXNzIChkYXRhKSB7XG4gICAgICBkYXRhID0gcHJlcGFyZURhdGEoZGF0YSwgJ1N1Y2Nlc3MnKTtcbiAgICAgIHRvYXN0ZXIuc3VjY2VzcyhkYXRhKTtcbiAgICB9XG5cbiAgICBmdW5jdGlvbiBpbmZvKGRhdGEpIHtcbiAgICAgIHByZXBhcmVEYXRhKGRhdGEsICdJbmZvJyk7XG4gICAgICB0b2FzdGVyLmluZm8oZGF0YSk7XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gZXJyb3IoZGF0YSkge1xuICAgICAgcHJlcGFyZURhdGEoZGF0YSwgJ0Vycm9yJyk7XG4gICAgICB0b2FzdGVyLmVycm9yKGRhdGEpO1xuICAgIH1cblxuICAgIGZ1bmN0aW9uIHNob3dSZXNwb25zZUVycm9yKHJlc3BvbnNlLCB0aXRsZSkge1xuICAgICAgZm9yKHZhciBrZXkgaW4gcmVzcG9uc2UpIHtcbiAgICAgICAgaWYgKHJlc3BvbnNlLmhhc093blByb3BlcnR5KGtleSkgJiYgKHJlc3BvbnNlW2tleV0gaW5zdGFuY2VvZiBBcnJheSkpIHtcbiAgICAgICAgICBmb3IodmFyIGkgaW4gcmVzcG9uc2Vba2V5XSkge1xuICAgICAgICAgICAgaWYgKHJlc3BvbnNlW2tleV0uaGFzT3duUHJvcGVydHkoaSkpIHtcbiAgICAgICAgICAgICAgZXJyb3Ioe1xuICAgICAgICAgICAgICAgIHRpdGxlOiB0aXRsZSA/IHRpdGxlIDogJ0Vycm9yJyxcbiAgICAgICAgICAgICAgICBib2R5OiByZXNwb25zZVtrZXldW2ldXG4gICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH1cbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICBlcnJvcih7XG4gICAgICAgICAgICB0aXRsZTogdGl0bGUsXG4gICAgICAgICAgICBib2R5OiByZXNwb25zZVtrZXldXG4gICAgICAgICAgfSk7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG5cbiAgICBmdW5jdGlvbiBwcmVwYXJlRGF0YShkYXRhLCB0aXRsZSkge1xuICAgICAgaWYgKCEoZGF0YSBpbnN0YW5jZW9mIE9iamVjdCkpIHtcbiAgICAgICAgZGF0YSA9IHtcbiAgICAgICAgICBib2R5OiBkYXRhXG4gICAgICAgIH07XG4gICAgICB9XG5cbiAgICAgIGRhdGEudGl0bGUgPSBkYXRhLnRpdGxlID8gZGF0YS50aXRsZSA6IHRpdGxlO1xuXG4gICAgICByZXR1cm4gZGF0YTtcbiAgICB9XG4gIH1cblxuXG59KSgpO1xuIiwiKGZ1bmN0aW9uICgpIHtcbiAgJ3VzZSBzdHJpY3QnO1xuXG4gIGFuZ3VsYXJcbiAgICAubW9kdWxlKCdib29raW5nJylcbiAgICAuc2VydmljZSgnYm9va2luZ1NlcnZpY2UnLCBib29raW5nU2VydmljZSk7XG5cbiAgYm9va2luZ1NlcnZpY2UuJGluamVjdCA9IFsnJGh0dHAnLCAnaGVscGVyU2VydmljZSddO1xuXG4gIC8qIEBuZ0luamVjdCAqL1xuICBmdW5jdGlvbiBib29raW5nU2VydmljZSgkaHR0cCwgaGVscGVyU2VydmljZSkge1xuICAgIC8qIGpzaGludCB2YWxpZHRoaXM6IHRydWUgKi9cbiAgICB2YXIgdm0gPSB0aGlzO1xuXG4gICAgdm0uYm9vayA9IGJvb2s7XG4gICAgdm0uY2FsY3VsYXRlID0gY2FsY3VsYXRlO1xuICAgIHZtLmNoZWNrQ291cG9uID0gY2hlY2tDb3Vwb247XG4gICAgdm0ucGF5bWVudFN5c3RlbXMgPSBwYXltZW50U3lzdGVtcztcblxuICAgIC8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy9cblxuICAgIGZ1bmN0aW9uIGJvb2soZGF0YSkge1xuICAgICAgcmV0dXJuICRodHRwLnBvc3QoJy9hcGkvYm9va2luZycsIGhlbHBlclNlcnZpY2UucHJlcGFyZUJvb2tpbmdGb3JSZXF1ZXN0KGRhdGEpKS50aGVuKGhlbHBlclNlcnZpY2UuZmV0Y2hSZXNwb25zZSk7XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gY2FsY3VsYXRlKG9yZGVycykge1xuICAgICAgcmV0dXJuICRodHRwLnBvc3QoJy9hcGkvYm9va2luZy9jYWxjdWxhdGUnLCB7b3JkZXJzOiBvcmRlcnN9KS50aGVuKGhlbHBlclNlcnZpY2UuZmV0Y2hSZXNwb25zZSkudGhlbihmdW5jdGlvbiAoZGF0YSkge1xuICAgICAgICByZXR1cm4gZGF0YTtcbiAgICAgIH0pO1xuICAgIH1cblxuICAgIGZ1bmN0aW9uIGNoZWNrQ291cG9uKGNvdXBvbikge1xuICAgICAgcmV0dXJuICRodHRwLmdldCgnL2FwaS9ib29raW5nL2NvdXBvbnMvJyArIGNvdXBvbikudGhlbihoZWxwZXJTZXJ2aWNlLmZldGNoUmVzcG9uc2UpO1xuICAgIH1cbiAgICBcbiAgICBmdW5jdGlvbiBwYXltZW50U3lzdGVtcygpIHtcbiAgICAgIHJldHVybiAkaHR0cC5nZXQoJy9hcGkvcGF5bWVudC1zeXN0ZW1zJykudGhlbihoZWxwZXJTZXJ2aWNlLmZldGNoUmVzcG9uc2UpO1xuICAgIH1cbiAgfVxufSkoKTtcbiIsIihmdW5jdGlvbiAoKSB7XG4gICd1c2Ugc3RyaWN0JztcblxuICBhbmd1bGFyXG4gICAgLm1vZHVsZSgnYm9va2luZycpXG4gICAgLnNlcnZpY2UoJ2NoaWxkU2VydmljZScsIGNoaWxkU2VydmljZSk7XG5cbiAgY2hpbGRTZXJ2aWNlLiRpbmplY3QgPSBbJyRyZXNvdXJjZScsICckaHR0cCcsICdoZWxwZXJTZXJ2aWNlJ107XG5cbiAgLyogQG5nSW5qZWN0ICovXG4gIGZ1bmN0aW9uIGNoaWxkU2VydmljZSgkcmVzb3VyY2UsICRodHRwLCBoZWxwZXJTZXJ2aWNlKSB7XG4gICAgLyoganNoaW50IHZhbGlkdGhpczogdHJ1ZSAqL1xuICAgIHZhciB2bSA9IHRoaXM7XG5cbiAgICB2YXIgQ2hpbGQgPSAkcmVzb3VyY2UoJy9hcGkvY2hpbGRyZW4vOmNoaWxkSWQnLCB7Y2hpbGRJZDogJ0BpZCd9LCB7XG4gICAgICB1cGRhdGU6IHttZXRob2Q6ICdQVVQnfVxuICAgIH0pO1xuXG4gICAgdm0uYWxsID0gYWxsO1xuICAgIHZtLmdldCA9IGdldDtcbiAgICB2bS5zdG9yZSA9IHN0b3JlO1xuICAgIHZtLnVwZGF0ZSA9IHVwZGF0ZTtcbiAgICB2bS5yZW1vdmUgPSByZW1vdmU7XG4gICAgdm0uc3dpbU9wdGlvbnMgPSBzd2ltT3B0aW9ucztcbiAgICB2bS55ZWFyc0luU2Nob29sID0geWVhcnNJblNjaG9vbDtcblxuICAgIC8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vXG5cbiAgICBmdW5jdGlvbiB5ZWFyc0luU2Nob29sKCkge1xuICAgICAgcmV0dXJuICRodHRwLmdldCgnL2FwaS95ZWFycy1pbi1zY2hvb2wnKS50aGVuKGhlbHBlclNlcnZpY2UuZmV0Y2hSZXNwb25zZSk7XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gc3dpbU9wdGlvbnMoKSB7XG4gICAgICByZXR1cm4gJGh0dHAuZ2V0KCcvYXBpL3N3aW0tb3B0aW9ucycpLnRoZW4oaGVscGVyU2VydmljZS5mZXRjaFJlc3BvbnNlKTtcbiAgICB9XG5cbiAgICBmdW5jdGlvbiBhbGwoKSB7XG4gICAgICByZXR1cm4gQ2hpbGQucXVlcnkoKVxuICAgICAgICAgIC4kcHJvbWlzZVxuICAgICAgICAgIC50aGVuKGZ1bmN0aW9uIChjaGlsZHJlbikge1xuICAgICAgICAgICAgZm9yKHZhciBpID0gMCwgY2hpbGQ7IChjaGlsZCA9IGNoaWxkcmVuW2ldKTsgaSsrKSB7XG4gICAgICAgICAgICAgIGNoaWxkLmJpcnRoZGF5ID0gbW9tZW50KGNoaWxkLmJpcnRoZGF5KS50b0RhdGUoKTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgcmV0dXJuIGNoaWxkcmVuO1xuICAgICAgICAgIH0pO1xuICAgIH1cblxuICAgIGZ1bmN0aW9uIGdldChpZCkge1xuICAgICAgcmV0dXJuIENoaWxkLmdldCh7Y2hpbGRJZDogaWR9KVxuICAgICAgICAuJHByb21pc2VcbiAgICAgICAgLnRoZW4oZnVuY3Rpb24gKGNoaWxkKSB7XG4gICAgICAgICAgY2hpbGQuYmlydGhkYXkgPSBtb21lbnQoY2hpbGQuYmlydGhkYXkpLnRvRGF0ZSgpO1xuXG4gICAgICAgICAgcmV0dXJuIGNoaWxkO1xuICAgICAgICB9KTtcbiAgICB9XG5cbiAgICBmdW5jdGlvbiBzdG9yZShkYXRhKSB7XG4gICAgICByZXR1cm4gQ2hpbGQuc2F2ZShoZWxwZXJTZXJ2aWNlLnByZXBhcmVEYXRlc0ZvclJlcXVlc3QoZGF0YSwgWydiaXJ0aGRheSddLCB0cnVlKSkuJHByb21pc2U7XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gdXBkYXRlKGRhdGEpIHtcbiAgICAgIHJldHVybiBDaGlsZC51cGRhdGUoaGVscGVyU2VydmljZS5wcmVwYXJlRGF0ZXNGb3JSZXF1ZXN0KGRhdGEsIFsnYmlydGhkYXknXSwgdHJ1ZSkpLiRwcm9taXNlO1xuICAgIH1cblxuICAgIGZ1bmN0aW9uIHJlbW92ZShpZCkge1xuICAgICAgcmV0dXJuIENoaWxkLmRlbGV0ZSh7Y2hpbGRJZDogaWR9KS4kcHJvbWlzZTtcbiAgICB9XG4gIH1cbn0pKCk7XG4iLCIoZnVuY3Rpb24oKSB7XG4gICd1c2Ugc3RyaWN0JztcblxuICBhbmd1bGFyXG4gICAgLm1vZHVsZSgnYm9va2luZycpXG4gICAgLnNlcnZpY2UoJ2V2ZW50U2VydmljZScsIGV2ZW50U2VydmljZSk7XG5cbiAgZXZlbnRTZXJ2aWNlLiRpbmplY3QgPSBbJyRodHRwJywgJ2hlbHBlclNlcnZpY2UnXTtcblxuICAvKiBAbmdJbmplY3QgKi9cbiAgZnVuY3Rpb24gZXZlbnRTZXJ2aWNlKCRodHRwLCBoZWxwZXJTZXJ2aWNlKSB7XG4gICAgLyoganNoaW50IHZhbGlkdGhpczogdHJ1ZSAqL1xuICAgIHZhciB2bSA9IHRoaXM7XG5cbiAgICB2bS5hbGwgPSBhbGw7XG5cbiAgICAvLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy9cblxuICAgIGZ1bmN0aW9uIGFsbCgpIHtcbiAgICAgIHJldHVybiAkaHR0cC5nZXQoJy9hcGkvZXZlbnRzJylcbiAgICAgICAgLnRoZW4oaGVscGVyU2VydmljZS5mZXRjaFJlc3BvbnNlKVxuICAgICAgICAudGhlbihmdW5jdGlvbiAoZXZlbnRzKSB7XG4gICAgICAgICAgZm9yKHZhciBpID0gMCwgZXZlbnQ7IChldmVudCA9IGV2ZW50c1tpXSk7IGkrKykge1xuICAgICAgICAgICAgZXZlbnQuc3RhcnRfZGF0ZSA9IG1vbWVudChldmVudC5zdGFydF9kYXRlKTtcbiAgICAgICAgICAgIGV2ZW50LmVuZF9kYXRlID0gbW9tZW50KGV2ZW50LmVuZF9kYXRlKTtcbiAgICAgICAgICB9XG5cbiAgICAgICAgICByZXR1cm4gZXZlbnRzO1xuICAgICAgICB9KTtcbiAgICB9XG4gIH1cbn0pKCk7XG4iLCIoZnVuY3Rpb24gKCkge1xuICAndXNlIHN0cmljdCc7XG5cbiAgYW5ndWxhclxuICAgIC5tb2R1bGUoJ2Jvb2tpbmcnKVxuICAgIC5zZXJ2aWNlKCdoZWxwZXJTZXJ2aWNlJywgaGVscGVyU2VydmljZSk7XG5cbiAgLyogQG5nSW5qZWN0ICovXG4gIGZ1bmN0aW9uIGhlbHBlclNlcnZpY2UoJGZpbHRlciwgYWxlcnRTZXJ2aWNlLCBzdG9yZSkge1xuICAgIC8qIGpzaGludCB2YWxpZHRoaXM6IHRydWUgKi9cbiAgICB2YXIgdm0gPSB0aGlzO1xuXG4gICAgdm0uZmV0Y2hSZXNwb25zZSA9IGZldGNoUmVzcG9uc2U7XG4gICAgdm0ucHVzaEV2ZW50RGF0ZXMgPSBwdXNoRXZlbnREYXRlcztcbiAgICB2bS5TaG93VmFsaWRhdGlvbkVycm9ycyA9IFNob3dWYWxpZGF0aW9uRXJyb3JzO1xuICAgIHZtLnJlZGlyZWN0ID0gcmVkaXJlY3Q7XG4gICAgdm0ucHJlcGFyZU9yZGVycyA9IHByZXBhcmVPcmRlcnM7XG4gICAgdm0ucHJlcGFyZVByb2R1Y3RzID0gcHJlcGFyZVByb2R1Y3RzO1xuICAgIHZtLnByZXBhcmVCb29raW5nRm9yUmVxdWVzdCA9IHByZXBhcmVCb29raW5nRm9yUmVxdWVzdDtcbiAgICB2bS5wcmVwYXJlRGF0ZXNGb3JSZXF1ZXN0ID0gcHJlcGFyZURhdGVzRm9yUmVxdWVzdDtcblxuICAgIC8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy9cblxuICAgIGZ1bmN0aW9uIHByZXBhcmVPcmRlcnMob3JkZXJzKVxuICAgIHtcbiAgICAgIHZhciBfb3JkZXJzID0gW107XG5cbiAgICAgIGZvciAodmFyIGkgPSAwLCBvcmRlcjsgKG9yZGVyID0gb3JkZXJzW2ldKTsgaSsrKSB7XG4gICAgICAgIHZhciBzZWxlY3RlZERhdGVzID0gW107XG4gICAgICAgIGZvciAodmFyIGogPSAwLCB3ZWVrOyAod2VlayA9IG9yZGVyLndlZWtzW2pdKTsgaisrKSB7XG4gICAgICAgICAgZm9yICh2YXIgayA9IDAsIGRheTsgKGRheSA9IHdlZWsuZGF0ZXNba10pOyBrKyspIHtcbiAgICAgICAgICAgIGlmIChkYXkuc2VsZWN0ZWQpIHtcbiAgICAgICAgICAgICAgdmFyIGRhdGUgPSBuZXcgRGF0ZShkYXkuZGF0ZSk7XG4gICAgICAgICAgICAgIGlmKCEgKGRhdGUuZ2V0TWludXRlcygpIHx8IGRhdGUuZ2V0SG91cnMoKSkpIHtcbiAgICAgICAgICAgICAgICBkYXRlLnNldE1pbnV0ZXMoZGF0ZS5nZXRNaW51dGVzKCkgLSBkYXRlLmdldFRpbWV6b25lT2Zmc2V0KCkpO1xuICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgc2VsZWN0ZWREYXRlcy5wdXNoKGRhdGUpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH1cbiAgICAgICAgfVxuXG4gICAgICAgIF9vcmRlcnMucHVzaCh7XG4gICAgICAgICAgX2lkOiBvcmRlci5faWQsXG4gICAgICAgICAgY2hpbGRfaWQ6IG9yZGVyLmNoaWxkX2lkLFxuICAgICAgICAgIGV2ZW50X2lkOiBvcmRlci5ldmVudF9pZCxcbiAgICAgICAgICBzZWxlY3RlZERhdGVzOiBzZWxlY3RlZERhdGVzLFxuICAgICAgICAgIHZhcmlhbnRzOiBvcmRlci52YXJpYW50c1xuICAgICAgICB9KTtcbiAgICAgIH1cblxuICAgICAgcmV0dXJuIF9vcmRlcnM7XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gcmVkaXJlY3QodXJsLCBtZXRob2QsIGZpZWxkcykge1xuICAgICAgJCgnPGZvcm0+Jywge1xuICAgICAgICBtZXRob2Q6IG1ldGhvZCxcbiAgICAgICAgYWN0aW9uOiB1cmxcbiAgICAgIH0pLmFwcGVuZChmaWVsZHMpLmFwcGVuZFRvKCdib2R5Jykuc3VibWl0KCk7XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gZmV0Y2hSZXNwb25zZShkYXRhKSB7XG4gICAgICByZXR1cm4gZGF0YS5kYXRhO1xuICAgIH1cblxuICAgIGZ1bmN0aW9uIFNob3dWYWxpZGF0aW9uRXJyb3JzKGVycm9ycykge1xuICAgICAgaWYgKGVycm9ycy5sZW5ndGgpIHtcbiAgICAgICAgZm9yICh2YXIgaSA9IDAsIGVycm9yOyAoZXJyb3IgPSBlcnJvcnNbaV0pOyBpKyspIHtcbiAgICAgICAgICBhbGVydFNlcnZpY2UuZXJyb3IoZXJyb3IubWVzc2FnZSk7XG4gICAgICAgIH1cbiAgICAgIH1cblxuICAgICAgcmV0dXJuIGVycm9ycztcbiAgICB9XG5cbiAgICBmdW5jdGlvbiBleGlzdHNEYXkoZGF0ZXMsIGRhdGUpIHtcbiAgICAgIGRhdGUuc2V0SG91cnMoMCk7XG4gICAgICBkYXRlLnNldE1pbnV0ZXMoMCk7XG5cbiAgICAgIHJldHVybiAkZmlsdGVyKCdmaWx0ZXInKShkYXRlcywgZnVuY3Rpb24gKGRheSkge1xuICAgICAgICByZXR1cm4gbW9tZW50KGRhdGUpLmRpZmYobW9tZW50KGRheS5kYXRlKSkgPT09IDA7XG4gICAgICB9KS5sZW5ndGg7XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gZmluZEV2ZW50KGV2ZW50cywgZXZlbnRJZCkge1xuICAgICAgZm9yKHZhciBpID0gMCwgZXZlbnQ7IChldmVudCA9IGV2ZW50c1tpXSk7IGkrKykge1xuICAgICAgICBpZihldmVudC5pZCA9PT0gZXZlbnRJZCkge1xuICAgICAgICAgIHJldHVybiBhbmd1bGFyLmNvcHkoZXZlbnQpO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gcHVzaEV2ZW50RGF0ZXMoZXZlbnRzLCBmb3JtKSB7XG4gICAgICAvL1ByZXBhcmF0aW9uIGZvciBlZGl0IG9yZGVyXG4gICAgICAvKmlmIChmb3JtLndlZWtzKSB7XG4gICAgICAgdmFyIHNlbGVjdGVkRGF0ZXMgPSBbXTtcblxuICAgICAgIGZvciAodmFyIGsgPSAwLCBwcmV2aW91c1dlZWs7IChwcmV2aW91c1dlZWsgPSBmb3JtLndlZWtzW2tdKTsgaysrKSB7XG4gICAgICAgZm9yICh2YXIgcCA9IDAsIGRheTsgKGRheSA9IHByZXZpb3VzV2Vlay5kYXRlc1twXSk7IHArKykge1xuICAgICAgIGlmIChkYXkuc2VsZWN0ZWQpIHtcbiAgICAgICBzZWxlY3RlZERhdGVzLnB1c2goZGF5KTtcbiAgICAgICB9XG4gICAgICAgfVxuICAgICAgIH1cbiAgICAgICB9Ki9cblxuICAgICAgLy9GaW5kIHRoZSBldmVudFxuICAgICAgdmFyIGV2ZW50ID0gZmluZEV2ZW50KGV2ZW50cywgZm9ybS5ldmVudF9pZCk7XG5cbiAgICAgIGZvcm0uZXZlbnQgPSBldmVudDtcbiAgICAgIGZvcm0ud2Vla3MgPSBbXTtcblxuICAgICAgdmFyIHN0YXJ0RGF0ZSA9IGFuZ3VsYXIuY29weShldmVudC5zdGFydF9kYXRlKTtcblxuICAgICAgLy8gUmVtb3ZlIHNhdHVyZGF5IGFuZCBzdW5kYXkgZnJvbSBwZXJpb2QgaWYgZnJvbSB0aGlzIHN0YXJ0LiBTdW5kYXkgaXMgMFxuICAgICAgaWYgKHN0YXJ0RGF0ZS53ZWVrZGF5KCkgPT09IDApIHtcbiAgICAgICAgc3RhcnREYXRlLmFkZCgxLCAnZCcpO1xuICAgICAgfSBlbHNlIGlmIChzdGFydERhdGUud2Vla2RheSgpID09PSA2KSB7XG4gICAgICAgIHN0YXJ0RGF0ZS5hZGQoMiwgJ2QnKTtcbiAgICAgIH1cblxuXG4gICAgICBmb3IgKHZhciBzdGFydFdlZWsgPSBzdGFydERhdGUsIG1vbmRheU9mV2VlayA9IG5ldyBEYXRlKHN0YXJ0RGF0ZSk7IChzdGFydFdlZWsgPD0gZXZlbnQuZW5kX2RhdGUpOyBzdGFydFdlZWsgPSBuZXcgRGF0ZSgobmV3IERhdGUobW9uZGF5T2ZXZWVrKSkuc2V0RGF0ZSgobmV3IERhdGUobW9uZGF5T2ZXZWVrKSkuZ2V0RGF0ZSgpICsgNykpLCBzdGFydERhdGUgPSBuZXcgRGF0ZShzdGFydFdlZWspKSB7XG4gICAgICAgIC8vIEdldCBkaWZmZXJlbmNlIGJldHdlZW4gcmVhbCBzdGFydCBldmVudCBhbmQgbW9uZGF5ICgxIC0gYmVjYXVzZSBNb25kYXkgaXMgMSlcbiAgICAgICAgdmFyIGRpZmZlcmVuY2UgPSBtb21lbnQoc3RhcnRXZWVrKS53ZWVrZGF5KCkgLSAxO1xuXG4gICAgICAgIC8vR2V0IG1vbmRheSBvZiB3ZWVrXG4gICAgICAgIG1vbmRheU9mV2VlayA9IG5ldyBEYXRlKHN0YXJ0V2Vlayk7XG4gICAgICAgIG1vbmRheU9mV2Vlay5zZXREYXRlKG1vbmRheU9mV2Vlay5nZXREYXRlKCkgLSBkaWZmZXJlbmNlKTtcblxuICAgICAgICAvL0dldCBlbmQgb2Ygd2Vlayhub3cgZnJpZGF5KVxuICAgICAgICB2YXIgZnJpZGF5T2ZXZWVrID0gbmV3IERhdGUobW9uZGF5T2ZXZWVrKTtcbiAgICAgICAgZnJpZGF5T2ZXZWVrLnNldERhdGUoZnJpZGF5T2ZXZWVrLmdldERhdGUoKSArIDQpO1xuICAgICAgICB2YXIgZW5kT2ZXZWVrID0gbmV3IERhdGUoZnJpZGF5T2ZXZWVrKTtcblxuICAgICAgICB2YXIgd2VlayA9IHtcbiAgICAgICAgICBtb25kYXk6IG5ldyBEYXRlKG1vbmRheU9mV2VlayksXG4gICAgICAgICAgZnJpZGF5OiBuZXcgRGF0ZShlbmRPZldlZWspLFxuICAgICAgICAgIHNlbGVjdGVkOiBmYWxzZSxcbiAgICAgICAgICBhbGxvdzogdHJ1ZSwvL0FsbG93IHNlbGVjdCBmdWxsIHdlZWsgb3Igbm90LiBNdXN0IGJlICdmYWxzZScgaWYgYXQgbGVhc3Qgb25lIHdlZWtkYXkgaXMgZnVsbHlcbiAgICAgICAgICBmdWxseTogdHJ1ZSxcbiAgICAgICAgICBkYXRlczogW10sXG4gICAgICAgICAgc2VsZWN0ZWRBbnlEYXRlczogZmFsc2VcbiAgICAgICAgfTtcblxuICAgICAgICAvL0lmIGV2ZW50IGZpbmlzaCBiZWZvcmUgZnJpZGF5XG4gICAgICAgIGlmIChlbmRPZldlZWsgPiBldmVudC5lbmRfZGF0ZSkge1xuICAgICAgICAgIGVuZE9mV2VlayA9IGV2ZW50LmVuZF9kYXRlO1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKG1vbmRheU9mV2VlayA8IHN0YXJ0RGF0ZSkge1xuICAgICAgICAgIGZvciAodmFyIHREYXRlID0gbmV3IERhdGUobW9uZGF5T2ZXZWVrKTsgdERhdGUgPCBzdGFydERhdGU7IHREYXRlLnNldERhdGUodERhdGUuZ2V0RGF0ZSgpICsgMSkpIHtcbiAgICAgICAgICAgIHdlZWsuZGF0ZXMucHVzaCh7XG4gICAgICAgICAgICAgIGRhdGU6IGFuZ3VsYXIuY29weSh0RGF0ZSksXG4gICAgICAgICAgICAgIGFsbG93OiBmYWxzZSxcbiAgICAgICAgICAgICAgc2VsZWN0ZWQ6IGZhbHNlLFxuICAgICAgICAgICAgICBmdWxseTogZmFsc2VcbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgIH1cbiAgICAgICAgfVxuXG4gICAgICAgIGZvciAodmFyIGRhdGUgPSBuZXcgRGF0ZShzdGFydERhdGUpOyBkYXRlIDw9IGVuZE9mV2VlazsgZGF0ZS5zZXREYXRlKGRhdGUuZ2V0RGF0ZSgpICsgMSkpIHtcbiAgICAgICAgICAvKmpzaGludCAtVzA4MyAqL1xuICAgICAgICAgIHZhciBleGNlcHQgPSAkZmlsdGVyKCdmaWx0ZXInKShldmVudC5leGNlcHRpb25fZGF0ZXMsIChmdW5jdGlvbiAoZXhjZXB0aW9uRGF0ZSkge1xuICAgICAgICAgICAgICBleGNlcHRpb25EYXRlID0gbW9tZW50KGV4Y2VwdGlvbkRhdGUuZGF0ZSkudG9EYXRlKCk7XG4gICAgICAgICAgICAgIHJldHVybiBleGNlcHRpb25EYXRlLnRvRGF0ZVN0cmluZygpID09PSBkYXRlLnRvRGF0ZVN0cmluZygpO1xuICAgICAgICAgICAgfSkpLFxuICAgICAgICAgICAgYWxsb3cgPSAoZXhjZXB0Lmxlbmd0aCA+IDApID8gZmFsc2UgOiB0cnVlO1xuXG4gICAgICAgICAgd2Vlay5kYXRlcy5wdXNoKHtcbiAgICAgICAgICAgIGRhdGU6IGFuZ3VsYXIuY29weShkYXRlKSxcbiAgICAgICAgICAgIGFsbG93OiBhbGxvdyxcbiAgICAgICAgICAgIHNlbGVjdGVkOiBmYWxzZSxcbiAgICAgICAgICAgIGZ1bGx5OiBhbGxvdyA/IGV4aXN0c0RheShmb3JtLmV2ZW50LmJ1c3lfZGF0ZXMsIGRhdGUpIDogZmFsc2VcbiAgICAgICAgICB9KTtcbiAgICAgICAgfVxuXG4gICAgICAgIGlmIChmcmlkYXlPZldlZWsgPiBlbmRPZldlZWspIHtcbiAgICAgICAgICBmb3IgKGRhdGUgPSBuZXcgRGF0ZShlbmRPZldlZWspLCBkYXRlLnNldERhdGUoZGF0ZS5nZXREYXRlKCkgKyAxKTsgZGF0ZSA8PSBmcmlkYXlPZldlZWs7IGRhdGUuc2V0RGF0ZShkYXRlLmdldERhdGUoKSArIDEpKSB7XG4gICAgICAgICAgICB3ZWVrLmRhdGVzLnB1c2goe1xuICAgICAgICAgICAgICBkYXRlOiBhbmd1bGFyLmNvcHkoZGF0ZSksXG4gICAgICAgICAgICAgIGFsbG93OiBmYWxzZSxcbiAgICAgICAgICAgICAgc2VsZWN0ZWQ6IGZhbHNlLFxuICAgICAgICAgICAgICBmdWxseTogZmFsc2VcbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgIH1cbiAgICAgICAgfVxuXG4gICAgICAgIGZvcih2YXIgayA9IDAsIGRheU9mV2VlazsgKGRheU9mV2VlayA9IHdlZWsuZGF0ZXNba10pOyBrKyspIHtcbiAgICAgICAgICAvL0hhLWhhIHdoYXQgaXMgaXQ/IVxuICAgICAgICAgIGlmKGRheU9mV2Vlay5hbGxvdyAmJiAhZGF5T2ZXZWVrLmZ1bGx5KSB7XG4gICAgICAgICAgICB3ZWVrLmZ1bGx5ID0gZmFsc2U7XG4gICAgICAgICAgfVxuXG4gICAgICAgICAgaWYoZGF5T2ZXZWVrLmFsbG93ICYmIGRheU9mV2Vlay5mdWxseSkge1xuICAgICAgICAgICAgd2Vlay5hbGxvdyA9IGZhbHNlO1xuICAgICAgICAgIH1cbiAgICAgICAgfVxuXG4gICAgICAgIGZvcm0ud2Vla3MucHVzaCh3ZWVrKTtcbiAgICAgIH1cbiAgICB9XG5cbiAgICBmdW5jdGlvbiBwcmVwYXJlUHJvZHVjdHMocHJvZHVjdHMpIHtcbiAgICAgIHZhciBsb2NhbFByb2R1Y3RzID0gc3RvcmUuZ2V0KCdwcm9kdWN0cycpO1xuXG4gICAgICByZXR1cm4gYW5ndWxhci5mb3JFYWNoKHByb2R1Y3RzLCBmdW5jdGlvbiAocHJvZHVjdCkge1xuICAgICAgICBwcm9kdWN0LmltYWdlUGF0aCA9IHByb2R1Y3QuaW1hZ2VfdGh1bWJuYWlsID8gJy8nICsgcHJvZHVjdC5pbWFnZV90aHVtYm5haWwucGF0aCA6ICcnO1xuXG4gICAgICAgIGlmIChsb2NhbFByb2R1Y3RzKSB7XG4gICAgICAgICAgdmFyIGxvY2FsUHJvZHVjdCA9ICRmaWx0ZXIoJ2ZpbHRlcicpKGxvY2FsUHJvZHVjdHMsIHtpZDogcHJvZHVjdC5pZH0pWzBdO1xuXG4gICAgICAgICAgaWYgKGxvY2FsUHJvZHVjdCkge1xuICAgICAgICAgICAgcHJvZHVjdC5zZWxlY3RlZFZhcmlhbnQgPSBsb2NhbFByb2R1Y3Quc2VsZWN0ZWRWYXJpYW50O1xuICAgICAgICAgICAgcHJvZHVjdC5zZWxlY3RlZFF1YW50aXR5ID0gbG9jYWxQcm9kdWN0LnNlbGVjdGVkUXVhbnRpdHk7XG4gICAgICAgICAgfVxuICAgICAgICB9XG5cbiAgICAgICAgcHJvZHVjdC5wcmljZSA9IDA7XG5cbiAgICAgICAgdmFyIHZhcmlhbnQgPSBwcm9kdWN0LnZhcmlhbnRzLmZpbHRlcihmdW5jdGlvbiAodmFyaWFudCkge1xuICAgICAgICAgIHJldHVybiBwcm9kdWN0LnNlbGVjdGVkVmFyaWFudCAmJiBwcm9kdWN0LnNlbGVjdGVkVmFyaWFudCA9PT0gdmFyaWFudC5pZDtcbiAgICAgICAgfSlbMF07XG5cbiAgICAgICAgaWYgKHZhcmlhbnQgJiYgcHJvZHVjdC5zZWxlY3RlZFF1YW50aXR5KSB7XG4gICAgICAgICAgcHJvZHVjdC5wcmljZSA9IHZhcmlhbnQucHJpY2UgKiBwcm9kdWN0LnNlbGVjdGVkUXVhbnRpdHk7XG4gICAgICAgIH1cbiAgICAgIH0pO1xuICAgIH1cblxuICAgIGZ1bmN0aW9uIHByZXBhcmVCb29raW5nRm9yUmVxdWVzdChfZGF0YSkge1xuICAgICAgdmFyIGRhdGEgPSBhbmd1bGFyLmNvcHkoX2RhdGEpO1xuXG4gICAgICBkYXRhLnZhcmlhbnRzID0gW107XG5cbiAgICAgIGFuZ3VsYXIuZm9yRWFjaChkYXRhLnByb2R1Y3RzLCBmdW5jdGlvbiAocHJvZHVjdCkge1xuICAgICAgICBpZiAocHJvZHVjdC5zZWxlY3RlZFZhcmlhbnQgJiYgcHJvZHVjdC5zZWxlY3RlZFF1YW50aXR5KSB7XG4gICAgICAgICAgZGF0YS52YXJpYW50cy5wdXNoKHtcbiAgICAgICAgICAgIGlkOiBwcm9kdWN0LnNlbGVjdGVkVmFyaWFudCxcbiAgICAgICAgICAgIGNvdW50OiBwcm9kdWN0LnNlbGVjdGVkUXVhbnRpdHlcbiAgICAgICAgICB9KTtcbiAgICAgICAgfVxuICAgICAgfSk7XG5cbiAgICAgIGRlbGV0ZSBkYXRhLnByb2R1Y3RzO1xuXG4gICAgICByZXR1cm4gZGF0YTtcbiAgICB9XG5cbiAgICBmdW5jdGlvbiBwcmVwYXJlRGF0ZXNGb3JSZXF1ZXN0KG9iamVjdCwgcHJvcGVydGllcywgbmVlZENsb25lKSB7XG4gICAgICB2YXIgbG9jYWxPYmplY3QgPSBuZWVkQ2xvbmUgPyBhbmd1bGFyLmNvcHkob2JqZWN0KSA6IG9iamVjdDtcblxuICAgICAgcHJvcGVydGllcy5mb3JFYWNoKGZ1bmN0aW9uIChwcm9wZXJ0eSkge1xuICAgICAgICBsb2NhbE9iamVjdFtwcm9wZXJ0eV0gPSBtb21lbnQobG9jYWxPYmplY3RbcHJvcGVydHldKS5mb3JtYXQoJ1lZWVktTU0tREQnKTtcbiAgICAgIH0pO1xuXG4gICAgICByZXR1cm4gbG9jYWxPYmplY3Q7XG4gICAgfVxuICB9XG59KSgpO1xuIiwiKGZ1bmN0aW9uICgpIHtcbiAgJ3VzZSBzdHJpY3QnO1xuXG4gIGFuZ3VsYXJcbiAgICAubW9kdWxlKCdib29raW5nJylcbiAgICAuc2VydmljZSgnb3JkZXJTZXJ2aWNlJywgb3JkZXJTZXJ2aWNlKTtcblxuICBvcmRlclNlcnZpY2UuJGluamVjdCA9IFsnJGZpbHRlcicsICckcScsICdzdG9yZScsICdjaGlsZFNlcnZpY2UnLCAnZXZlbnRTZXJ2aWNlJywgJyRodHRwJywgJ2hlbHBlclNlcnZpY2UnXTtcblxuICAvKiBAbmdJbmplY3QgKi9cbiAgZnVuY3Rpb24gb3JkZXJTZXJ2aWNlKCRmaWx0ZXIsICRxLCBzdG9yZSwgY2hpbGRTZXJ2aWNlLCBldmVudFNlcnZpY2UsICRodHRwLCBoZWxwZXJTZXJ2aWNlKSB7XG4gICAgLyoganNoaW50IHZhbGlkdGhpczogdHJ1ZSAqL1xuICAgIHZhciB2bSA9IHRoaXM7XG5cbiAgICB2bS5wcm9taXNlcyA9ICRxLmFsbChbY2hpbGRTZXJ2aWNlLmFsbCgpLCBldmVudFNlcnZpY2UuYWxsKCldKS50aGVuKGZ1bmN0aW9uIChkYXRhKSB7XG4gICAgICB2bS5jaGlsZHJlbiA9IGRhdGFbMF07XG4gICAgICB2bS5ldmVudHMgPSBkYXRhWzFdO1xuICAgIH0pO1xuXG4gICAgdm0uY3VzdG9tZXJJZCA9IHN0b3JlLmdldCgnY3VzdG9tZXItaWQnKTtcblxuICAgIGlmKHZtLmN1c3RvbWVySWQpIHtcbiAgICAgIHN0b3JlLnJlbW92ZSgnY3VzdG9tZXItaWQnKTtcbiAgICAgIHN0b3JlLnJlbW92ZSgnb3JkZXJzJyk7XG4gICAgICBzdG9yZS5yZW1vdmUoJ2luZGV4Jyk7XG4gICAgfVxuXG4gICAgdm0ub3JkZXJzID0gc3RvcmUuZ2V0KCdvcmRlcnMnKTtcbiAgICB2bS5pbmRleCA9IHN0b3JlLmdldCgnaW5kZXgnKTtcblxuICAgIGlmICghdm0ub3JkZXJzKSB7XG4gICAgICB2bS5vcmRlcnMgPSBbXTtcbiAgICAgIHZtLmluZGV4ID0gMTtcbiAgICB9XG5cbiAgICB2bS52YWxpZGF0ZSA9IHZhbGlkYXRlO1xuXG4gICAgdm0uY291bnQgPSBjb3VudDtcbiAgICB2bS5hbGwgPSBhbGw7XG4gICAgdm0uYWRkID0gYWRkO1xuICAgIHZtLmdldCA9IGdldDtcbiAgICB2bS51cGRhdGUgPSB1cGRhdGU7XG4gICAgdm0ucmVtb3ZlID0gcmVtb3ZlO1xuICAgIHZtLmNhbGN1bGF0ZSA9IGNhbGN1bGF0ZTtcbiAgICB2bS5jbGVhciA9IGNsZWFyO1xuICAgIHZtLnByb2R1Y3RzID0gcHJvZHVjdHM7XG4gICAgdm0uc2F2ZVByb2R1Y3RzID0gc2F2ZVByb2R1Y3RzO1xuXG4gICAgLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy9cblxuICAgIGZ1bmN0aW9uIHByb2R1Y3RzKCkge1xuICAgICAgcmV0dXJuICRodHRwLmdldCgnL2FwaS9wcm9kdWN0cycpLnRoZW4oaGVscGVyU2VydmljZS5mZXRjaFJlc3BvbnNlKS50aGVuKGhlbHBlclNlcnZpY2UucHJlcGFyZVByb2R1Y3RzKTtcbiAgICB9XG5cbiAgICBmdW5jdGlvbiBjb3VudCgpIHtcbiAgICAgIHJldHVybiB2bS5vcmRlcnMubGVuZ3RoO1xuICAgIH1cblxuICAgIGZ1bmN0aW9uIGNsZWFyKCkge1xuICAgICAgdmFyIG9yZGVycyA9IGFuZ3VsYXIuY29weSh2bS5vcmRlcnMpO1xuXG4gICAgICB2YXIgbG9jYWxQcm9taXNlID0gJHEuZGVmZXIoKTtcblxuICAgICAgdm0ucHJvbWlzZXMudGhlbihmdW5jdGlvbiAoKSB7XG4gICAgICAgIHN0b3JlLnJlbW92ZSgnb3JkZXJzJyk7XG4gICAgICAgIHN0b3JlLnJlbW92ZSgnaW5kZXgnKTtcbiAgICAgICAgc3RvcmUucmVtb3ZlKCdwcm9kdWN0cycpO1xuXG4gICAgICAgIGxvY2FsUHJvbWlzZS5yZXNvbHZlKHRydWUpO1xuICAgICAgfSk7XG5cbiAgICAgIHJldHVybiBsb2NhbFByb21pc2UucHJvbWlzZTtcbiAgICB9XG5cbiAgICBmdW5jdGlvbiBzYXZlUHJvZHVjdHMocHJvZHVjdHMpIHtcbiAgICAgIHZhciBsb2NhbFByb21pc2UgPSAkcS5kZWZlcigpO1xuXG4gICAgICB2bS5wcm9taXNlcy50aGVuKGZ1bmN0aW9uICgpIHtcbiAgICAgICAgc3RvcmUuc2V0KCdwcm9kdWN0cycsIHByb2R1Y3RzKTtcblxuICAgICAgICBsb2NhbFByb21pc2UucmVzb2x2ZSh0cnVlKTtcbiAgICAgIH0pO1xuXG4gICAgICByZXR1cm4gbG9jYWxQcm9taXNlLnByb21pc2U7XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gY2FsY3VsYXRlKG9yZGVyKSB7XG4gICAgICB2YXIgc3VtID0gMDtcblxuICAgICAgZm9yICh2YXIgaSA9IDAsIHdlZWs7ICh3ZWVrID0gb3JkZXIud2Vla3NbaV0pOyBpKyspIHtcbiAgICAgICAgaWYgKHdlZWsuc2VsZWN0ZWQpIHtcbiAgICAgICAgICBzdW0gKz0gb3JkZXIuZXZlbnQuY29zdF9wZXJfd2VlaztcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICBmb3IgKHZhciBqID0gMCwgd2Vla2RheTsgKHdlZWtkYXkgPSB3ZWVrLmRhdGVzW2pdKTsgaisrKSB7XG4gICAgICAgICAgICBpZih3ZWVrZGF5LnNlbGVjdGVkKSB7XG4gICAgICAgICAgICAgIHN1bSArPSBvcmRlci5ldmVudC5jb3N0X3Blcl9kYXk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICB9XG5cbiAgICAgIGFuZ3VsYXIuZm9yRWFjaChvcmRlci52YXJpYW50cywgZnVuY3Rpb24gKHZhcmlhbnQpIHtcbiAgICAgICAgc3VtICs9IHZhcmlhbnQucHJpY2UgKiB2YXJpYW50LmNvdW50O1xuICAgICAgfSk7XG5cbiAgICAgIHJldHVybiBzdW07XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gdmFsaWRhdGUob3JkZXIsIGNoaWxkcmVuLCBldmVudHMpIHtcbiAgICAgIHZhciBlcnJvcnMgPSBbXTtcblxuICAgICAgdmFyIGNoaWxkID0gJGZpbHRlcignZmlsdGVyJykoY2hpbGRyZW4sIHtpZDogb3JkZXIuY2hpbGRfaWR9KTtcbiAgICAgIGlmICghKG9yZGVyLmNoaWxkX2lkICYmIGNoaWxkLmxlbmd0aCkpIHtcbiAgICAgICAgZXJyb3JzLnB1c2goe1xuICAgICAgICAgIHRpdGxlOiAnY2hpbGQnLFxuICAgICAgICAgIG1lc3NhZ2U6ICdOZWNlc3Nhcnkgc2VsZWN0IGNoaWxkJ1xuICAgICAgICB9KTtcbiAgICAgIH1cblxuICAgICAgdmFyIGV2ZW50ID0gJGZpbHRlcignZmlsdGVyJykoZXZlbnRzLCB7aWQ6IG9yZGVyLmV2ZW50X2lkfSk7XG4gICAgICBpZiAoIShvcmRlci5ldmVudF9pZCAmJiBldmVudC5sZW5ndGgpKSB7XG4gICAgICAgIGVycm9ycy5wdXNoKHtcbiAgICAgICAgICB0aXRsZTogJ2V2ZW50JyxcbiAgICAgICAgICBtZXNzYWdlOiAnTmVjZXNzYXJ5IHNlbGVjdCBldmVudCdcbiAgICAgICAgfSk7XG4gICAgICB9XG5cbiAgICAgIC8vQ2hlY2sgdGhhdCBzZWxlY3RlZCBhcyBtaW5pbXVtIG9uZSBkYXlcbiAgICAgIHZhciBzZWxlY3RlZCA9IGZhbHNlO1xuICAgICAgZm9yICh2YXIgaSA9IDAsIHdlZWs7IG9yZGVyLndlZWtzICYmICh3ZWVrID0gb3JkZXIud2Vla3NbaV0pOyBpKyspIHtcbiAgICAgICAgZm9yICh2YXIgaiA9IDAsIGRheTsgKGRheSA9IHdlZWsuZGF0ZXNbal0pOyBqKyspIHtcbiAgICAgICAgICBpZiAoZGF5LnNlbGVjdGVkKSB7XG4gICAgICAgICAgICBzZWxlY3RlZCA9IHRydWU7XG4gICAgICAgICAgICBicmVhaztcbiAgICAgICAgICB9XG4gICAgICAgIH1cblxuICAgICAgICBpZiAoc2VsZWN0ZWQpIHtcbiAgICAgICAgICBicmVhaztcbiAgICAgICAgfVxuICAgICAgfVxuXG4gICAgICBpZiAoIXNlbGVjdGVkKSB7XG4gICAgICAgIGVycm9ycy5wdXNoKHtcbiAgICAgICAgICB0aXRsZTogJ2RhdGVzJyxcbiAgICAgICAgICBtZXNzYWdlOiAnTmVjZXNzYXJ5IHNlbGVjdCBhdCBsZWFzdCBvbmUgZGF5J1xuICAgICAgICB9KTtcbiAgICAgIH1cblxuICAgICAgcmV0dXJuIGVycm9ycy5yZXZlcnNlKCk7XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gYWxsKCkge1xuICAgICAgdmFyIG9yZGVycyA9IGFuZ3VsYXIuY29weSh2bS5vcmRlcnMpO1xuXG4gICAgICB2YXIgbG9jYWxQcm9taXNlID0gJHEuZGVmZXIoKTtcblxuICAgICAgdm0ucHJvbWlzZXMudGhlbihmdW5jdGlvbiAoKSB7XG4gICAgICAgIGZvciAodmFyIGkgPSAwLCBvcmRlcjsgKG9yZGVyID0gb3JkZXJzW2ldKTsgaSsrKSB7XG4gICAgICAgICAgc2V0Q2hpbGQob3JkZXIpO1xuICAgICAgICAgIHNldEV2ZW50KG9yZGVyKTtcbiAgICAgICAgfVxuXG4gICAgICAgIGxvY2FsUHJvbWlzZS5yZXNvbHZlKG9yZGVycyk7XG4gICAgICB9KTtcblxuICAgICAgcmV0dXJuIGxvY2FsUHJvbWlzZS5wcm9taXNlO1xuICAgIH1cblxuICAgIGZ1bmN0aW9uIGFkZChvcmRlcikge1xuICAgICAgdmFyIGxvY2FsUHJvbWlzZSA9ICRxLmRlZmVyKCk7XG5cbiAgICAgIHZtLnByb21pc2VzLnRoZW4oZnVuY3Rpb24gKCkge1xuICAgICAgICBvcmRlci5faWQgPSB2bS5pbmRleCsrO1xuICAgICAgICB2bS5vcmRlcnMucHVzaChvcmRlcik7XG4gICAgICAgIHNhdmVPcmRlcnMoKTtcblxuICAgICAgICBsb2NhbFByb21pc2UucmVzb2x2ZShcbiAgICAgICAgICBhbmd1bGFyLmNvcHkob3JkZXIpXG4gICAgICAgICk7XG4gICAgICB9KTtcblxuXG4gICAgICByZXR1cm4gbG9jYWxQcm9taXNlLnByb21pc2U7XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gZ2V0KGlkKSB7XG4gICAgICB2YXIgbG9jYWxQcm9taXNlID0gJHEuZGVmZXIoKTtcblxuICAgICAgdm0ucHJvbWlzZXMudGhlbihmdW5jdGlvbiAoKSB7XG4gICAgICAgIHZhciBvcmRlciA9IGFuZ3VsYXIuY29weSgkZmlsdGVyKCdmaWx0ZXInKSh2bS5vcmRlcnMsIHtfaWQ6IGlkfSlbMF0pO1xuXG4gICAgICAgIHNldENoaWxkKG9yZGVyKTtcbiAgICAgICAgc2V0RXZlbnQob3JkZXIpO1xuXG4gICAgICAgIGxvY2FsUHJvbWlzZS5yZXNvbHZlKG9yZGVyKTtcbiAgICAgIH0pO1xuXG5cbiAgICAgIHJldHVybiBsb2NhbFByb21pc2UucHJvbWlzZTtcbiAgICB9XG5cbiAgICBmdW5jdGlvbiB1cGRhdGUoaWQsIG9yZGVyKSB7XG4gICAgICB2YXIgbG9jYWxQcm9taXNlID0gJHEuZGVmZXIoKTtcblxuICAgICAgdmFyIF9vcmRlciA9ICRmaWx0ZXIoJ2ZpbHRlcicpKHZtLm9yZGVycywge19pZDogaWR9KVswXTtcblxuICAgICAgX29yZGVyLmNoaWxkX2lkID0gb3JkZXIuY2hpbGRfaWQ7XG4gICAgICBfb3JkZXIuY2hpbGQgPSBvcmRlci5jaGlsZDtcbiAgICAgIF9vcmRlci5ldmVudF9pZCA9IG9yZGVyLmV2ZW50X2lkO1xuICAgICAgX29yZGVyLmV2ZW50ID0gb3JkZXIuZXZlbnQ7XG4gICAgICBfb3JkZXIuZGF0ZXMgPSBvcmRlci5kYXRlcztcblxuICAgICAgc2F2ZU9yZGVycygpO1xuXG4gICAgICBsb2NhbFByb21pc2UucmVzb2x2ZShfb3JkZXIpO1xuXG4gICAgICByZXR1cm4gbG9jYWxQcm9taXNlLnByb21pc2U7XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gcmVtb3ZlKGlkKSB7XG4gICAgICB2YXIgbG9jYWxQcm9taXNlID0gJHEuZGVmZXIoKTtcblxuICAgICAgdmFyIG9yZGVyID0gJGZpbHRlcignZmlsdGVyJykodm0ub3JkZXJzLCB7X2lkOiBpZH0pWzBdO1xuXG4gICAgICB2bS5vcmRlcnMuc3BsaWNlKHZtLm9yZGVycy5pbmRleE9mKG9yZGVyKSwgMSk7XG5cbiAgICAgIHNhdmVPcmRlcnMoKTtcblxuICAgICAgbG9jYWxQcm9taXNlLnJlc29sdmUodHJ1ZSk7XG5cbiAgICAgIHJldHVybiBsb2NhbFByb21pc2UucHJvbWlzZTtcbiAgICB9XG5cbiAgICBmdW5jdGlvbiBzYXZlT3JkZXJzKCkge1xuICAgICAgc3RvcmUuc2V0KCdvcmRlcnMnLCB2bS5vcmRlcnMpO1xuICAgICAgc3RvcmUuc2V0KCdpbmRleCcsIHZtLmluZGV4KTtcbiAgICB9XG5cbiAgICBmdW5jdGlvbiBzZXRDaGlsZChvcmRlcikge1xuICAgICAgb3JkZXIuY2hpbGQgPSAkZmlsdGVyKCdmaWx0ZXInKSh2bS5jaGlsZHJlbiwge2lkOiBvcmRlci5jaGlsZF9pZH0pWzBdO1xuICAgIH1cblxuICAgIGZ1bmN0aW9uIHNldEV2ZW50KG9yZGVyKSB7XG4gICAgICBvcmRlci5ldmVudCA9ICRmaWx0ZXIoJ2ZpbHRlcicpKHZtLmV2ZW50cywge2lkOiBvcmRlci5ldmVudF9pZH0pWzBdO1xuICAgIH1cblxuICB9XG59KSgpO1xuIiwiKGZ1bmN0aW9uICgpIHtcbiAgJ3VzZSBzdHJpY3QnO1xuXG4gIGFuZ3VsYXJcbiAgICAubW9kdWxlKCdib29raW5nJylcbiAgICAuc2VydmljZSgncHJvZmlsZVNlcnZpY2UnLCBwcm9maWxlKTtcblxuICBwcm9maWxlLiRpbmplY3QgPSBbJyRodHRwJywgJ2hlbHBlclNlcnZpY2UnXTtcblxuICAvKiBAbmdJbmplY3QgKi9cbiAgZnVuY3Rpb24gcHJvZmlsZSgkaHR0cCwgaGVscGVyU2VydmljZSkge1xuICAgIC8qIGpzaGludCB2YWxpZHRoaXM6IHRydWUgKi9cbiAgICB2YXIgdm0gPSB0aGlzO1xuXG4gICAgdm0uZ2V0ID0gZ2V0O1xuICAgIHZtLnVwZGF0ZSA9IHVwZGF0ZTtcbiAgICB2bS5jb3VudHJpZXMgPSBjb3VudHJpZXM7XG5cbiAgICAvLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vXG5cbiAgICBmdW5jdGlvbiBnZXQoKSB7XG4gICAgICByZXR1cm4gJGh0dHAuZ2V0KCcvYXBpL3Byb2ZpbGUnKVxuICAgICAgICAudGhlbihoZWxwZXJTZXJ2aWNlLmZldGNoUmVzcG9uc2UpXG4gICAgICAgIC50aGVuKGZ1bmN0aW9uIChkYXRhKSB7XG4gICAgICAgICAgZGF0YS5lbWFpbCA9IGRhdGEudXNlci5lbWFpbDtcbiAgICAgICAgICByZXR1cm4gZGF0YTtcbiAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gdXBkYXRlKGRhdGEpIHtcbiAgICAgIHJldHVybiAkaHR0cC5wdXQoJy9hcGkvcHJvZmlsZScsIGRhdGEpXG4gICAgICAgIC50aGVuKGhlbHBlclNlcnZpY2UuZmV0Y2hSZXNwb25zZSlcbiAgICAgICAgLnRoZW4oZnVuY3Rpb24gKGRhdGEpIHtcbiAgICAgICAgICBkYXRhLmVtYWlsID0gZGF0YS51c2VyLmVtYWlsO1xuICAgICAgICAgIHJldHVybiBkYXRhO1xuICAgICAgICB9KTtcbiAgICB9XG5cbiAgICBmdW5jdGlvbiBjb3VudHJpZXMoKSB7XG4gICAgICByZXR1cm4gJGh0dHAuZ2V0KCcvYXBpL2NvdW50cmllcycpLnRoZW4oaGVscGVyU2VydmljZS5mZXRjaFJlc3BvbnNlKTtcbiAgICB9XG4gIH1cbn0pKCk7XG4iLCIoZnVuY3Rpb24gKCkge1xuICAndXNlIHN0cmljdCc7XG5cbiAgYW5ndWxhclxuICAgIC5tb2R1bGUoJ2Jvb2tpbmcnKVxuICAgIC5jb21wb25lbnQoJ3NlbGVjdERhdGUnLCB7XG4gICAgICBiaW5kaW5nczoge1xuICAgICAgICBkYXRlOiAnPW5nTW9kZWwnXG4gICAgICB9LFxuICAgICAgdGVtcGxhdGVVcmw6ICcvYm9va2luZy9jb21wb25lbnRzL3NlbGVjdC1kYXRlL3NlbGVjdC1kYXRlLmh0bWwnLFxuICAgICAgY29udHJvbGxlckFzOiAnU2VsZWN0RGF0ZUN0cmwnLFxuICAgICAgY29udHJvbGxlcjogJ1NlbGVjdERhdGVDb250cm9sbGVyJ1xuICAgIH0pO1xufSkoKTsiLCIoZnVuY3Rpb24gKCkge1xuICAndXNlIHN0cmljdCc7XG5cbiAgYW5ndWxhclxuICAgIC5tb2R1bGUoJ2Jvb2tpbmcnKVxuICAgIC5jb250cm9sbGVyKCdTZWxlY3REYXRlQ29udHJvbGxlcicsIFNlbGVjdERhdGVDb250cm9sbGVyKTtcblxuICAvKiBAbmdJbmplY3QgKi9cbiAgZnVuY3Rpb24gU2VsZWN0RGF0ZUNvbnRyb2xsZXIoJGZpbHRlciwgbW9udGhzKSB7XG4gICAgLyoganNoaW50IHZhbGlkdGhpczogdHJ1ZSAqL1xuICAgIHZhciB2bSA9IHRoaXM7XG5cbiAgICB2bS5kYXlzID0gJGZpbHRlcigncmFuZ2UnKShbXSwgMSwgMzEpO1xuICAgIHZtLm1vbnRocyA9IG1vbnRocztcbiAgICB2bS55ZWFycyA9ICRmaWx0ZXIoJ3JhbmdlJykoW10sIG1vbWVudCgpLmdldCgneWVhcicpIC0gMTAwLCBtb21lbnQoKS5nZXQoJ3llYXInKSwgJ2Rlc2MnKTtcblxuICAgIGluaXREYXRlKCk7XG5cbiAgICB2bS5jaGFuZ2UgPSBjaGFuZ2U7XG5cbiAgICAvLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy9cblxuICAgIGZ1bmN0aW9uIGNoYW5nZSgpIHtcbiAgICAgIGlmICghdm0uZGF5IHx8IHZtLm1vbnRoID09PSB1bmRlZmluZWQgfHwgIXZtLnllYXIpIHtcbiAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuXG4gICAgICB3aGlsZSAoIW1vbWVudChbdm0ueWVhciwgdm0ubW9udGgsIHZtLmRheV0pLmlzVmFsaWQoKSkge1xuICAgICAgICAtLXZtLmRheTtcbiAgICAgIH1cblxuICAgICAgdm0uZGF0ZSA9IG1vbWVudChbdm0ueWVhciwgdm0ubW9udGgsIHZtLmRheV0pO1xuICAgIH1cblxuICAgIGZ1bmN0aW9uIGluaXREYXRlKCkge1xuICAgICAgdmFyIGRhdGUgPSBtb21lbnQodm0uZGF0ZSk7XG4gICAgICBpZiAoZGF0ZS5pc1ZhbGlkKCkpIHtcbiAgICAgICAgdm0uZGF5ID0gcGFyc2VJbnQoZGF0ZS5mb3JtYXQoJ0QnKSk7XG4gICAgICAgIHZtLm1vbnRoID0gZGF0ZS5tb250aCgpO1xuICAgICAgICB2bS55ZWFyID0gZGF0ZS55ZWFyKCk7XG4gICAgICB9XG4gICAgfVxuXG4gIH1cbn0pKCk7XG4iXSwic291cmNlUm9vdCI6Ii9zb3VyY2UvIn0=
