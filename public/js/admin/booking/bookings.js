$(document).ready(function () {
    $('#bookings-table-table').dataTable({
        "columns": [
            {
                searchable: false,
                orderable: true
            },
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            {
                searchable: false,
                orderable: false
            }
        ],
        "order": [[0, "desc"]]
    });

    $('#orders-table').dataTable({
        "columns": [
            {
                searchable: false,
                orderable: true
            },
            null,
            null,
            null,
            null,
            null,
            {
                searchable: false,
                orderable: false
            },
            {
                searchable: false,
                orderable: false
            },
            {
                searchable: false,
                orderable: false
            }
        ],
        "order": [[0, "desc"]]
    });

    var customerSelect = $('#customer'),
        childSelect = $('#child'),
        eventSelect = $('#event');

    customerSelect.select2({
        ajax: {
            url: '/admin/api/customers',
            delay: 250,
            processResults: function (data) {
                var customers = [];

                for (var i = 0, customer; (customer = data[i]); i++) {
                    customers.push({
                        id: customer.id,
                        text: customer.first_name + ' ' + customer.last_name
                    });
                }

                return {
                    results: customers
                };
            },
            transport: function (params, success, failure) {
                //Remove start and end spaces
                params.data.q = $.trim(params.data.q);

                //Send only not empty params
                if (params.data.q) {
                    params.success = success;
                    params.error = failure;
                    $.ajax(params.url, params);
                }
            }
        }
    });

    customerSelect.on('change', function (event) {
        var customerId = $(this).val();

        // Disable child select
        childSelect.attr('disabled', true);

        //Clear all customer's children
        childSelect.find('option:not(\'.hide\')').remove();

        $.ajax('/admin/api/customers/' + customerId + '/children', {
            success: function (data) {

                var optionExample = childSelect.find('option.hide');
                for (var i = 0, child; (child = data[i]); i++) {
                    optionExample
                        .clone()
                        .removeClass('hide')
                        .val(child.id)
                        .text(child.first_name + ' ' + child.last_name)
                        .appendTo('#child');
                }

                // Enable child select
                childSelect.attr('disabled', false);
            },
            error: function (error) {
                alert('Sorry, some error on server. Please contact with administrator!');
            }
        });
    });

    eventSelect.on('change', function (event) {
        var eventId = $(this).val();

        if (!eventId) {

        }

        // Get all event's dates!
    });
});
