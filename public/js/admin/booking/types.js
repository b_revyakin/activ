$(document).ready(function () {
    $('#types-table').dataTable({
        "columns": [
            null,
            null,
            null,
            null,
            {"searchable": false, "orderable": false}
        ],
        // dom: 'Bfrti',
        // buttons: [
        //     {
        //         extend: 'excel',
        //         className: 'btn green btn-outline dt-button'
        //     },
        //     {
        //         extend: 'pdf',
        //         className: 'btn yellow btn-outline dt-button'
        //     }
        // ]
    });

    $('.dt-button').addClass('btn btn-default');
});
