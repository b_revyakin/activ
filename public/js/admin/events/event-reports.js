$(document).ready(function () {
    $('#event-reports').dataTable({
        iDisplayLength: -1,
        "scrollX": true,
        "searching": false,
        "ordering": false,
        "paging": false,
        "info": false,
        "columnDefs": [
            {"width": "200px", "targets": 0}
        ]
    });
});