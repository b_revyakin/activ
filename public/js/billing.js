(function() {
  var StripeBilling  = {
    init: function() {
      this.form = $('#payment-form');
      this.submitButton = this.form.find('input[type=submit]');
      this.submitButtonValue = this.submitButton.val();

      var stripeKey = $('meta[name="stripe-publish-key"]').attr('content');
      Stripe.setPublishableKey(stripeKey);

      this.bindEvents();
    },

    bindEvents: function() {
      this.form.on('submit', $.proxy(this.sendToken, this));
    },

    sendToken: function(event) {
      this.submitButton.val('Wait').prop('disabled', true);

      Stripe.createToken(this.form, $.proxy(this.stripeResponseHandler, this));

      event.preventDefault();
    },

    stripeResponseHandler: function(status, response) {
      if(response.error) {
        this.form.find('.payment-errors').show().text(response.error.message).addClass('alert').addClass('alert-danger');

        return this.submitButton.val(this.submitButtonValue).prop('disabled', false);
      }

      $('<input>', {
        type: 'hidden',
        name: 'stripe_token',
        value: response.id
      }).appendTo(this.form);

      this.form[0].submit();
    }
  };

  StripeBilling.init();
})();