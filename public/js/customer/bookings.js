$(function () {
  $('.btn-view').on('click', function () {
    var bookingId = getBookingId(this),
      attribute = getDataBookingIdAttribute(bookingId);

    $(this).hide();
    $('tr[' + attribute + ']').show();
    $('.btn-hide[' + attribute + ']').show();
  });

  $('.btn-hide').on('click', function () {
    var bookingId = getBookingId(this),
      attribute = getDataBookingIdAttribute(bookingId);

    $(this).hide();
    $('tr[' + attribute + ']').hide();
    $('.btn-view[' + attribute + ']').show();
  });

  function getBookingId(element) {
    return $(element).attr('data-booking-id');
  }

  function getDataBookingIdAttribute(bookingId) {
    return 'data-booking-id=\'' + bookingId + '\'';
  }
});