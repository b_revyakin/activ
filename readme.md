# Install Mcrypt on php 7.0
sudo apt-get upgrade
sudo apt-get install php7.0-mcrypt

# Increase PHP script execution time with Nginx because camps can be very fatty for generating pdf documents
https://easyengine.io/tutorials/php/increase-script-execution-time/
1. nano /etc/php/7.0/fpm/php.ini
   max_execution_time = 300
   
2. nano /etc/php/7.0/fpm/pool.d/www.conf
   request_terminate_timeout = 300
   
3. nano /etc/nginx/sites-available/example.com
location ~ \.php$ {
	...
	fastcgi_read_timeout 300; 
}

4. service php7.0-fpm reload
   service nginx reload
   
   
# Some points about what install and how to run

- sudo apt-get install php-curl
- sudo apt-get install libgtk2.0-0:i386


"Site Manager access to admin system so that they can access registers and child
details. So that they will be able to access the following pages: Events -> All events
(with no 'Show', 'Edit', 'capacities' or 'deactivate' buttons in the action column, JUST
'Details'). Once they click on the Details button they can access all links on the top
menu, but cannot click any actions (Cancel). The log in email for this can be
admin@activcamps.com and the password to be camps2016."