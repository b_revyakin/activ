<?php

$serverError = 'Server Error!';

return [
    'created_success' => 'The :object was created!',
    'created_fail' => $serverError . ' ' . 'The :object was not created.',

    'updated_success' => 'The :object was updated.',
    'updated_fail' => $serverError . ' ' . 'The :object was not updated.',

    'removed_success' => 'The :object was removed.',
    'removed_fail' => $serverError . ' ' . 'The :object was removed.',

    'activated_success' => 'The :object was activated.',
    'activated_fail' => $serverError . ' ' . 'The :object was not activated.',

    'deactivated_success' => 'The :object was deactivated.',
    'deactivated_fail' => $serverError . ' ' . 'The :object was not deactivated.',
];
