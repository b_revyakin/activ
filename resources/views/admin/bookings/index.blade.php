@extends('layouts.admin')

@section('title')
    Bookings
@endsection

@section('breadcrumbs')
    <li><span>Bookings</span></li>
@endsection

@section('js')
    <script src="/js/admin/booking/bookings.js"></script>
    <script src="/js/modal-window.js"></script>
@endsection

@section('content')
    <div class="portlet light portlet-fit portlet-datatable ">
        <div class="portlet-title">
            <div class="caption">
                Latest Bookings
            </div>
        </div>
        <div class="portlet-body">
            <div class="table-container">
                <table class="table table-bordered" id="bookings-table-table">
                    <thead>
                    <tr>
                        <th>Booking ID</th>
                        <th>Parent Email</th>
                        <th>Parent First Name</th>
                        <th>Parent Last Name</th>
                        <th>Booking Total</th>
                        <th>Orders</th>
                        <th>Status</th>
                        <th>Payment Type</th>
                        <th class="actions">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($bookings as $booking)
                        <tr data-id="{{ $booking->id }}">
                            <td>{{ $booking->id }}</td>
                            <td>{{ $booking->customer->user->email }}</td>
                            <td>{{ $booking->customer->first_name }}</td>
                            <td>{{ $booking->customer->last_name }}</td>
                            <td>{{ $booking->getPrice() }}</td>
                            <td>
                                <a href="{{ route('admin::bookings.orders', [$booking->id]) }}">{{ $booking->orders->count() }}</a>
                            </td>
                            <td>
                                    <span class="label label-status
                                    {{ $booking->isProcessing() ? 'label-warning' :'' }}
                                    {{ $booking->isPaid() ? 'label-success' :'' }}
                                    {{ $booking->isCancelled() ? 'label-danger' :'' }}">
                                    {{ $booking->showStatus() }}</span>
                            </td>
                            <td>
                                {{ $booking->payment()->showType() }}
                            </td>
                            <td class="btn-group-xs">
                                @if(! $booking->isCancelled())
                                    @permission('booking.cancel')
                                    <button type="button" class="btn btn-xs btn-danger btn-cancel" data-toggle="modal"
                                            data-target=".bs-modal-sm"
                                            data-title="{{ trans('messages.admin.booking.cancel.title') }}"
                                            data-content="{{ trans('messages.admin.booking.cancel.content') }}"
                                            data-url="{{ route('admin::api::bookings.cancel', [$booking->id]) }}"
                                            data-ajax="true"
                                            data-action="cancel"
                                            data-id="{{ $booking->id }}"
                                            data-method="POST">Cancel
                                    </button>
                                    @endpermission
                                @endif

                                @if($booking->isProcessing())
                                    @permission('booking.success')
                                    <button type="button" class="btn btn-xs btn-success btn-confirm" data-toggle="modal"
                                            data-target=".bs-modal-sm"
                                            data-title="{{ trans('messages.admin.booking.confirm.title') }}"
                                            data-content="{{ trans('messages.admin.booking.confirm.content') }}"
                                            data-url="{{ route('admin::api::bookings.success', [$booking->id]) }}"
                                            data-ajax="true"
                                            data-action="confirm"
                                            data-id="{{ $booking->id }}"
                                            data-method="POST">Confirm
                                    </button>
                                    @endpermission
                                @endif
                                <a href="{{ route('admin::bookings.show', $booking->id) }}"
                                   class="btn btn-default">Show</a>
                                <a href="{{ route('admin::parents.show', $booking->customer->id) }}"
                                   class="btn btn-default">Account Details</a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

                <a class="btn btn-success pull-right"
                   href="{{ route('admin::parents.index') }}">
                    Add New Booking
                </a>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    @include('partials.modal-windows.booking.action')
@endsection
