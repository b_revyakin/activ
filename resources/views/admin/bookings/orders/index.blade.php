@extends('layouts.admin')

@section('title')
    Booking Camps
@endsection

@section('breadcrumbs')
    <li><span>Event Bookings</span></li>
@endsection

@section('js')
    <script src="/js/admin/booking/bookings.js"></script>
@endsection

@section('content')
    <div class="portlet light portlet-fit portlet-datatable ">
        <div class="portlet-title">
            <div class="caption">
                Booking Camps
            </div>
        </div>
        <div class="portlet-body">
            <div class="table-container">
                @include('partials.orders.index', compact('orders'))
            </div>
        </div>
    </div>
@endsection
