@extends('layouts.admin')

@section('title')
    {{--    {{ $product->name }}--}}
@endsection

@section('breadcrumbs')
    <li><span>Bookings</span></li>
@endsection

@section('content')
    <div class="portlet light portlet-fit portlet-datatable ">
        <div class="portlet-title">
            <div class="caption">
                Show Booking
            </div>
        </div>
        <div class="portlet-body">
            <div class="table-container">
                <dl class="dl-horizontal">
                    <dt>Booking Id</dt>
                    <dd>{{ $booking->id }}</dd>
                    <dt>Price</dt>
                    <dd>{{ $booking->getPrice() }}</dd>
                    <dt>Status</dt>
                    <dd>{{ $booking->showStatus() }}</dd>
                    <dt>Parent</dt>
                    <dd>{{ $booking->customer->first_name }}  {{ $booking->customer->last_name }}</dd>
                    <dt>Payment:</dt>
                    <dd>
                        Type: {{ $booking->payment()->showType() }}<br>
                        Status: {{ $booking->payment()->showStatus() }}<br>
                    </dd>
                    <dt>Bookings:</dt>
                  <div>
                    @include('partials.emails.booking.content', compact('booking'))
                  </div>
                  <div class="clearfix"></div>
                </dl>
            </div>
        </div>
    </div>
@endsection
