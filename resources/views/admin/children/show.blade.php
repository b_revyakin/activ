@extends('layouts.admin')

@section('title')
    Child
@endsection

@section('breadcrumbs')
    {{--<li><a href="{{ route('admin::bookings.index') }}" class="active"></a></li>--}}
@endsection

@section('content')
    <div class="portlet light portlet-fit portlet-datatable ">
        <div class="portlet-title">
            <div class="caption">
                Show Children
            </div>
        </div>
        <div class="portlet-body">
            <div class="table-container">
                <dl class="dl-horizontal">
                    <dt>Name</dt>
                    <dd>
                        {{ $child->getNameAttribute() }}
                    </dd>
                    <dt>Parent</dt>
                    <dd>
                        {{ $child->parent->first_name }} {{ $child->parent->last_name }}
                    </dd>
                    <dt>Email</dt>
                    <dd>{{ $child->email }}</dd>
                    <dt>Age</dt>
                    <dd>{{ $child->getAgeAttribute() }}</dd>
                    <dt>Birthday</dt>
                    <dd>{{ $child->birthday }}</dd>
                    <dt>Sex</dt>
                    <dd>{{ $child->getSexStringAttribute() }}</dd>
                    <dt>Relationship</dt>
                    <dd>{{ $child->relationship }}</dd>
                    <dt>School</dt>
                    <dd>{{ $child->school }}</dd>
                    <dt>Year In School</dt>
                    <dd>{{ $child->year_in_school }}</dd>
                </dl>
            </div>
        </div>
    </div>
@endsection
