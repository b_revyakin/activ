@extends('layouts.admin')

@section('title')
    Add New Coupon
@endsection

@section('breadcrumbs')
    <li><a href="{{ route('admin::coupons.index') }}">Coupons</a></li>  <i class="fa fa-circle"></i>
    <li><span>Add New Coupon</span></li>
@endsection

@section('content')
    <div class="portlet light portlet-fit portlet-datatable ">
        <div class="portlet-title">
            <div class="caption">
                Create New Coupon
            </div>
        </div>
        <div class="portlet-body">
            <div class="table-container">
                <form action="{{ route('admin::coupons.store') }}" method="POST">
                    {{ csrf_field() }}

                    <div class="form-group">
                        <label for="phrase">Phrase</label>
                        <input type="text" class="form-control" id="phrase" placeholder="Phrase" name="phrase"
                               value="{{ old('phrase') }}">
                    </div>

                    <div class="form-group">
                        <label for="percent">Percent</label>
                        <input type="text" class="form-control" id="percent" placeholder="Percent"
                               name="percent"
                               value="{{ old('percent') }}">
                    </div>

                    <button type="submit" class="btn btn-default">
                        Create
                    </button>
                </form>
            </div>
        </div>
    </div>
@endsection
