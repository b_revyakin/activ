@extends('layouts.admin')

@section('title')
    Edit '{{ $coupon->phrase }}'
@endsection

@section('breadcrumbs')
    <li><a href="{{ route('admin::coupons.index') }}">Coupons</a></li>  <i class="fa fa-circle"></i>
    <li><a href="{{ route('admin::coupons.show', [$coupon->id]) }}">{{ $coupon->phrase }}</a></li>  <i
            class="fa fa-circle"></i>
    <li><span>Edit</span></li>
@endsection

@section('content')
    <div class="portlet light portlet-fit portlet-datatable ">
        <div class="portlet-title">
            <div class="caption">
                Edit Coupon
            </div>
        </div>
        <div class="portlet-body">
            <div class="table-container">
                <form action="{{ route('admin::coupons.update', [$coupon->id]) }}" method="POST">
                    {{ csrf_field() }}
                    <input type="hidden" name="_method" value="PUT">

                    <div class="form-group">
                        <label for="phrase">Phrase</label>
                        <input type="text" class="form-control" id="phrase" placeholder="Phrase" name="phrase"
                               value="{{ old('phrase', $coupon->phrase) }}">
                    </div>

                    <div class="form-group">
                        <label for="percent">Percent</label>
                        <input type="text" class="form-control" id="percent" placeholder="Percent"
                               name="percent"
                               value="{{ old('percent', $coupon->percent) }}">
                    </div>

                    <button type="submit" class="btn btn-default">
                        Update
                    </button>
                </form>
            </div>
        </div>
    </div>
@endsection
