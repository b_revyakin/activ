@extends('layouts.admin')

@section('title')
    Event Archive
@endsection

@section('breadcrumbs')
    <li><span>Event Archive</span></li>
@endsection

@section('js')
    <script src="/js/admin/events/events.js"></script>
@endsection

@section('content')
    <div class="portlet light portlet-fit portlet-datatable ">
        <div class="portlet-title">
            <div class="caption">
                Event Archive
            </div>
            <div class="tools"></div>
        </div>
        <div class="portlet-body">
            <div>
            @include('partials.events.index', compact('events'))
            </div>
        </div>
    </div>
@endsection
