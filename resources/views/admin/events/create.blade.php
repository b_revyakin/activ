@extends('layouts.admin')

@section('title')
    Create New Event
@endsection

@section('breadcrumbs')
    <li><a href="{{ route('admin::events.index') }}">Events</a></li> <i class="fa fa-circle"></i>
    <li><span>Create New Event</span></li>
@endsection

@section('js')
    <script src="/js/admin/events/events.js"></script>
    <script src="{{ asset('js/admin/fileinput/js/fileinput.js') }}"></script>
    <script src="/js/admin/text-editor.js"></script>
@endsection

@section('content')
    <div class="portlet light portlet-fit portlet-datatable ">
        <div class="portlet-title">
            <div class="caption">
                Create New Event
            </div>
        </div>
        <div class="portlet-body">
            <div class="table-container">
                <form action="{{ route('admin::events.store') }}" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}

                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" class="form-control" id="name" placeholder="Name" name="name"
                               value="{{ old('name') }}">
                    </div>

                    <div class="form-group">
                        <label for="venue">Venue</label>
                        <select class="form-control" name="venue_id" id="venue">
                            @foreach($venues as $venue)
                                <option {{ old('venue_id') && $venue->id == old('venue_id') ? 'selected' : '' }} value="{{ $venue->id }}">
                                    {{ $venue->name }}
                                </option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="season">Season</label>
                        <select class="form-control" name="season_id" id="season">
                            @foreach($seasons as $season)
                                <option {{ old('season_id') && $season->id == old('season_id') ? 'selected' : '' }} value="{{ $season->id }}">
                                    {{ $season->name }}
                                </option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="start-date">Event Start Date</label>
                        <input type="text" class="form-control datepicker" id="start-date" name="start_date"
                               value="{{ old('start_date') }}">
                    </div>

                    <div class="form-group">
                        <label for="start-time">Event Start Time</label>
                        <input type="time" class="form-control" id="start-time" name="start_time"
                               value="{{ old('start_time') }}">
                    </div>

                    <div class="form-group">
                        <label for="end-date">Event End Date</label>
                        <input type="text" class="form-control datepicker" id="end-date" name="end_date"
                               value="{{ old('end_date') }}">
                    </div>

                    <div class="form-group">
                        <label for="end-time">Event End Time</label>
                        <input type="time" class="form-control" id="end-time" name="end_time"
                               value="{{ old('end_time') }}">
                    </div>

                    <div>
                        <h3>Exception Dates:</h3>

                        <div class="form-group">
                            <label for="exception-date">Date</label>
                            <input type="text" class="form-control datepicker" id="exception-date">
                        </div>

                        <button class="btn btn-success" type="button" id="exception-date-add">Add</button>

                        <div>
                            <ul class="exception-dates">

                                <li id="exception-date-example">
                                    <span></span>
                                    <input type="hidden" name="exception_dates[]">
                                    <button type="button" class="btn btn-xs btn-danger exception-date-delete">
                                        Delete
                                    </button>
                                </li>


                                @foreach(old('exception_dates', []) as $exceptionDate)
                                    <li>
                                        <span>{{ $exceptionDate }}</span>
                                        <input type="hidden" name="exception_dates[]" value="{{ $exceptionDate }}">
                                        <button type="button" class="btn btn-xs btn-danger exception-date-delete">
                                            Delete
                                        </button>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="places">Maximum Places</label>
                        <input type="text" class="form-control" id="places" name="places"
                               value="{{ old('places') }}">
                    </div>

                    <div class="form-group">
                        <label for="cost-per-day">Cost Per Day</label>
                        <input type="text" class="form-control" id="cost-per-day" name="cost_per_day"
                               value="{{ old('cost_per_day') }}">
                    </div>

                    <div class="form-group">
                        <label for="cost-per-week">Cost Per Week</label>
                        <input type="text" class="form-control" id="cost-per-week" name="cost_per_week"
                               value="{{ old('cost_per_week') }}">
                    </div>

                    <div class="form-group">
                        <label for="individual-day-booking">Allow Individual Day Booking</label>
                        <select name="allow_individual_day_booking" id="individual-day-booking"
                                class="form-control">
                            <option {{ old('allow_individual_day_booking') == 1 ? 'selected' : '' }} value="1">
                                True
                            </option>
                            <option {{ old('allow_individual_day_booking') == 0 ? 'selected' : '' }} value="0">
                                False
                            </option>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="min-age">Minimum Age</label>
                        <input type="number" class="form-control" id="min-age" name="min_age"
                               value="{{ old('min_age') }}">
                    </div>

                    <div class="form-group">
                        <label for="max-age">Maximum Age</label>
                        <input type="number" class="form-control" id="max-age" name="max_age"
                               value="{{ old('max_age') }}">
                    </div>

                    <div class="form-group">
                        <label for="description">Event Details</label>
                        <textarea class="form-control text-editor" name="description" id="description" cols="30"
                                  rows="10">{{ old('description') }}</textarea>
                    </div>

                    <div class="form-group">
                        <label for="excerpt">Event Overview</label>
                        <textarea class="form-control text-editor" name="excerpt" id="excerpt" cols="30"
                                  rows="10">{{ old('excerpt') }}</textarea>
                    </div>

                    <div class="form-group">
                        <label class="control-label">Select Photos</label>
                        <input id="input-1" type="file" class="file" name="files[]" multiple
                               data-show-upload="false"
                               data-allowed-file-extensions='["jpg", "gif", "jpeg"]'>
                    </div>

                    <button type="submit" class="btn btn-default">
                        Create
                    </button>
                </form>
            </div>
        </div>
    </div>
@endsection
