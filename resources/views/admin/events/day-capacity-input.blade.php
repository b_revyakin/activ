<div class="col-md-2">

    <div class="form-group text-center">
        <label for="name">{{ $dayCapacity->date->toDateString() }}</label>
        <input type="number" class="form-control" id="name"
               placeholder="Name"
               name="{{ $dayCapacity->date->toDateString() }}"
               value="{{ old($dayCapacity->date->toDateString(), $dayCapacity->capacity) }}">
    </div>

</div>
