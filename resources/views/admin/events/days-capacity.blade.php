@extends('layouts.admin')

@section('title')
    {{ $event->name }}'s Capacities
@endsection

@section('breadcrumbs')
    <li><a href="{{ route('admin::events.index') }}">Events</a></li>  <i class="fa fa-circle"></i>
    <li><a href="{{ route('admin::events.show', [$event->id]) }}">{{ $event->name }}</a></li>  <i class="fa fa-circle"></i>
    <li><span>Capacities</span></li>
@endsection

@section('js')
@endsection

@section('content')
    <div class="portlet light portlet-fit portlet-datatable ">

        <div class="portlet-body">
            <div class="table-container">
                <form action="{{ route('admin::events.day-capacities', [$event->id]) }}" method="POST">
                    {{ csrf_field() }}
                    <input type="hidden" name="_method" value="PUT">
                    <div class="row">
                        <div class="col-md-10 col-md-offset-2">
                            <div class="col-md-2 text-center">
                                <h4>Monday</h4>
                            </div>
                            <div class="col-md-2 text-center">
                                <h4>Tuesday</h4>
                            </div>
                            <div class="col-md-2 text-center">
                                <h4>Wednesday</h4>
                            </div>
                            <div class="col-md-2 text-center">
                                <h4>Thursday</h4>
                            </div>
                            <div class="col-md-2 text-center">
                                <h4>Friday</h4>
                            </div>
                        </div>
                    </div>

                    @php($counter = 0)
                    @foreach($days as $dayCapacity)

                        @if($dayCapacity->date->isMonday() || !$counter)
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-md-10 col-md-offset-2">
                                        @endif

                                        @if(!$counter && $dayCapacity->date->dayOfWeek > 1)
                                            @for($i = 1; $i < $dayCapacity->date->dayOfWeek; $i++)
                                                <div class="col-md-2">
                                                </div>
                                            @endfor
                                        @endif

                                        @if($dayCapacity->date->isMonday())
                                            @include('admin.events.day-capacity-input')
                                        @endif

                                        @if($dayCapacity->date->isTuesday())
                                            @include('admin.events.day-capacity-input')
                                        @endif

                                        @if($dayCapacity->date->isWednesday())
                                            @include('admin.events.day-capacity-input')
                                        @endif

                                        @if($dayCapacity->date->isThursday())
                                            @include('admin.events.day-capacity-input')
                                        @endif

                                        @if($dayCapacity->date->isFriday())
                                            @include('admin.events.day-capacity-input')
                                        @endif

                                        @php($counter++)

                                        @if($dayCapacity->date->isFriday() || ($days->count() === $counter))
                                    </div>
                                </div>
                            </div>
                        @endif

                    @endforeach

                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-10 col-md-offset-2">
                                <div class="col-md-2">
                                    <button type="submit" class="btn btn-success">Update</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
