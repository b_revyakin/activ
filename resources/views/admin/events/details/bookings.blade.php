@extends('admin.events.details.layout')

@section('content-table')
    <div id="bookings-table">
        <table class="table table-bordered datatable-with-filters">
            <thead>
            <tr>
                <th>Booking ID</th>
                <th>Child First Name</th>
                <th>Child Last Name</th>
                <th>Age</th>
                <th>Parent First Name</th>
                <th>Parent Last Name</th>
                <th>Events</th>
                <th>Booking Total</th>
                <th>Status</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody>
            @foreach($event->getActiveOrders() as $order)
                <tr data-id="{{ $order->id }}">
                    <td>{{ $order->booking->id }}</td>
                    <td>{{ $order->child->first_name }}</td>
                    <td>{{ $order->child->last_name }}</td>
                    <td>{{ $order->child->getAgeDependenceOfEvent($event) }}</td>
                    <td>{{ $order->child->parent->first_name }}</td>
                    <td>{{ $order->child->parent->last_name }}</td>
                    <td>{{ $order->event->name }}</td>
                    <td>{{ $order->getPrice() }}</td>
                    <td>
                        <span class="label label-status
                        {{ $order->booking->isProcessing() ? 'label-warning' :'' }}
                        {{ $order->booking->isPaid() ? 'label-success' :'' }}
                        {{ $order->booking->isCancelled() ? 'label-danger' :'' }}">
                            {{ $order->booking->showStatus() }}</span>
                    </td>
                    <td class="btn-group-xs">

                        @permission('booking.cancel')
                            <button type="button" class="btn btn-xs btn-danger btn-cancel" data-toggle="modal"
                                    data-target=".bs-modal-sm"
                                    data-title="{{ trans('messages.admin.booking.cancel.title') }}"
                                    data-content="{{ trans('messages.admin.booking.cancel.content') }}"
                                    data-url="{{ route('admin::api::orders.cancel', [$order->id]) }}"
                                    data-ajax="true"
                                    data-action="cancel-order"
                                    data-id="{{ $order->id }}"
                                    data-method="POST">Cancel
                            </button>
                      @endpermission
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

    @include('partials.modal-windows.booking.action')
@endsection