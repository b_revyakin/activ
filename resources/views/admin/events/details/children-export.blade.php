<html>
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}"/>

    <title>Activ</title>

    <style>
        .page-break {
            page-break-after: always;
        }
        .page-break:last-child {
            display: none;
            page-break-after: always;
        }

        .pull-right {
            float: right !important;
        }

        .sub-header {
            font-weight: bold;
            font-size: large;
            display: block;
        }

        .bold {
            font-weight: bold;
        }

        .section {
            margin-top: 0;
            margin-bottom: 0;
        }
    </style>
</head>

<body>
@foreach($sortChildren as $child)

    {{ \Carbon\Carbon::now()->format('m-d-Y') }}<span style="margin-left: 250px"> Activ Camps</span>

    <h1>{{ $event->name }}</h1>

    <p class="section">
        <span class="sub-header">Children Details</span>
        <span class="bold">Surname: </span>{{ $child[0]->last_name }}<br>
        <span class="bold">Forename: </span>{{ $child[0]->first_name }}<br>
        <span class="bold">Age: </span>{{ $child[0]->getAgeDependenceOfEvent($event) }}<br>
        <span class="bold">Date of birth: </span>{{ $child[0]->getBirthday()->format('m-d-Y') }}<br>
        <span class="bold">Gender: </span>{{ $child[0]->sex ? 'Male' : 'Female' }}<br>
        <span class="bold">Year: </span>: {{ $child[0]->yearInSchool ? $child[0]->yearInSchool->title : '' }}<br>
        <span class="bold">School Name: </span>{{ $child[0]->school }}<br>
        <span class="bold">Doctor Name: </span>{{ $child[0]->doctor_name }}<br>
        <span class="bold">Doctor Contact No.: </span>{{ $child[0]->doctor_telephone_number }}<br>
        <span class="bold">Special Requirement: </span>{{ $child[0]->special_requirements }}<br>
        <span class="bold">Medication: </span>{{ $child[0]->medication }}<br>
        <span class="bold">Epipen: </span>{{ $child[0]->epi_pen ? 'Yes' : 'No' }}<br>
        <span class="bold">First Aid Administration: </span>{{ $child[0]->medical_advice_and_treatment? 'Yes' : 'No' }}<br>
        <span class="bold">Emergency Contact: </span>{{ $child[0]->emergency_contact_name }}<br>
        <span class="bold">Emergency Contact No: </span>{{ $child[0]->emergency_contact_number }}<br>
        {{--Notes: <br>--}}
        <span class="bold">Swim Ability: </span>{{ $child[0]->canSwim ? $child[0]->canSwim->option : '' }}<br>
    </p>

    <p class="section">
        <span class="sub-header">Parent details</span>
        Parent Name: {{ $child[0]->parent->first_name . ' ' . $child[0]->parent->last_name }}<br>
        E-mail Address: {{ $child[0]->parent->user->email }}<br>
        Mobile number: {{ $child[0]->parent->mobile }}<br>
        Home number: {{ $child[0]->parent->home_tel }}<br>
        @if($child[0]->parent->work_tel)
            Work number: {{ $child[0]->parent->work_tel }}<br>
        @endif
        @if($child[0]->parent->address_line_1)
            Address 1: {{ $child[0]->parent->address_line_1 }}<br>
        @endif
        @if($child[0]->parent->address_line_2)
            Address 2: {{ $child[0]->parent->address_line_2 }}<br>
        @endif
        @if($child[0]->parent->address_line_3)
            Address 3: {{ $child[0]->parent->address_line_3 }}<br>
        @endif
        Postcode: {{ $child[0]->parent->post_code }}<br>
    </p>

    <p class="section">
        <span class="sub-header">Venue And Dates Details</span>
        Booking dates: {{ implode(array_map(function ($item) { return (new Carbon\Carbon($item['date']))->format('d-m-Y'); }, $child[1]->toArray()), ', ') }}<br>
    </p>

    @if($child[0]->authorised_person_name_1 && $child[0]->authorised_person_number_1)
        <p class="section">
            <span class="sub-header">Emergency Contact Details For Pickup</span>
            Name: {{ $child[0]->authorised_person_name_1 }}<br>
            Mobile: {{ $child[0]->authorised_person_number_1 }}<br>
            @if( $child[0]->authorised_person_name_2 && $child[0]->authorised_person_number_2 )
                Name: {{ $child[0]->authorised_person_name_2 }}<br>
                Mobile: {{ $child[0]->authorised_person_number_2 }}<br>
            @endif
            @if( $child[0]->authorised_person_name_3 && $child[0]->authorised_person_number_3 )
                Name: {{ $child[0]->authorised_person_name_3 }}<br>
                Mobile: {{ $child[0]->authorised_person_number_3 }}<br>
            @endif
        </p>
    @endif


    <div class="page-break"></div>
@endforeach

</body>
</html>