@extends('admin.events.details.layout')

@php($dateChildren = [])

@foreach($event->getArrayWorkDates() as $date)
    @php($formattedDate = $date->toDateString())
    @php($dateChildren[$formattedDate] = [])

    @for ($age = $event->min_age; $age <= $event->max_age; $age++)
        @php($dateChildren[$formattedDate][$age] = 0)
    @endfor
@endforeach

@php($bookings = [])
@for ($age = $event->min_age; $age <= $event->max_age; $age++)
    @php($bookings[$age] = 0)
@endfor



@section('content-table')
    <div id="events-register-table">
        <table class="table table-bordered ">
            <thead>
            <tr>
                <th>Age</th>
                @foreach($event->getArrayWorkDates() as $date)
                    <th>{{ $date->day . '.' . $date->month }}</th>
                @endforeach
                <th>Total Units</th>
                <th>Total Bookings</th>
            </tr>
            </thead>
            <tbody>
            @for ($age = $event->min_age; $age <= $event->max_age; $age++)
                <tr>
                    <td>{{ $age }}</td>
                    @foreach($event->getArrayWorkDates() as $date)
                        @php($formattedDate = $date->toDateString())

                        @php($dateChildren[$formattedDate][$age] += $event->getCountOrdersInDateWithChildAge($date, $age))
                        <td>{{ $dateChildren[$formattedDate][$age] }}</td>
                    @endforeach
                    <td>{{ array_sum(array_pluck($dateChildren, $age)) }}</td>
                    <td>{{ $bookings[$age] = $event->getCountBookingsByAge($age) }}</td>
                </tr>
            @endfor
            <tr>
                <td>Total</td>
                @php($total = 0)
                @foreach($event->getArrayWorkDates() as $date)
                    @php($totalByDate = array_sum($dateChildren[$date->toDateString()]))
                    @php($total += $totalByDate)
                    <td>{{ $totalByDate }}</td>
                @endforeach
                <td>{{ $total }}</td>
                <td>{{ array_sum($bookings) }}</td>
            </tr>
            </tbody>
        </table>
    </div>

    @include('partials.modal-windows.booking.action')
@endsection
