@extends('admin.events.details.layout')

@section('content-table')
    <div id="events-register-table">
        <table class="table table-bordered datatable-with-filters" id="events-table">
            <thead>
            <tr>
                <th>Booking ID</th>
                <th>Children First Name</th>
                <th>Children Last Name</th>
                <th>Age</th>
                <th>Gender</th>
                @foreach($event->getArrayWorkDates() as $date)
                    <th>{{ $date->day . '.' . $date->month }}</th>
                @endforeach
                <th>Epipen</th>
                <th>Medical</th>
                <th>Special requirements</th>
                <th>Paid</th>
                <th>Status</th>
                <th class="actions">Actions</th>
            </tr>
            </thead>
            <tbody>
            @foreach($event->getActiveOrders() as $order)
                <tr data-id="{{ $order->id }}">
                    <td>{{ $order->booking->id }}</td>
                    <td>{{ $order->child->first_name }}</td>
                    <td>{{ $order->child->last_name }}</td>
                    <td>{{ $order->child->getAgeDependenceOfEvent($event) }}</td>
                    <td>{{ $order->child->sex }}</td>
                    @foreach($event->getArrayWorkDates() as $date)
                        @if(!empty($order->dates()))
                            <td>
                                @foreach($order->dates as $dateOrder)
                                    @if($dateOrder->date->toDateString() == $date->toDateString())
                                        <i class="fa fa-check"></i>
                                    @endif
                                @endforeach
                            </td>
                        @else
                            <td></td>
                        @endif
                    @endforeach
                    <td>{{ $order->child->epi_pen ? 'Yes' : 'No' }}</td>
                    <td>{{ $order->child->medication }}</td>
                    <td class="show-text ">{!! $order->child->special_requirements !!}</td>
                    <td>{{ $order->getPrice() }}</td>
                    <td>
                        <span class="label label-status
                        {{ $order->booking->isProcessing() ? 'label-warning' :'' }}
                        {{ $order->booking->isPaid() ? 'label-success' :'' }}
                        {{ $order->booking->isCancelled() ? 'label-danger' :'' }}">
                            {{ $order->booking->showStatus() }}</span>
                    </td>
                    <td class="btn-group-xs">
                        <a href="{{ route('admin::bookings.show', [$order->booking->id]) }}"
                           class="btn btn-primary">
                            Show
                        </a>
                        <a href="{{ route('admin::events.details.child.export', [$event->id, $order->child->id]) }}"
                           class="btn btn-primary">
                            Export Child
                        </a>

                        @permission('booking.cancel')
                            <button type="button" class="btn btn-xs btn-danger btn-cancel" data-toggle="modal"
                                    data-target=".bs-modal-sm"
                                    data-title="{{ trans('messages.admin.booking.cancel.title') }}"
                                    data-content="{{ trans('messages.admin.booking.cancel.content') }}"
                                    data-url="{{ route('admin::api::orders.cancel', [$order->id]) }}"
                                    data-ajax="true"
                                    data-action="cancel-order"
                                    data-id="{{ $order->id }}"
                                    data-method="POST">Cancel
                            </button>
                        @endpermission
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

    @include('partials.modal-windows.booking.action')
@endsection
