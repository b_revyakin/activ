@extends('admin.events.details.layout')

@section('content-table')
    <div id="event-report-table">
        @include('partials.events.report', compact('event'))
    </div>
@endsection
