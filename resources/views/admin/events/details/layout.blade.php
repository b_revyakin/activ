@extends('layouts.admin')

@section('title')
  {{ $event->name }}
@endsection

@section('breadcrumbs')
  <li><a href="{{ route('admin::events.index') }}">Events</a></li> <i class="fa fa-circle"></i>
  <li><span>{{ $event->name }}</span></li>
@endsection

@section('js')
  <script src="/js/admin/events/datatable.js"></script>
  <script src="/js/modal-window.js"></script>
@endsection

@section('content')

  <div class="portlet light portlet-fit portlet-datatable ">
    <div class="portlet-title">
      <div class="btn-group event-buttons">
        <a id="bookings" class="btn btn-default {{ $page === 'bookings' ? 'active disable' : '' }}"
           href="{{ route('admin::events.details.bookings', [$event->id]) }}">Bookings</a>
        <a id="event-register" class="btn btn-default  {{ $page === 'register' ? 'active disable' : '' }}"
           href="{{ route('admin::events.details.register', [$event->id]) }}">Event Register</a>
        <a id="event-register"
           class="btn btn-default  {{ $page === 'age-breaking' ? 'active disable' : '' }}"
           href="{{ route('admin::events.details.age-breaking', [$event->id]) }}">Age Breakdown</a>
        <a id="event-report" class="btn btn-default  {{ $page === 'report' ? 'active disable' : '' }}"
           href="{{ route('admin::events.details.report', [$event->id]) }}">Event Report</a>
        <a class="btn btn-default"
           href="{{ route('admin::events.details.children.export', [$event->id]) }}">Child Details</a>
        <a class="btn btn-default"
           href="{{ route('admin::events.details.register.export', [$event->id]) }}">Downloadable Registers</a>
        <a class="btn btn-default"
           href="{{ route('admin::events.details.mailing-lists.export', [$event->id]) }}">Mailing Lists</a>
      </div>
    </div>
    <div class="portlet-body">
      <div class="table-container">

        @yield('content-table')
      </div>
    </div>
  </div>



@endsection