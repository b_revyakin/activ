@extends('layouts.admin')

@section('title')
    Events
@endsection

@section('breadcrumbs')
    <li><span>Events</span></li>
@endsection

@section('js')
    <script src="/js/admin/events/events.js"></script>
@endsection

@section('content')
    <div class="portlet light portlet-fit portlet-datatable ">
        <div class="portlet-title">
            <div class="caption">
                Events
            </div>
        </div>
        <div class="portlet-body">
            @include('partials.events.index', compact('events'))

            @permission('event.create')
            <a class="btn btn-success pull-right"
               href="{{ route('admin::events.create') }}">
                Add New Event
            </a>
            @endpermission
          <div class="clearfix"></div>
        </div>
    </div>
@endsection
