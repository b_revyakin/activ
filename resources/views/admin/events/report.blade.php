@extends('layouts.admin')

@section('title')
  Event Reports
@endsection

@section('breadcrumbs')
  <li><span>Event Reports</span></li>
@endsection

@section('js')
  <script src="/js/admin/events/event-reports.js"></script>
@endsection

@php($totals = [])
@foreach($dates as $date)
  @foreach($eventsByVenue as $events)
    @php($totals[$events->first()->venue->id][$date->toDateString()] = 0)
  @endforeach
@endforeach

@php($revenues = [])
@php($bookings = [])
@foreach($eventsByVenue as $events)
  @php($revenues[$events->first()->venue->id] = [])
  @php($bookings[$events->first()->venue->id] = [])

  @foreach($events as $event)
    @php($revenues[$events->first()->venue->id][$event->id] = 0)
    @php($bookings[$events->first()->venue->id][$event->id] = 0)
  @endforeach
@endforeach

@section('content')
  <div class="portlet light portlet-fit portlet-datatable ">
    <div class="portlet-body">
      <div class="row">
        <div class="col-md-12">
          <table class="table table-bordered" id="event-reports">
            <thead>
            <tr>
              <th></th>
              @foreach($dates as $date)
                <th>{{ $date->format('d.m') }}</th>
              @endforeach
              <th>Total Units</th>
              <th>Total Bookings</th>
              @if(Auth::user()->isAdmin())
                <th>Total Revenue</th>
              @endif
            </tr>
            </thead>
            <tbody>
            @foreach($eventsByVenue as $events)
              @php($unitsByEvents = [])
              @php($venue = $events->first()->venue)

              @php($startDay = $events->first()->start_date)
              @php($endDay = $events->first()->end_date)

              <tr class="row-venue">
                <td>{{ $venue->name }}</td>
                @php($i = 1)
                @foreach($dates as $date)
                  @if($date->between($startDay, $endDay))
                    <td>{{ $i++ }}</td>
                  @else
                    <td></td>
                  @endif
                @endforeach
                <td></td>
                <td></td>
                @if(Auth::user()->isAdmin())
                  <td></td>
                @endif
              </tr>

              @foreach($events as $event)
                @php($startDay = $event->start_date)
                @php($endDay = $event->end_date)

                @php($unitsByEvents[$event->id] = [])

                <tr class="row-event">
                  <td>{{ $event->name }}</td>
                  @foreach($dates as $date)
                    @if($date->between($startDay, $endDay))
                      <td>{{ $unitsByEvents[$event->id][$date->toDateString()] = $event->getCountOrdersInDate($date) }}</td>
                    @else
                      <td></td>
                    @endif
                  @endforeach

                  <td>{{ array_sum($unitsByEvents[$event->id]) }}</td>
                  <td>
                    @php($bookings[$venue->id][$event->id] = $event->getActiveOrders()->count())
                    {{ $bookings[$venue->id][$event->id] }}
                  </td>
                  @if(Auth::user()->isAdmin())
                    @php($revenues[$venue->id][$event->id] = $event->getActiveOrders()->sum('price'))
                    <td>£&nbsp;{{ $revenues[$venue->id][$event->id] }}</td>
                  @endif
                </tr>
              @endforeach

              <!-- Total Row by event's group -->
              <tr class="row-total">
                <td>Total</td>
                @foreach($dates as $date)
                  @if(is_venue_and_season_date($date, $events->first()->venue, $events->first()->season))
                    @php($totally = array_sum(array_pluck($unitsByEvents, $date->toDateString())))
                    @php($totals[$events->first()->venue->id][$date->toDateString()] = $totally)
                    <td class="{{ (!$totally) ? 'zero' : '' }}">{{ $totally }}</td>
                  @else
                    <td></td>
                  @endif
                @endforeach
                <td>{{ array_sum($totals[$events->first()->venue->id]) }}</td>
                <td>{{ array_sum($bookings[$venue->id]) }}</td>
                @if(Auth::user()->isAdmin())
                  <td>£&nbsp;{{ array_sum($revenues[$venue->id])}}</td>
                @endif
              </tr>

              <!-- Empty Row -->
              <tr class="row-empty">
                <td></td>
                @foreach($dates as $date)
                  <td></td>
                @endforeach
                <td></td>
                <td></td>
                @if(Auth::user()->isAdmin())
                  <td></td>
                @endif
              </tr>
            @endforeach

            <tr id="row-final">
              @php($total = [])
              <td>Total</td>
              @foreach($dates as $date)
                @php($totally = array_sum(array_pluck($totals, $date->toDateString())))
                @php($total[$date->toDateString()] = $totally)
                <td class="{{ (!$totally) ? 'zero' : '' }}">{{ $totally }}</td>
              @endforeach
              <td>{{ array_sum($total) }}</td>
              <td>{{ array_sum(array_collapse($bookings)) }}</td>
              @if(Auth::user()->isAdmin())
                <td>£&nbsp;{{ array_sum(array_collapse($revenues)) }}</td>
              @endif
            </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
@endsection
