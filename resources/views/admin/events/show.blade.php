@extends('layouts.admin')

@section('title')
    {{ $event->name }}
@endsection

@section('breadcrumbs')
    <li><a href="{{ route('admin::events.index') }}">Events</a></li> <i class="fa fa-circle"></i>
    <li><span>{{ $event->name }}</span></li>
@endsection

@section('js')
    <script src="/js/admin/carousel.js"></script>
@endsection

@section('content')
    <div class="portlet light portlet-fit portlet-datatable ">
        <div class="portlet-title">
            <div class="caption">
                Show Event
            </div>
        </div>
        <div class="portlet-body">
            <div class="table-container">
                <dl class="dl-horizontal">
                    <dt>Name</dt>
                    <dd>{{ $event->name }}</dd>
                    <dt>Status</dt>
                    <dd>
                        @include('partials.active-labels', ['model' => $event])
                    </dd>
                    <dt>Venue</dt>
                    <dd>{{ $event->venue->name }}</dd>
                    <dt>Season</dt>
                    <dd>{{ $event->season->name }}</dd>
                    <dt>Start Date</dt>
                    <dd>{{ $event->start_date->format('d-m-Y') }}</dd>
                    <dt>Start Time</dt>
                    <dd>{{ $event->start_time }}</dd>
                    <dt>End Date</dt>
                    <dd>{{ $event->end_date->format('d-m-Y') }}</dd>
                    <dt>End Time</dt>
                    <dd>{{ $event->end_time }}</dd>
                    <dt>Exception Dates:</dt>
                    <dd>
                        @foreach($event->exceptionDates as $exceptionDate)
                            <span>
                                        {{ $exceptionDate->date }}
                                    </span>
                        @endforeach
                    </dd>
                    <dt>Places</dt>
                    <dd>{{ $event->places }}</dd>
                    <dt>Cost Per Day</dt>
                    <dd>{{ $event->cost_per_day }}</dd>
                    <dt>Cost Per Week</dt>
                    <dd>{{ $event->cost_per_week }}</dd>
                    <dt>Allow Individual Day Booking</dt>
                    <dd>{{ $event->allow_individual_day_booking ? 'True' : 'False' }}</dd>
                    <dt>Minimum Age</dt>
                    <dd>{{ $event->min_age }}</dd>
                    <dt>Maximum Age</dt>
                    <dd>{{ $event->max_age }}</dd>
                    <dt>Details</dt>
                    <dd>{!! $event->description !!}</dd>
                </dl>

                @include('partials.photos.show', [ 'model' => $event, 'routeName' => 'admin::event.photos' ])
            </div>
        </div>
    </div>
@endsection
