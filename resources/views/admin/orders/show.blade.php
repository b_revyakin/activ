@extends('layouts.admin')

@section('title')
    Booking By Event: {{ $order->event->name }}
@endsection

@section('breadcrumbs')
    <li><span>Event Bookings</span></li>
@endsection

@section('content')
    <div class="portlet light portlet-fit portlet-datatable ">
        <div class="portlet-title">
            <div class="caption">
                Show Booking
            </div>
        </div>
        <div class="portlet-body">
            <div class="table-container">
                <dl class="dl-horizontal">
                    <dt>Booking Id</dt>
                    <dd>{{ $order->booking->id }}</dd>
                    <dt>Price</dt>
                    <dd>{{ $order->booking->getPrice() }}</dd>
                    <dt>Status</dt>
                    <dd>{{ $order->booking->showStatus() }}</dd>
                    <dt>Parent</dt>
                    <dd>{{ $order->child->parent->first_name }}  {{ $order->child->parent->last_name }}</dd>
                    <dt>Children:</dt>
                    <dd>
                        Name: {{ $order->child->first_name }} {{ $order->child->last_name }}<br>
                        Birthday: {{ $order->child->birthday }}
                    </dd>
                    <dt>Event</dt>
                    <dd>{{ $order->event->name }}</dd>
                    <dt>Payment:</dt>
                    <dd>
                        Type: {{ $order->booking->payment()->showType() }}<br>
                        Status: {{ $order->booking->payment()->showStatus() }}<br>
                    </dd>
                </dl>
            </div>
        </div>
    </div>
@endsection
