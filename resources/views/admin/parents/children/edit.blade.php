@extends('layouts.admin')

@section('title')
    Edit {{ $parent->name }}'s child {{ $child->name }}
@endsection

@section('breadcrumbs')
    <li><a href="{{ route('admin::parents.index') }}">Parents</a></li> <i class="fa fa-circle"></i>
    <li><a href="{{ route('admin::parents.show', [$parent->id]) }}">{{ $parent->name }}</a></li> <i class="fa fa-circle"></i>
    <li><a href="{{ route('admin::parents.children.index', [$parent->id]) }}">Children</a></li> <i class="fa fa-circle"></i>
    <li><a href="{{ route('admin::parents.children.show', [$parent->id, $child->id]) }}">{{ $child->name }}</a></li> <i class="fa fa-circle"></i>
    <li><span>Edit</span></li>
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Update New Child to {{ $parent->name() }}</div>

                    <div class="panel-body">
                        @include('partials.validation-errors')

                        <form action="{{ route('admin::parents.children.update', [$parent->id, $child->id]) }}"
                              method="POST">
                            {{ csrf_field() }}
                            <input type="hidden" name="_method" value="PUT">

                            <div class="form-group">
                                <label for="first_name">First Name</label>
                                <input type="text" class="form-control" id="first_name" placeholder="First Name"
                                       name="first_name"
                                       value="{{ old('first_name', $child->first_name) }}">
                            </div>

                            <div class="form-group">
                                <label for="last_name">Last Name</label>
                                <input type="text" class="form-control" id="last_name" placeholder="Last Name"
                                       name="last_name"
                                       value="{{ old('last_name', $child->last_name) }}">
                            </div>
{{--

                            <div class="form-group">
                                <label for="email">Email Address</label>
                                <input type="email" class="form-control" id="email" placeholder="Email Address"
                                       name="email"
                                       value="{{ old('email', $child->email) }}">
                            </div>
--}}

                            <div class="form-group">
                                <label for="birthday">Date of Birth</label>
                                <input type="text" class="form-control datepicker" id="birthday"
                                       placeholder="Date of Birth"
                                       name="birthday"
                                       value="{{ old('birthday', $child->birthday->format('d-m-Y')) }}">
                            </div>

                            <div class="form-group">
                                <label for="sex">Male / Female</label>
                                <select name="sex" id="sex" class="form-control">
                                    <option {{ old('sex', $child->sex) ? 'selected' : '' }} value="1">Male</option>
                                    <option {{ !old('sex', $child->sex) ? 'selected' : '' }} value="0">Female</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="relationship">Relationship to Child</label>
                                <input type="text" class="form-control" id="relationship" name="relationship"
                                       value="{{ old('relationship', $child->relationship) }}">
                            </div>

                            <div class="form-group">
                                <label for="school">School</label>
                                <input type="text" class="form-control" id="school" placeholder="School" name="school"
                                       value="{{ old('school', $child->school) }}">
                            </div>
                            
                            <div class="form-group">
                                <label for="year_in_school">Year in School</label>
                                <select class="form-control" id="year_in_school" name="year_in_school_id">
                                    @foreach($yearInSchool as $item)
                                        <option {{ $child->year_in_school_id === $item->id ? 'selected' : '' }} value="{{ $item->id }}">{{ $item->title }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="can_swim">Can child Swim?</label>
                                <select class="form-control" id="can_swim" name="can_swim_id">
                                    @foreach($swimOptions as $item)
                                        <option {{ $child->can_swim_id === $item->id ? 'selected' : '' }} value="{{ $item->id }}">{{ $item->number . ' - ' . $item->option }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="emergency_contact_name">Emergency Contact Name</label>
                                <input type="text" class="form-control" id="emergency_contact_name"
                                       placeholder="Emergency Contact Name"
                                       name="emergency_contact_name"
                                       value="{{ old('emergency_contact_name', $child->emergency_contact_name) }}">
                            </div>

                            <div class="form-group">
                                <label for="emergency_contact_number">Emergency Contact Number</label>
                                <input type="text" class="form-control" id="emergency_contact_number"
                                       placeholder="Emergency Contact Number"
                                       name="emergency_contact_number"
                                       value="{{ old('emergency_contact_number', $child->emergency_contact_number) }}">
                            </div>

                            <div class="form-group">
                                <label for="doctor_name">Doctor's Name</label>
                                <input type="text" class="form-control" id="doctor_name" placeholder="Doctor's Name"
                                       name="doctor_name"
                                       value="{{ old('doctor_name', $child->doctor_name) }}">
                            </div>

                            <div class="form-group">
                                <label for="doctor_telephone_number">Doctor's Telephone Number</label>
                                <input type="text" class="form-control" id="doctor_telephone_number"
                                       placeholder="Doctor's Telephone Number" name="doctor_telephone_number"
                                       value="{{ old('doctor_telephone_number', $child->doctor_telephone_number) }}">
                            </div>

                            <div class="form-group">
                                <label for="medical_advice_and_treatment">Medical Advice & Treatment</label>
                                <select name="medical_advice_and_treatment" id="medical_advice_and_treatment"
                                        class="form-control">
                                    <option {{ old('medical_advice_and_treatment', $child->medical_advice_and_treatment) ? 'selected' : '' }} value="1">
                                        Yes
                                    </option>
                                    <option {{ !old('medical_advice_and_treatment', $child->medical_advice_and_treatment) ? 'selected' : '' }} value="0">
                                        No
                                    </option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="epi_pen">EpiPen</label>
                                <select name="epi_pen" id="epi_pen" class="form-control">
                                    <option {{ old('epi_pen', $child->epi_pen) ? 'selected' : '' }} value="1">Yes
                                    </option>
                                    <option {{ !old('epi_pen', $child->epi_pen) ? 'selected' : '' }} value="0">No
                                    </option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="medication">Medication</label>
                                <textarea name="medication" id="medication" cols="30" rows="4"
                                          class="form-control">{{ old('medication', $child->medication) }}</textarea>
                            </div>

                            <div class="form-group">
                                <label for="special_requirements">Special Requirements</label>
                                <textarea name="special_requirements" id="special_requirements" cols="30" rows="4"
                                          class="form-control">{{ old('special_requirements', $child->special_requirements) }}</textarea>
                            </div>

                            <button type="submit" class="btn btn-default">
                                Update
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
