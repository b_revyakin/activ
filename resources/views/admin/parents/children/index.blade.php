@extends('layouts.admin')

@section('title')
    {{ $parent->name }}'s Children
@endsection

@section('breadcrumbs')
    <li><a href="{{ route('admin::parents.index') }}">Parents</a></li> <i class="fa fa-circle"></i>
    <li><a href="{{ route('admin::parents.show', [$parent->id]) }}">{{ $parent->name }}</a></li> <i class="fa fa-circle"></i>
    <li><span>Children</span></li>
@endsection

@section('js')
    <script src="/js/admin/users/children.js"></script>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <h3>{{ $parent->name }}'s Children</h3>
            <div class="col-md-12">

                <table class="table table-bordered" id="children-table">
                    <thead>
                    <tr>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th class="age">Age</th>
                        <th class="actions">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($children as $child)
                        <tr>
                            <td>{{ $child->first_name }}</td>
                            <td>{{ $child->last_name }}</td>
                            <td>{{ $child->age }}</td>
                            <td class="btn-group-xs">
                                <a href="{{ route('admin::parents.children.show', [$parent->id, $child->id]) }}"
                                   class="btn btn-primary">
                                    Show
                                </a>
                                <a href="{{ route('admin::parents.children.edit', [$parent->id, $child->id]) }}"
                                   class="btn btn-warning">
                                    Edit
                                </a>
                                <form action="{{ route('admin::parents.children.destroy', [$parent->id, $child->id]) }}"
                                      method="POST" class="inline">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="_method" value="DELETE">

                                    <button type="submit" class="btn btn-danger btn-xs">Remove</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <div>
                    <a class="btn btn-default"
                       href="{{ route('admin::parents.show', [$parent->id]) }}">
                        Back To Parent
                    </a>
                    <a class="btn btn-success pull-right"
                       href="{{ route('admin::parents.children.create', [$parent->id]) }}">
                        Add New Child
                    </a>
                </div>
            </div>
        </div>
    </div>
@endsection
