@extends('layouts.admin')

@section('title')
    {{ $child->name }} is child of {{ $parent->name }}
@endsection

@section('breadcrumbs')
    <li><a href="{{ route('admin::parents.index') }}">Parents</a></li> <i class="fa fa-circle"></i>
    <li><a href="{{ route('admin::parents.show', [$parent->id]) }}">{{ $parent->name }}</a></li> <i class="fa fa-circle"></i>
    <li><a href="{{ route('admin::parents.children.index', [$parent->id]) }}">Children</a></li> <i class="fa fa-circle"></i>
    <li><span>{{ $child->name }}</span></li>
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <span>{{ $parent->name }}'s Child</span>
                        <span class="btn-group-xs pull-right">
                            <a class="btn btn-primary"
                               href="{{ route('admin::parents.children.index', [$parent->id]) }}">
                                All Children
                            </a>
                            <a href="{{ route('admin::parents.children.edit', [$parent->id, $child->id]) }}"
                               class="btn btn-warning">Edit</a>
                        </span>
                    </div>

                    <div class="panel-body">
                        <dl class="dl-horizontal">
                            <dt>First Name</dt>
                            <dd>{{ $child->first_name }}</dd>
                            <dt>Last Name</dt>
                            <dd>{{ $child->last_name }}</dd>
                            {{--<dt>Email Address</dt>
                            <dd>{{ $child->email }}</dd>--}}
                            <dt>Date of Birth</dt>
                            <dd>{{ (new \Carbon\Carbon($child->birthday))->format('d-m-Y') }} ({{ $child->age }} years)</dd>
                            <dt>Sex</dt>
                            <dd>{{ $child->sex_string }}</dd>
                            <dt>Relationship to Child</dt>
                            <dd>{{ $child->relationship }}</dd>
                            <dt>School</dt>
                            <dd>{{ $child->school }}</dd>
                            <dt>Year in School</dt>
                            <dd>{{ $child->yearInSchool->title }}</dd>
                            <dt>Doctor's Name</dt>
                            <dd>{{ $child->doctor_name }}</dd>
                            <dt>Doctor's Telephone Number</dt>
                            <dd>{{ $child->doctor_telephone_number }}</dd>
                            <dt>Can Swim?</dt>
                            <dd>{{ $child->canSwim->number . ' - ' . $child->canSwim->option }}</dd>
                            <dt>Emergency Contact Name</dt>
                            <dd>{{ $child->emergency_contact_name }}</dd>
                            <dt>Emergency Contact Number</dt>
                            <dd>{{ $child->emergency_contact_number }}</dd>
                            <dt>Medical Advice & Treatment</dt>
                            <dd>{{ $child->medical_advice_and_treatment ? 'Yes' : 'No' }}</dd>
                            <dt>EpiPen</dt>
                            <dd>{{ $child->epi_pen ? 'Yes' : 'No' }}</dd>
                            <dt>Medication</dt>
                            <dd>
                                <span class="text">{{ $child->medication }}</span>
                            </dd>
                            <dt>Special Requirements</dt>
                            <dd>
                                <span class="text">{{ $child->special_requirements }}</span>
                            </dd>
                        </dl>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
