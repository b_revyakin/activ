@extends('layouts.admin')

@section('title')
    Create New Parent
@endsection

@section('breadcrumbs')
    <li><a href="{{ route('admin::parents.index') }}">Parents</a></li>  <i class="fa fa-circle"></i>
    <li><span>Create</span></li>
@endsection

@section('content')
    <div class="portlet light portlet-fit portlet-datatable ">
        <div class="portlet-body">
            <div class="table-container">
                @include('partials.customer-register-form', [
                                           'route' => 'admin::parents.store',
                                           'buttonTitle' => 'Create'
                                       ])
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script type="text/javascript" src="/js/countries.js"></script>
@endsection
