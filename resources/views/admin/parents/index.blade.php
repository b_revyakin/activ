@extends('layouts.admin')

@section('title')
    Parents
@endsection

@section('breadcrumbs')
    <li><span>Parents</span></li>
@endsection

@section('js')
    <script src="/js/admin/users/customers.js"></script>
@endsection

@section('content')
    <div class="portlet light portlet-fit portlet-datatable ">
        <div class="portlet-title">
            <div class="caption">
                Parents
            </div>
        </div>
        <div class="portlet-body">
            <div class="table-container">
                <table class="table table-bordered" id="parents-table">
                    <thead>
                    <tr>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Email</th>
                        <th class="status">Active</th>
                        <th>Children</th>
                        <th>Booking</th>
                        <th class="actions">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($parents as $parent)
                        <tr>
                            <td>{{ $parent->first_name }}</td>
                            <td>{{ $parent->last_name }}</td>
                            <td>{{ $parent->user->email }}</td>
                            <td>
                                @include('partials.active-labels', ['model' => $parent])
                            </td>
                            <td>
                                <a href="{{ route('admin::parents.children.index', [$parent->id]) }}">{{ $parent->children()->count() }}</a>
                            </td>
                            <td>
                                <a href="{{ route('admin::parents.bookings.index', $parent->id) }}">View</a>
                            </td>
                            <td class="btn-group-xs">
                                <a href="{{ route('admin::parents.bookings.create', [$parent->id]) }}"
                                   class="btn btn-success">
                                    Book
                                </a>
                                <a href="{{ route('admin::parents.show', [$parent->id]) }}"
                                   class="btn btn-primary">
                                    Show
                                </a>
                                <a href="{{ route('admin::parents.edit', [$parent->id]) }}"
                                   class="btn btn-warning">
                                    Edit
                                </a>
                                @include('partials.active-forms', ['model' => $parent, 'route' => 'admin::parents'])
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

                <a class="btn btn-success pull-right" href="{{ route('admin::parents.create') }}">
                    Add New Parent
                </a>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
@endsection
