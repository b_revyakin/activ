@extends('layouts.admin')

@section('title')
    {{ $parent->name }}
@endsection

@section('breadcrumbs')
    <li><a href="{{ route('admin::parents.index') }}">Parents</a></li>  <i class="fa fa-circle"></i>
    <li><span>{{ $parent->name }}</span></li>
@endsection

@section('content')
  <div class="portlet light portlet-fit portlet-datatable ">

    <div class="portlet-body">
      <div class="table-container">
        <dl class="dl-horizontal">
          <dt>Title</dt>
          <dd>{{ $parent->title }}</dd>
          <dt>First Name</dt>
          <dd>{{ $parent->first_name }}</dd>
          <dt>Last Name</dt>
          <dd>{{ $parent->last_name }}</dd>
          <dt>Email</dt>
          <dd>{{ $parent->user->email }}</dd>
          <dt>Active</dt>
          <dd>
            @include('partials.active-labels', ['model' => $parent])
          </dd>
          <dt>Mobile Number</dt>
          <dd>{{ $parent->mobile }}</dd>
          <dt>Home Telephone</dt>
          <dd>{{ $parent->home_tel }}</dd>
          <dt>Work Telephone</dt>
          <dd>{{ $parent->work_tel }}</dd>
          <dt>Address Line 1</dt>
          <dd>{{ $parent->address_line_1 }}</dd>
          <dt>Address Line 2</dt>
          <dd>{{ $parent->address_line_2 }}</dd>
          <dt>Town / City</dt>
          <dd>{{ $parent->city }}</dd>
          <dt>County</dt>
          <dd>{{ $parent->county }}</dd>
          <dt>Country</dt>
          <dd>{{ $parent->country->title }}</dd>
          <dt>Post Code</dt>
          <dd>{{ $parent->post_code }}</dd>
          <dt>How did you hear about us?</dt>
          <dd>{{ $parent->hear_from }}</dd>
        </dl>
      </div>
    </div>
  </div>
@endsection
