@extends('layouts.admin')

@section('title')
    Bookings
@endsection

@section('breadcrumbs')
    <li><span>Payments</span></li>
@endsection

@section('js')
    <script src="/js/admin/payments/payments.js"></script>
@endsection

@section('content')
    <div class="portlet light portlet-fit portlet-datatable ">
        <div class="portlet-title">
            <div class="caption">
                Payments by Booking
            </div>
        </div>
        <div class="portlet-body">
            <div class="table-container">
                <table class="table table-bordered" id="payments-data-table">
                    <thead>
                    <tr>
                        <th>Payment ID</th>
                        <th>Amount</th>
                        <th>Type</th>
                        <th>Status</th>
                        <th>Date</th>
                        <th class="actions">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($payments as $payment)
                        <tr>
                            <td>{{ $payment->id }}</td>
                            <td>{{ $payment->amount }}</td>
                            <td>{{ $payment->type }}</td>
                            <td>
                                <span class="label {{ $payment->isProcessing() ? 'label-warning' :'' }}
                                {{ $payment->isPaid() ? 'label-success' :'' }}
                                {{ $payment->isCancelled() ? 'label-danger' :'' }}">
                                    {{ $payment->showStatus() }}</span>
                            </td>
                            <td>{{ $payment->updated_at }}</td>
                            <td class="btn-group-xs">
                                @if($payment->isProcessing())
                                    <form action=""
                                          method="POST">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="_method" value="DELETE">

                                        <button class="btn btn-danger btn-xs">Cancel</button>
                                    </form>
                                    <form action=""
                                          method="POST">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="_method" value="PUT">

                                        <button class="btn btn-success btn-xs">Confirm</button>
                                    </form>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
