@extends('layouts.admin')

@section('title')
    Add New Product
@endsection

@section('breadcrumbs')
    <li><a href="{{ route('admin::products.index') }}">Products</a></li> <i class="fa fa-circle"></i>
    <li><a href="{{ route('admin::products.create') }}" class="active">Add New Product</a></li>
@endsection

@section('js')
  <script src="{{ asset('js/admin/fileinput/js/fileinput.js') }}"></script>
@endsection

@section('content')
    <div class="portlet light portlet-fit portlet-datatable ">
        <div class="portlet-title">
            <div class="caption">
                Create New Product
            </div>
        </div>
        <div class="portlet-body">
            <div class="table-container">
              <form action="{{ route('admin::products.store') }}" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}

                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" class="form-control" id="name" placeholder="Name" name="name"
                               value="{{ old('name') }}">
                    </div>

                <div class="form-group">
                  <label class="control-label">Select Image</label>
                  <input id="input-1" type="file" class="file" name="image"
                         data-show-upload="false"
                         data-allowed-file-extensions='["jpg", "gif", "jpeg", "png"]'>
                </div>

                <button type="submit" class="btn btn-default">
                        Create
                    </button>
                </form>
            </div>
        </div>
    </div>
@endsection
