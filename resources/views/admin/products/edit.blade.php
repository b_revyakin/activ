@extends('layouts.admin')

@section('title')
    Edit '{{ $product->name }}'
@endsection

@section('breadcrumbs')
    <li><a href="{{ route('admin::products.index') }}" class="active">Products</a></li> <i class="fa fa-circle"></i>
    <li><a href="{{ route('admin::products.show', [$product->id]) }}">{{ $product->name }}</a></li> <i class="fa fa-circle"></i>
    <li><a href="{{ route('admin::products.edit', [$product->id]) }}" class="active">Edit</a></li>
@endsection

@section('js')
    <script src="{{ asset('js/admin/fileinput/js/fileinput.js') }}"></script>
@endsection

@section('content')
    <div class="portlet light portlet-fit portlet-datatable ">
        <div class="portlet-title">
            <div class="caption">
                Edit Product
            </div>
        </div>
        <div class="portlet-body">
            <div class="table-container">
                <form action="{{ route('admin::products.update', [$product->id]) }}" method="POST"
                      enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <input type="hidden" name="_method" value="PUT">

                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" class="form-control" id="name" placeholder="Name" name="name"
                               value="{{ old('name', $product->name) }}">
                    </div>

                    @if($product->image)
                        <img class="image" src="/{{ $product->imageThumbnail->path }}" alt="Product Image">
                    @endif

                    <div class="form-group">
                        <label class="control-label">Select Image</label>
                        <input id="input-1" type="file" class="file" name="image"
                               data-show-upload="false"
                               data-allowed-file-extensions='["jpg", "gif", "jpeg", "png"]'>
                    </div>

                    <button type="submit" class="btn btn-default">
                        Update
                    </button>
                </form>
            </div>
        </div>
    </div>
@endsection
