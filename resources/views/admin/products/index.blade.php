@extends('layouts.admin')

@section('title')
    Products
@endsection

@section('breadcrumbs')
    <li><a href="{{ route('admin::products.index') }}" class="active">Products</a></li>
@endsection

@section('js')
    <script src="/js/admin/booking/products.js"></script>
@endsection

@section('content')
    <div class="portlet light portlet-fit portlet-datatable">
        <div class="portlet-title">
            <div class="caption">
                Products
            </div>
        </div>
        <div class="portlet-body">
            <div class="table-container">
                <table class="table table-bordered" id="products-table">
                    <thead>
                    <tr>
                        <th>Product Name</th>
                        <th>Image</th>
                        <th class="status">Status</th>
                        <th class="actions">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($products as $product)
                        <tr>
                            <td>{{ $product->name }}</td>
                            <td>
                                @if($product->imageThumbnail)
                                    <img class="image" src="/{{ $product->imageThumbnail->path }}" alt="Product Image">
                                @endif
                            </td>
                            <td>
                                @include('partials.active-labels', ['model' => $product])
                            </td>
                            <td class="btn-group-xs">
                                <a href="{{ route('admin::products.show', [$product->id]) }}"
                                   class="btn btn-primary">
                                    Show
                                </a>
                                <a href="{{ route('admin::products.edit', [$product->id]) }}"
                                   class="btn btn-warning">
                                    Edit
                                </a>
                                <a href="{{ route('admin::products.variants.index', [$product->id]) }}"
                                   class="btn btn-default">
                                    Variants
                                </a>
                                @include('partials.active-forms', ['model' => $product, 'route' => 'admin::products'])
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

                <a class="btn btn-success pull-right"
                   href="{{ route('admin::products.create') }}">
                    Add New Product
                </a>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
@endsection
