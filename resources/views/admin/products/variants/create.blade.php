@extends('layouts.admin')

@section('title')
  Add New Variant
@endsection

@section('breadcrumbs')
    <li><a href="{{ route('admin::products.index') }}" class="active">Products</a></li> <i class="fa fa-circle"></i>
    <li><a href="{{ route('admin::products.show', [$product->id]) }}" class="active">{{ $product->name }}</a></li> <i
            class="fa fa-circle"></i>
    <li><a href="{{ route('admin::products.variants.index', [$product->id]) }}" class="active">Variants</a></li> <i
            class="fa fa-circle"></i>
    <li>Create</li>
@endsection

@section('content')
    <div class="portlet light portlet-fit portlet-datatable ">
        <div class="portlet-title">
            <div class="caption">
              Add New Variant
            </div>
        </div>
        <div class="portlet-body">
            <div class="table-container">
              <form action="{{ route('admin::products.variants.store', [$product->id]) }}" method="POST">
                    {{ csrf_field() }}

                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" class="form-control" id="name" placeholder="Name" name="name"
                               value="{{ old('name') }}">
                    </div>

                    <div class="form-group">
                        <label for="price">Price</label>
                        <input type="text" class="form-control" id="price" placeholder="Price" name="price"
                               value="{{ old('price') }}">
                    </div>

                    <div class="form-group">
                        <label for="price">Count</label>
                        <input type="text" class="form-control" id="price" placeholder="Count" name="count"
                               value="{{ old('count') }}">
                    </div>

                    <button type="submit" class="btn btn-default">
                        Create
                    </button>
                </form>
            </div>
        </div>
    </div>
@endsection
