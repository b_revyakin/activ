@extends('layouts.admin')

@section('title')
    Edit '{{ $productVariant->name }}'
@endsection

@section('breadcrumbs')
    <li><a href="{{ route('admin::products.index') }}" class="active">Products</a></li> <i class="fa fa-circle"></i>
    <li><a href="{{ route('admin::products.show', [$product->id]) }}" class="active">{{ $product->name }}</a></li> <i
            class="fa fa-circle"></i>
    <li><a href="{{ route('admin::products.variants.index', [$product->id]) }}" class="active">Types</a></li> <i
            class="fa fa-circle"></i>
    <li>{{ $productVariant->name }}</li> <i class="fa fa-circle"></i>
    <li>Edit</li>
@endsection

@section('content')
    <div class="portlet light portlet-fit portlet-datatable">
        <div class="portlet-title">
            <div class="caption">
                Edit Variant
            </div>
        </div>
        <div class="portlet-body">
            <div class="table-container">
                <form action="{{ route('admin::products.variants.update', [$product->id, $productVariant->id]) }}"
                      method="POST">
                    {{ csrf_field() }}
                    <input type="hidden" name="_method" value="PUT">

                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" class="form-control" id="name" placeholder="Name" name="name"
                               value="{{ old('name', $productVariant->name) }}">
                    </div>

                    <div class="form-group">
                        <label for="price">Price</label>
                        <input type="text" class="form-control" id="price" placeholder="Price" name="price"
                               value="{{ old('price', $productVariant->price) }}">
                    </div>

                    <div class="form-group">
                        <label for="price">Count</label>
                        <input type="text" class="form-control" id="price" placeholder="Count" name="count"
                               value="{{ old('count', $productVariant->count) }}">
                    </div>

                    <button type="submit" class="btn btn-default">
                        Update
                    </button>
                </form>
            </div>
        </div>
    </div>
@endsection
