@extends('layouts.admin')

@section('title')
    Variants
@endsection

@section('breadcrumbs')
    <li><a href="{{ route('admin::products.index') }}" class="active">Products</a></li> <i class="fa fa-circle"></i>
    <li><a href="{{ route('admin::products.show', [$product->id]) }}" class="active">{{ $product->name }}</a></li> <i
            class="fa fa-circle"></i>
    <li>Variants</li>
@endsection

@section('js')
    <script src="/js/admin/booking/types.js"></script>
@endsection

@section('content')
    <div class="portlet light portlet-fit portlet-datatable">
        <div class="portlet-title">
            <div class="caption">
                Variants
            </div>
        </div>
        <div class="portlet-body">
            <div class="table-container">
                <table class="table table-bordered" id="types-table">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Price</th>
                        <th>Count</th>
                        <th class="status">Status</th>
                        <th class="actions">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($variants as $variant)
                        <tr>
                            <td>{{ $variant->name }}</td>
                            <td>{{ $variant->price }}</td>
                            <td>{{ $variant->count }}</td>
                            <td>
                                @include('partials.active-labels', ['model' => $variant])
                            </td>
                            <td class="btn-group-xs">
                                <a href="{{ route('admin::products.variants.edit', [$product->id, $variant->id]) }}"
                                   class="btn btn-warning">
                                    Edit
                                </a>
                                @if($variant->isActive())
                                    <form action="{{ route('admin::products.variants.deactivate', [$product->id, $variant->id]) }}"
                                          method="POST" class="inline">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="_method" value="PUT">
                                        <button type="submit" class="btn btn-danger btn-xs">Archive</button>
                                    </form>
                                @else
                                    <form action="{{ route('admin::products.variants.activate', [$product->id, $variant->id]) }}"
                                          method="POST" class="inline">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="_method" value="PUT">
                                        <button type="submit" class="btn btn-success btn-xs">Activate</button>
                                    </form>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

                <a class="btn btn-success pull-right"
                   href="{{ route('admin::products.variants.create', [$product->id]) }}">
                    Add New Variant
                </a>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
@endsection
