@extends('layouts.admin')

@section('title')
    Add New Season
@endsection

@section('breadcrumbs')
    <li><a href="{{ route('admin::seasons.index') }}">Seasons</a></li>  <i class="fa fa-circle"></i>
    <li><span>Add New Season</span></li>
@endsection

@section('content')
    <div class="portlet light portlet-fit portlet-datatable ">
        <div class="portlet-title">
            <div class="caption">
                Create New Season
            </div>
            <div class="actions">
                <a href="{{ route('admin::seasons.index') }}" class="btn btn-primary btn-sm">All Seasons</a>
            </div>
        </div>
        <div class="portlet-body">
            <div class="table-container">
                <form action="{{ route('admin::seasons.store') }}" method="POST">
                    {{ csrf_field() }}

                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" class="form-control" id="name" placeholder="Name" name="name"
                               value="{{ old('name') }}">
                    </div>

                    <div class="form-group">
                        <label for="from">Season Start Date</label>
                        <input type="text" class="form-control datepicker" id="from" name="from"
                               value="{{ old('from') }}">
                    </div>

                    <div class="form-group">
                        <label for="to">Season End Date</label>
                        <input type="text" class="form-control datepicker" id="to" name="to"
                               value="{{ old('to') }}">
                    </div>

                    <div class="form-group">
                        <label for="description">Description</label>
                        <textarea class="form-control" name="description" id="description" cols="30"
                                  rows="10">{{ old('description') }}</textarea>
                    </div>

                    <button type="submit" class="btn btn-default">
                        Create
                    </button>
                </form>
            </div>
        </div>
    </div>
@endsection
