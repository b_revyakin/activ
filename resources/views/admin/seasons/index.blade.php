@extends('layouts.admin')

@section('title')
    Seasons
@endsection

@section('breadcrumbs')
    <li><span>Seasons</span></li>
@endsection

@section('js')
    <script src="/js/admin/events/seasons.js"></script>
@endsection

@section('content')
    <div class="portlet light portlet-fit portlet-datatable ">
        <div class="portlet-title">
            <div class="caption">
                Seasons
            </div>
        </div>
        <div class="portlet-body">
            <div class="table-container">
                <table class="table table-bordered" id="seasons-table">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th class="status">Active</th>
                        <th class="actions">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($seasons as $season)
                        <tr>
                            <td>{{ $season->name }}</td>
                            <td>
                                @include('partials.active-labels', ['model' => $season])
                            </td>
                            <td>
                                <div class="btn-group-xs">
                                    {{--
                                    <a href="{{ route('admin::seasons.show', [$season->id]) }}"
                                       class="btn btn-primary">
                                        Show
                                    </a>
                                    --}}
                                    <a href="{{ route('admin::seasons.edit', [$season->id]) }}"
                                       class="btn btn-warning">
                                        Edit
                                    </a>
                                    @include('partials.active-forms', ['model' => $season, 'route' => 'admin::seasons'])
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

                <a class="btn btn-success pull-right"
                   href="{{ route('admin::seasons.create') }}">
                    Add New Season
                </a>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
@endsection
