@extends('layouts.admin')

@section('title')
    Site Managers
@endsection

@section('breadcrumbs')
    <li><span>Site Managers</span></li>
@endsection

@section('js')
    <script src="/js/admin/users/admins.js"></script>
@endsection

@section('content')
    <div class="portlet light portlet-fit portlet-datatable ">
        <div class="portlet-title">
            <div class="caption">
                All Site Managers
            </div>
        </div>
        <div class="portlet-body">
            <div class="table-container">
                <table class="table table-bordered" id="admins-table">
                    <thead>
                    <tr>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Email</th>
                        <th class="status">Status</th>
                        <th class="actions">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($admins as $admin)
                        <tr>
                            <td>{{ $admin->staff->first_name }}</td>
                            <td>{{ $admin->staff->last_name }}</td>
                            <td>{{ $admin->email }}</td>
                            <td>
                                @include('partials.active-labels', ['model' => $admin->staff])
                            </td>
                            <td>
                                <a href="{{ route('admin::site-managers.show', [$admin->id]) }}"
                                   class="btn btn-primary btn-xs">
                                    Show
                                </a>
                                <a href="{{ route('admin::site-managers.edit', [$admin->id]) }}"
                                   class="btn btn-warning btn-xs">
                                    Edit
                                </a>
                                @include('partials.active-forms', ['model' => $admin->staff, 'route' => 'admin::staffs'])
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <a class="btn btn-success pull-right" href="{{ route('admin::site-managers.create') }}">
                    Create New Site Manager
                </a>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
@endsection
