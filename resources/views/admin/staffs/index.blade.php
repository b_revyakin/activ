@extends('layouts.admin')

@section('title')
    Staffs
@endsection

@section('breadcrumbs')
    <li><span>Staffs</span></li>
@endsection

@section('js')
    <script src="/js/admin/users/staffs.js"></script>
@endsection

@section('content')
    <div class="portlet light portlet-fit portlet-datatable ">
        <div class="portlet-title">
            <div class="caption">
                All Staffs
            </div>
        </div>
        <div class="portlet-body">
            <div class="table-container">
                <table class="table table-bordered" id="staffs-table">
                    <thead>
                    <tr>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Email</th>
                        <th class="status">Status</th>
                        <th class="actions">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($staffs as $staff)
                        <tr>
                            <td>{{ $staff->staff->first_name }}</td>
                            <td>{{ $staff->staff->last_name }}</td>
                            <td>{{ $staff->email }}</td>
                            <td>
                                @include('partials.active-labels', ['model' => $staff->staff])
                            </td>
                            <td>
                                <a href="{{ route('admin::staffs.show', [$staff->id]) }}"
                                   class="btn btn-primary btn-xs">
                                    Show
                                </a>
                                <a href="{{ route('admin::staffs.edit', [$staff->id]) }}"
                                   class="btn btn-warning btn-xs">
                                    Edit
                                </a>
                                @include('partials.active-forms', ['model' => $staff->staff, 'route' => 'admin::staffs'])
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <a class="btn btn-success pull-right" href="{{ route('admin::staffs.create') }}">
                    Create New Staff
                </a>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
@endsection
