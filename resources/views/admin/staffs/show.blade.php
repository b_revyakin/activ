@extends('layouts.admin')

@section('title')
  {{ $staff->staff->name }}
@endsection

@section('breadcrumbs')
  <li><a href="{{ route('admin::staffs.index') }}">Staffs</a></li>  <i class="fa fa-circle"></i>
  <li><span>{{ $staff->staff->name }}</span></li>
@endsection

@section('content')
  <div class="page-content-inner">
    <div class="row">
      <div class="col-md-12">
        <div class="profile-sidebar">
          <div class="portlet light">
            <div class="profile-userpic">
              <img src="{{ $staff->avatar() }}" class="img-responsive" alt="">
            </div>
            <div class="profile-usertitle">
              <div class="profile-usertitle-name"> {{ $staff->staff->first_name }} {{ $staff->staff->last_name }}</div>
              <div class="profile-desc-text"> {{ $staff->email }} </div>
            </div>
          </div>
          <div class="portlet light ">
            <div>
              <h4 class="profile-desc-title">About</h4>
              <span class="profile-desc-text">{{ $staff->staff->about }}</span>
            </div>
          </div>
        </div>
        <div class="profile-content">
          <div class="row">
            <div class="col-md-12">
              <div class="portlet light ">
                <div class="portlet-title tabbable-line">
                  <div class="caption">
                    Show Staff
                  </div>
                  <div class="actions">
                    <a href="{{ route('admin::staffs.index') }}" class="btn btn-primary btn-sm">All Staffs</a>
                    <a href="{{ route('admin::staffs.edit', [$staff->id]) }}" class="btn btn-warning btn-sm ">Edit</a>
                  </div>
                </div>
                <div class="portlet-body">
                  <dl class="dl-horizontal">
                    <dt>First Name</dt>
                    <dd>{{ $staff->staff->first_name }}</dd>
                    <dt>Last Name</dt>
                    <dd>{{ $staff->staff->last_name }}</dd>
                    <dt>Email</dt>
                    <dd>{{ $staff->email }}</dd>
                    <dt>Active</dt>
                    <dd>
                      @include('partials.active-labels', ['model' => $staff->staff])
                    </dd>
                    <dt>Mobile Number</dt>
                    <dd>{{ $staff->staff->mobile }}</dd>
                    <dt>Qualifications</dt>
                    <dd>{{ $staff->staff->qualifications }}</dd>
                    <dt>About</dt>
                    <dd>{{ $staff->staff->about }}</dd>
                  </dl>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
