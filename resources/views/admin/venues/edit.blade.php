@extends('layouts.admin')

@section('title')
    Edit {{ $venue->name }}
@endsection

@section('breadcrumbs')
    <li><a href="{{ route('admin::venues.index') }}">Venues</a></li> <i class="fa fa-circle"></i>
    <li><a href="{{ route('admin::venues.show', [$venue->id]) }}">{{ $venue->name }}</a></li> <i class="fa fa-circle"></i>
    <li><span>Edit</span></li>
@endsection

@section('js')
    <script src="/js/admin/events/venues.js"></script>
    <script src="{{ asset('js/admin/fileinput/js/fileinput.js') }}"></script>
@endsection

@section('content')
    <div class="portlet light portlet-fit portlet-datatable ">
        <div class="portlet-title">
            <div class="caption">
                Edit Venue
            </div>
            <div class="actions">
                <a href="{{ route('admin::venues.index') }}" class="btn btn-primary btn-sm">All Venues</a>
            </div>
        </div>
        <div class="portlet-body">
            <div class="table-container">
                <form action="{{ route('admin::venues.update', [$venue->id]) }}" method="POST"
                      enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <input type="hidden" name="_method" value="PUT">

                    <div class="form-group">
                        <label for="name">Venue Name</label>
                        <input type="text" class="form-control" id="name" placeholder="Venue Name" name="name"
                               value="{{ old('name', $venue->name) }}">
                    </div>

                    <div class="form-group">
                        <label for="address-line-1">Address Line 1</label>
                        <input type="text" class="form-control" id="address-line-1" placeholder="Address Line 1"
                               name="address_line_1"
                               value="{{ old('address_line_1', $venue->address_line_1) }}">
                    </div>

                    <div class="form-group">
                        <label for="address-line-2">Address Line 2</label>
                        <input type="text" class="form-control" id="address-line-2" placeholder="Address Line 2"
                               name="address_line_2"
                               value="{{ old('address_line_2', $venue->address_line_2) }}">
                    </div>

                    <div class="form-group">
                        <label for="country">Country</label>
                        <input type="text" class="form-control" id="country" placeholder="Country" name="country"
                               value="{{ old('county', $venue->country) }}">
                    </div>

                    <div class="form-group">
                        <label for="city">Town / City</label>
                        <input type="text" class="form-control" id="city" placeholder="Town / City" name="city"
                               value="{{ old('city', $venue->city) }}">
                    </div>

                    <div class="form-group">
                        <label for="post-code">Post Code</label>
                        <input type="text" class="form-control" id="post-code" placeholder="Post Code" name="post_code"
                               value="{{ old('post_code', $venue->post_code) }}">
                    </div>

                    <div class="form-group">
                        <label for="description">Information</label>
                        <textarea name="description" id="description" cols="30" rows="10"
                                  class="form-control">{{ old('description', $venue->description) }}</textarea>
                    </div>

                    <div class="form-group">
                        <label for="input-1" class="control-label">Select Photos</label>
                        <input id="input-1" type="file" class="file" name="files[]" multiple
                               data-show-upload="false"
                               data-allowed-file-extensions='["jpg", "png", "gif", "jpeg"]'>
                    </div>

                    <button type="submit" class="btn btn-default">
                        Update
                    </button>
                </form>
            </div>
        </div>
    </div>
@endsection
