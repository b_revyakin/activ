@extends('layouts.admin')

@section('title')
    Events Venue
@endsection

@section('breadcrumbs')
    <li><span>Venues</span></li>
@endsection

@section('js')
    <script src="/js/admin/events/events.js"></script>
@endsection

@section('content')
    <div class="portlet light portlet-fit portlet-datatable ">
        <div class="portlet-title">
            <div class="caption">
                Manage Current Events
            </div>
        </div>
        <div class="portlet-body">
            <div class="table-container">
                @include('partials.events.index', compact('events'))

                <div class="pull-right">
                    <a class="btn btn-success"
                       href="{{ route('admin::venues.index') }}">
                        See Venues
                    </a>
                    <a class="btn btn-success"
                       href="{{ route('admin::events.create') }}">
                        Add New Event
                    </a>
                </div>
            </div>
        </div>
    </div>
@endsection
