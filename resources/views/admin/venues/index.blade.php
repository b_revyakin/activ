@extends('layouts.admin')

@section('title')
    Venues
@endsection

@section('breadcrumbs')
    <li><span>Venues</span></li>
@endsection

@section('js')
    <script src="/js/admin/events/venues.js"></script>
@endsection

@section('content')
    <div class="portlet light portlet-fit portlet-datatable ">
        <div class="portlet-title">
            <div class="caption">
                Venues
            </div>
        </div>
        <div class="portlet-body">

            <div class="row">
                <div class="col-md-12">

                    <table class="table table-bordered" id="venues-table">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Country</th>
                            <th>City</th>
                            <th class="count">Events</th>
                            <th class="status">Status</th>
                            <th class="actions">Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($venues as $venue)
                            <tr>
                                <td>{{ $venue->name }}</td>
                                <td>{{ $venue->country }}</td>
                                <td>{{ $venue->city }}</td>
                                <td>
                                    <a href="{{ route('admin::venues.events', $venue->id) }}">{{ $venue->events()->count() }}</a>
                                </td>
                                <td>
                                    @include('partials.active-labels', ['model' => $venue])
                                </td>
                                <td>
                                    <div class="btn-group-xs">
                                        <a href="{{ route('admin::venues.show', [$venue->id]) }}"
                                           class="btn btn-primary">
                                            Show
                                        </a>
                                        <a href="{{ route('admin::venues.edit', [$venue->id]) }}"
                                           class="btn btn-warning">
                                            Edit
                                        </a>
                                        @include('partials.active-forms', ['model' => $venue, 'route' => 'admin::venues'])
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                    <a class="btn btn-success pull-right"
                       href="{{ route('admin::venues.create') }}">
                        Add New Venue
                    </a>
                </div>
            </div>
        </div>
    </div>
@endsection
