@extends('layouts.admin')

@section('title')
    {{ $venue->name }}
@endsection

@section('breadcrumbs')
    <li><a href="{{ route('admin::venues.index') }}">Venues</a></li>
    <i class="fa fa-circle"></i>
    <li><span>{{ $venue->name }}</span></li>
@endsection

@section('js')

    <script src="/js/admin/carousel.js"></script>

@endsection

@section('content')
    <div class="portlet light portlet-fit portlet-datatable ">
        <div class="portlet-title">
            <div class="caption">
                Show Venue
            </div>
            <div class="actions">
                <a href="{{ route('admin::venues.index') }}" class="btn btn-primary btn-sm">All Venues</a>
                <a href="{{ route('admin::venues.edit', [$venue->id]) }}" class="btn btn-warning btn-sm">Edit</a>
            </div>
        </div>
        <div class="portlet-body">
            <div class="table-container">
                <dl class="dl-horizontal">
                    <dt>Name</dt>
                    <dd>{{ $venue->name }}</dd>
                    <dt>Address Line 1</dt>
                    <dd>{{ $venue->address_line_1 }}</dd>
                    <dt>Address Line 2</dt>
                    <dd>{{ $venue->address_line_2 }}</dd>
                    <dt>Country</dt>
                    <dd>{{ $venue->country }}</dd>
                    <dt>City</dt>
                    <dd>{{ $venue->city }}</dd>
                    <dt>Post Code</dt>
                    <dd>{{ $venue->post_code }}</dd>
                    <dt>Status</dt>
                    <dd>
                        @include('partials.active-labels', ['model' => $venue])
                    </dd>
                    <dt>Information</dt>
                    <dd>{!! $venue->description !!}</dd>
                </dl>


                @include('partials.photos.show', [ 'model' => $venue, 'routeName' => 'admin::venue.photos' ])
            </div>
        </div>
    </div>
@endsection
