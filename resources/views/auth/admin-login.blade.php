<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Login Form For Admins & Staff | Activ</title>

    <!-- Fonts -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet'
          type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700" rel='stylesheet' type='text/css'>

    <!-- Styles -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    {{-- <link href="{{ elixir('css/app.css') }}" rel="stylesheet"> --}}
    <link rel="stylesheet" href="/css/admin/login.css">

    <style>
        body {
            font-family: 'Lato';
        }

        .fa-btn {
            margin-right: 6px;
        }
    </style>
</head>
<body>

<div class="container">
    <div class="row">
        <div class="container" id="formContainer">
            <form class="form-signin" id="login" role="form" method="POST"
                  action="{{ route('auth.login') }}">

                @include('partials.flash')

                <h3 class="form-signin-heading">Admin Login</h3>
                <a href="#" id="flipToRecover" class="flipLink">
                    <div id="triangle-topright"></div>
                </a>

                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                {{ csrf_field() }}

                <input type="email" class="form-control" name="email" id="loginEmail" placeholder="Email address"
                       required autofocus>
                <input type="password" class="form-control" name="password" id="loginPass" placeholder="Password"
                       required>

                <input type="checkbox" name="remember"> Remember Me

                <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
            </form>

            <form class="form-signin" id="recover" role="form">
                <h3 class="form-signin-heading">Enter email address</h3>
                <a href="#" id="flipToLogin" class="flipLink">
                    <div id="triangle-topleft"></div>
                </a>
                <input type="email" class="form-control" name="loginEmail" id="loginEmail" placeholder="Email address"
                       required autofocus>
                <button class="btn btn-lg btn-primary btn-block" type="submit">Recover password</button>
            </form>

        </div>
    </div>
</div>

<!-- JavaScripts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
{{-- <script src="{{ elixir('js/app.js') }}"></script> --}}
<script src="/js/admin/login.js"></script>

</body>
</html>
