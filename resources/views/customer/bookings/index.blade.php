@extends('layouts.app')

@section('js')
    <script src="/js/customer/bookings.js"></script>
    <script src="/js/modal-window.js"></script>
@endsection

@section('page-header')
    Your Bookings
@endsection

@section('content')
    <div class="buttons pull-right">

        <a class="btn btn-success button" href="{{ route('customer::bookings.create') }}">Add New Booking</a>
    </div>
    
    @include('partials.customer.bookings.index', compact('bookings'))
@endsection
