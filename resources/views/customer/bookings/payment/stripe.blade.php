@extends('layouts.app')

@section('page-header')
  Booking №{{ $bookings->id }} {{ $bookings->created_at->format('d-m-Y') }}
@endsection

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-md-6 col-lg-offset-3">
        <div class="panel panel-default">
          <div class="panel-heading">
            <div class="panel-title">
              <h3>Payment {{ $bookings->getPrice() }}</h3>
            </div>
          </div>
          <div class="panel-body">
            <form role="form" id="payment-form" method="post" action="" autocomplete="false">
              {!! csrf_field() !!}
              <div class="row">
                <div class="col-xs-12">
                  <div class="payment-errors"></div>
                </div>
              </div>
              <div class="row">
                <div class="col-xs-12">
                  <div class="form-group">
                    <label for="cardNumber">CARD NUMBER</label>
                    <div class="input-group">
                      <input type="text" class="form-control" name="cardNumber" placeholder="Valid Card Number" required autofocus data-stripe="number" />
                      <span class="input-group-addon"><i class="fa fa-credit-card" aria-hidden="true"></i></span>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-xs-7 col-md-7">
                  <div class="form-group">
                    <label for="expMonth">EXPIRATION DATE</label>
                    <div class="row">
                      <div class="col-xs-6 col-lg-6 pl-ziro">
                        <input type="text" class="form-control" name="expMonth" placeholder="MM" required data-stripe="exp_month" />
                      </div>
                      <div class="col-xs-6 col-lg-6 pl-ziro">
                        <input type="text" class="form-control" name="expYear" placeholder="YY" required data-stripe="exp_year" />
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-xs-5 col-md-5 pull-right">
                  <div class="form-group">
                    <label for="cvCode">CV CODE</label>
                    <input type="password" class="form-control" name="cvCode" placeholder="CV" required data-stripe="cvc" />
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-xs-6">
                  <a href="{{ route('customer::bookings.index') }}" class="btn btn-danger btn-lg btn-block">Cancel</a>
                </div>
                <div class="col-xs-6">
                  <input class="btn btn-success btn-lg btn-block" type="submit" value="Pay">
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('js')
  <script type="text/javascript" src="http://cdn.jsdelivr.net/jquery.validation/1.13.1/jquery.validate.min.js"></script>
  <script src="https://js.stripe.com/v2/"></script>
  <script src="/js/billing.js"></script>
@endsection
