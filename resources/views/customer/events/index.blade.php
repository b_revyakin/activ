@extends('layouts.app')

@section('page-header')
    Camps
@endsection

@section('content')
    @include('partials.events', ['events' => $events])
@endsection
