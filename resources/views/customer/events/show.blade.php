@extends('layouts.app')

@section('page-header')
    {{ $event->name }}
    <h3 class="header-time">{{ $event->getPeriod() }} | {{ $event->getTimes() }}</h3>
@endsection

@section('js')

    <script src="/js/customer/carousel.js"></script>
@endsection

@section('content')

    <div class="column-left">

        @if($event->photos->first())
        <div class="fotogallery">
            <img class="event-mail-photo"
                 src="{{ route('customer::events.photos.show', [$event->id, $event->photos->first()->id]) }}"
                 alt="mountains">
        </div>

        <div class="photos">
            @foreach($event->photos as $photo)
                <img class="event-other-photo"
                     src="{{ route('customer::events.photos.show', [$event->id, $photo->id]) }}" alt="mountains">
            @endforeach
        </div>
        @endif
    
        <div class="when-and-where">
            <h3>When and Where</h3>
            <img class="hide" src="/image/map.png" alt="map">
            <div>
                <h4>Date &amp; Time</h4>

                <div>
                    {{ $event->getPeriod() }}
                </div>
                <div>
                    {{ $event->getTimes() }}
                </div>
            </div>

            <div>
                <h4>Address</h4>

                <div>
                    {{ $event->venue->name }}
                </div>
                <div>
                    {{ $event->venue->address_line_1 }},
                </div>
                <div>
                    {{ $event->venue->city }}
                </div>
                <div>
                    {{ $event->venue->post_code }}
                </div>
            </div>
        </div>
    </div>

    <div class="column-right">
        <div class="subject-info">
            @if($event->cost_per_day || $event->cost_per_week)
                <h3>Prices <span class="{{ !$event->cost_per_day ? 'hide' : '' }}">from {{ $event->getPricePerDay() }} per day</span> <span class="{{ !($event->cost_per_day && $event->cost_per_week) ? 'hide' : '' }}">and</span> <span class="{{ (!$event->cost_per_week) ? 'hide' : '' }}">from {{ $event->getPricePerWeek() }} per week</span></h3>
            @endif

            <div>{!! $event->description !!}</div>
        </div>
        <div class="margin-top-15">
            <a class="button" href="{{ route('customer::bookings.create') }}">Book Now...</a>
        </div>
    </div>
@endsection
