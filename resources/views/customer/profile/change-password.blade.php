@extends('layouts.app')

@section('page-header')
    Change Password
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-body">
                        @include('partials.validation-errors')
                        <form action="{{ route('customer::profile.change-password') }}" method="POST">
                            {!! csrf_field() !!}
                            <input type="hidden" name="_method" value="PUT">

                            <div class="form-group">
                                <label for="old_password">Old Password*</label>
                                <input type="password" class="form-control" id="old_password"
                                       placeholder="Old Password" name="old_password"
                                       value="{{ old('old_password') }}">
                            </div>
                            <div class="form-group">
                                <label for="password">New Password*</label>
                                <input type="password" class="form-control" id="password"
                                       placeholder="New Password" name="password">
                            </div>
                            <div class="form-group">
                                <label for="password_confirmation">Confirm New Password*</label>
                                <input type="password" class="form-control" id="password_confirmation"
                                       placeholder="Confirm New Password" name="password_confirmation">
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-user"></i>Change Password
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection