@extends('layouts.app')

{{--@section('title')--}}
{{--{{ $parent->name }}--}}
{{--@endsection--}}

@section('page-header')
    Your Profile
@endsection

@section('content')
    @include('partials.customer.show', ['model' => $customer])
@endsection