@extends('layouts.app')

@section('page-header')
    {{ $season->name }}
@endsection

@section('content')
    @include('partials.events', ['events' => $events])
@endsection
