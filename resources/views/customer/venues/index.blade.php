@extends('layouts.app')

@section('page-header')
    Venues
@endsection

@section('content')
    @foreach($venues->chunk(3) as $venues)
        <div class="row subject-one">
            @foreach($venues as $venue)
                <div class="col-md-4 section">
                    @if($venue->photo())
                        <img class="subject-image"
                             src="{{ route('customer::venues.photos.show', [$venue->id, $venue->photo()->id]) }}"
                             alt="mountains">
                    @else
                        <img class="subject-image" src="/image/mountains.jpg" alt="mountains">
                    @endif
                    <h3 class="subject-name">
                        {{ $venue->name }}
                    </h3>
                    <div class="subject-description">
                        {!! $venue->description !!}
                    </div>

                    <a class="btn btn-success button" href="{{ route('customer::venues.show', [$venue->id]) }}">View
                        Camps</a>
                </div>
            @endforeach
        </div>
    @endforeach
@endsection
