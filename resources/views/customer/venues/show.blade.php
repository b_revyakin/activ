@extends('layouts.app')

@section('page-header')
    {{ $venue->name }}
@endsection

@section('content')
    @include('partials.events', ['events' => $events])
@endsection
