<style>

    table.email-table-padding td {
        padding: 10px;
    }

</style>

<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet"
      integrity="sha256-7s5uDGW3AHqw6xtJmNNtr+OBRJUlgkNJEo78P4b0yRw= sha512-nNo+yCHEyn0smMxSswnf/OnX6/KwJuZTlNZBjauKhTK0c+zT+q5JOCx0UFhXQ6rJR9jg6Es8gPuD2uZcYDLqSw=="
      crossorigin="anonymous">
<div class="container col-md-8 col-md-offset-2">
    <img src="{{ asset('/image/email-logo.png') }}" alt="Activ Camps - The best kids camps in London.">

    <p>
        Dear {{ $customer->first_name }},
    </p>

    <p>
        Many thanks for your recent booking with Activ Camps. Your payment has been received and we look forward to
        seeing you on camp on the following dates:
    </p>

    <p>
        @include('partials.emails.booking.content', compact('booking'))
    </p>

    <h4>IMPORTANT INFORMATION</h4>
    <p>
        Please click the following link to be taken to the <a href="www.activcamps.com/faqs">FAQ section</a> on our
        website.
        This should provide you with all the information you will need for your time on camp, including timings and what
        to bring. If you have any questions that remain unanswered please do not hesitate to <a
                href="www.activcamps.com/contact-us">contact us</a>.
    </p>

    <h4>MEDICATION</h4>
    <p>
        All essential medication, including all prescribed adrenaline auto-injectors (i.e. Emerade, EpiPen, Jext), must
        be
        handed in to the Activ Camps Lead First Aider each morning for safe keeping and the appropriate form must be
        filled in.
    </p>

    <ul>
        <li>
            A child will not be allowed to stay on camp if the medication/adrenaline auto-injector is out of date, or
            not appropriately labelled with the child's name
        </li>
        <li>
            In accordance with government healthcare recommendations, your child should have access to more than one
            adrenaline auto-injector device on camp.
        </li>
    </ul>

    <h4>CANCELLATIONS & CHANGING BOOKING DATES</h4>
    <p>
        If you need to change or cancel your booking with us then please let us know by email as soon as possible. As
        changes and cancellations require considerable administration time and have a major impact on our staffing
        levels, the following charges will apply:
    </p>

    <table class="email-table-padding" cellspacing="10">
        <thead>
        <tr>
            <th></th>
            <th>Changes</th>
            <th>Cancellations</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <th>4 Weeks</th>
            <td>
                Free
            </td>
            <td>
                Free
            </td>
        </tr>
        <tr>
            <th>3 Weeks</th>
            <td>
                £10 Per Change
            </td>
            <td>
                £10 per day irrelevant of the number of children
            </td>
        </tr>
        <tr>
            <th>
                2 Weeks
            </th>
            <td>
                £10 Per Change
            </td>
            <td>
                £15 per day for 2 + children £10 per day for 1 child
            </td>
        </tr>
        <tr>
            <th>
                1 Week
            </th>
            <td>
                Sadly, no changes can be made with such short notice and will be seen as a cancellation instead.
            </td>
            <td>
                No refund
            </td>
        </tr>
        </tbody>
    </table>

    <p>
        For full booking terms and conditions please <a href="www.activcamps.com/terms-of-business/">click here</a>.
    </p>

    <p>
        If you require any further details please do not hesitate to <a href="www.activcamps.com/contact-us">get in touch</a>.
    </p>

    <p>
        We look forward to seeing you soon and hope you have a FUNtastic time with us at Activ Camps.
    </p>

    <p>
        Best wishes,
    </p>

    <p>
        Lily, Will, P, Max and Chloe,
    </p>
    <p>
        Activ Camps.
    </p>

</div>