<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet"
      integrity="sha256-7s5uDGW3AHqw6xtJmNNtr+OBRJUlgkNJEo78P4b0yRw= sha512-nNo+yCHEyn0smMxSswnf/OnX6/KwJuZTlNZBjauKhTK0c+zT+q5JOCx0UFhXQ6rJR9jg6Es8gPuD2uZcYDLqSw=="
      crossorigin="anonymous">
<div class="container col-md-8 col-md-offset-2">
    <img src="{{ asset('/image/email-logo.png') }}" alt="Activ Camps - The best kids camps in London.">

    <p>
        Dear {{ $customer->first_name }},
    </p>

    <p>
        Many thanks for your recent booking with Activ Camps. We cannot confirm your booking until payment has been
        received. If you have trouble completing your payment through Stripe or have not received a booking
        confirmation despite completing payment please do <a href="www.activcamps.com/contact-us">get in touch</a>.
    </p>

    <p>
        @include('partials.emails.booking.content', compact('booking'))
    </p>

    <p>
        You will receive a booking confirmation email once full payment has been received.
    </p>

    <p>
        For full booking terms and conditions please <a href="www.activcamps.com/terms-of-business/">click here</a>.
    </p>

    <p>
        Many thanks,
    </p>

    <p>
        Lily, Will, P, Max and Chloe,
    </p>
    <p>
        Activ Camps.
    </p>
</div>