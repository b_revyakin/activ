<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet"
      integrity="sha256-7s5uDGW3AHqw6xtJmNNtr+OBRJUlgkNJEo78P4b0yRw= sha512-nNo+yCHEyn0smMxSswnf/OnX6/KwJuZTlNZBjauKhTK0c+zT+q5JOCx0UFhXQ6rJR9jg6Es8gPuD2uZcYDLqSw=="
      crossorigin="anonymous">
<div class="container col-md-8 col-md-offset-2">
    <img src="{{ asset('/image/email-logo.png') }}" alt="Activ Camps - The best kids camps in London.">

    <p>
        Dear {{ $customer->first_name }},
    </p>

    <p>
        Many thanks for your recent booking with Activ Camps. You have selected to pay using childcare vouchers. Please
        be aware that we are unable to confirm this booking until full payment has been received.
    </p>
    <ul>
        <li>
            Childcare voucher bookings will be held on our system for one week to allow for payment to reach us. Please
            complete payment as soon as possible and email us with confirmation of your payment once it has been
            completed.
        </li>
        <li>
            You will then receive a booking confirmation once full payment has been received.
            Please allow three days for payment to reach us.
        </li>
    </ul>
    <p>
        For details of our childcare voucher providers <a href="www.activcamps.com/parent-guide/childcare-vouchers/">click
            here</a>.
    </p>

    <p>
        @include('partials.emails.booking.content', compact('booking'))
    </p>

    <p>
        We look forward to confirming your booking once payment has been received. <b>If you do not receive a booking
        confirmation email from us and have sent the payment please contact us to ensure your booking has not been
        cancelled.</b>
    </p>

    <p>
        In the meantime, if you require any further details please do not hesitate to <a
                href="www.activcamps.com/contact-us">get in touch</a>.
    </p>

    <p>
        For full booking terms and conditions please <a href=" www.activcamps.com/terms-of-business/">click here</a>.
    </p>

    <p>
        Thank you,
    </p>

    <p>
        Lily, Will, P, Max and Chloe,
    </p>
    <p>
        Activ Camps.
    </p>
</div>