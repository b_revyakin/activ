<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet"
      integrity="sha256-7s5uDGW3AHqw6xtJmNNtr+OBRJUlgkNJEo78P4b0yRw= sha512-nNo+yCHEyn0smMxSswnf/OnX6/KwJuZTlNZBjauKhTK0c+zT+q5JOCx0UFhXQ6rJR9jg6Es8gPuD2uZcYDLqSw=="
      crossorigin="anonymous">
<div class="container col-md-8 col-md-offset-2">
    <img src="{{ asset('/image/email-logo.png') }}" alt="Activ Camps - The best kids camps in London.">

    <p>
        Dear {{ $customer->first_name }},
    </p>

    <p>
        Welcome to Activ Camps. Your account has been successfully set up. You can now book your non-stop fun holidays
        by visiting <a href="http://booking.activcamps.com">https://booking.activcamps.com</a>, using the following
        email address to log in:
    </p>

    <p>
        {{ $customer->user->email }}
    </p>

    <p>
        If you require any assistance with your booking please do not hesitate to <a
                href="www.activcamps.com/contact-us">get in touch</a>.
    </p>


    <p>
        Thank you,
    </p>

    <p>
        Lily, Will, P, Max and Chloe,
    </p>
    <p>
        Activ Camps.
    </p>
</div>