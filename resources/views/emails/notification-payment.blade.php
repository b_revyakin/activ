<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Activ</title>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha256-7s5uDGW3AHqw6xtJmNNtr+OBRJUlgkNJEo78P4b0yRw= sha512-nNo+yCHEyn0smMxSswnf/OnX6/KwJuZTlNZBjauKhTK0c+zT+q5JOCx0UFhXQ6rJR9jg6Es8gPuD2uZcYDLqSw=="
          crossorigin="anonymous">
</head>
<body>
<div class="container col-md-offset-3">
    <div class="row">
        <img src="{{ asset('/image/email-logo.png') }}"/>

        <div><strong>Thank you for booking on to one of our Activ Camps! Your payment has been received and your booking
                is
                now confirmed.</strong></div>
        <div class="list-group">
            <dt>Children:</dt>
            <dd>
                @foreach($booking->orders as $order)
                    {{ $order->child->getNameAttribute() }} / Age {{ $order->child->getAgeAttribute() }} years <br>
                @endforeach
            </dd>
        </div>
        <div class="col-md-4 column-left">
            <dd>Venue/Camps</dd>
            <dd>Dates booked</dd>
            <br>
            <dd>Cost</dd>
            <dd>Camps cost:</dd>
            <dd>Promotional discount</dd>
            <dd>Extras</dd>
            <dd>Total to pay</dd>
            <br>
        </div>
        <div class="col-md-4 column-right">
            <dd></dd>
            <dd>{{ $booking->payment()->amount }}</dd>
            <dd>Dates booked</dd>
            <br>
            <dd></dd>
            <dd>{{ $booking->getPrice() }}</dd>
            <dd>£0.00</dd>
            <dd>£0.00</dd>
            <dd>{{ $booking->getPrice() }}</dd>
            <br>
        </div>
    </div>
    <div class="list-group col-md-8">
        <p>Please click the link below for your site specific parents guide <a
                    href="{{ URL::to('') }}">{{ URL::to('') }}</a>.
            This should provide you with all the information you will need for your time on camp. We look forward to
            seeing you on camp!
        </p>
    </div>
</div>
</body>
</html>