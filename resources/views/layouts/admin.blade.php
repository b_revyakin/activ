<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="csrf-token" content="{{ csrf_token() }}"/>

  <title>Activ</title>

  <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet'
        type='text/css'>

  <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet"
        type="text/css"/>


  <link href="/css/admin/libs/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
  <link href="/css/admin/libs/bootstrap-switch.min.css" rel="stylesheet" type="text/css"/>
  <link href="/css/admin/libs/datatables.min.css" rel="stylesheet" type="text/css"/>

  <link href="/css/admin/libs/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css"/>

  <link href="/css/admin/components.min.css" rel="stylesheet" id="style_components" type="text/css"/>
  <link href="/css/admin/plugins.min.css" rel="stylesheet" type="text/css"/>
  <link href="/css/admin/layout.min.css" rel="stylesheet" type="text/css"/>
  <link href="/css/admin/default.min.css" rel="stylesheet" type="text/css" id="style_color"/>
  <link href="/css/admin/profile.min.css" rel="stylesheet" type="text/css" id="style_color"/>

  <link rel="stylesheet" href="/components/bootstrap3-wysihtml5-bower/dist/bootstrap3-wysihtml5.min.css">
  <link rel="stylesheet" href="/components/redactor/redactor/redactor.css"/>
  <link href="{{ asset('js/admin/fileinput/css/fileinput.css') }}" rel="stylesheet">

  <link href="/components/magnific-popup/dist/magnific-popup.css" rel="stylesheet">
  <link rel="stylesheet" href="{{url('/css/admin/custom.css')}}">


  <link href="/css/plugins/select2/select2.min.css" rel="stylesheet" type="text/css"/>
  <link href="/css/plugins/select2/select2-bootstrap.min.css" rel="stylesheet" type="text/css"/>


  @yield('css')
</head>
<body class="page-container-bg-solid">
<div class="page-wrapper">
  <div class="page-wrapper-row">
    <div class="page-wrapper-top">
      <div class="page-header">
        <div class="page-header-top">
          <div class="full-width">
            <div class="page-logo">
              <a href="{{url('/')}}">
                <img src="{{url('/img/logo.png')}}" alt="logo" class="logo-default">
              </a>
            </div>
            <a href="javascript:;" class="menu-toggler"></a>
            @if (! Auth::guest())
              <div class="top-menu">
                <ul class="nav navbar-nav pull-right">
                  <li class="dropdown dropdown-user dropdown-dark">
                    <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown"
                       data-hover="dropdown" data-close-others="true">
                      <img alt="" class="img-circle" src="{{ Auth::user()->avatar() }}">
                      <span class="username username-hide-mobile">{{ Auth::user()->staff->name() }}</span>
                    </a>
                    <ul class="dropdown-menu dropdown-menu-default">
                      <li>
                        <a href="{{ route('admin::profile.info') }}">
                          <i class="icon-user"></i> Profile </a>
                      </li>
                      <li>
                        <a href="{{ route('auth.logout') }}">
                          <i class="icon-key"></i> Logout </a>
                      </li>
                    </ul>
                  </li>
                </ul>
              </div>
            @endif
          </div>
        </div>
        <div class="page-header-menu">
          <div class="full-width">
            <div class="hor-menu  ">
              <ul class="nav navbar-nav">
                <li>
                  <a href="{{ url('/') }}"> Dashboard</a>
                </li>

                <li class="menu-dropdown classic-menu-dropdown">
                  <a href="javascript:;"> Events
                    <span class="arrow"></span>
                  </a>
                  <ul class="dropdown-menu pull-left">
                    @role('admin|staff')
                    <li>
                      <a href="{{ route('admin::events.index', ['only' => 'person']) }}"
                         class="nav-link">
                        <i class="fa fa-btn fa-users"></i> My Events </a>
                    </li>
                    @endrole
                    <li>
                      <a href="{{ route('admin::events.index') }}" class="nav-link">
                        <i class="fa fa-btn fa-users"></i>
                        All Events
                      </a>
                    </li>
                    <li>
                      <a href="{{ route('admin::events.archive') }}" class="nav-link">
                        <i class="fa fa-btn fa-users"></i>
                        Event Archive
                      </a>
                    </li>
                    <li>
                      {{--href="{{ route('admin::events.report') }}"--}}
                      <a data-toggle="modal" data-target="#modal-select-season" href="#"
                         class="nav-link">
                        <i class="fa fa-btn fa-users"></i>
                        Event Reports
                      </a>
                    </li>
                    <li>
                      <a href="{{ route('admin::venues.index') }}" class="nav-link">
                        <i class="fa fa-btn fa-globe"></i>
                        Venues
                      </a>
                    </li>
                    <li>
                      <a href="{{ route('admin::seasons.index') }}" class="nav-link">
                        <i class="fa fa-btn fa-calendar"></i>
                        Seasons
                      </a>
                    </li>
                  </ul>
                </li>
                <li class="menu-dropdown classic-menu-dropdown">
                  <a href="javascript:;"> Booking
                    <span class="arrow"></span>
                  </a>
                  <ul class="dropdown-menu pull-left">
                    <li>
                      <a href="{{ route('admin::bookings.index') }}" class="nav-link">
                        <i class="fa fa-btn fa-ticket"></i>
                        Booking
                      </a>
                    </li>
                    <li>
                      <a href="{{ route('admin::orders.index') }}" class="nav-link">
                        <i class="fa fa-btn fa-ticket"></i>
                        Event Bookings
                      </a>
                    </li>
                    @role('admin')
                    <li>
                      <a href="{{ route('admin::products.index') }}" class="nav-link">
                        <i class="fa fa-btn fa-cart-plus"></i>
                        Products
                      </a>
                    </li>
                    <li>
                      <a href="{{ route('admin::coupons.index') }}" class="nav-link">
                        <i class="fa fa-btn fa-fire"></i>
                        Coupons
                      </a>
                    </li>
                    @endrole


                  </ul>
                </li>
                <li class="menu-dropdown classic-menu-dropdown">
                  <a href="javascript:;"> Users
                    <span class="arrow"></span>
                  </a>
                  <ul class="dropdown-menu pull-left">
                    @role('admin')
                    <li>
                      <a href="{{ route('admin::admins.index') }}" class="nav-link">
                        <i class="fa fa-btn fa-eye"></i>
                        Admins
                      </a>
                    </li>
                    <li>
                      <a href="{{ route('admin::staffs.index') }}" class="nav-link">
                        <i class="fa fa-btn fa-user-secret"></i>
                        Staff
                      </a>
                    </li>
                    <li>
                      <a href="{{ route('admin::site-managers.index') }}" class="nav-link">
                        <i class="fa fa-btn fa-user-secret"></i>
                        Site Managers
                      </a>
                    </li>
                    @endrole
                    <li>
                      <a href="{{ route('admin::parents.index') }}" class="nav-link">
                        <i class="fa fa-btn fa-users"></i>
                        Customers
                      </a>
                    </li>

                  </ul>
                </li>
                @role('admin')
                <li>
                  <a href="{{ route('admin::payment-systems.index') }}">Payment Systems</a>
                </li>
                @endrole
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="page-wrapper-row full-height">
    <div class="page-wrapper-middle">
      <div class="page-container">
        <div class="page-content-wrapper">
          <div class="page-head">
            <div class="container full-width">
              <div class="page-title">
                <h1>
                  @yield('title')
                </h1>
              </div>
            </div>
          </div>
          <div class="page-content">
            <div class="full-width">
              <ul class="page-breadcrumb breadcrumb">
                @yield('breadcrumbs')
              </ul>

              <div class="page-content-inner">
                @include('partials.flash')

                @yield('content')
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- JavaScripts -->
<script src="/components/jquery/dist/jquery.min.js"></script>
<script src="/components/bootstrap/dist/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/components/moment/min/moment.min.js"></script>
<script type="text/javascript" src="/components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>

<script src="/components/bootstrap3-wysihtml5-bower/dist/bootstrap3-wysihtml5.all.min.js"></script>
<script src="/components/bootstrap3-wysihtml5-bower/dist/commands.js"></script>
<script src="/components/bootstrap3-wysihtml5-bower/dist/templates.js"></script>

<script src="/components/select2/dist/js/select2.js"></script>
<script src="/js/admin/datepicker.js"></script>
<script src="/components/redactor/redactor/redactor.js"></script>

<!-- Datatables and Plugins -->
@if(App::environment('production'))
  <script src="/js/vendor/datatables.all.min.js"></script>
  <script src="/components/datatables/media/js/dataTables.bootstrap.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.1.1/js/dataTables.buttons.min.js"></script>
  <script src="//cdn.datatables.net/buttons/1.1.1/js/buttons.flash.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
  <script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
  <script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
  <script src="//cdn.datatables.net/buttons/1.1.1/js/buttons.html5.min.js"></script>
  <script src="//cdn.datatables.net/buttons/1.1.1/js/buttons.print.min.js"></script>

  <script src="/components/datatables-buttons/js/buttons.colVis.js"></script>
  <script src="/components/datatables-buttons/js/buttons.bootstrap.js"></script>

  <script src="/components/magnific-popup/dist/jquery.magnific-popup.min.js"></script>

  {{--<script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script>--}}
  <script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
  <script type="text/javascript" src="/components/slick-carousel/slick/slick.min.js"></script>
@else
  <script src="/js/vendor/datatables.all.min.js"></script>
  <script src="/components/datatables/media/js/dataTables.bootstrap.min.js"></script>
  <script src="/js/vendor/dataTables.buttons.min.js"></script>
  <script src="/js/vendor/buttons.flash.min.js"></script>
  <script src="/js/vendor/jszip.min.js"></script>
  <script src="/js/vendor/pdfmake.min.js"></script>
  <script src="/js/vendor/vfs_fonts.js"></script>
  <script src="/js/vendor/buttons.html5.min.js"></script>
  <script src="/js/vendor/buttons.print.min.js"></script>
  <script src="/components/datatables-buttons/js/buttons.colVis.js"></script>
  <script src="/components/datatables-buttons/js/buttons.bootstrap.js"></script>

  <script src="/components/magnific-popup/dist/jquery.magnific-popup.min.js"></script>

  <script src="/js/vendor/jquery-migrate-1.2.1.min.js"></script>
  <script type="text/javascript" src="/components/slick-carousel/slick/slick.min.js"></script>
@endif


<script src="/js/admin/app.min.js" type="text/javascript"></script>
<script src="/js/admin/layout.min.js"></script>
<script src="/js/extensions.js"></script>

{{-- <script src="{{ elixir('js/app.js') }}"></script> --}}
@yield('js')

@include('partials.modal-windows.events.select-season')

</body>
</html>
