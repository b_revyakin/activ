@include('partials.validation-errors')

<form action="{{ route($route) }}" method="POST">
    {{ csrf_field() }}

    <div class="form-group">
        <label for="title">Title</label>
        <select class="form-control" name="title" id="title">
            <option {{ old('title') === 'Mr' ? 'selected' : '' }} value="">-select-</option>
            <option {{ old('title') === 'Mr' ? 'selected' : '' }} value="Mr">Mr</option>
            <option {{ old('title') === 'Miss' ? 'selected' : '' }} value="Miss">Miss</option>
            <option {{ old('title') === 'Mrs' ? 'selected' : '' }} value="Mrs">Mrs</option>
            <option {{ old('title') === 'Ms' ? 'selected' : '' }} value="Ms">Ms</option>
            <option {{ old('title') === 'Dr' ? 'selected' : '' }} value="Dr">Dr</option>
        </select>
    </div>
    <div class="form-group">
        <label for="first_name">First Name*</label>
        <input type="text" class="form-control" id="first_name"
               placeholder="First Name" name="first_name"
               value="{{ old('first_name') }}">
    </div>
    <div class="form-group">
        <label for="last_name">Last Name*</label>
        <input type="text" class="form-control" id="last_name"
               placeholder="Last Name" name="last_name"
               value="{{ old('last_name') }}">
    </div>
    <div class="form-group">
        <label for="email">Email*</label>
        <input type="email" class="form-control" id="email"
               placeholder="Email" name="email"
               value="{{ old('email') }}">
    </div>
    <div class="form-group">
        <label for="confirm-email">Confirm Email*</label>
        <input type="email" class="form-control" id="confirm-email"
               placeholder="Email" name="email_confirmation"
               value="{{ old('email_confirmation') }}">
    </div>
    <div class="form-group">
        <label for="subscribe_to_newsletter">Subscribe to Newsletter*</label>
        <select class="form-control" name="subscribe_to_newsletter"
                id="subscribe_to_newsletter">
            <option value="1">Yes</option>
            <option value="0">No</option>
        </select>
    </div>
    <div class="form-group">
        <label for="password">Password*</label>
        <input type="password" class="form-control" id="password"
               placeholder="Password" name="password">
    </div>
    <div class="form-group">
        <label for="password_confirmation">Confirm Password*</label>
        <input type="password" class="form-control" id="password_confirmation"
               placeholder="Confirm Password" name="password_confirmation">
    </div>
    <div class="form-group">
        <label for="mobile">Mobile Number*</label>
        <input type="text" class="form-control" id="mobile"
               placeholder="Mobile Number" name="mobile"
               value="{{ old('mobile') }}">
    </div>
    <div class="form-group">
        <label for="home_tel">Home Telephone*</label>
        <input type="text" class="form-control" id="home_tel"
               placeholder="Home Telephone" name="home_tel"
               value="{{ old('home_tel') }}">
    </div>
    <div class="form-group">
        <label for="work_tel">Work Telephone*</label>
        <input type="text" class="form-control" id="work_tel"
               placeholder="Work Telephone" name="work_tel"
               value="{{ old('work_tel') }}">
    </div>
    <div class="form-group">
        <label for="address_line_1">Address Line 1*</label>
        <input type="text" class="form-control" id="address_line_1"
               placeholder="Address Line 1" name="address_line_1"
               value="{{ old('address_line_1') }}">
    </div>
    <div class="form-group">
        <label for="address_line_2">Address Line 2</label>
        <input type="text" class="form-control" id="address_line_2"
               placeholder="Address Line 2" name="address_line_2"
               value="{{ old('address_line_2') }}">
    </div>
    <div class="form-group">
        <label for="city">Town / City*</label>
        <input type="text" class="form-control" id="city"
               placeholder="Town / City" name="city"
               value="{{ old('city') }}">
    </div>
    <div class="form-group">
        <label for="country">Country*</label>
        <select name="country_id" id="country" class="countries form-control">
            @foreach($countries as $country)
                <option value="{{ $country->id }}">{{ $country->title }}</option>
            @endforeach
        </select>
    </div>
    <div class="form-group">
        <label for="post_code">Post Code*</label>
        <input type="text" class="form-control" id="post_code"
               placeholder="Post Code" name="post_code"
               value="{{ old('post_code') }}">
    </div>
    <div class="form-group">
        <label for="hear_from">How did you hear about us?</label>
        <input type="text" class="form-control" id="hear_from"
               placeholder="" name="hear_from"
               value="{{ old('hear_from') }}">
    </div>

    <div class="form-group">
        <button type="submit" class="btn btn-primary">
            <i class="fa fa-btn fa-user"></i>{{ $buttonTitle }}
        </button>
    </div>
</form>
