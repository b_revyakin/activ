<table class="table table-bordered table-responsive table-hover" id="customer-bookings">
  <thead>
  <tr>
    <th>Booking Id</th>
    <th>Booking Date</th>
    <th>Price</th>
    <th>Payment Type</th>
    <th>Payment Status</th>
    <th>Actions</th>
  </tr>
  </thead>
  <tbody>
  @foreach($bookings as $booking)
    <tr data-id="{{ $booking->id }}">
      <td>{{ $booking->id }}</td>
      <td>{{ $booking->created_at->format('d-m-Y') }}</td>
      <td>{{ $booking->getPrice() }}</td>
      <td>{{ $booking->payment()->getType() }}</td>
      <td>
                    <span class="label label-status
                        {{ $booking->isProcessing() ? 'label-warning' :'' }}
                    {{ $booking->isPaid() ? 'label-success' :'' }}
                    {{ $booking->isCancelled() ? 'label-danger' :'' }}">
                        {{ $booking->showStatus() }}</span>
      </td>
      <td>
        <button data-booking-id="{{ $booking->id }}" class="btn btn-primary btn-xs btn-view">View</button>
        <button data-booking-id="{{ $booking->id }}" class="btn btn-primary btn-xs btn-hide"
                style="display: none">Hide
        </button>
        @if(Auth::user()->is('customer'))
          @if($booking->isProcessing())
            <button type="button" class="btn btn-xs btn-danger" data-toggle="modal"
                    data-target=".bs-modal-sm"
                    data-title="{{ trans('messages.customer.booking.cancel.title') }}"
                    data-content="{{ trans('messages.customer.booking.cancel.content') }}"
                    data-url="{{ route('customer::bookings.destroy', [$booking->id]) }}"
                    data-method="DELETE">Cancel
            </button>
            @if($booking->payment()->isOnline())
              @if($paymentSystems->count())
                @if($paymentSystems->count() > 1)
                  <button type="button" class="btn btn-xs btn-default" data-toggle="modal"
                          data-target=".payment-systems"
                          data-code="{{ $booking->encryptedCode }}"
                          data-url="{{ route('customer::bookings.payment.stripe', $booking->id) }}">Pay
                  </button>
                @else
                  @if($paymentSystems->first()->system == 'sagepay')
                    <form id="form" method="POST" action="{{ config('sagepay.endPointPayment') }}">
                      <input type="hidden" name="VPSProtocol" value="3.00">
                      <input type="hidden" name="TxType" value="PAYMENT">
                      <input type="hidden" name="Vendor" value="{{ config('sagepay.vendor') }}">
                      <input type="hidden" name="Crypt" value="{{ $booking->encryptedCode }}">

                      <input type="submit" class="btn btn-xs btn-default" value="Pay">
                    </form>
                  @else
                    <a id="stripe-system" href="{{ route('customer::bookings.payment.stripe', $booking->id) }}" class="btn btn-xs btn-default">Pay</a>
                  @endif
                @endif
              @endif
            @endif
          @endif
        @else
          @if($booking->isProcessing())
            <button type="button" class="btn btn-xs btn-danger btn-cancel" data-toggle="modal"
                    data-target=".bs-modal-sm"
                    data-title="{{ trans('messages.admin.booking.cancel.title') }}"
                    data-content="{{ trans('messages.admin.booking.cancel.content') }}"
                    data-url="{{ route('admin::api::bookings.cancel', [$booking->id]) }}"
                    data-ajax="true"
                    data-action="cancel"
                    data-id="{{ $booking->id }}"
                    data-method="POST">Cancel
            </button>

            <button type="button" class="btn btn-xs btn-success btn-confirm" data-toggle="modal"
                    data-target=".bs-modal-sm"
                    data-title="{{ trans('messages.admin.booking.confirm.title') }}"
                    data-content="{{ trans('messages.admin.booking.confirm.content') }}"
                    data-url="{{ route('admin::api::bookings.success', [$booking->id]) }}"
                    data-ajax="true"
                    data-action="confirm"
                    data-id="{{ $booking->id }}"
                    data-method="POST">Confirm
            </button>
          @endif
          <a href="{{ route('admin::bookings.show', $booking->id) }}"
             class="btn btn-default btn-xs">Show</a>
        @endif
      </td>
    </tr>
    <tr style="display: none" data-booking-id="{{ $booking->id }}">
      <td colspan="6">
        <div id="children">
          <div class="order">
            @foreach($booking->orders as $order)
              <div class="child">
                {{ $order->child()->withTrashed()->first()->fullname }} /
                Age {{ $order->child()->withTrashed()->first()->age }} years
              </div>
              <div>
                <div>
                  <span>Venue / Camps</span> <span>{{ $order->event->venue->name }}
                    - {{ $order->event->name }}</span>
                </div>
                <div>
                  <span>Price</span> <span>{{ $order->price }}</span>
                </div>
                <div>
                  <span>Date booked</span>
                  <ul>
                    @foreach($order->dates as $date)
                      <li>{{ $date->getDate() }}</li>
                    @endforeach
                  </ul>
                </div>
              </div>
            @endforeach
            @if($booking->variants->count())
              <div>
                <span>Products</span>
                <ul>
                  @foreach($booking->variants as $variant)
                    <li>{{ $variant->product->name . ' Size ' . $variant->name . ' x' . $variant->pivot->count . ' Price ' . $variant->pivot->price }}</li>
                  @endforeach
                </ul>
              </div>
            @endif
          </div>
        </div>
      </td>
    </tr>
  @endforeach
  </tbody>
</table>

<div class="modal fade payment-systems" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel"
     id="payment-systems">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                  aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="exampleModalLabel">Select Payment System</h4>
      </div>
      <div class="modal-body">
        <div class="text-center">
          @foreach($paymentSystems as $paymentSystem)
            @if($paymentSystem->system == 'sagepay')
              <form style="display: inline" id="form" method="POST" action="{{ config('sagepay.endPointPayment') }}">
                <input type="hidden" name="VPSProtocol" value="3.00">
                <input type="hidden" name="TxType" value="PAYMENT">
                <input type="hidden" name="Vendor" value="{{ config('sagepay.vendor') }}">
                <input type="hidden" name="Crypt" value="{{ $booking->encryptedCode }}">

                <input type="submit" class="btn btn-primary" value="SAGEPAY">
              </form>
            @endif
            @if($paymentSystem->system == 'stripe')
              <a id="stripe-system" href="" class="btn btn-primary">STRIPE</a>
            @endif
          @endforeach
        </div>
      </div>
      <div class="modal-footer">

      </div>
    </div>
  </div>
</div>

@include('partials.modal-windows.booking.action')
