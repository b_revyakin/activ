<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">

                <div class="panel-body">
                    <dl class="dl-horizontal">
                        <dt>Title</dt>
                        <dd>{{ $model->title }}</dd>
                        <dt>First Name</dt>
                        <dd>{{ $model->first_name }}</dd>
                        <dt>Last Name</dt>
                        <dd>{{ $model->last_name }}</dd>
                        <dt>Email</dt>
                        <dd>{{ $model->user->email }}</dd>
                        <dt>Active</dt>
                        <dd>
                            @include('partials.active-labels', ['model' => $model])
                        </dd>
                        <dt>Mobile Number</dt>
                        <dd>{{ $model->mobile }}</dd>
                        <dt>Home Telephone</dt>
                        <dd>{{ $model->home_tel }}</dd>
                        <dt>Work Telephone</dt>
                        <dd>{{ $model->work_tel }}</dd>
                        <dt>Address Line 1</dt>
                        <dd>{{ $model->address_line_1 }}</dd>
                        <dt>Address Line 2</dt>
                        <dd>{{ $model->address_line_2 }}</dd>
                        <dt>Town / City</dt>
                        <dd>{{ $model->city }}</dd>
                        <dt>County</dt>
                        <dd>{{ $model->county }}</dd>
                        <dt>Country</dt>
                        <dd>{{ $model->country->title }}</dd>
                        <dt>Post Code</dt>
                        <dd>{{ $model->post_code }}</dd>
                        <dt>How did you hear about us?</dt>
                        <dd>{{ $model->hear_from }}</dd>
                    </dl>
                </div>
            </div>
        </div>
    </div>
</div>