<style>

    table.email-table-padding td {
        padding: 10px;
    }

</style>

<div class="col-md-12">

    <table class="email-content email-table-padding" cellspacing="10">
        @foreach($booking->orders as $order)
            <tr>
                <td colspan="2">
                    {{ $order->child->getNameAttribute() }} / Age {{ $order->child->getAgeAttribute() }} years <br>
                </td>
            </tr>
            <tr>
                <td width="350px">Venue/Camps</td>
                <td class="right-column">{{ $order->event->venue->name }} - {{ $order->event->name }}</td>
            </tr>

            <tr>
                <td>Dates booked</td>
                <td>
                    @foreach($order->dates as $date)
                        {{ $date->getDate() }}<br>
                    @endforeach
                </td>
            </tr>
        @endforeach
      @if($booking->variants->count())
        <tr>
          <td>Products</td>
          <td>
            @foreach($booking->variants as $variant)
              {{ $variant->product->name . ' Size ' . $variant->name . ' x' . $variant->pivot->count . ' Price ' . $variant->pivot->price }}
              <br>
            @endforeach
          </td>
        </tr>
      @endif

        <tr>
            <td>Cost</td>
            <td>{{ $booking->getPriceWithoutDiscount() }}</td>
        </tr>

        <tr>
            <td>Promotional discount</td>
            <td>{{ $booking->getDiscount() }}</td>
        </tr>

        {{--<tr>--}}
        {{--<td>Extras</td>--}}
        {{--<td>£0.00</td>--}}
        {{--</tr>--}}

        <tr>
            <td>Total to pay</td>
            <td>{{ $booking->getPrice() }}</td>
        </tr>

    </table>

</div>
