<div class="col-md-8 col-md-offset-2">
    <p>Please click the link below for your site specific parents guide <a
                href="{{ URL::to('') }}">{{ URL::to('') }}</a>.
        This should provide you with all the information you will need for your time on camp. We look forward to
        seeing you on camp!
    </p>
</div>