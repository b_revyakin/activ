<table class="table table-bordered" id="events-table">
    <thead>
    <tr>
        <th>Name</th>
        <th>Venue</th>
        <th class="date">Start Date</th>
        <th class="date">End Date</th>
        <th class="count">Total Booking</th>
        <th class="status">Status</th>
        <th class="actions">Actions</th>
    </tr>
    </thead>
    <tbody>
    @foreach($events as $event)
        <tr>
            <td>{{ $event->name }}</td>
            <td>
                <a href="{{ route('admin::venues.show', [$event->venue->id]) }}">
                    {{ $event->venue->name }}
                </a>
            </td>
            <td>{{ $event->start_date->format('d-m-Y') }}</td>
            <td>{{ $event->end_date->format('d-m-Y') }}</td>
            <td>{{ $event->getActiveOrders()->count() }}</td>
            <td>
                @include('partials.active-labels', ['model' => $event])
            </td>
            <td class="btn-group-xs" width="210px">
                @permission('event.show')
                    <a href="{{ route('admin::events.show', [$event->id]) }}"
                       class="btn btn-primary">
                      Show
                    </a>
                @endpermission

                <a href="{{ route('admin::events.details.bookings', [$event->id]) }}"
                   class="btn btn-default">
                    Details
                </a>
                @permission('event.edit')
                    <a href="{{ route('admin::events.edit', [$event->id]) }}"
                       class="btn btn-warning">
                      Edit
                    </a>
                @endpermission
                @permission('event.capacities')
                  <a href="{{ route('admin::events.day-capacities', [$event->id]) }}"
                     class="btn btn-warning">
                      Capacities
                  </a>
                @endpermission
                @include('partials.active-forms', ['model' => $event, 'route' => 'admin::events'])
            </td>
        </tr>
    @endforeach
    </tbody>
</table>