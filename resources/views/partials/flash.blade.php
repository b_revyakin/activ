@if (session('success'))
    <div class="alert alert-success">
        {!! nl2br(session('success')) !!}
    </div>
@endif

@if (session('error'))
    <div class="alert alert-danger">
        {{ session('error') }}
    </div>
@endif

@if (session('info'))
    <div class="alert alert-info">
        {{ session('info') }}
    </div>
@endif

@if (session('error-deactivate'))
    <div class="alert alert-danger">
        {{ session('error-deactivate') }}
        @if(session('email'))
            <a class="btn btn-default btn-xs" href="{{ route('customer::repeat-confirm-email', session('email')) }}">Resend
                verification email</a>
        @endif
    </div>
@endif

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif