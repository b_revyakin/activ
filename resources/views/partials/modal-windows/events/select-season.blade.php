<div class="modal fade" role="dialog" aria-labelledby="selectSeasonsModal" id="modal-select-season">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="exampleModalLabel">Please select a season</h4>
            </div>
            <form id="form" action="{{ route('admin::events.report') }}" method="GET">

                <div class="modal-body">
                    <select class="input-group seasons" name="season_id" id="modal-select-seasons">
                        <option value="" disabled selected>Select a season</option>
                        @foreach($seasons as $season)
                            <option value="{{ $season->id }}">{{ $season->name }}</option>
                        @endforeach
                    </select>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-success">OK</button>
                </div>

            </form>
        </div>
    </div>
</div>

<script type="application/javascript">
    $(document).ready(function () {
        $('#modal-select-seasons').select2({
            placeholder: {
                id: '-1',
                text: 'Select an season'
            },
            width: '100%'
        });
    });
</script>