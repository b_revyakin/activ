<div class="photos">
    @if(!empty($model->photos))
        Photos:
        @foreach($model->photos as $photo)
            <div class="photo-wrap">
                <form action="{{ route($routeName, [$model->id, $photo->id]) }}"
                      method="POST">
                    {!! csrf_field() !!}
                    <input type="hidden" name="_method" value="DELETE">
                    <button type="submit" class="close">x</button>
                </form>
                <a href="{{ route($routeName, [$model->id, $photo->id]) }}">
                    <img class="min-photo img-thumbnail"
                         src="{{ route($routeName, [$model->id, $photo->id]) }}"
                         alt="">
                </a>
            </div>
        @endforeach
    @endif
</div>